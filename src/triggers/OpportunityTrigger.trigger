trigger OpportunityTrigger on opportunity (before insert, before update) {
    
    OpportunityTriggerHandlerClass instance = OpportunityTriggerHandlerClass.getInstance();
    
    try {
        if(System_Configs__c.getValues('Opportunity').Is_Run__c && OpportunityRecursion.fireTrigger) { // Controls the trigger from custom setting.
            if(Trigger.isBefore && Trigger.isUpdate){
                instance.beforeUpdateMethod(Trigger.New,Trigger.old,Trigger.NewMap,Trigger.oldMap);
            }
         }
    } catch(Exception e) {
        system.debug(e.getMessage());
    }  
}