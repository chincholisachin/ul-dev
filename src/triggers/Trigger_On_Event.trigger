/*##############################################################################
########################## sObject Trigger #####################################

##Developed By : Baba S
##Created Date : 08 June 16
##Last MOdified Date : 08 June 16
##Company : ET Marlabs

##################################################################################
################################################################################*/

trigger Trigger_On_Event on Event(after insert, after update, before insert, 
before update) 
{
	//-- Instantiate the handler
	Trigger_On_Event_Handler handler = Trigger_On_Event_Handler.getInstance();

	//-- Before Insert
	if (Trigger.isInsert && Trigger.isBefore) {
		handler.onBeforeInsert(Trigger.new);
	} 

	//-- Before Update
	if (Trigger.isUpdate && Trigger.isBefore) {
		handler.onBeforeUpdate(Trigger.old, Trigger.oldMap, Trigger.new, Trigger.newMap);
	} 
	
	//-- After Update
	if (Trigger.isUpdate && Trigger.isAfter) {        
		handler.onAfterUpdate(Trigger.old, Trigger.oldMap, Trigger.new, Trigger.newMap);
	}
    // -- After Insert 
    if (Trigger.isAfter && Trigger.isInsert) {        
		handler.onAfterInsert(Trigger.new,Trigger.newMap);
    }
}