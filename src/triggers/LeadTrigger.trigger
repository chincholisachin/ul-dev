/* Author : Upendar Shivanadri*/
trigger LeadTrigger on Lead(after insert, after update, before insert,before update) {
    
    LeadTriggerHandler instance = LeadTriggerHandler.getInstance();
    
    try {
        if(System_Configs__c.getValues('Lead').Is_Run__c) { // Controls the trigger from custom setting.
            if(Trigger.isBefore) {  
                system.debug('***LeadRecursion.fireTrigger***Lead***'+LeadRecursion.fireTrigger);
                if(Trigger.isInsert) {
                    instance.beforeInsertMethod(Trigger.new,Trigger.NewMap);
                }
                if(Trigger.isUpdate && LeadRecursion.fireTrigger) {
                    
                    instance.beforeUpdateMethod(Trigger.new,Trigger.NewMap,Trigger.old,Trigger.oldmap);
                }
            }
            if(Trigger.isAfter) {  
                if(Trigger.isInsert) {
                    instance.afterInsertMethod(Trigger.new);
                }
                if(Trigger.isUpdate && LeadRecursion.fireTrigger) {
                    LeadRecursion.fireTrigger = false;
                    instance.afterUpdateMethod(Trigger.new,Trigger.old,Trigger.NewMap,Trigger.oldMap);
                } 
            }          
            
        }
    } catch(Exception e) {
        system.debug(e.getMessage());
    }
}