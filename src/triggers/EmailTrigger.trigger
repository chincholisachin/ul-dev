trigger EmailTrigger on EmailMessage (after insert, after update, before insert, before update) {
    
    EmailMessageTriggerHandler instance = EmailMessageTriggerHandler.getInstance();
    
    if(System_Configs__c.getValues('EmailMessage').Is_Run__c){
        if(Trigger.isBefore){
            if(Trigger.isInsert){
                instance.beforeInsertMethod(Trigger.new,Trigger.newMap);
                }
            }
        }
    }