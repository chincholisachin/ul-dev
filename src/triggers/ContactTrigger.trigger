/* Author : ET
 * Description : 
 *    */
trigger ContactTrigger on Contact(before insert, after insert, after update) {
    
    contactTriggerHandler instance = contactTriggerHandler.getInstance();
    
    try {
        if(System_Configs__c.getValues('Contact').Is_Run__c) {  // Controls the trigger from custom setting.
            if(Trigger.isBefore) {
                if(Trigger.isInsert) {
                    instance.beforeInsertMethod(Trigger.new, Trigger.NewMap);
                }
            }
            if(Trigger.isAfter) {
                if(Trigger.isInsert) {
                    instance.afterInsertMethod(Trigger.new,Trigger.NewMap);
                }
                if(Trigger.isUpdate && contactRecursion.fireTrigger) {
                    contactRecursion.fireTrigger = false;
                    system.debug(Trigger.new+'*******');
                    instance.afterUpdateMethod(Trigger.new,Trigger.NewMap,Trigger.old,Trigger.OldMap);
                }
            }
        }
    } catch(Exception e) {
        system.debug(e.getMessage());
    }
}