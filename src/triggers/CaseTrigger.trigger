/*Trigger Events : 
* Before Update : In After Sales, to place an order to Ul Server.
*/
trigger CaseTrigger on Case (after insert, after update, before insert, before update) {
    
    CaseTriggerHandler instance = CaseTriggerHandler.getInstance();
    
    system.debug('---Current Value in Case Trigger: CaseRecursion.fireTrigger'+CaseRecursion.fireTrigger);
    system.debug('---Current Value in Case Trigger: CaseRecursion.fireCancellation'+CaseRecursion.fireCancellation);
    system.debug('---Current Value in Case Trigger: CaseRecursion.isComingFromCancellation'+CaseRecursion.isComingFromCancellation);
    
    
    
    try {
        if(System_Configs__c.getValues('Case').Is_Run__c) { // Controls the trigger from custom setting.
            if(Trigger.isBefore) {  
                system.debug('***CaseRecursion.fireTrigger***CASE***'+CaseRecursion.fireTrigger);
                if(Trigger.isInsert) {
                    instance.beforeInsertMethod(Trigger.new,Trigger.NewMap);
                }
                if(Trigger.isUpdate && CaseRecursion.fireTrigger) {
                  system.debug('---inside observation 1---');
                    //CaseRecursion.fireTrigger = false; //*** Commented this code to let after update work,Need to evaluate.
                    instance.beforeUpdateMethod(Trigger.new,Trigger.old,Trigger.NewMap,Trigger.oldMap);
                    //instance.CaseAssignmentQueueForFulfilment(Trigger.NewMap,Trigger.oldMap);
                    
                }
                if(Trigger.isUpdate && CaseRecursion.fireTrigger == false && CaseRecursion.isComingFromCancellation){
            system.debug('---inside observation 2---');
                     instance.cancellationProcessing(Trigger.new,Trigger.old,Trigger.NewMap,Trigger.oldMap);
                    
                     CaseRecursion.isComingFromCancellation = false;
                 }
            }
            
            if(Trigger.isAfter) {
            	if(Trigger.isInsert) {
                    instance.afterInsertMethod(Trigger.new,Trigger.NewMap);
                }
                if(Trigger.isUpdate) {                   
                    instance.afterUpdateMethod(Trigger.new,Trigger.old,Trigger.NewMap,Trigger.oldMap);
                }
            }
        }
    } catch(Exception e) {
        system.debug(e.getMessage());
    }
}