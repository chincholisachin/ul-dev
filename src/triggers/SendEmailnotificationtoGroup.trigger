trigger SendEmailnotificationtoGroup on EmailMessage (after insert, after update) {

List<case> casesToUpdate = new list<case>();
    for(EmailMessage newEmail : trigger.new) {
         case c = new case();
        
        if(newEmail.Incoming!=null&& newEmail.Incoming == True) {
            c.id = newEmail.ParentId;            
            c.Email_Notification_to_Group__c = True;
        }
    casesToUpdate.add(c);
    }
    
 if(casesToUpdate.size() > 0) {
 try{
   update casesToUpdate;
   }
   catch (Exception e) {
   system.debug('**** Exception ***'+e.getMessage()) ;
   }
   }
}