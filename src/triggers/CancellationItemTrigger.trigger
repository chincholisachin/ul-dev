trigger CancellationItemTrigger on Cancellation_Item__c (after insert,after update){

    CancellationItemTriggerHandler instance = CancellationItemTriggerHandler.getInstance();
    try {
        if(System_Configs__c.getValues('CancellationItem').Is_Run__c) {
            system.debug('***CaseRecursion.fireCancellation***'+CaseRecursion.fireCancellation);
            if(Trigger.isAfter && Trigger.isInsert && CaseRecursion.fireCancellation){
                system.debug('*** Firing Can Item After Insert Method ***');
                instance.afterInsertMethod(Trigger.New,Trigger.Old,Trigger.NewMap,Trigger.OldMap);
            }

            if(Trigger.isAfter && Trigger.isUpdate && CaseRecursion.fireCancellation){
                system.debug('*** Firing Can Item After Update Method ***');
                instance.afterUpdateMethod(Trigger.New,Trigger.Old,Trigger.NewMap,Trigger.OldMap);
                }
          }
        }
        catch(Exception e) {
            system.debug('***Exception in Can Item Trigger***'+e.getMessage());
        }
    }