public class CaseAssignmentQueue{
    public static Case CaseAssignmentQueueMathod(case caseRecord,Set<String> oiItemList){
       list<Case> listOfAfterSalesCase =  new List<Case>();
       String paymentMethod = '';
       system.debug('---oiItemList---'+oiItemList);
       
       if(oiItemList.size() > 0){
          listOfAfterSalesCase = [SELECT ID from Case WHERE Type = 'After sales' AND OI_Code__c IN: oiItemList and status!='Closed'];
       }
       //QueueSobject queueOwner= new QueueSobject();
        map<String,ID> queueMap = new map<String,ID>();
        queueMap = QueryHelper.getQueueMap();
        
		system.debug('---queueMap---'+queueMap);
		system.debug('---listOfAfterSalesCase---'+listOfAfterSalesCase);
		
          paymentMethod = (caseRecord.O_PaymentMethod__c != Null ? caseRecord.O_PaymentMethod__c : '');
       
       if(paymentMethod!='' && paymentMethod.contains('Bajaj Finserv')){
           //queueOwner= new QueueSobject();
           //queueOwner=[Select QueueId from QueueSobject where SobjectType = 'Case' AND Queue.Name= 'Bajaj Finserv Queue' Limit 1];
           //caseRecord.OwnerId = queueOwner.QueueId;
           if(queueMap.containsKey('Bajaj Finserv Queue')){
              caseRecord.OwnerId = queueMap.get('Bajaj Finserv Queue');
           }
           //system.debug(queueOwner.QueueId+'QQQQQ');
           system.debug(caseRecord+'Bajaj');
       }else if(listOfAfterSalesCase.size() > 0){
       		system.debug('---inside After Sales Condition---');
           /*queueOwner= new QueueSobject();
           queueOwner=[Select QueueId from QueueSobject where SobjectType = 'Case' AND Queue.Name= 'After Sales Queue' Limit 1];
           caseRecord.OwnerId = queueOwner.QueueId;*/
           if(queueMap.containsKey('After Sales Queue')){
              caseRecord.OwnerId = queueMap.get('After Sales Queue');
           }  
       }else{
           //system.debug(queueOwner.QueueId+'Else Part ');
           system.debug(caseRecord+'@@@@@@2');
           /*queueOwner= new QueueSobject();
           queueOwner=[Select QueueId from QueueSobject where SobjectType = 'Case' AND Queue.Name= 'Cancellation Queue' Limit 1];
           caseRecord.OwnerId = queueOwner.QueueId;*/
           if(queueMap.containsKey('Cancellation Queue')){
              caseRecord.OwnerId = queueMap.get('Cancellation Queue');
           }
       }
       return caseRecord;
    }
}