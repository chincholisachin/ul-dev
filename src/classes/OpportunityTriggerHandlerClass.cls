public class OpportunityTriggerHandlerClass{
    
    private static OpportunityTriggerHandlerClass instance;
    
    public static OpportunityTriggerHandlerClass getInstance(){
        if(instance == null){
            instance = new OpportunityTriggerHandlerClass();
        }
        return instance;
    }
    
    public void beforeUpdateMethod(list<Opportunity>newList,list<Opportunity>oldList,Map<ID,Opportunity> newMap,Map<ID,Opportunity> oldMap){
        
        system.debug('---inside before update method---');
        
        for(Opportunity opp :newList){
        
            system.debug('---newMap SubStage---'+newMap.get(opp.id).Sub_Stage__c);
            system.debug('---oldMap SubStage---'+oldMap.get(opp.id).Sub_Stage__c);
            
            if(newMap.get(opp.id).Sub_Stage__c!=oldMap.get(opp.id).Sub_Stage__c){
                
                if(oldMap.get(opp.id).Sub_Stage__c=='Req Documentation' && oldMap.get(opp.id).Req_Documentation_TAT_Breached__c == true){
                    opp.Req_Documentation_TAT_Breached__c = false;
                }
                else if(oldMap.get(opp.id).Sub_Stage__c=='Design/Quote prep' && oldMap.get(opp.id).Design_Quote_Prep_TAT_Breached__c == true){
                    opp.Design_Quote_Prep_TAT_Breached__c = false;
                }
                else if(oldMap.get(opp.id).Sub_Stage__c=='Re design/Re quote' && oldMap.get(opp.id).Re_design_Re_quote_TAT_Breached__c == true){
                    opp.Re_design_Re_quote_TAT_Breached__c = false;
                }
                else if(oldMap.get(opp.id).Sub_Stage__c=='First advance awaited' && oldMap.get(opp.id).First_advance_awaited_TAT_Breached__c == true){
                    opp.First_advance_awaited_TAT_Breached__c = false;
                }
                else if(oldMap.get(opp.id).Sub_Stage__c=='Site diagram' && oldMap.get(opp.id).Site_Check_Diagram_TAT_Breached__c == true){
                    opp.Site_Check_Diagram_TAT_Breached__c = false;
                }
                else if(oldMap.get(opp.id).Sub_Stage__c=='Site check' && oldMap.get(opp.id).Site_Check_TAT_Breached__c == true){
                    opp.Site_Check_TAT_Breached__c = false;
                }
                else if(oldMap.get(opp.id).Sub_Stage__c=='Post order Redesign / Requote' && oldMap.get(opp.id).Post_Order_Redesign_TAT_Breached__c == true){
                    opp.Post_Order_Redesign_TAT_Breached__c = false;
                }
                else if(oldMap.get(opp.id).Sub_Stage__c=='Post Order Reproposal' && oldMap.get(opp.id).Post_Order_Reproposal_TAT_Breached__c == true){
                    opp.Post_Order_Reproposal_TAT_Breached__c = false;
                }
                 else if(oldMap.get(opp.id).Sub_Stage__c=='Pending internal approval' && oldMap.get(opp.id).Pending_internal_approval_TAT_Breached__c == true){
                    opp.Pending_internal_approval_TAT_Breached__c = false;
                }
                 else if(oldMap.get(opp.id).Sub_Stage__c=='Second advance awaited' && oldMap.get(opp.id).Second_advance_awaited_TAT_Breached__c == true){
                    opp.Second_advance_awaited_TAT_Breached__c = false;
                }
            }
           
        }
    }
}