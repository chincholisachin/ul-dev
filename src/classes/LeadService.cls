/* Author : Upendar Shivanadri
   Description : To Create Tasks and Contact if lead does not have contact */
public class LeadService {
    
  /*  public static void createTasksFromLead(LeadModel leadData){
         list<Task> taskListToCreate = new list<Task>();
        
         
         for(Lead eachLead:leadData.firstVisitLeads){
             
                    Task eachTaskToCreate = new Task();
                 
                    eachTaskToCreate.ActivityDate  = eachLead.First_Visit_Date__c.Date();
                    eachTaskToCreate.Subject = 'First Visit Date'; 
                    eachTaskToCreate.OwnerId = eachLead.Consultant_Name__c;
                    eachTaskToCreate.WhoId   = eachLead.id;
                    eachTaskToCreate.status = 'Pending';
                    eachTaskToCreate.Priority = 'Normal';
                    
                    taskListToCreate.add(eachTaskToCreate);
             
             }
              for(Lead eachLead:leadData.nextvisitLeads){
             
                    Task eachTaskToCreate = new Task();
                 
                    eachTaskToCreate.ActivityDate  = eachLead.Next_Action_Date__c.Date();
                    eachTaskToCreate.Subject = 'Next Visit Date'; 
                    eachTaskToCreate.OwnerId = eachLead.Consultant_Name__c;
                    eachTaskToCreate.WhoId   = eachLead.id;
                    eachTaskToCreate.status = 'Pending';
                    eachTaskToCreate.Priority = 'Normal';
                    
                    taskListToCreate.add(eachTaskToCreate);
             
             }
             
             ServiceFacade.createTasksFromLead(taskListToCreate);
              
    } */
    
    // Creating Contact against Lead from Email and First Name Lastname
    
    public static void createContactFromEmail(list<Lead> newLeads){
        try{
            //*** Local Variables
            list<String> emailAddresses = new List<String>();
            list<Contact> listContacts = new list<Contact>();
            Set<String> takenEmails = new Set<String>();
            map<String,Contact> emailToContactMap = new map<String,Contact>();
            map<String,Contact> emailToUpdateContactMap = new map<String,Contact>();
            list<Lead> leadsToUpdate = new list<Lead>();
            list<Contact> newContacts = new list<Contact>();
            
            //***Added to override standard functionality where multiple Emails are found***
            map<String,Contact> emailToConMap_New = new map<String,Contact>();
            
            //First exclude any Leads where the contact is set
            for (Lead leadobj:newLeads) {
                
                if (leadobj.Contact__c==null &&
                    leadobj.Email!='')
                {
                    emailAddresses.add(leadobj.Email);
                }
            }
            
            listContacts = ServiceFacade.fetchContactsByEmail(emailAddresses);

            for (Contact c:listContacts) {
                takenEmails.add(c.Email);
                emailToConMap_New.put(c.Email,c);
                CalloutService.generateServiceLog(JSON.serialize('{"message":"Fetch contacts for email", "email":"'+ c.Email + '"}'), null, 'LEAD_CONTACT_CREATION', null, c.Id);
            }
                  system.debug('****listContacts *****'+listContacts); 
            for (Lead leadObj:newLeads){
                if (leadObj.Email!=null && leadObj.LastName != null && leadObj.Contact__c==null && !takenEmails.contains(leadObj.Email) ) {
                        Contact cont = new Contact(FirstName=leadObj.FirstName != null ? leadObj.FirstName : '', LastName=leadObj.LastName, Email=leadObj.Email,Phone=leadObj.Phone);                     
                        cont.Customer_ID__c = Decimal.valueOf(leadObj.Customer_ID__c);
                        emailToContactMap.put(leadObj.Email,cont);
                        leadsToUpdate.add(leadObj);
                        CalloutService.generateServiceLog(JSON.serialize('{"message":"Creating contact for lead email", "email":"'+ leadObj.Email + '"}'), null, 'LEAD_CONTACT_CREATION', null, null);
                } else if(leadObj.Contact__c==null && leadObj.Email!=null && takenEmails.contains(leadObj.Email)){
                        CalloutService.generateServiceLog(JSON.serialize('{"message":"Found contact for lead", "email":"'+ leadObj.Email + '"}"'), null, 'LEAD_CONTACT_CREATION', null, null);
                       if(emailToConMap_New.containsKey(leadObj.Email)){
                           leadObj.Contact__c= emailToConMap_New.get(leadObj.Email).id; 
                           CalloutService.generateServiceLog(JSON.serialize('{"message":"Tagging contact for lead", "email":"'+ leadObj.Email + '"}'), null, 'LEAD_CONTACT_CREATION', null, leadObj.Contact__c);
                       }
                       
                }/*else if(leadObj.Contact__c==null && leadObj.Email!=null && !takenEmails.contains(leadObj.Email)){
                    Contact cont = new Contact(LastName=leadObj.Name,Email=leadObj.Email);
                        emailToContactMap.put(leadObj.Email,cont);
                        leadsToUpdate.add(leadObj);
                }  */          
            }
   
            newContacts = ServiceFacade.insertnewContactList(emailToContactMap.values());
            
            //SF-1034 -- Code modification 
            if(newContacts != null && newContacts.size()>0){
                for(Contact conObj:newContacts){
                    emailToUpdateContactMap.put(conObj.Email,conObj);
                }
                for(Lead leadObj:leadsToUpdate){
                    Contact newContact = emailToUpdateContactMap.get(leadObj.Email); 
                    leadObj.Contact__c= newContact.Id;
                }
            }
            else
                CalloutService.generateServiceLog(JSON.serialize('{"message":"insertnewContactList returns null", "email":"'+ newLeads[0].Email + '"}'), null, 'LEAD_CONTACT_CREATION', null, newLeads[0].id);
        }catch(Exception e){
            system.debug('***Exception e***'+e.getMessage());
            CalloutService.generateServiceLog(JSON.serialize('{"message":"Exeception in Lead contact creation.", "error" : "' + e.getMessage() + '"}'), null, 'LEAD_CONTACT_CREATION', null, null);
        }
    } 
    
    
    //** fetch Open Leads by Phone
    public static list<Lead> fetchOpenLeadsByPhone(String phoneNumber){
        system.debug('---Lead Service - inside fetchOpenLeadsByPhone---');
        list<Lead> leadList = new list<Lead>();
        String phoneNum = String.valueOf(phoneNumber).trim();
        try{
            system.debug('***try - phoneNumber***'+phoneNum);
            leadList =  [SELECT id,Name,Status,Lead_Stage__c,Sub_Stage__c,IsConverted,convertedOpportunityID,OwnerID from Lead where Phone=:phoneNum ORDER BY isConverted];
            system.debug('***leadList***'+leadList.size());
            system.debug('***leadList***'+leadList);
            return leadList;
        }
        catch(Exception e){
            system.debug('*** Exception e***'+e.getMessage());
        }
        system.debug('***leadList***'+leadList.size());
        return null;
    }
    
    public static void updateOpenCTITasks(list<lead> newList,list<lead> oldList,map<ID,lead> newMap,map<ID,lead> oldMap){
        set<ID> setOfLeadIDs = new set<ID>();
        for(Lead eachLead:newList){
            if((eachLead.Lead_stage__c =='Closed' && oldMap.get(eachLead.ID).Lead_stage__c!='Closed') || (eachLead.Sub_Stage__c =='Visit Scheduled' && oldMap.get(eachLead.ID).Sub_Stage__c!='Visit Scheduled')){
                setOfLeadIDs.add(eachLead.ID);
            }
        }
        list<Task> taskList  = new list<Task>();
        taskList = ServiceFacade.getTasksByLeadIDs(setOfLeadIDs);
        set<ID> setofTaskIDsToComplete = new set<ID>();
        for(Task eachTask :taskList){
            if(eachTask.Subject.contains('Call customer to schedule visit') && eachTask.Status == 'Open'){
                setofTaskIDsToComplete.add(eachTask.ID);
            }
        }
        
        if(!setofTaskIDsToComplete.isEmpty()){
            ServiceFacade.updateTasksAsComplete(setofTaskIDsToComplete);
        }
    }
}