//Author:   Vickal
//Dated:    March 14, 2016
//Purpose: unit testing for OrderOItemPageController.

@isTest(seeAllData = false)
private class OrderOItemPageControllerTest {
    
    public static testMethod void orderOITest(){
        list<contact> cont = new list<contact>();
        cont              = Utility_TestClass.createContacts();
        cont[0].Customer_ID__c = 23344;
        insert cont[0];
        
        Order__x ord = new Order__x(ExternalID='R1234567', Code__c = 'O8765433');
        ord.consumer_id__c = cont[0].Customer_ID__c;
        Database.insertAsync(ord);
        system.debug(cont[0].Customer_ID__c+' ####### '+ ord.consumer_id__c);
        system.assert(ord != null);
        orderitem__x oItem = new orderitem__x(ExternalID='OI123456');
        oItem.order_code__c = ord.Code__c;
        Database.insertAsync(oItem);
        
      
        PageReference PageRef = Page.OrderOItmPage;
        Test.setCurrentPage(PageRef);
        ApexPages.currentPage().getParameters().put('id', cont[0].Id);
        ApexPages.StandardController stdController = new ApexPages.StandardController(cont[0]);
        OrderOItemPageController OOItem = new OrderOItemPageController(stdController);
        OrderOItemPageController.getOrders(cont[0].Id);
        OrderOItemPageController.getOrderItems(ord.code__c);
   }
}