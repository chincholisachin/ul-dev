@isTest
global class ServiceOrderInsertResponse1 implements HttpCalloutMock {
	
	// Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        // Optionally, only send a mock response for a specific endpoint
        // and method.
        //System.assertEquals('http://api.salesforce.com/foo/bar', req.getEndpoint());
        //System.assertEquals('GET', req.getMethod());
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"status":"failure","error":{"message":"order : R123 not found","errors":[{"message":"order : R123 not found","code":null,"data":null}]}}');
        res.setStatusCode(422);
        res.setStatus('failure');
        return res;
    }
	
}