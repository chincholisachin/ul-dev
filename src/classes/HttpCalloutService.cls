public with sharing class HttpCalloutService {
    
    @future(callout=true)
    public static void afterSalesOrder_Callout(String caseID,String oCode,String oNum,String oiCode){
        
        system.debug('*** oCode,onum,oiCode***'+oCode+'='+oNum+'='+oiCode);
        
        if(oCode!=NULL && oiCode!=NULL){
            //Order Item Attributes
            WS_AS_RequestFormatClass.cls_order_item oi_wrap = new WS_AS_RequestFormatClass.cls_order_item(); 
            oi_wrap.code = oiCode;
            oi_wrap.status= 'NEEDS_REPLACEMENT';
            //Order Attributes 
            WS_AS_RequestFormatClass.cls_order o_wrap = new WS_AS_RequestFormatClass.cls_order(); 
            o_wrap.code = oCode;
            o_wrap.num= oNum; 
            o_wrap.order_item = oi_wrap;
            
            //Order Wrap
            WS_AS_RequestFormatClass wrap = new WS_AS_RequestFormatClass(); 
            wrap.order = o_wrap;
            
            String jsonBody = json.serialize(wrap);
            system.debug('***wrap***'+jsonBody);
            HttpResponse res;
            HttpRequest req = new HttpRequest();
            // /v1/order-items/listener/replacement-trigger
            req.setEndPoint('http://private-9b2cc-orders26.apiary-mock.com/v1/order-items/listener/replacement-trigger'); 
            req.setMethod('POST');
            req.setHeader('Content-Type','application/json');
            req.setBody(jsonBody);
            Http http_req = new Http();
            res = http_req.send(req);
            System.Debug(res.toString());
            System.Debug(res.getBody());
            if(res.getBody() == 'OK'){
                System.Debug('*** Updating Case ****');
                Case caseToUpdate = [SELECT id,Order__c,Order_Item__c,Status,Replacement_Order_Placed_with_UL__c from Case where ID=:caseID];
                caseToUpdate.Replacement_Order_Placed_with_UL__c = True;
                System.Debug('*** Updating Case ****');
                try{
                    update caseToUpdate;
                }catch(Exception e){
                    System.Debug('*** Exception e****'+e.getMessage());
                }
                
            }
            //eachCaseToSendDetails.Replacement_Order_Placed_with_UL__c = TRUE;
        }
    }    
}