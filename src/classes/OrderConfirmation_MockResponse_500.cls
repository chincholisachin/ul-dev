/*** This class is mock response Generator for BFL Order confirmation Callout - response status = 500 ***/

@isTest
global with sharing class OrderConfirmation_MockResponse_500 implements HttpCalloutMock{
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
    	
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setStatusCode(500);
        return res;
    }
}