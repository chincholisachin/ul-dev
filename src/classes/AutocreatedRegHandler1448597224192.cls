global class AutocreatedRegHandler1448597224192 implements Auth.RegistrationHandler{
    class RegHandlerException extends Exception {}      // Used to throw registration exceptions

    global boolean canCreateUser(Auth.UserData data) {
        // Only Urban Ladder accounts are allowed
        string email = data.email;
        string domain = email.substring(email.lastIndexOf('@') +1);

        return(domain == 'urbanladder.com');
    }

    global User createUser(Id portalId, Auth.UserData data){
        if(!canCreateUser(data)) {
            throw new RegHandlerException('Login using your Urban Ladder account.');            
            return null;
        }

        User u;

        // Check if the user already exists with the username
        for(User rec : [SELECT Id FROM User WHERE Username =: data.email AND IsActive = true])
            u = rec;

        // If there are no users then create new one
        if(u == null){
            u = new User();
            Profile p = [SELECT Id FROM profile WHERE name='Chatter Free User'];

            u.username      = data.email;
            u.email         = data.email;
            u.lastName      = data.lastName;
            u.firstName     = data.firstName;
            String alias    = data.email;

            //Alias must be 8 characters or less
            if(alias.length() > 8) {
                alias = alias.substring(0, 8);
            }
            u.alias             = alias;
            u.languagelocalekey = 'en_US';
            u.localesidkey      = 'en_IN';
            u.emailEncodingKey  = 'UTF-8';
            u.timeZoneSidKey    = 'Asia/Kolkata';
            u.profileId         = p.Id;
        }

        return u;
    }

    global void updateUser(Id userId, Id portalId, Auth.UserData data){
        User u = new User(id=userId);
        u.email = data.email;
        u.lastName = data.lastName;
        u.firstName = data.firstName;
        update(u);
    }
}