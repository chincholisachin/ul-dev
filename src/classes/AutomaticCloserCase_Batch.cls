global class AutomaticCloserCase_Batch implements Database.Batchable<sObject> {
    
    global id recortypeid;
    
    global AutomaticCloserCase_Batch(){
        system.debug('---constructor begin---');
        recortypeid = [select id from RecordType where sobjectType=  'Case' And Name= 'Customer Service'  limit 1].id;
        system.debug('---recortypeid---'+recortypeid);
    }
    
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        system.debug('---start begin---');
        Set<String> setOfAfterSalesStatus = new Set<String>{'Repaired Product Delivered','Repaired','New Product Delivered','Delivered'};
        String Closedstatus = 'Closed' ;
        String query = 'SELECT Id,After_Sales_Status__c ,Status,ownerID,RecordtypeID,Reason FROM Case where After_Sales_Status__c IN: setOfAfterSalesStatus AND Status !=: Closedstatus ';
        return Database.getQueryLocator(query);
    }
   
    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        system.debug('---execute begin---');
        system.debug('---scope---'+scope);
        QueueSobject queueOwner= new QueueSobject();
        queueOwner = [Select QueueId from QueueSobject where SobjectType = 'Case' AND Queue.Name = 'Customer Service - AfterSales' Limit 1];
        system.debug('---queueOwner---'+queueOwner);
        List<Case> caseToUpdate = new List<Case>();
        for(sObject temp : scope){
            Case tempCase =(Case)temp;
            system.debug('tempCase'+tempCase );
            tempCase.RecordtypeID = recortypeid ;
            tempCase.OwnerID = queueOwner.QueueId;
            tempCase.Status = 'Closed';
            tempCase.Reason = 'Communicated to customer';
            caseToUpdate.add(tempCase);
        }
        system.debug('---caseToUpdate---'+caseToUpdate);
        
        if(caseToUpdate.size() > 0){
            try {
                database.update (caseToUpdate,false);
            }catch(Exception ex) {
                System.debug('---Error::---'+ex.getMessage());
            }
        }
    }
    global void finish(Database.BatchableContext BC) {
        system.debug('---finish begin---');
    }
}