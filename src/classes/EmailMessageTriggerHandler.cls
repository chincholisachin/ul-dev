public with sharing class EmailMessageTriggerHandler {
	
	private static EmailMessageTriggerHandler instance;
	//*** Singleton Pattern to obtain instance of the Handler***
    public static EmailMessageTriggerHandler getInstance(){
        if(instance==null){
            instance = new EmailMessageTriggerHandler();
        }
        return instance;
    }
    
    public void beforeInsertMethod(list<EmailMessage> newList,map<ID,EmailMessage> newMap){
    	set<ID> setOfCaseIDs = new set<ID>();
    	for(EmailMessage eachEM:newList){
    		if(eachEM.Incoming){
    			setOfCaseIDs.add(eachEM.parentID);
    		}
    	}
    	
    	list<Case> caseList = [SELECT id,CaseNumber,Type,CNR__c,Status,(SELECT id,Status,Subject from Tasks where Subject='CNR-Call' and status='Open') from Case where ID in :setOfCaseIDs AND Status='Waiting' AND CNR__c=true];
    	list<Task> taskListToUpdate = new list<Task>();
    	
    	if(!caseList.isEmpty()){
    	  for(Case eachCase:caseList){
    	  	for(Task eachTask:eachCase.Tasks){
    	  		eachTask.status = 'Complete';
    	  		eachTask.CTI_Call_Log_Status__c='Emailed Back';
                taskListToUpdate.add(eachTask);
    	  	} 
    	  	eachCase.status = 'Open';
    	  	eachCase.SLA_On_Hold_Until_Datetime__c = null ;
    	  	eachCase.CNR__c = false ;	  	
    	  }
    	  //----- Update the case staus & cnr 
    	  CaseService.updateCNRCase(caseList);
    	}
    	update taskListToUpdate;
    }
}