/*
 *  Author: Baba S
    Dated : 07/07/2016
    Purpose: Run CaseOwnerChange_Batch for every 15 min
*
*/
global class CaseOwnerChangeBatch_Schedule implements Schedulable {
   
    global void execute(SchedulableContext sc) {
      
        CaseOwnerChange_Batch caseOwnerChangeBatch = new CaseOwnerChange_Batch(); 
        database.executebatch(caseOwnerChangeBatch);
       
   }
}