global class scheduledBatch_AutomaticCaseCloser implements Schedulable{
   
   global void execute(SchedulableContext sc) {
      
     AutomaticCloserCase_Batch b = new AutomaticCloserCase_Batch();
     database.executeBatch(b);
   }
}