@RestResource(urlMapping='/UL_SendCallLogsToSFDC/*')
global class WS_UL_SendCallLogsToSFDC {
    
    @HttpPost
    global static void sendCallLogs(){
        RestRequest req = RestContext.request;
        
        //System.debug('*** Req Body***'+req.requestBody.toString());
        //System.debug('*** Req Params***'+req.params.get('data'));
        String jsonString = req.params.get('data');
        system.debug('***jsonString***'+jsonString);
        
         Service_Logs__c serviceLogs = new Service_Logs__c(Callout_Service_Name__c ='CTI Call Log',Request_Format__c = jsonString);
        
         insert serviceLogs;
        
        DataFromCall callData_Test = new DataFromCall();
        
        callData_Test  = (DataFromCall)System.JSON.deserialize(jsonString,DataFromCall.class);
        system.debug('***callData_Test***'+callData_Test);
        system.debug('***callData_Test***'+callData_Test.CallerID);
        
        String agentIDAliasUserName = callData_Test.AgentID;
        system.debug('---agentIDAliasUserName---'+agentIDAliasUserName);
        
        map<String,User> userMap = UserService.getUserDetailsMap();
        system.debug('---userMap---'+userMap);
        map<ID,Profile> profileMap = OrgService.getprofilesDetailsMap();
        system.debug('---profileMap---'+profileMap);
        String agentProfileName;
        if(userMap.containskey(agentIDAliasUserName)){
            agentProfileName = profileMap.get(userMap.get(agentIDAliasUserName).ProfileID).Name;
        }
        List<Lead_Profile_Names__c> leadProfileNames = Lead_Profile_Names__c.getall().values();
        system.debug('---leadProfileNames---'+leadProfileNames);
        set<string> profileNames = new set<string>();
        for(Lead_Profile_Names__c eachLPN :leadProfileNames){
            profileNames.add(eachLPN.Profile_Names__c);
        }
        system.debug('---profileNames---'+profileNames);
        if(profileNames.contains(agentProfileName)){
             CTIService.processCallForLeads(callData_Test);
        }
        else{
            CTIService.processCallToCreateTask(callData_Test);
        }
    }
    
    global class DataFromCall{
        public string monitorUCID {get;set;} //Call Unique ID
        public string UUI {get;set;}         //This is CalledID by the customer. Which we have defined in the Campaign.
        public string Did {get;set;}
        public string Location{get;set;}
        public string CallerID{get;set;}
        public string PhoneName{get;set;}
        
        public string Skill{get;set;}
        public string StartTime{get;set;}
        public string EndTime{get;set;}
        public string TimeToAnswer{get;set;}
        public string Duration{get;set;}
        public string FallBackRule{get;set;}
        public string DialedNumber{get;set;}
        public string Type{get;set;}
        public string AgentID{get;set;}
        public string AgentUniqueID{get;set;}
        public string Disposition{get;set;}
        public string HangupBy{get;set;}
        public string Comments{get;set;}
        
        public string Status{get;set;}
        public string AudioFile{get;set;}
        public string TransferType{get;set;}
        public string TransferredTo{get;set;}
        public string DialStatus{get;set;}
        public string Apikey{get;set;}
        public string source{get;set;}
        public string CustomerStatus{get;set;}
    }
    
}