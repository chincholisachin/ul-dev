/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class CES_Ctrl_Test {
    
    static testMethod void myUnitTestPositiveTesting() {
        Account acc = new Account(Name= 'Test account'); 
        insert acc;
        contact con = new Contact(lastName = 'test last name', Email = 'test2@urbanLader.com');
        insert con;
        Entitlement ent = new Entitlement(Name='CS Standard SLA/Entitlement', Type ='Phone Support', AccountId=acc.Id);
        insert ent;
        
        Case caseInstance = new Case(contactId=con.Id, SuppliedEmail = 'test2@urbanLader.com', Status='Open', Origin='Email', EntitlementId=ent.Id,Subject='New Subject',Description='Test');
        insert caseInstance;
        
        Test.startTest();
        
        CES_Ctrl cesCtrlObj = new CES_Ctrl();
        cesCtrlObj.saveCES();
        PageReference tpageRef = Page.CES_Page;
        Test.setCurrentPage(tpageRef);
        ApexPages.currentPage().getParameters().put('caseId', caseInstance.Id);
        ApexPages.currentPage().getParameters().put('cesVal', '2');
        CES_Ctrl cesCtrlObj1 = new CES_Ctrl();
        cesCtrlObj1.saveCES();
        cesCtrlObj1.updateCES();
        Test.stopTest();
    }
    static testMethod void myUnitTestPositiveTesting4() {
        Account acc = new Account(Name= 'Test account'); 
        insert acc;
        contact con = new Contact(lastName = 'test last name', Email = 'test2@urbanLader.com');
        insert con;
        Entitlement ent = new Entitlement(Name='CS Standard SLA/Entitlement', Type ='Phone Support', AccountId=acc.Id);
        insert ent;
        
        Case caseInstance = new Case(contactId=con.Id, SuppliedEmail = 'test2@urbanLader.com', Status='Open', Origin='Email', EntitlementId=ent.Id,Subject='New Subject',Description='Test');
        insert caseInstance;
        
        Test.startTest();
        
        CES_Ctrl cesCtrlObj = new CES_Ctrl();
        cesCtrlObj.saveCES();
        PageReference tpageRef = Page.CES_Page;
        Test.setCurrentPage(tpageRef);
        ApexPages.currentPage().getParameters().put('caseId', caseInstance.Id);
        ApexPages.currentPage().getParameters().put('cesVal', '4');
        CES_Ctrl cesCtrlObj1 = new CES_Ctrl();
        cesCtrlObj1.saveCES();
        cesCtrlObj1.updateCES();
        Test.stopTest();
    }
     static testMethod void myUnitTestPositiveTesting1() {
        Account acc = new Account(Name= 'Test account'); 
        insert acc;
        contact con = new Contact(lastName = 'test last name', Email = 'test2@urbanLader.com');
        insert con;
        Entitlement ent = new Entitlement(Name='CS Standard SLA/Entitlement', Type ='Phone Support', AccountId=acc.Id);
        insert ent;
        
        Case caseInstance = new Case(contactId=con.Id, SuppliedEmail = 'test2@urbanLader.com', Status='Open', Origin='Email', EntitlementId=ent.Id,Subject='New Subject',Description='Test');
        insert caseInstance;
        
        Test.startTest();
        
        CES_Ctrl cesCtrlObj = new CES_Ctrl();
        cesCtrlObj.saveCES();
        ApexPages.currentPage().getParameters().put('caseId', con.Id);
        ApexPages.currentPage().getParameters().put('cesVal', '2');
        CES_Ctrl cesCtrlObj1 = new CES_Ctrl();
        cesCtrlObj1.saveCES();
        cesCtrlObj1.updateCES();
        Test.stopTest();
    }
    
    static testMethod void myUnitTestPositiveTesting2() {
        Account acc = new Account(Name= 'Test account'); 
        insert acc;
        contact con = new Contact(lastName = 'test last name', Email = 'test2@urbanLader.com');
        insert con;
        Entitlement ent = new Entitlement(Name='CS Standard SLA/Entitlement', Type ='Phone Support', AccountId=acc.Id);
        insert ent;
        
        Case caseInstance = new Case(contactId=con.Id, SuppliedEmail = 'test2@urbanLader.com', Status='Open', Origin='Email', EntitlementId=ent.Id,Subject='New Subject',Description='Test');
        insert caseInstance;
        
        Test.startTest();
        ApexPages.currentPage().getParameters().put('caseId','');
        ApexPages.currentPage().getParameters().put('cesVal','3' );
        CES_Ctrl cesCtrlObj1 = new CES_Ctrl();
        cesCtrlObj1.saveCES();
        cesCtrlObj1.updateCES();
        Test.stopTest();
    }
     static testMethod void myUnitTestNegativeTesting() {
        Account acc = new Account(Name= 'Test account'); 
        insert acc;
        contact con = new Contact(lastName = 'test last name', Email = 'test2@urbanLader.com');
        insert con;
        Entitlement ent = new Entitlement(Name='CS Standard SLA/Entitlement', Type ='Phone Support', AccountId=acc.Id);
        insert ent;
        
        Case caseInstance = new Case(contactId=con.Id, SuppliedEmail = 'test2@urbanLader.com', Status='Open', Origin='Email', EntitlementId=ent.Id,Subject='New Subject',Description='Test');
        insert caseInstance;
        
        Test.startTest();
        PageReference tpageRef = Page.CES_Page;
        Test.setCurrentPage(tpageRef);
        ApexPages.currentPage().getParameters().put('caseId', caseInstance.Id);
        ApexPages.currentPage().getParameters().put('cesVal', '');
        
        CES_Ctrl cesCtrlObj1 = new CES_Ctrl();
        cesCtrlObj1.CES_Score = null;
        cesCtrlObj1.saveCES();
        cesCtrlObj1.updateCES();
        Test.stopTest();
    }
    static testMethod void myUnitTestNegativeTesting1() {
        Account acc = new Account(Name= 'Test account'); 
        insert acc;
        contact con = new Contact(lastName = 'test last name', Email = 'test2@urbanLader.com');
        insert con;
        Entitlement ent = new Entitlement(Name='CS Standard SLA/Entitlement', Type ='Phone Support', AccountId=acc.Id);
        insert ent;
        
        Case caseInstance = new Case(contactId=con.Id, SuppliedEmail = 'test2@urbanLader.com', Status='Open', Origin='Email', EntitlementId=ent.Id,Subject='New Subject',Description='Test');
        insert caseInstance;
        
        Test.startTest();
        PageReference tpageRef = Page.CES_Page;
        Test.setCurrentPage(tpageRef);
        ApexPages.currentPage().getParameters().put('caseId', caseInstance.Id);
        ApexPages.currentPage().getParameters().put('cesVal', '');
        
        CES_Ctrl cesCtrlObj1 = new CES_Ctrl();
        
        cesCtrlObj1.saveCES();
        cesCtrlObj1.CES_Score = null;
        cesCtrlObj1.updateCES();
        Test.stopTest();
    }
    
}