@isTest
public class Test_CTI_CallLogToContactAndCase {
    
        public static Contact contactobj;
        public static Case caseobj;
        public static Task taskobj;
        
    public static void init() {
    
    contactobj = new contact();
       
        contactobj.FirstName = 'Urban';
        contactobj.lastName = 'Ladder';
        contactobj.Email = 'testuser@urbanladder.com';
        insert contactobj;
       // caseobj = UL_Utilities.createcase(contactobj.Id);
       // insert caseobj;
        taskobj = UL_Utilities.createtask(contactobj.Id);
        //taskobj.Phone = '9702345679';
        taskobj.Location__c = 'Bangalore';
        insert taskobj;
       }
    
    public static void init1() {
     
        caseobj = UL_Utilities.createcase(contactobj.Id);
        insert caseobj;
        taskobj = UL_Utilities.createtask(caseobj.Id);
        //taskobj.Phone = '9702345679';
        taskobj.Location__c = 'Bangalore';
        insert taskobj;
        
    }
    
     public static testmethod void testone () {
     
     init();
     init1();
     
     Test.startTest();
    //CTI_CallLogToContactAndCase cw = new CTI_CallLogToContactAndCase();
    
    WS_UL_SendCallLogsToSFDC.DataFromCall callLogWrapper = Utility_TestClass.createCallLogWrapper();
    List<Contact> conList = new List<Contact>();
    conList = Utility_TestClass.createContacts();
    insert conList;
    List<Case> caseList = new List<Case>();
    caseList = Utility_TestClass.createCases();
    insert caseList;
    
     CTI_CallLogToContactAndCase.getContactIDFromPhone(callLogWrapper);
     CTI_CallLogToContactAndCase.createTaskWithCallLogDetails(conList[0].ID,callLogWrapper);
     CTI_CallLogToContactAndCase.findBestMatchingCase_existingCustomer(conList[0].ID,callLogWrapper);
     
      Test.stopTest();
     
     }
    
}