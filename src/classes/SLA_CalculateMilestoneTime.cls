global class SLA_CalculateMilestoneTime implements Support.MilestoneTriggerTimeCalculator{
    
    global Integer calculateMilestoneTriggerTime(String caseID,String milestoneTypeID){
  try{
        System.Debug('***Entered Post Matching Criteria on Milestone***');
        Datetime startDateTime;
        Long millisecs;
        Case caseInAction = [SELECT id,Type,Sub_Category__c,After_Sales_Status__c,Product_Picked_for_Repair_Date__c,Product_Repaired_Delivery_Scheduled_Date__c,Visit_Scheduled_Date__c,Inward_Date__c,QC_Fail_Date__c,(SELECT id from CaseMilestones) from Case where ID=:caseID];

        system.debug('***caseInAction***'+caseInAction);
        system.debug('***caseInAction***'+caseInAction.CaseMilestones);
        
        Datetime currentDateTime = DateTime.Now(); //This will stamp the datetime when this milestone is triggered.
        
        BusinessHours bh = [SELECT Id, MondayEndTime, MondayStartTime, TuesdayEndTime, TuesdayStartTime, WednesdayEndTime, WednesdayStartTime, ThursdayEndTime, ThursdayStartTime, FridayEndTime, FridayStartTime, SaturdayEndTime, SaturdayStartTime, SundayEndTime, SundayStartTime FROM BusinessHours WHERE IsDefault=true];
        system.debug('***businessHours***'+bh);

       MilestoneType mt = [SELECT Name FROM MilestoneType WHERE Id=:milestoneTypeId];
       
       system.debug('***Milestone Type***'+mt);

       if(mt.Name.contains('Response')){

         system.debug('***Response SLA***'+mt.Name);

        if(caseInAction.After_Sales_Status__c=='Visit Scheduled'){
            startDateTime = caseInAction.Visit_Scheduled_Date__c;
        }
        
        else if(caseInAction.After_Sales_Status__c=='Product Repaired Delivery Scheduled'){
            startDateTime = caseInAction.Product_Repaired_Delivery_Scheduled_Date__c;
        }
        //Product Picked For Repair
        else if(caseInAction.After_Sales_Status__c=='Product Picked For Repair'){
            startDateTime = caseInAction.Product_Picked_for_Repair_Date__c;
        }
        
        //Calculate TargetDateTime from the Specified Date and Specified BusinessHours
        if(caseInAction.After_Sales_Status__c =='Visit Scheduled' || caseInAction.After_Sales_Status__c =='Product Repaired Delivery Scheduled'){
            millisecs  = getMillisecsFromHours(10); //Should come from Custom Setting.
        }
        else if(caseInAction.After_Sales_Status__c =='Product Picked For Repair'){
            millisecs  = getMillisecsFromHours(20); //Should come from Custom Setting.
        }
    }
    else if(mt.Name.contains('Resolution')){

        system.debug('***Resolution SLA***'+mt.Name);

        if(caseInAction.Type=='Delivery delay' && caseInAction.Sub_Category__c=='OOS' && caseInAction.Inward_Date__c!=null){
             startDateTime = caseInAction.Inward_Date__c;
        }
        else if(caseInAction.Type=='Delivery delay' && caseInAction.Sub_Category__c=='QC fail'&& caseInAction.QC_Fail_Date__c!=null){
            startDateTime = caseInAction.QC_Fail_Date__c;
        }
        millisecs  = getMillisecsFromHours(150);
    }
       
        Datetime targetDateTime = BusinessHours.add(bh.ID,startDateTime,millisecs);

        Long  diffInMillisecondsFromNowToTargetDate = BusinessHours.diff(bh.ID,DateTime.Now(),targetDateTime);
        
        Long mins = getMinutesFromMilliseconds(diffInMillisecondsFromNowToTargetDate);

        return Integer.valueOf(mins);
  }
  catch(Exception e){
    system.debug(e.getMessage());
    return 10;
  }
  return null;
    }

    global static Long getMinutesFromMilliseconds(Long milliSecs){
        Long milliseconds = milliSecs;
        Long seconds = milliseconds / 1000;
        Long minutes = seconds / 60;
        //Long hours = minutes / 60;
        //Long days = hours / 24;
        //return minutes; // Commented for UAT to set it to 4 min.
        return 4;
    }

    global static Long getMillisecsFromHours(Integer Hours){
        Long milliseconds = Hours*60*60*1000;
        Long seconds = Hours * 60* 60;
        Long minutes = Hours * 60;
        //Long hours = minutes / 60;
        //Long days = hours / 24;
        return milliseconds;
    }
}