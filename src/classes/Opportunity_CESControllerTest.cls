/*
*Purpose: Test class for Controller of Opportunity CES_Page
*Date : 06/01/2017
*VERSION    AUTHOR                     DETAIL                                 
*1.0        Pavithra Gajendra          INITIAL DEVELOPMENT               
*
*/
@isTest
private class Opportunity_CESControllerTest {

	//------- Scenario When CES score & Opp Id exists
    static testMethod void myUnitTest() {
    	
       PageReference pageRef = Page.OpportunityCES_Page;
       Test.setCurrentPage(pageRef);
       
       //----- Add parameters to page URL  
    
       ApexPages.currentPage().getParameters().put('oppId', '006p0000004XHXJ');
       ApexPages.currentPage().getParameters().put('cesVal', '1');
       
       //----- Instantiate a new controller with all parameters in the page 
       Opportunity_CESController controller = new Opportunity_CESController();
       
       //----- Save the CES record
       controller.saveCES();
       
       //----- Update the CES record
       controller.updateCES();
        
    }
    
    //------- Scenario When CES score is null 
    static testMethod void myUnitTest1() {
       ApexPages.currentPage().getParameters().put('oppId', '006p0000004XHXJ');
       ApexPages.currentPage().getParameters().put('cesVal', '');
       Opportunity_CESController controller = new Opportunity_CESController();
       
       //----- Save the CES record
       controller.saveCES();
       
       //----- Update the CES record
       controller.updateCES();
    }
    
    //------- Scenario When Opp Id is null 
    static testMethod void myUnitTest2() {
       ApexPages.currentPage().getParameters().put('oppId', null);
       ApexPages.currentPage().getParameters().put('cesVal', '3');
       Opportunity_CESController controller = new Opportunity_CESController();
       
       //----- Save the CES record
       controller.saveCES();
       
       //----- Update the CES record
       controller.updateCES();
    }
    
    //------- Scenario When CES value is more than 4 
    static testMethod void myUnitTest3() {
       ApexPages.currentPage().getParameters().put('oppId', '006p0000004XHXJ');
       ApexPages.currentPage().getParameters().put('cesVal', '5');
       Opportunity_CESController controller = new Opportunity_CESController();
       
       //----- Save the CES record
       controller.saveCES();
       
       //----- Update the CES record
       controller.updateCES();
    }
    
}