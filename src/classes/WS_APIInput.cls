public  class WS_APIInput {
	
	public CaseDetails[] OrderCancelData;
	
	public class CaseDetails {
		public String Origin;	//Spree
		public String Type;	//Cancellation
		public String Sub_Category;	//To Be Cancelled
		public String Order;	//R772992426
		public String Parent_Order;
		public String Payment_Method;	//Card On Delivery
		public Integer O_FacilityID;	//12
		public String Customer_Id;	//12887
		public String Type_of_Refund;	//
		public String Reason_For_Cancellation;	//Wrong Product Delivered
		public String Channel_Code;	//spree
		public String subject ; // Cancelled from self-serve
		public String description ; //cancellation
		public Cancellation_items_Data[] Cancellation_items_Data;
	}
	
	public class Cancellation_items_Data {
		public String OI_Code;	//2018762-1
		public String OI_Name;	//Arabia Coffee Table (Teak Finish)
		public String OI_Reason;	//Size Does Not Fit
		public String OI_Total_Price;	//4999.0
		public String OI_ExternalID;	//2018762\-1-R772992426
		public String OI_Eligibility;	//ELIGIBLE
		public String OI_Parent_SKU;	//FNBDST11BE15021
		public string OI_Delivery_Date;
	}

}