//Author: Vickal
//Dated : 3/3/2016
//Purpose: Get list of order and realted order items.

/*
*Class to get Order and Order Items
*/

global class OrderOItemPageController {

    public static  ID currentCaseContact{get;set;}
    public static  contact customer{get;set;}
    public ApexPages.StandardController stdController;
    public OrderOItemPageController(ApexPages.StandardController stdController) {
        
      }

    /*
    *This method will all orders related to the current contact
    */
    @RemoteAction
    global static List<order__x> getOrders(string contId){
        List<order__x> ords = new List<order__x>();
        try{
        if(contId != null){
            customer = [SELECT id,Customer_ID__c,firstName,LastName from Contact where ID =: contId LIMIT 1];
        }
        ords = [SELECT ExternalId,Id, Code__c,placed_at__c,discount_total__c,status_text__c,total__c,payment_method__c from Order__x where consumer_id__c =: customer.Customer_ID__c];
        return ords;
        }catch(exception e){
            system.debug('***Exception***'+e.getMessage());
            return null;
        }
        
    }
    
    /*
    *This method will all orders Items related to the Orders.
    */
    @RemoteAction
    global static List<orderitem__x> getOrderItems(string oCode){
        List<orderitem__x> oItems = new List<orderitem__x>();
        try{
        if(oCode != null){
            oItems = [SELECT ExternalID,item_name__c,Code__c,FC_facility__c,facility_id__c, discount_total__c,lms_status_text__c, total_price__c,sku__c,requested_delivery_date__c,promised_delivery_date__c FROM orderitem__x WHERE order_code__c =: oCode];
        }
        return oItems;
        }catch(Exception e){
            system.debug('***Exception***'+e.getMessage());
            return null;
        }
    }
}