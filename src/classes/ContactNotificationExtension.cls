public class ContactNotificationExtension {
    
    public ContactNotificationExtension(ApexPages.StandardController controller) {
        Contact conIns;
        if(!Test.isRunningTest()) { controller.addFields(new List<String>{'Success_Message__c', 'Error_Message__c'}); } 
        else {
            conIns = [select Error_Message__c, Success_Message__c from contact where id=: controller.getId()];
        }
        conIns = (Contact)controller.getRecord();
        if(conIns.Error_Message__c != '' && conIns.Error_Message__c != null) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, conIns.Error_Message__c));
        } else if(conIns.Success_Message__c != '' && conIns.Success_Message__c != null) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.CONFIRM, conIns.Success_Message__c));
        }
    }
}