/*Author : Sachin Chincholi
Purpose  : Org related data like Profiles,Org Schema will be queried.
Last Modified Reason :
*/

public class OrgService {
    
    private static boolean isProfileInfoCached = false;
    private static map<ID,Profile>  profilesDetailsMap = new map<ID,Profile>();
    
    public static map<ID,Profile> getprofilesDetailsMap(){
        if(isProfileInfoCached){
            return profilesDetailsMap;
        }
        cacheProfileDetails();
        return profilesDetailsMap;
    }
    
    private static void cacheProfileDetails(){
        for(Profile eachProfile :[SELECT id,Name from Profile]){
            profilesDetailsMap.put(eachProfile.ID,eachProfile);
        }
        isProfileInfoCached = true;
    }
}