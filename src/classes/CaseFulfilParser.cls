public class CaseFulfilParser{
 public cls_Data[] Data;
 public class cls_Data {
  public String Origin; //Email
  public String Type; //Cancellation
  public String Sub_Category; //Fulfil Cancellation
  public String Order; //R778495514
  public String Parent_Order; //
  public String Payment_Method; //
  //public String Status; //
  //public String Priority; //
  public Integer O_FacilityID; //4
  public String Reason_For_Cancellation; //
  //public String Type_of_Refund; //No Refund
  public String Customer_Id; //269595
  public cls_Cancellation_items_Data[] Cancellation_items_Data;
 }
 public class cls_Cancellation_items_Data {
  public String OI_Code; //1394982-1
  public String OI_Name; //Winchester Sofa Cobalt Three Seater
  public String OI_Reason; //Item has been cancelled
  public String OI_Total_Price; //
  public String OI_Delivery_Date; //
  public String OI_ExternalID; //
 }
 public static CaseFulfilParser parse(String json){
  return (CaseFulfilParser) System.JSON.deserialize(json, CaseFulfilParser.class);
 }
}