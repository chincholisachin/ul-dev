public class OrderAdditionalDetailsController {
    
    public string dcName {get;set;}
    
    public OrderAdditionalDetailsController(ApexPages.StandardController controller) {
      try{
        dcName = '';
        String currentOrderID = (String)Controller.getID();
        Order__x currentOrder = [SELECT id,facility_id__c,Current_facility_Id__c from Order__x where ID=:currentOrderID];
        Regions__c eachRegion = Regions__c.getInstance(String.valueOf(currentOrder.Current_facility_Id__c));
        dcName = eachRegion.Region__c; 
        system.debug('---dcName---'+dcName);
        }
        catch(Exception e){
            system.debug('---Exception in loading DC Name---'+e.getMessage());
        }
    }

}