public class updateLeadBatchClass implements Database.Batchable<sObject>{
     public  Database.QueryLocator start(Database.BatchableContext BC) {
         
           try{    
                return Database.getQueryLocator([select id ,CreatedDate__c,IsConverted from Lead WHERE CreatedDate >= LAST_N_DAYS:60 and IsConverted = false]);
            }
        catch(Exception e) {
            system.debug('*** Exception Name***'+e.getMessage());
        }
        return null;
     }
    
    public void execute(Database.BatchableContext BC,List<Lead> scope) {
        list<lead> leadListToUpdate = new list<lead>();
        try{
            if(!scope.isEmpty()) {
                for (Lead eachLead : scope) {
            
            if(eachLead.IsConverted == False) {
                eachLead.State = 'Closed';
                eachLead.Sub_Stage__c = 'Duplicate';
                eachLead.Customer_ID__c += Date.today();
                leadListToUpdate.add(eachLead);
            }
            
        }
        //database.update(scope, false); 
        //Fix for the repeated creation of Opportunities.
        LeadRecursion.fireTrigger = false;
        if(!leadListToUpdate.isEmpty()){
            database.update(leadListToUpdate,false);
        }
        
                
            }
           }   
        catch(Exception e) {
            
            system.debug('**** Exception Name***'+e.getMessage());
        }
    }
    public void finish(Database.BatchableContext BC) {
        
    }

}