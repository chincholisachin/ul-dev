/*Author: Baba S
Dated : 07/07/2016
Purpose: To change Case Owner from CSD-Email(Temp) to CSD-Email.
*/

global class CaseOwnerChange_Batch implements Database.Batchable<sObject> {
    public string emailqId ;
    public string misscallqId ;
    public string deliveryqId;
    public string prodqId;
    
    global CaseOwnerChange_Batch(){
        emailqId   = [select Id, name from Group where Type = 'Queue' AND name = 'Customer Service Desk - Email' Limit 1].id;
        misscallqId     = [select Id, name from Group where Type = 'Queue' AND name = 'Customer Service Inbound Call(Missed)' Limit 1].id;
        deliveryqId     = [select Id, name from Group where Type = 'Queue' AND name = 'Escalations' Limit 1].id;
        prodqId         = [select Id, name from Group where Type = 'Queue' AND name = 'Product Q and A' Limit 1].id;        
    }
    global Database.QueryLocator start(Database.BatchableContext BC) {
        string qId   = [select Id, name from Group where Type = 'Queue' AND name = 'Customer Service - Temp' Limit 1].id;
        String query = 'SELECT Id,ownerId,origin,type,Sub_Category__c,After_Sales_Status__c, CaseNumber FROM Case where OwnerId=:qId';
        return Database.getQueryLocator(query);
    }
   
    global void execute(Database.BatchableContext BC, List<case> csList) {
        list<Case> caseListToUpdate = new list<Case>();
        
        for(case cs : csList)
         {
            
            if(cs.origin == 'Email'){
                cs.OwnerId =emailqId;
            }else if(cs.origin == 'Miss Call'){
                cs.OwnerId =misscallqId;
            }else if(cs.origin == 'Delivery App'){
                cs.OwnerId =deliveryqId;
            }else if(cs.Origin == 'Product Q and A'){
                cs.ownerId = prodqId;
            }  
            caseListToUpdate.add(cs);         
         }
         if(caseListToUpdate.size()>0)
         update caseListToUpdate ;
    }   
    
    global void finish(Database.BatchableContext BC) {
    }
}