/* Author : Satyanarayana
Description : CustomerMergeExtension; Merges Secondary Account(Contact) details with Primary Account(Contact).

*/
public class CustomerMergeExtension {
    
    public string selectedPrimaryCustomer {get; set;}
    public string selectedCustomer2 {get; set;}
    public string selectedCustomer1 {get; set;}
    public Contact con {get; set;}
    public boolean btnFlag {get; set;}
    public boolean successFlag {get; set;}
    public ApexPages.StandardController stdController;
    
    public CustomerMergeExtension(ApexPages.StandardController controller) {
        
        successFlag = false;
        selectedPrimaryCustomer  = '';
        selectedCustomer1 = '';
        selectedCustomer2 = '';
        if(!Test.isRunningTest()) {
            controller.addFields(new string[]{'Customer_ID__c'});       
        } 
        this.stdController = controller;
        con = (Contact)stdController.getRecord();
        if(Test.isRunningTest()) {
            con = [select id, Customer_ID__c, Name, Secondary_Contact__c from Contact where id =: controller.getId()];
        }
        if(con.Customer_ID__c == null) {
            btnFlag = false;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Customer Id not available for <b>'+con.Name+'</b>, you can\'t Merge'));
        } else {
            btnFlag = true;
        }
    }
    
    /*public pageReference mergeCustomers() {

return null;
}*/
    
    public pagereference mergeSecondaryCustomerToPrimaryCustomer() {
        
        Id primaryContactId;
        try {
            primaryContactId = selectedPrimaryCustomer;
            Id secondaryContactId = primaryContactId !=  selectedCustomer1 ? selectedCustomer1 : selectedCustomer2;
            system.debug(primaryContactId);
            system.debug(secondaryContactId);
            system.debug(selectedCustomer1);
            system.debug(selectedCustomer2);
            string primaryCustomerId = (con.Id == primaryContactId) ? string.valueOf(con.Customer_ID__c) : ContactService.getcustomerIdForContact(con.Secondary_Contact__c); 
            string secondaryCustomerId = (con.Id == primaryContactId) ? ContactService.getcustomerIdForContact(con.Secondary_Contact__c) : string.valueOf(con.Customer_ID__c);
            if(primaryCustomerId != null && secondaryCustomerId != null) {
                successFlag = CustomerMergeCalloutService.customerMergeCallout(con.Id, primaryCustomerId, secondaryCustomerId);
                //successFlag = true;
                if(successFlag) {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Record Merged Successfully.Thank you!'));
                }
            } else {
                successFlag = false;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Selected Customer don\'t have CustomerId'));
            }
        } catch(Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
        }
        
        if(con.Id != primaryContactId) {
            return null;
            //new pageReference('/'+primaryContactId);
        } 
        return null; 
    } 
}