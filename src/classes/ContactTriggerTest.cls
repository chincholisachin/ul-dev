@isTest(seeAllData=false)
private class ContactTriggerTest {
    
    private static testMethod void positiveTesting() {
        System_Configs__c texec = new System_Configs__c(Name='Contact', Is_Run__c = true);
        insert texec;
        UL_Wulverine_Credentials__c ul = new UL_Wulverine_Credentials__c(Name = 'Wulverine Server', User_name__c = 'test', Password__c ='1234qwer');
        insert ul;
        UL_EndPoint_URLs__c ue = new UL_EndPoint_URLs__c(Name = 'Customer_Insert_UL', End_point_URL__c = 'https://stg-salesforce.urbanladder.com/v1/consumers', Time_Out__c = 5);
        insert ue;
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new CustomerInsertResponse());
        Test.startTest();
        Contact con = new Contact(LastName = 'TestName', phone='9854325678', Email ='test@urbanLadder5.co.in');
        insert con; // Insert Opertaion on Trigger
        
        con.FirstName = 'Testing';
        con.Salutation = 'Mr';
        system.debug('01111');
        update con;
        Test.stopTest();
    } 
    
    private static testMethod void negativeTesting() {
        
        Contact con = new Contact(LastName = 'TestName', phone='9854325679', Email ='test@urbanLadder4.co.in');
        insert con; // Insert Opertaion on Trigger
        con.FirstName = 'Testing';
        con.Salutation = 'Mr';
        update con;
    } 
    
    private static testMethod void positiveTesting2() {
        
        System_Configs__c texec = new System_Configs__c(Name='Contact', Is_Run__c = true);
        insert texec;
        UL_Wulverine_Credentials__c ul = new UL_Wulverine_Credentials__c(Name = 'Wulverine Server', User_name__c = 'test', Password__c ='1234qwer');
        insert ul;
        UL_EndPoint_URLs__c ue = new UL_EndPoint_URLs__c(Name = 'Customer_Insert_UL', End_point_URL__c = 'https://stg-salesforce.urbanladder.com/v1/consumers', Time_Out__c = 5);
        insert ue;
        Test.startTest();
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new CustomerInsertResponse());
        Contact con = new Contact(LastName = 'TestName', phone='9854325687', Email ='test@urbanLadder1.co.in');
        insert con; // Insert Opertaion on Trigger
        Test.setMock(HttpCalloutMock.class, new CustomerInsertResponse3());
        Contact con1 = new Contact(LastName = 'TestName', phone='9854325697', Email ='test@urbanLadder1.co.in');
        Database.DMLOptions dml = new Database.DMLOptions(); 
        dml.DuplicateRuleHeader.AllowSave = true;
        Database.SaveResult sr2 = Database.insert(con1, dml);
        
        //insert con1; // Insert Opertaion on Trigger
        Test.stopTest();
    }
    
    private static testMethod void positiveTesting3() {
        
        System_Configs__c texec = new System_Configs__c(Name='Contact', Is_Run__c = true);
        insert texec;
        UL_Wulverine_Credentials__c ul = new UL_Wulverine_Credentials__c(Name = 'Wulverine Server', User_name__c = 'test', Password__c ='1234qwer');
        insert ul;
        UL_EndPoint_URLs__c ue = new UL_EndPoint_URLs__c(Name = 'Customer_Update_UL', End_point_URL__c = 'https://stg-salesforce.urbanladder.com/v1/consumers/', Time_Out__c = 5);
        insert ue;
        UL_EndPoint_URLs__c ue1 = new UL_EndPoint_URLs__c(Name = 'Customer_Insert_UL', End_point_URL__c = 'https://stg-salesforce.urbanladder.com/v1/consumers', Time_Out__c = 5);
        insert ue1;
        
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new CustomerUpdateResponse());
        
        Contact con = new Contact(LastName = 'TestName', phone='9854325777', Email ='test@urbanLadder3.co.in');
        insert con; // Insert Opertaion on Trigger
        Test.startTest();
        system.debug(con); 
        con.Salutation = 'Mrs';
        con.LastName = 'testing';
        con.Customer_ID__c = 256;
        system.debug('$$$$$$$');
        system.debug(con);
        //Test.setMock(HttpCalloutMock.class, new MockHttpResponse());
        contactRecursion.fireTrigger = true;
        update con;
        system.debug(con);
        Test.stopTest();
    }
    
    private static testMethod void missedCallContact() {
        
        System_Configs__c texec = new System_Configs__c(Name='Contact', Is_Run__c = true);
        insert texec;
        UL_Wulverine_Credentials__c ul = new UL_Wulverine_Credentials__c(Name = 'Wulverine Server', User_name__c = 'test', Password__c ='1234qwer');
        insert ul;
        UL_EndPoint_URLs__c ue = new UL_EndPoint_URLs__c(Name = 'Customer_Update_UL', End_point_URL__c = 'https://stg-salesforce.urbanladder.com/v1/consumers/', Time_Out__c = 5);
        insert ue;
        UL_EndPoint_URLs__c ue1 = new UL_EndPoint_URLs__c(Name = 'Customer_Insert_UL', End_point_URL__c = 'https://stg-salesforce.urbanladder.com/v1/consumers', Time_Out__c = 5);
        insert ue1;
        
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new CustomerUpdateResponse());
        
        Contact con = new Contact(LastName = 'Missed call', phone='9854323677');
        insert con; // Insert Opertaion on Trigger
        Test.startTest();
        ContactRecursion.fireTrigger = true;
        con.Email = 'test2@urbanladder.com';
        con.Customer_ID__c = null;
        update con;
        system.debug([select Email from contact]);
        Test.stopTest();
    }
    
    public static testMethod void updateAccountsTest(){
        Account acc = new Account(name='TestName');
        insert acc;
        AccountService.updateAccounts(new list<Account>{acc});
        
        acc.Name ='';
        AccountService.updateAccounts(new list<Account>{acc});
        AccountService.createNewAccounts(new list<Account>{new Account(Name='')});
    }
}