public class InitializeTestdata {
    
    // 1. Create Lead
    public static Lead createLead(){
        Lead lead = new Lead();
        
        lead.LastName = 'Testing Lead';
        lead.Email    = 'test@some.com';
        lead.Company  = 'Test Company';
        lead.Requirement__c = 'Full Furniture; Wardrobes; Partial Furniture';
        lead.Lead_stage__c = 'Open';
        lead.Sub_Stage__c = 'Yet to Contact';
        return lead;
    }
    
    // 2. Create Sales-Ops User
    public static User createUser1(){
     Profile p = [SELECT Id FROM Profile WHERE Name='Sales Ops']; 
          
          User insertUser= new User(Alias = 'new123', Email='inmoTestUser0068@inmotestorg.com', 
             EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
             LocaleSidKey='en_US', ProfileId = p.Id, 
             TimeZoneSidKey='America/Los_Angeles', UserName='inmoTestUser0068@inmotestorg.com');
    
    return insertUser;
    }

    //3. Create Account 
    public static Account createAccount(){
      return new Account(Name='Test Account');
    }

    //4.Create Opp
    public static Opportunity createOpportunity(Id accountId, String product){

      Opportunity opportunity = new Opportunity(accountId = accountId);
      opportunity.Name = 'Test Name '+product;
      opportunity.Requirement__c   = product;
      opportunity.StageName   = 'Designing';
      opportunity.Sub_Stage__c = 'Req Documentation';
      opportunity.CloseDate   = Date.Today()+14;
      opportunity.Email__c= 'test@some.com';
      opportunity.Site_City__c = 'Bangalore'; 

      return opportunity;
    }
    /*
       5. Create custom-setting for the LeadEventOwerShip 
          @param-1 leadOwnerId = queueId
          @param-2 eventOwnerId = userId

        return type Lead_Event_Owner__c(custom-setting);
   */
   public static Lead_Event_Owner__c createLeadEventOwerShip(Id leadOwnerId, Id eventOwnerId){

     Lead_Event_Owner__c leadEventOwerShip = new Lead_Event_Owner__c();
     LeadEventOwerShip.Name                = 'LeadEventOwerShip';
     leadEventOwerShip.Lead_Owner__c       = leadOwnerId;
     leadEventOwerShip.Event_Owner__c      = eventOwnerId;

     return leadEventOwerShip;
   }

   /*
      6. Update lead' to Qualified to create EVENT
             @param-1 LEAD       = LEAD(Obj)
             @param-2 CONSULTANT = userId(Id)

       return Lead(with all the requried fields for Visit-Scheduled)
   */
   public static Lead updateLeadsStageAndSubStage(Lead lead, Id consultantId){

        lead.Consultant_Name__c = consultantId;
        lead.Lead_stage__c = 'Qualified';
        lead.Sub_Stage__c = 'Visit Scheduled';
        lead.Proposed_Visit_Date_1__c = System.now() +1;
        lead.Service_Order_Number__c = 'R123';
         //Adding Site Address
        lead.Site_Street__c='Test Street';
        lead.Site_Area__c = 'Test Area';
        lead.Site_City__c = 'Bangalore';
        lead.Site_State__c = 'Test State';
        lead.Place_of_Visit__c = 'Site of the customer';
        lead.Site_Zip_Code__c = 123123;

        return lead;
   }


}