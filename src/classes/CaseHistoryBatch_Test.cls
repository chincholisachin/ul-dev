@isTest(seeAllData=false)
private class CaseHistoryBatch_Test {
    private static testMethod void testone() {
    	
    	//----- Inserting custom setting value for testing 
        System_Configs__c sysCustom = new System_Configs__c();
        sysCustom.Name = 'EmailMessage';
        sysCustom.Is_Run__c = true ;
        insert sysCustom ;
        
        case caseInstance = new case();
        caseInstance.Email_Responded__c        = TRUE;
        //caseInstance.CS_Agent_Assigned_Time__c = Date.today();
        //caseInstance.First_Response_User__c    = 'Ameeth';
        //caseInstance.Agent_Picks_up_Time__c    = Date.today();
        //caseInstance.Email_Responded__c        = TRUE;
         caseInstance.origin = 'Email';
        caseInstance.Type = 'Feedback';
        caseInstance.Sub_Category__c = 'Test';
        insert caseInstance;
        caseInstance.status = 'pending';
        Update caseInstance;
       /* caseHistory ch = new caseHistory();
        ch = [select id, Field,NewValue,OldValue,caseId from caseHistory where caseId = 'caseInstance.Id'];
        ch.Field = 'Email_Responded__c';
        insert ch;*/
        EmailMessage em = new EmailMessage();
        em.Status = '3';
        em.BccAddress = 'upendar@gmail.com';
        em.CcAddress = 'test@test.com';
        em.TextBody = 'Hi How are you';
        em.FromAddress = 'upendar@yahoo.com';
        em.ParentId = caseInstance.Id;
        em.MessageDate = Date.today();
        insert em;
       /* Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@test.com',
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
        LocaleSidKey='en_IN', ProfileId = p.Id,
        TimeZoneSidKey='Asia/Kolkata', UserName='ameeth@testorg.com');  
        insert u;*/
        System.Test.startTest();
        Database.executeBatch(new CaseHistory_Batch());
        System.Test.stopTest();
    }
}