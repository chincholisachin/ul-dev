@isTest(seealldata=false)
private class ContactNotificationExtensionTest {
    
    private static testMethod void test1() {
        Account acc = new Account(Name= 'Test account'); 
        insert acc;
        contact con = new Contact(lastName = 'test last name', Email = 'test2@urbanLader.com', Error_Message__c = 'test');
        insert con;
        ApexPages.StandardController std = new ApexPages.StandardController(con);
        ContactNotificationExtension ext = new ContactNotificationExtension(std);
    }
    
    private static testMethod void test2() {
        Account acc = new Account(Name= 'Test account'); 
        insert acc;
        contact con = new Contact(lastName = 'test last name', Email = 'test2@urbanLader.com', Success_Message__c = 'test');
        insert con;
        ApexPages.StandardController std = new ApexPages.StandardController(con);
        ContactNotificationExtension ext = new ContactNotificationExtension(std);
    }
}