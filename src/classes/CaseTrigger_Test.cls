@isTest(seeAllData=false)
private class CaseTrigger_Test {
    
    public static Contact contactobj;
    public static Case caseobj;
    
    private static testmethod void testone() {
        
        System_Configs__c texec = new System_Configs__c(Name='Case', Is_Run__c = true);
        insert texec;
        Account acc = new Account(Name= 'Test account'); 
        insert acc;
        Entitlement ent = new Entitlement(Name='UL : CS Standard SLA/Entitlement', Type ='Phone Support', AccountId=acc.Id);
        insert ent;
        Case caseInstance = new Case(Status='Open', Origin='Web', EntitlementId=ent.Id);
        insert caseInstance;
        /*contactobj = new contact();

Test.startTest();

contactobj.FirstName = 'Urban';
contactobj.lastName = 'Ladder';
contactobj.Email = 'testuser@urbanladder.com';
insert contactobj;

Account acc = new Account();
acc.Name ='Test';
insert acc;

Entitlement ent = new Entitlement();

ent.AccountID = acc.Id;
ent.Name = 'UL : CS Standard SLA/Entitlement';
insert ent;

caseobj = UL_Utilities.createcase(contactobj.Id);
caseobj.entitlementID = ent.id;
insert caseobj;

Test.stopTest(); */
        
    } 
    
    private static testmethod void negativeTest1() {
        
        System_Configs__c texec = new System_Configs__c(Name='Case', Is_Run__c = true);
        insert texec;
        Account acc = new Account(Name= 'Test account'); 
        insert acc;
        Entitlement ent = new Entitlement(Name='CS Standard SLA/Entitlement', Type ='Phone Support', AccountId=acc.Id);
        insert ent;
        Case caseInstance = new Case(Status='Open', Origin='Web', EntitlementId=ent.Id);
        insert caseInstance;
    }
    
    private static testmethod void negativeTest2() {
        
        Account acc = new Account(Name= 'Test account'); 
        insert acc;
        Entitlement ent = new Entitlement(Name='CS Standard SLA/Entitlement', Type ='Phone Support', AccountId=acc.Id);
        insert ent;
        Case caseInstance = new Case(Status='Open', Origin='EMAIL', EntitlementId=ent.Id, Email_Responded__c=true);
        insert caseInstance;
    }
    
    private static testmethod void testtwo() {
        
        System_Configs__c texec = new System_Configs__c(Name='Case', Is_Run__c = true);
        insert texec;
        Account acc = new Account(Name= 'Test account'); 
        insert acc;
        contact con = new Contact(lastName = 'test last name', Email = 'test2@urbanLader.com');
        insert con;
        Entitlement ent = new Entitlement(Name='CS Standard SLA/Entitlement', Type ='Phone Support', AccountId=acc.Id);
        insert ent;
        List<Case> caseInstanceList = new List<case>();
        Case caseInstance = new Case(contactId=con.Id, SuppliedEmail = 'test2@urbanLader.com', Status='Open', Origin=Constants.ORIGIN_EMAIL, EntitlementId=ent.Id, Email_Responded__c=true);
        //insert caseInstance;
        Case caseInstance2 = new Case(Suppliedname='test name', SuppliedEmail = 'test3@urbanLader.com', Status='Open', Origin=Constants.ORIGIN_EMAIL, EntitlementId=ent.Id, Email_Responded__c=true);
        //insert caseInstance2;
       // caseInstanceList.add(caseInstance);
       // caseInstanceList.add(caseInstance2);
        //insert caseInstanceList;
        insert caseInstance;
        insert caseInstance2;
        caseInstance.Order__c = '12342';
        caseInstance.Order_Item__c = '2345321';
        caseInstance.Type ='Cancellation';// ConsCasePAPageController_Testtants.TYPE_AFTERSALES;
        caseInstance.Sub_Category__c = 'Process Cancellation';//Constants.SC_PRODUCTCOMPLAINT;
        caseInstance.After_Sales_Status__c =Constants.AS_ST_VISITSCHEDULED;
        caseInstance.Visit_Scheduled_Date__c = Date.today();
        caseInstance.Refund_Status__c = 'Not Applicable';
        //caseInstance.Status =Constants.STATUS_CLOSED;
        update caseInstance;
    }
    
    private static testmethod void testtwo_one() {
        
        System_Configs__c texec = new System_Configs__c(Name='Case', Is_Run__c = true);
        insert texec;
        Account acc = new Account(Name= 'Test account'); 
        insert acc;
        contact con = new Contact(lastName = 'test last name', Email = 'test2@urbanLader.com');
        insert con;
        Entitlement ent = new Entitlement(Name='CS Standard SLA/Entitlement', Type ='Phone Support', AccountId=acc.Id);
        insert ent;
        List<Case> caseInstanceList = new List<case>();
        Case caseInstance = new Case(contactId=con.Id, SuppliedEmail = 'test2@urbanLader.com', Status='Open', Origin='Call', EntitlementId=ent.Id);
        //insert caseInstance;
        Case caseInstance2 = new Case(Suppliedname='test name', SuppliedEmail = 'test3@urbanLader.com', Status='Open', Origin=Constants.ORIGIN_EMAIL, EntitlementId=ent.Id, Email_Responded__c=true);
        //insert caseInstance2;
       // caseInstanceList.add(caseInstance);
       // caseInstanceList.add(caseInstance2);
        //insert caseInstanceList;
        insert caseInstance;
        insert caseInstance2;
        caseInstance.Status_sent_to_Order_Service__c = 'cancellation_requested' ;
        update caseInstance ;
        caseInstance.Order__c = '12342';
        caseInstance.Order_Item__c = '2345321';
        caseInstance.Type ='Cancellation';// Constants.TYPE_AFTERSALES;
        caseInstance.Sub_Category__c = 'Process Cancellation';//Constants.SC_PRODUCTCOMPLAINT;
        //caseInstance.After_Sales_Status__c =Constants.AS_ST_VISITSCHEDULED;
        //caseInstance.Visit_Scheduled_Date__c = Date.today();
        //caseInstance.Status =Constants.STATUS_CLOSED;
        update caseInstance;
        
    }
    
    private static testmethod void testthree() {
        
        System_Configs__c texec = new System_Configs__c(Name='Case', Is_Run__c = true);
        insert texec;
        Account acc = new Account(Name= 'Test account'); 
        insert acc;
        contact con = new Contact(lastName = 'test last name', Email = 'test2@urbanLader.com');
        insert con;
        /*Order__x orderInfoExt = new Order__x(consumer_id__c = con.Id, code__c = 'Test');
Database.insertAsync (orderInfoExt); //insertImmediate
Orderitem__x orderItemInfoExt = new Orderitem__x(order_code__c = orderInfoExt.ExternalId, amount_due__c= '1000');
Database.insertAsync (orderItemInfoExt);*/
        Entitlement ent = new Entitlement(Name='CS Standard SLA/Entitlement', Type ='Phone Support', AccountId=acc.Id);
        insert ent;
        List<Case> caseInstanceList = new List<case>();
        Case caseInstance = new Case(contactId=con.Id, SuppliedEmail = 'test2@urbanLader.com', Status='Open', Origin=Constants.ORIGIN_EMAIL, EntitlementId=ent.Id, Email_Responded__c=true);
        //insert caseInstance;
        Case caseInstance2 = new Case(Suppliedname='test name', SuppliedEmail = 'test3@urbanLader.com', Status='Open', Origin=Constants.ORIGIN_EMAIL, EntitlementId=ent.Id, Email_Responded__c=true);
        //insert caseInstance2;
        caseInstanceList.add(caseInstance);
        caseInstanceList.add(caseInstance2);
        insert caseInstanceList;
        //test.startTest();
        caseInstance.Type = Constants.TYPE_AFTERSALES;
        caseInstance.Sub_Category__c = Constants.SC_PRODUCTCOMPLAINT;
        caseInstance.After_Sales_Status__c =Constants.AS_ST_VISITSCHEDULED;
        caseInstance.Visit_Scheduled_Date__c = Date.today();
        caseInstance.After_Sales_Status__c = 'Needs Replacement';
        caseInstance.Order__c = '234';
        //orderInfoExt.ExternalId;
        caseInstance.Order_Item__c = '7777';
        caseInstance.After_Sales_Defects__c = 'Bad Quality';
        //orderItemInfoExt.ExternalId;
        update caseInstance;
        //Test.stopTest();
    }
    
    private static testMethod void testFour() {
        
        contact con = new Contact(lastName = 'Miss Call Contact',Phone='9898098980');
        insert con;
        Caseservice.createNewCaseFromContact(con);
        //Caseservice.fetchLastUpdatedOpenCaseByContact(con);
    }
    
    private static testMethod void orderReplaced() {
        
        System_Configs__c texec = new System_Configs__c(Name='Case', Is_Run__c = true);
        insert texec;
        UL_EndPoint_URLs__c ue = new UL_EndPoint_URLs__c(Name='Order_Retrigger_UL', End_point_URL__c = '/v1/order-items/listener/replacement-trigger', Time_Out__c = 5);
        insert ue;
        UL_Wulverine_Credentials__c uc = new UL_Wulverine_Credentials__c(User_Name__c = 'salesforce@urbanladder.com', Password__c = 'VXJiYU5fRm9yY0U2NzM=', Name = 'Wulverine Server',Sandbox_Username__c = 'salesforce@urbanladder.com', Sandbox_Password__c	 = 'VXJiYU5fRm9yY0U2NzM=',Wulverine_Sandbox_Host__c='https://esgp3-wulverine.urbanladder.com',Wulverine_Production_Host__c='https://stg-salesforce.urbanladder.com');
        insert uc;
        Case caseInstance = new Case(Order_Item__c = '10395-R2345632',SuppliedEmail = 'test2@urbanLader.com', Status='Open', Origin=Constants.ORIGIN_EMAIL, Email_Responded__c=true);
        insert caseInstance;
        Test.setMock(HttpCalloutMock.class, new CustomerInsertResponse());
        Test.startTest();
        CaseRecursion.fireTrigger = true;
        caseInstance.After_Sales_Status__c = 'Needs Replacement';
        update caseInstance;
        Test.stopTest();
    }
    
    private static testMethod void orderReplaced2() {
        
        System_Configs__c texec = new System_Configs__c(Name='Case', Is_Run__c = true);
        insert texec;
        UL_EndPoint_URLs__c ue = new UL_EndPoint_URLs__c(Name='Order_Retrigger_UL', End_point_URL__c = '/v1/order-items/listener/replacement-trigger', Time_Out__c = 5);
        insert ue;
        UL_Wulverine_Credentials__c uc = new UL_Wulverine_Credentials__c(User_Name__c = 'salesforce@urbanladder.com', Password__c = 'VXJiYU5fRm9yY0U2NzM=', Name = 'Wulverine Server',Sandbox_Username__c = 'salesforce@urbanladder.com', Sandbox_Password__c	 = 'VXJiYU5fRm9yY0U2NzM=',Wulverine_Sandbox_Host__c='https://esgp3-wulverine.urbanladder.com',Wulverine_Production_Host__c='https://stg-salesforce.urbanladder.com');
        insert uc;
        Case caseInstance = new Case(OI_Code__c = '10395-R2345632',SuppliedEmail = 'test2@urbanLader.com', Status='Open', Origin=Constants.ORIGIN_EMAIL, Email_Responded__c=true);
        insert caseInstance;
        Test.setMock(HttpCalloutMock.class, new CustomerInsertResponse());
        Test.startTest();
        CaseRecursion.fireTrigger = true;
        caseInstance.After_Sales_Status__c = 'Needs Replacement';
        update caseInstance;
        Test.stopTest();
    }
    
    private static testMethod void orderReplaced3() {
        
        System_Configs__c texec = new System_Configs__c(Name='Case', Is_Run__c = true);
        insert texec;
        UL_EndPoint_URLs__c ue = new UL_EndPoint_URLs__c(Name='Order_Retrigger_UL', End_point_URL__c = '/v1/order-items/listener/replacement-trigger', Time_Out__c = 5);
        insert ue;
       UL_Wulverine_Credentials__c uc = new UL_Wulverine_Credentials__c(User_Name__c = 'salesforce@urbanladder.com', Password__c = 'VXJiYU5fRm9yY0U2NzM=', Name = 'Wulverine Server',Sandbox_Username__c = 'salesforce@urbanladder.com', Sandbox_Password__c	 = 'VXJiYU5fRm9yY0U2NzM=',Wulverine_Sandbox_Host__c='https://esgp3-wulverine.urbanladder.com',Wulverine_Production_Host__c='https://stg-salesforce.urbanladder.com');
        insert uc;
        Case caseInstance = new Case(Order_Item__c = '103952345632',SuppliedEmail = 'test2@urbanLader.com', Status='Open', Origin=Constants.ORIGIN_EMAIL, Email_Responded__c=true);
        insert caseInstance;
        Test.setMock(HttpCalloutMock.class, new CustomerInsertResponse2());
        Test.startTest();
        CaseRecursion.fireTrigger = true;
        caseInstance.After_Sales_Status__c = 'Needs Replacement';
        update caseInstance;
        Test.stopTest();
    }
    
    
    
        
    private static testMethod void duplicateFinaceCase() {
        
               System_Configs__c texec = new System_Configs__c(Name='Case', Is_Run__c = true);
        insert texec;
        UL_EndPoint_URLs__c ue = new UL_EndPoint_URLs__c(Name='Order_Retrigger_UL', End_point_URL__c = '/v1/order-items/listener/replacement-trigger', Time_Out__c = 5);
        insert ue;
        UL_Wulverine_Credentials__c uc = new UL_Wulverine_Credentials__c(User_Name__c = 'salesforce@urbanladder.com', Password__c = 'VXJiYU5fRm9yY0U2NzM=', Name = 'Wulverine Server',Sandbox_Username__c = 'salesforce@urbanladder.com', Sandbox_Password__c	 = 'VXJiYU5fRm9yY0U2NzM=',Wulverine_Sandbox_Host__c='https://esgp3-wulverine.urbanladder.com',Wulverine_Production_Host__c='https://stg-salesforce.urbanladder.com');
        insert uc;
        Case duplicateCase = new Case();
        duplicateCase.Type = 'Finance';
        duplicateCase.Sub_Category__c = 'Refunds';
        duplicateCase.Amount_of_Refund__c=100;
        duplicateCase.O_Code__c='100';
        duplicateCase.OI_Code__c='100';
        
        Order__x ord1 = new Order__x(ExternalID='R123456');
        Database.insertAsync(ord1);
        
        orderitem__x oi = new orderitem__x();
        Database.insertAsync(oi);
        duplicateCase.order__c = ord1.ExternalId;
        insert duplicateCase;
              
          
        case newcase  = duplicateCase.clone();
        insert newcase;
        system.debug('newcase222'+newCase);    
        
        Test.startTest();
        CaseRecursion.fireTrigger = true;
        Test.stopTest();
        
        
    }
     private static testmethod void mergeWrapperTest1() {
         
         CallOutModel.MergeRequestWrapper wr = new CallOutModel.MergeRequestWrapper('123','456','123','');
         
     }
    
    private static testmethod void ownerChanged() {
        
        System_Configs__c texec = new System_Configs__c(Name='Case', Is_Run__c = true);
        insert texec;
        
        Regions__c region = new Regions__c(Name='2', Region__c = 'Bangalore South');
        insert region;
        
        Group testGroup = new Group(Name = 'DC Ops', Type = 'Queue');
        insert testGroup;
        
        QueueSobject testQueue = new QueueSObject(QueueId = testGroup.Id, SobjectType = 'Case');
        System.runAs(new User(Id = UserInfo.getUserId())) {   
            insert testQueue;
        }
        Order__x ord = new Order__x(ExternalID='R123456');
        Database.insertAsync(ord);
        
        Case caseInstance = new Case(O_FacilityID__c = 2, order__c = ord.ExternalId, SuppliedEmail = 'test2@urbanLader.com', Status='Open', Origin=Constants.ORIGIN_EMAIL, Email_Responded__c=true, type='Category', Sub_Category__c = 'catalog');
        insert caseInstance;
        CaseRecursion.fireTrigger = true;
        caseInstance.OwnerId = testGroup.Id;
        update caseInstance;
        
    }
    
    private static testmethod void ownerChangeQue() {
        
        System_Configs__c texec = new System_Configs__c(Name='Case', Is_Run__c = true);
        insert texec;
        
        Regions__c region = new Regions__c(Name='3', Region__c = 'Bangalore South');
        insert region;
        
        Group testGroup = new Group(Name = 'DC Ops', Type = 'Queue');
        insert testGroup;
        
        QueueSobject testQueue = new QueueSObject(QueueId = testGroup.Id, SobjectType = 'Case');
        System.runAs(new User(Id = UserInfo.getUserId())) {   
            insert testQueue;
        }
        Order__x ord = new Order__x(ExternalID='R123456');
        Database.insertAsync(ord);
        contact con = new Contact(lastName = 'test last name', Email = 'test2@urbanLader.com');
        insert con;
        List<case> csList = new List<Case>();
        Case caseInstance = new Case(O_FacilityID__c = 2, order__c = ord.ExternalId, SuppliedEmail = 'test2@urbanLader.com', Status='Open', Origin=Constants.ORIGIN_EMAIL, Email_Responded__c=true, type='Category', Sub_Category__c = 'catalog');
        insert caseInstance;
        csList.add(caseInstance);
        CaseRecursion.fireTrigger = true;
        Update csList; 
        //CaseService.changeOwnerDCOpsQueue(csList);
        //CaseService.fetchCasesByContact(con);
        //CaseService.fetchLastUpdatedOpenCaseByContact(con);
       //CaseRecursion.fireTrigger = true;
       // caseInstance.OwnerId = testGroup.Id;
       // update caseInstance;
        
    }
    
     private static testMethod void orderCancelled1() {
        
       System_Configs__c texec = new System_Configs__c(Name='Case', Is_Run__c = true);
        insert texec;
        UL_EndPoint_URLs__c ue = new UL_EndPoint_URLs__c(Name='Cancellation_Order', End_point_URL__c = '/v1/orders', Time_Out__c = 5);
        insert ue;
        UL_EndPoint_URLs__c urlCRS = new UL_EndPoint_URLs__c(Name='Customer Request Status', End_point_URL__c = '/v1/orders', Time_Out__c = 5);
        insert urlCRS;
        UL_Wulverine_Credentials__c uc = new UL_Wulverine_Credentials__c(User_Name__c = 'salesforce@urbanladder.com', Password__c = 'VXJiYU5fRm9yY0U2NzM=', Name = 'Wulverine Server',Sandbox_Username__c = 'salesforce@urbanladder.com', Sandbox_Password__c	 = 'VXJiYU5fRm9yY0U2NzM=',Wulverine_Sandbox_Host__c='https://esgp3-wulverine.urbanladder.com',Wulverine_Production_Host__c='https://stg-salesforce.urbanladder.com');
        insert uc;
        Case caseInstance = new Case(Order__c = 'R52345632',O_Code__c = 'R52345632',SuppliedEmail = 'test2@urbanLader.com', Status='Open', Origin=Constants.ORIGIN_EMAIL,Type='Cancellation');
        caseInstance.Item_Codes__c = 'R1223-54322';
        caseInstance.Cancellation_Status__c = 'Created';
        caseInstance.Status_sent_to_Order_Service__c = 'cancellation_requested' ;
        insert caseInstance;
        
        Cancellation_Item__c    ci = new Cancellation_Item__c();
        ci.Case__c  =   caseInstance.Id;
        ci.Approval_Status__c   = 'Approved';
        ci.OI_Code__c   =   'R1223-54322';
        ci.O_Code__c = 'R52345632';
        ci.Processing_Status__c = 'Not Processed';       
        insert ci;
         
        Test.setMock(HttpCalloutMock.class, new CancellationOIRespone_Cancel());
        Test.startTest();
        CaseRecursion.fireTrigger = true;
        caseInstance.Cancellation_Status__c = 'Process Cancellation';
        update caseInstance;
        caseInstance.Cancellation_Status__c = 'Cancelled';        
        update caseInstance;
        caseInstance.Refund_Status__c = 'Refund Initiated';        
        update caseInstance;
        Test.stopTest();
    }
    
     private static testMethod void orderCancelled2() {
        
       System_Configs__c texec = new System_Configs__c(Name='Case', Is_Run__c = true);
        insert texec;
        UL_EndPoint_URLs__c ue = new UL_EndPoint_URLs__c(Name='Cancellation_Order', End_point_URL__c = '/v1/orders', Time_Out__c = 5);
        insert ue;
        UL_Wulverine_Credentials__c uc = new UL_Wulverine_Credentials__c(User_Name__c = 'salesforce@urbanladder.com', Password__c = 'VXJiYU5fRm9yY0U2NzM=', Name = 'Wulverine Server',Sandbox_Username__c = 'salesforce@urbanladder.com', Sandbox_Password__c	 = 'VXJiYU5fRm9yY0U2NzM=',Wulverine_Sandbox_Host__c='https://esgp3-wulverine.urbanladder.com',Wulverine_Production_Host__c='https://stg-salesforce.urbanladder.com');
        insert uc;
        UL_EndPoint_URLs__c urlCRS = new UL_EndPoint_URLs__c(Name='Customer Request Status', End_point_URL__c = '/v1/orders', Time_Out__c = 5);
        insert urlCRS;
        Case caseInstance = new Case(Order__c = 'R52345632',O_Code__c = 'R52345632',SuppliedEmail = 'test2@urbanLader.com', Status='Open', Origin=Constants.ORIGIN_EMAIL,Type='Cancellation');
        caseInstance.Item_Codes__c = 'R1223-54322';
        caseInstance.Cancellation_Status__c = 'Created';
        insert caseInstance;
        caseInstance.Status_sent_to_Order_Service__c = 'cancellation_requested' ;
        caseInstance.Cancellation_Status__c = 'Cancellation - On Hold';
        update caseInstance;
        Cancellation_Item__c    ci = new Cancellation_Item__c();
        ci.Case__c  =   caseInstance.Id;
        ci.Approval_Status__c   = 'Approved';
        ci.OI_Code__c   =   'R1223-54322';
        ci.O_Code__c = 'R52345632';
        ci.Processing_Status__c = 'Not Processed';
        insert ci;
         
        Test.setMock(HttpCalloutMock.class, new CancellationOIResponse_Cancel_422());
        Test.startTest();
        CaseRecursion.fireTrigger = true;
        caseInstance.Cancellation_Status__c = 'Process Cancellation';
        update caseInstance;
        
        caseInstance.Cancellation_Status__c = 'Cancelled';               
        caseInstance.Refund_Status__c = 'Refund Initiated';
        update caseInstance;
        Test.stopTest();
    }
    private static testMethod void orderCancelled3() {
        
       System_Configs__c texec = new System_Configs__c(Name='Case', Is_Run__c = true);
        insert texec;
        UL_EndPoint_URLs__c ue = new UL_EndPoint_URLs__c(Name='Cancellation_Order', End_point_URL__c = '/v1/orders', Time_Out__c = 5);
        insert ue;
        UL_EndPoint_URLs__c urlCRS = new UL_EndPoint_URLs__c(Name='Customer Request Status', End_point_URL__c = '/v1/orders', Time_Out__c = 5);
        insert urlCRS;
        UL_Wulverine_Credentials__c uc = new UL_Wulverine_Credentials__c(User_Name__c = 'salesforce@urbanladder.com', Password__c = 'VXJiYU5fRm9yY0U2NzM=', Name = 'Wulverine Server',Sandbox_Username__c = 'salesforce@urbanladder.com', Sandbox_Password__c= 'VXJiYU5fRm9yY0U2NzM=',Wulverine_Sandbox_Host__c='https://esgp3-wulverine.urbanladder.com',Wulverine_Production_Host__c='https://stg-salesforce.urbanladder.com');
        insert uc;
        Case caseInstance = new Case(Order__c = 'R52345632',O_Code__c = 'R52345632',SuppliedEmail = 'test2@urbanLader.com', Status='Open', Origin=Constants.ORIGIN_EMAIL,Type='Cancellation');
        caseInstance.Item_Codes__c = 'R1223-54322';
        caseInstance.Cancellation_Status__c = 'Created';
        caseInstance.Status_sent_to_Order_Service__c = 'cancellation_requested' ;
        insert caseInstance;
        
        Cancellation_Item__c    ci = new Cancellation_Item__c();
        ci.Case__c  =   caseInstance.Id;
        ci.Approval_Status__c   = 'Approved';
        ci.OI_Code__c   =   'R1223-54322';
        ci.O_Code__c = 'R52345632';
        ci.Processing_Status__c = 'Not Processed';
        insert ci;
         
        Test.setMock(HttpCalloutMock.class, new CancellationOIResponse_Cancel_500());
        Test.startTest();
        CaseRecursion.fireTrigger = true;
        caseInstance.Cancellation_Status__c = 'Process Cancellation';
        update caseInstance;
        caseInstance.Cancellation_Status__c = 'Cancellation Rejected';
        caseInstance.Status = 'Closed';
        update caseInstance;
        Test.stopTest();
    }
    
    private static testMethod void orderCancelled4() {
        
       System_Configs__c texec = new System_Configs__c(Name='Case', Is_Run__c = true);
        insert texec;
        UL_EndPoint_URLs__c ue = new UL_EndPoint_URLs__c(Name='Cancellation_Order', End_point_URL__c = '/v1/orders', Time_Out__c = 5);
        insert ue;
        UL_EndPoint_URLs__c urlCRS = new UL_EndPoint_URLs__c(Name='Customer Request Status', End_point_URL__c = '/v1/orders', Time_Out__c = 5);
        insert urlCRS;
        UL_Wulverine_Credentials__c uc = new UL_Wulverine_Credentials__c(User_Name__c = 'salesforce@urbanladder.com', Password__c = 'VXJiYU5fRm9yY0U2NzM=', Name = 'Wulverine Server',Sandbox_Username__c = 'salesforce@urbanladder.com', Sandbox_Password__c	 = 'VXJiYU5fRm9yY0U2NzM=',Wulverine_Sandbox_Host__c='https://esgp3-wulverine.urbanladder.com',Wulverine_Production_Host__c='https://stg-salesforce.urbanladder.com');
        insert uc;
        Case caseInstance = new Case(Order__c = 'R52345632',O_Code__c = 'R52345632',SuppliedEmail = 'test2@urbanLader.com', Status='Open', Origin=Constants.ORIGIN_EMAIL,Type='Cancellation');
        caseInstance.Item_Codes__c = 'R1223-54322,R1223-54323';
        caseInstance.Cancellation_Status__c = 'Created';
        insert caseInstance;
        
        caseInstance.Status_sent_to_Order_Service__c = 'cancellation_requested';
        update caseInstance ;
        
        Cancellation_Item__c    ci = new Cancellation_Item__c();
        ci.Case__c  =   caseInstance.Id;
        ci.Approval_Status__c   = 'Approved';
        ci.OI_Code__c   =   'R1223-54322';
        ci.O_Code__c = 'R52345632';
        ci.Processing_Status__c = 'Not Processed';
        insert ci;
         
        Test.setMock(HttpCalloutMock.class, new CancellationOIResponse_Cancel_Exc());
        Test.startTest();
        CaseRecursion.fireTrigger = true;
        caseInstance.Cancellation_Status__c = 'Process Cancellation';
        update caseInstance;
        
        caseInstance.Cancellation_Status__c = 'Cancelled';
        caseInstance.Refund_Status__c = 'Not Applicable';
        caseInstance.Status = 'Closed';
        update caseInstance;
        Test.stopTest();
    }
    
     private static testmethod void testtwo_two() {
        
        System_Configs__c texec = new System_Configs__c(Name='Case', Is_Run__c = true);
        insert texec;
        Account acc = new Account(Name= 'Test account'); 
        insert acc;
        contact con = new Contact(lastName = 'test last name', Email = 'test2@urbanLader.com');
        insert con;
        Entitlement ent = new Entitlement(Name='CS Standard SLA/Entitlement', Type ='Phone Support', AccountId=acc.Id);
        insert ent;
        List<Case> caseInstanceList = new List<case>();
        Case caseInstance = new Case(contactId=con.Id, SuppliedEmail = 'test2@urbanLader.com', Status='Open', Origin='Call', EntitlementId=ent.Id);
        insert caseInstance;
        Case caseInstance2 = new Case(Suppliedname='test name', SuppliedEmail = 'test3@urbanLader.com', Status='Open', Origin=Constants.ORIGIN_EMAIL, EntitlementId=ent.Id, Email_Responded__c=true);
        insert caseInstance2;
       // caseInstanceList.add(caseInstance);
       // caseInstanceList.add(caseInstance2);
        //insert caseInstanceList;
        //insert caseInstance;
        //insert caseInstance2;
        caseInstance.Order__c = '12342';
        caseInstance.Order_Item__c = '2345321';
        caseInstance.Type ='General Enquiry';// Constants.TYPE_AFTERSALES;
        caseInstance.Sub_Category__c = 'Mattresses';//Constants.SC_PRODUCTCOMPLAINT;
        //caseInstance.After_Sales_Status__c =Constants.AS_ST_VISITSCHEDULED;
        //caseInstance.Visit_Scheduled_Date__c = Date.today();
        //caseInstance.Status ='Closed';
        update caseInstance;
        
    }
    
    private static testmethod void emailToCaseLoopBreakerPositive() {
        
        System_Configs__c texec = new System_Configs__c(Name='Case', Is_Run__c = true);
        insert texec;
        System_Configs__c e2cCS = new System_Configs__c(Name='EmailToCaseLoopBreak', Is_Run__c = true);
        insert e2cCS;
        
        Account acc = new Account(Name= 'Test account'); 
        insert acc;
        contact con = new Contact(lastName = 'test last name', Email = 'test2@urbanLader.com');
        insert con;
        Entitlement ent = new Entitlement(Name='CS Standard SLA/Entitlement', Type ='Phone Support', AccountId=acc.Id);
        insert ent;
        
        Case caseInstance = new Case(contactId=con.Id, SuppliedEmail = 'test2@urbanLader.com', Status='Open', Origin='Email', EntitlementId=ent.Id,Subject='Test E2C',Description='Test');
        insert caseInstance;
        
        Case caseInstance2 = new Case(Suppliedname='test name', SuppliedEmail = 'test2@urbanLader.com', Status='Open', Origin=Constants.ORIGIN_EMAIL, EntitlementId=ent.Id, Subject='Re:Test E2C',Description='Test');
        insert caseInstance2; 
        
        Case caseQueried = [SELECT Auto_Response_Disabled__c from Case where id=:caseInstance2.ID];
        system.assertEquals(true,caseQueried.Auto_Response_Disabled__c);
    }
    private static testmethod void emailToCaseLoopBreakerNegative() {
        
        System_Configs__c texec = new System_Configs__c(Name='Case', Is_Run__c = true);
        insert texec;
        System_Configs__c e2cCS = new System_Configs__c(Name='EmailToCaseLoopBreak', Is_Run__c = true);
        insert e2cCS;
        
        Account acc = new Account(Name= 'Test account'); 
        insert acc;
        contact con = new Contact(lastName = 'test last name', Email = 'test2@urbanLader.com');
        insert con;
        Entitlement ent = new Entitlement(Name='CS Standard SLA/Entitlement', Type ='Phone Support', AccountId=acc.Id);
        insert ent;
        
        Case caseInstance = new Case(contactId=con.Id, SuppliedEmail = 'test2@urbanLader.com', Status='Open', Origin='Email', EntitlementId=ent.Id,Subject='Test E2C');
        insert caseInstance;
        
        Case caseInstance2 = new Case(Suppliedname='test name', SuppliedEmail = 'test2@urbanLader.com', Status='Open', Origin=Constants.ORIGIN_EMAIL, EntitlementId=ent.Id, Subject='Re:Test E2C');
        insert caseInstance2; 
        
        Case caseQueried = [SELECT Auto_Response_Disabled__c from Case where id=:caseInstance2.ID];
        system.assertEquals(true,caseQueried.Auto_Response_Disabled__c);
    }
    
    private static testmethod void emailToCaseLoopBreakerNormalFlow() {
        
        System_Configs__c texec = new System_Configs__c(Name='Case', Is_Run__c = true);
        insert texec;
        System_Configs__c e2cCS = new System_Configs__c(Name='EmailToCaseLoopBreak', Is_Run__c = true);
        insert e2cCS;
        
        Account acc = new Account(Name= 'Test account'); 
        insert acc;
        contact con = new Contact(lastName = 'test last name', Email = 'test2@urbanLader.com');
        insert con;
        Entitlement ent = new Entitlement(Name='CS Standard SLA/Entitlement', Type ='Phone Support', AccountId=acc.Id);
        insert ent;
        
        Case caseInstance = new Case(contactId=con.Id, SuppliedEmail = 'test2@urbanLader.com', Status='Open', Origin='Email', EntitlementId=ent.Id,Subject='New Subject',Description='Test');
        insert caseInstance;
        
        Case caseInstance2 = new Case(Suppliedname='test name', SuppliedEmail = 'test2@urbanLader.com', Status='Open', Origin=Constants.ORIGIN_EMAIL, EntitlementId=ent.Id, Subject='002122',Description='Test');
        insert caseInstance2; 
        
        Case caseQueried = [SELECT Auto_Response_Disabled__c from Case where id=:caseInstance2.ID];
        system.assertEquals(false,caseQueried.Auto_Response_Disabled__c);
    }
    
    private static testmethod void negativeTestScenario1() {
        
        System_Configs__c texec = new System_Configs__c(Name='Case', Is_Run__c = true);
        insert texec;
        System_Configs__c e2cCS = new System_Configs__c(Name='EmailToCaseLoopBreak', Is_Run__c = true);
        insert e2cCS;
        
        Account acc = new Account(Name= 'Test account'); 
        insert acc;
        contact con = new Contact(lastName = 'test last name', Email = 'test2@urbanLader.com');
        insert con;
        Entitlement ent = new Entitlement(Name='CS Standard SLA/Entitlement', Type ='Phone Support', AccountId=acc.Id);
        insert ent;
        
        Case caseInstance = new Case(contactId=con.Id, SuppliedEmail = 'test2@urbanLader.com', Status='Open', Origin='Email', EntitlementId=ent.Id,Subject='New Subject',Description='Test');
        insert caseInstance;
        List<Case> caseList = new List<Case>();
        caseList.add(caseInstance);
        CaseTriggerHandler casetriggerHandlerObj = new CaseTriggerHandler();
        casetriggerHandlerObj.assignBFLCasesToDCOps(caseList);
        CaseService.fetchCasesByContact(new contact());
        CaseService.fetchCasesByContact(con);
        CaseService.fetchLastUpdatedOpenCaseByContact(new contact());
        CaseService.fetchLastUpdatedOpenCaseByContact(con);
    }
    
    /*** Below method will check for the BFL order confirmation response - Success scenario with response status as 200/ 409
    I have considered only status - 200, as both status 200 and 409 will use the same block of code
     ***/
    
    private static testMethod void orderConfirmationSuccessScenarioCallout() {
        
        System_Configs__c texec = new System_Configs__c(Name='Case', Is_Run__c = true);
        insert texec;
        UL_EndPoint_URLs__c ue = new UL_EndPoint_URLs__c(Name='BFL Order Confirmation', End_point_URL__c = '/v1/order/confirm', Time_Out__c = 3);
        insert ue;
        UL_Wulverine_Credentials__c uc = new UL_Wulverine_Credentials__c(User_Name__c = 'Dummy@example.com', Password__c = 'cGFzczEyMw==', Name = 'Wulverine Server',Sandbox_Username__c = 'sandbox@example.com', Sandbox_Password__c    = 'cGFzczEyMw==',Wulverine_Sandbox_Host__c='https://test.AbcOrganization.com',Wulverine_Production_Host__c='https://test.AbcOrganization.com');
        insert uc;
        Case caseInstance = new Case(Order_Item__c = '10395-R2345632',Order__c = '10395',O_Code__c = '10395',SF_Id__c = 'SF2234',O_FacilityID__c = 76,Type = 'BFL', SuppliedEmail = 'test2@urbanLader.com', Status='Open', Origin=Constants.ORIGIN_EMAIL, Email_Responded__c=true);
        insert caseInstance;
        Test.setMock(HttpCalloutMock.class, new OrderConfirmation_MockResponseGenerator());
        Test.startTest();
        CaseRecursion.fireTrigger = true;
        caseInstance.BFL_status__c = 'DO released & Confirmation pending';
        caseInstance.Disbursement_amount__c = 30000;
        caseInstance.DO_release_date__c = System.today().addDays(3);
        update caseInstance;
        Test.stopTest();
        
        System.assertEquals('Order confirmed & Disbursement pending',[Select id,BFL_status__c from case where id =:caseInstance.Id].BFL_status__c);
    }
    
    /*** Below method will check for the BFL order confirmation response - negative scenario - response status as 500 ***/
    
    private static testMethod void orderConfirmationResponseStatusAs500Scenario() {
        
        System_Configs__c texec = new System_Configs__c(Name='Case', Is_Run__c = true);
        insert texec;
        UL_EndPoint_URLs__c ue = new UL_EndPoint_URLs__c(Name='BFL Order Confirmation', End_point_URL__c = '/v1/order/confirm', Time_Out__c = 3);
        insert ue;
        UL_Wulverine_Credentials__c uc = new UL_Wulverine_Credentials__c(User_Name__c = 'Dummy@example.com', Password__c = 'cGFzczEyMw==', Name = 'Wulverine Server',Sandbox_Username__c = 'sandbox@example.com', Sandbox_Password__c    = 'cGFzczEyMw==',Wulverine_Sandbox_Host__c='https://test.AbcOrganization.com',Wulverine_Production_Host__c='https://test.AbcOrganization.com');
        insert uc;
        Case caseInstance = new Case(Order_Item__c = '10395-R2345632',Order__c = '10395',O_Code__c = '10395',SF_Id__c = 'SF2234',O_FacilityID__c = 76,Type = 'BFL', SuppliedEmail = 'test2@urbanLader.com', Status='Open', Origin=Constants.ORIGIN_EMAIL, Email_Responded__c=true);
        insert caseInstance;
        Test.setMock(HttpCalloutMock.class, new OrderConfirmation_MockResponse_500());
        Test.startTest();
        CaseRecursion.fireTrigger = true;
        caseInstance.BFL_status__c = 'DO released & Confirmation pending';
        caseInstance.Disbursement_amount__c = 30000;
        caseInstance.DO_release_date__c = System.today().addDays(3);
        update caseInstance;
        Test.stopTest();
        
        System.assertEquals('Order Confirmation Notification Failed',[Select id,BFL_status__c from case where id =:caseInstance.Id].BFL_status__c);
    }
    
    /*** Below method will check for the BFL order confirmation functionality - negative scenario - Where order code is balnk  ***/
    
    private static testMethod void orderConfirmationWhereOcodeIsBlankScenario() {
        
        System_Configs__c texec = new System_Configs__c(Name='Case', Is_Run__c = true);
        insert texec;
        UL_EndPoint_URLs__c ue = new UL_EndPoint_URLs__c(Name='BFL Order Confirmation', End_point_URL__c = '/v1/order/confirm', Time_Out__c = 3);
        insert ue;
        UL_Wulverine_Credentials__c uc = new UL_Wulverine_Credentials__c(User_Name__c = 'Dummy@example.com', Password__c = 'cGFzczEyMw==', Name = 'Wulverine Server',Sandbox_Username__c = 'sandbox@example.com', Sandbox_Password__c    = 'cGFzczEyMw==',Wulverine_Sandbox_Host__c='https://test.AbcOrganization.com',Wulverine_Production_Host__c='https://test.AbcOrganization.com');
        insert uc;
        Case caseInstance = new Case(Order_Item__c = '10395-R2345632',Order__c = '10395',O_Code__c = '',SF_Id__c = 'SF2234',O_FacilityID__c = 76,Type = 'BFL', SuppliedEmail = 'test2@urbanLader.com', Status='Open', Origin=Constants.ORIGIN_EMAIL, Email_Responded__c=true);
        insert caseInstance;
        Test.setMock(HttpCalloutMock.class, new OrderConfirmation_MockResponse_500());
        Test.startTest();
        CaseRecursion.fireTrigger = true;
        caseInstance.BFL_status__c = 'DO released & Confirmation pending';
        caseInstance.Disbursement_amount__c = 30000;
        caseInstance.DO_release_date__c = System.today().addDays(3);
        update caseInstance;
        Test.stopTest();
        
        System.assertEquals('DO released & Confirmation pending',[Select id,BFL_status__c from case where id =:caseInstance.Id].BFL_status__c); // Here order code is blank
    }
}