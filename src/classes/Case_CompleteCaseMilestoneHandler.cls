public with sharing class Case_CompleteCaseMilestoneHandler {
    
    
    
    private static Case_CompleteCaseMilestoneHandler instance;
    private static Datetime completionDate = Datetime.Now();
    //*** Singleton Pattern to obtain instance of the Handler***
    public static Case_CompleteCaseMilestoneHandler getInstance(){
        if(instance==null){
            instance = new Case_CompleteCaseMilestoneHandler();
        }
        return instance;
    }
    
    //*** After Insert Redirector***//
    public void afterInsertMethod(list<case> newList,list<case> oldList,map<ID,Case> newMap,map<ID,Case> oldMap){
        system.debug('*** After Insert Method Called***');
        //completeTimeMilestone(newMap,oldMap);
        
    }
    
    //*** After Update Redirector***//
    public void afterUpdateMethod(list<case> newList,list<case> oldList,map<ID,Case> newMap,map<ID,Case> oldMap){
        system.debug('*** After Update Method Called***');
        completeTimeMilestone(newMap,oldMap);
    }
    
    //*** Filter the Cases subject to exit the milestone criteria***//
    private static void completeTimeMilestone(map<ID,Case> newMap,map<ID,Case> oldMap){
        
        system.debug('*** completeTimeMilestone Method Called***');
        
        List<Id> updateCases_EmailFCR = new List<Id>();
        List<Id> updateCases_AS = new List<Id>();
        List<Id> updateCases_Re = new List<Id>();
        List<Id> updateCases_CanItemApproval = new List<Id>();
        
        
        system.debug('*** newMap ***'+newMap);
        
        for(Case eachCase:newMap.values()){
            
            system.debug('*** eachCase Values ***'+eachCase);
            
            //*** Completion of FCR***//
            if(eachCase.Origin == 'EMAIL' && eachCase.Email_Responded__c==TRUE){
                if(eachCase.Type != NULL && eachCase.Sub_Category__c !=NULL){
                    updateCases_EmailFCR.add(eachCase.ID);
                }
            }
            
            //*** Should come from Constants Class***
            if(eachCase.Type=='After sales' && eachCase.Sub_Category__c =='Product Complaint'){
                if(eachCase.After_Sales_Status__c != NULL && eachCase.After_Sales_Status__c != oldMap.get(eachCase.id).After_Sales_Status__c){
                    updateCases_AS.add(eachCase.ID);
                }
            }
            
            if(eachCase.Status != NULL && eachCase.Status != oldMap.get(eachCase.id).Status && eachCase.Status =='Closed'){
                updateCases_Re.add(eachCase.ID);
            }

            if(eachCase.Cancellation_Status__c!= NULL && eachCase.Cancellation_Status__c!= oldMap.get(eachCase.id).Cancellation_Status__c && eachCase.Cancellation_Status__c!='Cancellation - On Hold'){
                updateCases_CanItemApproval.add(eachCase.ID);
            }
            
        }
        
        system.debug('*** Cases to update ***'+updateCases_EmailFCR);
        system.debug('*** Cases to update ***'+updateCases_AS);
        
        if(!updateCases_EmailFCR.isEmpty()){
            milestone_RespondToEmailCompletion(updateCases_EmailFCR);
            
        }
        if(!updateCases_AS.isEmpty()){
            milestone_CSResponseTAT(updateCases_AS);
        }
        if(!updateCases_Re.isEmpty()){
            milestone_CSResolutionTAT(updateCases_Re);
        }
        if(!updateCases_CanItemApproval.isEmpty()){
            milestone_CancellationItemApprovalTAT(updateCases_CanItemApproval);
        }
    }
    
    private static void milestone_RespondToEmailCompletion(List<ID> updateCases){
        SLA_milestoneUtils.completeMilestone(updateCases,'CS: Respond to Incoming Email From Customer',completionDate);
    }
    
    private static void milestone_CSResponseTAT(List<ID> updateCases){
        SLA_milestoneUtils.completeMilestone(updateCases,'CS:Response TAT',completionDate);
    }
    
    private static void milestone_CSResolutionTAT(List<ID> updateCases){
        SLA_milestoneUtils.completeMilestone(updateCases,'CS:Resolution TAT',completionDate);
    }

    private static void milestone_CancellationItemApprovalTAT(List<ID> updateCases){
        SLA_milestoneUtils.completeMilestone(updateCases,'Cancellation Item : Approval SLA',completionDate);
    }
    
    
}