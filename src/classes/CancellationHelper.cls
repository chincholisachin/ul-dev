//** Author : Sachin
global class CancellationHelper {
    
    public CancellationHelper(ApexPages.StandardController ctrl){

    }

    /** BEGIN -  Pre Page Processing Logic **/
    @RemoteAction
    global static OrderAndOrderItemWrap sendCancellationCategory(String orderCode,String categorySelected){
       //OrderAndOrderItemWrap finalWrap =  testMockResponse(orderCode);
       OrderAndOrderItemWrap finalWrap =  sendCanEligibilityCallout(orderCode,'');
       system.debug('*** Final Wrap Sent to Page***'+finalWrap);
       if(finalWrap!=null){
            return finalWrap;
       }
       return null;
    }
    
      //*** This method will be used for actual rollout**//
    public static OrderAndOrderItemWrap sendCanEligibilityCallout(String orderCode,String jsonString){
        HttpResponse response;
        try {
            //String endPointURL = 'https://stg-salesforce.urbanladder.com/v1/consumers.json';
             Map<String, Object> bodyMap = new Map<String, Object>();
            CalloutModel.params calloutParams = CalloutService.populateCalloutParams('Cancellation_Order', 'Wulverine Server');
            system.debug('*** Callout Params***'+calloutParams);
            String endPointURL = calloutParams.endPointUrl+'/'+orderCode+'/cancel_eligibility';
            String userName = calloutParams.userName;
            String password = calloutParams.password;
            Integer timeOut_x = calloutParams.timeOut*1000;
            if(orderCode!=null || orderCode!='--None--'){
                response = CalloutService.callOutToExternalSystem(userName, password, endPointUrl, timeOut_x, 'GET',jsonString);
            }
            System.debug('responseBody: '+response.getBody());
            if(response.getStatus() == 'Created' || response.getStatus() == 'OK' || response.getStatus() == 'Success') {
                System.debug('responseBody: '+response.getBody());
                bodyMap = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
                return processCanEliResp(orderCode,bodyMap);
            }
            else{
                CalloutService.generateServiceLog(jsonString, null, 'Cancellation Order', response, orderCode);
                return null;
            }
        }catch(Exception ex) {
            System.debug('Error::'+ex.getMessage());
            CalloutService.generateServiceLog(jsonString, ex, 'Cancellation Order', response, orderCode);
            return null;
            //throw new customException(ex.getLineNumber()+' '+ex.getMessage());
        }
    }

    public static OrderAndOrderItemWrap processCanEliResp(String orderCode,Map<String, Object> rawBodyMap){
                Map<String, Object> postRawbodyMap = new Map<String,Object>();
                Map<String, Object> bodyMap = new Map<String,Object>();
                system.debug('---rawBodyMap---'+rawBodyMap);
                system.debug('---bodyMap---Extrating Data'+rawBodyMap.get('data'));
                postRawbodyMap = (Map<String, Object>)rawBodyMap.get('data');
                bodyMap = (Map<String, Object>)postRawbodyMap.get('cancel_eligibility');
                String orderCodeResp = (String)bodyMap.get('order_code');
                String orderEligibilty =  (String)bodyMap.get('eligibility');
                List<Object> orderReasonList = (List<Object>)bodyMap.get('reason');

                system.debug('---orderCode---'+orderCode);
                system.debug('---orderEligibilty---'+orderEligibilty);
                system.debug('---orderReasonList---'+orderReasonList);
                
                OrderWrap ordWrap = new OrderWrap(orderCodeResp,orderEligibilty,orderReasonList);
                system.debug('***ordWrap***'+ordWrap);

                Map<String, Object> oiMap;
                
                oiMap = (Map<String, Object>)bodyMap.get('order_items');
                system.debug('---oiMap---'+oiMap);
                system.debug('---oiMap keyset()---'+oiMap.keyset());
                
                Map<String,Map<String, object>> oiToOiDetailsMap = new Map<String,Map<String, object>>();
                list<OrderItemWrap> oiWrapList = new list<OrderItemWrap>();

                for(String oiCode : oiMap.keyset()){
                  Map<String, object> tempOIMap =   (Map<String,Object>)oiMap.get(oiCode);
                 
                  system.debug('***tempOIMap***'+tempOIMap);
                  String OI_Eligibility = (String)tempOIMap.get('eligibility');
                  list<object> OI_Reason = (list<object>)tempOIMap.get('reason');
                  
                  string OI_Parent_sku = (String)tempOIMap.get('parent_sku');

                  system.debug('***oiCode***'+oiCode);
                  system.debug('***OI_Eligibility***'+OI_Eligibility);
                  system.debug('***OI_Reason***'+OI_Reason);
                  system.debug('***OI_Parent_sku***'+OI_Parent_sku);

                  OrderItemWrap oiWrap = new OrderItemWrap(oiCode,OI_Eligibility,OI_Reason,OI_Parent_sku);
                  oiWrapList.add(oiWrap);
                  oiToOiDetailsMap.put(oiCode,tempOIMap);
                }
                OrderAndOrderItemWrap ordAndOIWrap = new OrderAndOrderItemWrap(ordWrap,oiWrapList);
                
                OrderAndOrderItemWrap revisedordAndOIWrap;
                
                map<string,orderitem__x> mapofOrderItems;

                if(Test.isRunningTest()){
                  mapofOrderItems = new map<string,orderitem__x>{'988484-67239'=> new orderitem__x(code__c='988484-67239',ExternalID='R123'),'988484-67240-1'=> new orderitem__x(code__c='988484-67240-1',ExternalID='R1234')};
                }else if(!Test.isRunningTest()){
                  mapofOrderItems =  fetchOrderItemsByOrder(orderCode);
                }

                for(OrderItemWrap oiWrap:ordAndOIWrap.oiWrapList){
                 if(mapofOrderItems.containsKey(oiWrap.order_item_code)){
                            oiwrap.order_item_name = mapofOrderItems.get(oiWrap.order_item_code).item_name__c;
                            oiwrap.total_price = Decimal.valueOf(mapofOrderItems.get(oiWrap.order_item_code).total_price__c);
                            oiwrap.order_item_external_id = mapofOrderItems.get(oiWrap.order_item_code).ExternalID;
                        }
                    }

                 system.debug('*** Processing OrderAndOrderItemWrap***'+ordAndOIWrap);

                if(orderEligibilty == 'NOT_ELIGIBLE'){
                     revisedordAndOIWrap = makeOrderItemsInEligibleForCan(ordAndOIWrap);
                }else{
                    if(!oiMap.keyset().isEmpty()){
                        revisedordAndOIWrap = validate(orderCode,oiMap.keyset(),ordAndOIWrap);
                    }
                }
                //system.debug('---oiToOiDetailsMap---'+oiToOiDetailsMap);
                //system.debug(((Map<String, object>)oiToOiDetailsMap.values()[0]).get('eligibility'));
                system.debug('*** Final ordAndOIWrap Sent Back***'+revisedordAndOIWrap);
                
                return revisedordAndOIWrap;
    }
    
    //** Method to Make Order Items Ineligible**
    global static OrderAndOrderItemWrap makeOrderItemsInEligibleForCan(OrderAndOrderItemWrap ordAndOIWrap){
        system.debug('*** inside makeOrderItemsInEligibleForCan method***');
        for(OrderItemWrap oiWrap:ordAndOIWrap.oiWrapList){
            oiWrap.order_item_revised_eligibility = 'NOT_ELIGIBLE';
            oiWrap.order_item_revised_reason = 'Order is Not Eligible';
            oiWrap.order_item_reason.add('Order is Not Eligible');
        }
        system.debug('*** Return from makeOrderItemsInEligibleForCan***'+ordAndOIWrap);
        return ordAndOIWrap;
    }


    global static OrderAndOrderItemWrap validate(String orderCode,set<String> listOfOrderItems,OrderAndOrderItemWrap ordAndOIWrap){
        system.debug('*** Inside Validate Method***');
        list<Case> existingCaseList = fetchCasesByOrdandOrdItems(orderCode,listOfOrderItems);
        
        system.debug('***Validate - existingCaseList***'+existingCaseList);
        
        system.debug('*** Inside Validate Method + ordAndOIWrap***'+ordAndOIWrap);

            OrderWrap oWrap = ordAndOIWrap.ordWrap;
            for(OrderItemWrap oiWrap:ordAndOIWrap.oiWrapList){
                if(oWrap.order_eligibility =='REQUIRE_APPROVAL'){
                    if(oiWrap.order_item_eligibility == 'NOT_ELIGIBLE'){
                             oiWrap.order_item_revised_eligibility = 'NOT_ELIGIBLE';
                             oiWrap.ApprovalStatus = 'Rejected';
                    }
                    if(oiWrap.order_item_eligibility != 'NOT_ELIGIBLE'){
                            oiWrap.order_item_revised_eligibility = 'Needs Approval';
                             oiWrap.ApprovalStatus = 'Needs Approval';
                    }
                    if(!oWrap.order_reason.isEmpty()){
                        for(Object obj:oWrap.order_reason){
                            oiWrap.order_item_reason.add(obj);
                        }
                    }
                }
                else if(oiWrap.order_item_eligibility == 'NOT_ELIGIBLE'){
                    oiWrap.order_item_revised_eligibility = 'NOT_ELIGIBLE';
                    oiWrap.ApprovalStatus = 'Rejected';
                }else if(oiWrap.order_item_eligibility == 'REQUIRE_APPROVAL'){
                    oiWrap.order_item_revised_eligibility = 'Needs Approval';
                    oiWrap.ApprovalStatus = 'Needs Approval';
                }

                system.debug('*** EO Order and OI Re App'+ordAndOIWrap);

                if(!existingCaseList.isEmpty()){
                    for(Case eachCase:existingCaseList){
                        //if(eachCase.O_Code__c == orderCode && eachCase.OI_Code__c == oiWrap.order_item_code){
                        if(eachCase.OI_Code__c == oiWrap.order_item_code){
                          if(oiWrap.order_item_eligibility == 'NOT_ELIGIBLE'){
                             oiWrap.order_item_revised_eligibility = 'NOT_ELIGIBLE';
                             oiWrap.ApprovalStatus = 'Rejected';
                          }else{
                             oiWrap.order_item_revised_eligibility = 'Needs Approval';
                             oiWrap.ApprovalStatus = 'Needs Approval';
                          }  
                        oiWrap.order_item_revised_reason += 'Existing After Sales Case is Present'+'\n';
                        oiWrap.order_item_reason.add('Existing After Sales Case is Present: '+eachCase.caseNumber);
                        oiWrap.order_item_existingCaseNumber += eachCase.CaseNumber+'\n';
                        oiWrap.order_item_existingCaseSFID = eachCase.id; 
                        }
                    }
                }
            }
        system.debug('*** return from validate method***'+ordAndOIWrap);
        return validate_existingCancellationItems(orderCode,listOfOrderItems,ordAndOIWrap);
    }

    public static OrderAndOrderItemWrap validate_existingCancellationItems(String orderCode,set<String> listOfOrderItems,OrderAndOrderItemWrap ordAndOIWrap){
        system.debug('*** Inside validate_existingCancellationItems***');
        list<Cancellation_Item__c> cancellationItemList = new list<Cancellation_Item__c>();
        cancellationItemList = fetchCancellationItems(orderCode,listOfOrderItems);
        system.debug('***cancellationItemList***'+cancellationItemList);

        for(OrderItemWrap oiWrap:ordAndOIWrap.oiWrapList){
             if(!cancellationItemList.isEmpty()){
                for(Cancellation_Item__c eachCi :cancellationItemList){
                    if(eachCi.OI_Code__c == oiWrap.order_item_code && eachCi.O_Code__c==orderCode){
                            if((eachCi.Approval_Status__c == 'Needs Approval' && eachCI.Processing_status__c=='Not Processed')||((eachCi.Approval_Status__c == 'Approved' || eachCi.Approval_Status__c == 'Auto Approved') && (eachCI.Processing_status__c=='Cancellation Requested' || eachCI.Processing_status__c=='CANCEL_INITIATED'|| eachCI.Processing_status__c=='CANCELLED' || eachCI.Processing_status__c=='Not Processed'))){
                                oiWrap.order_item_revised_eligibility = 'NOT_ELIGIBLE';
                                oiWrap.order_item_revised_reason +='Existing Cancellation Item is Present';
                                oiWrap.order_item_reason.add('Existing Cancellation Item is Present: '+eachCi.Name);
                            }
                    }
                }
             }
        }
        system.debug('*** return from validate Cancellation method'+ordAndOIWrap);
        return ordAndOIWrap;
    }

    //** Helper Method to Fetch Cancellation Items
    global static list<Cancellation_Item__c> fetchCancellationItems(String orderCode,set<String> listOfOrderItems){
        list<Cancellation_Item__c> cancellationItemList = new list<Cancellation_Item__c>();
        try{
        cancellationItemList = [SELECT id,Name,O_Code__c,OI_Code__c,Approval_Status__c,Processing_status__c from Cancellation_Item__c where O_Code__c=:orderCode AND OI_Code__c IN:listOfOrderItems];
        }catch(Exception e){
            system.debug('***Exception in the Fetch Cancellation Items Method***'+e.getMessage());
        }
        if(!cancellationItemList.isEmpty()){
            return cancellationItemList;
        }else{
            return new list<Cancellation_Item__c>();
        }
    }
        //*** Helper Method to Fetch Cases By Order and List of Order Items and by Category
    global static list<Case> fetchCasesByOrdandOrdItems(String orderCode,set<String> listOfOrderItems){
        system.debug('*** Inside fetchCasesByOrdandOrdItems Method***');
        system.debug('*** orderCode***'+orderCode);
        system.debug('*** listOfOrderItems***'+listOfOrderItems); 
        list<Case> listOfCases = new list<Case>();
        list<String> caseCatList = new list<String>{'After sales','Cancellation'};
        list<string> tempSet = new list<string>(listOfOrderItems);
        string queryString;
        string closedStatus='closed';
        //queryString = 'Select id,CaseNumber,O_Code__c,OI_Code__c,Type,Status,Cancellation_Status__c from Case where O_Code__c=:orderCode AND OI_Code__c IN:tempSet AND Type IN:caseCatList AND status !=:closedStatus';
        queryString = 'Select id,CaseNumber,O_Code__c,OI_Code__c,Type,Status,Cancellation_Status__c from Case where OI_Code__c IN:tempSet AND Type IN:caseCatList AND status !=:closedStatus';


        system.debug('*** Final Query queryString***'+queryString);

        try{
         listOfCases  = Database.query(queryString);
        }
        catch(Exception qe){
            system.debug('****Exception in the fetchCasesByOrdandOrdItems***'+qe.getMessage());
        }
        system.debug('*** list of Cases Returned from fetchCasesByOrdandOrdItems***'+listOfCases);
         if(!listOfCases.isEmpty()){
             return listOfCases;
         }else{
             return new list<Case>();
         }

    }
    
    //** Helper Method to fetch Order Items by Order Code**
    global static map<string,orderitem__x> fetchOrderItemsByOrder(String orderCode){
         system.debug('*** Inside fetchOrderItemsByOrder Method***');
         list<orderitem__x> listOfOrderitems = new list<orderitem__x>();
         map<string,orderitem__x> mapOfOICodeToOI = new map<string,orderitem__x>();

         string queryString = 'SELECT ExternalId,code__c,discount_total__c,FC_facility__c,facility_id__c,id__c,item_name__c,lms_status_string__c,order_code__c,total_price__c,lms_status_text__c,sku__c from orderitem__x where order_code__c=:orderCode';
        
        try{
         listOfOrderitems  = Database.query(queryString);
         for(orderitem__x oi:listOfOrderitems){
             mapOfOICodeToOI.put(oi.code__c,oi);
         }
        }
        catch(Exception e){
            system.debug('****Exception in the fetchOrderItemsByOrder***'+e.getMessage());
        }
        system.debug('*** Map returned from fetchOrderItemsByOrder***'+mapOfOICodeToOI);
         if(!mapOfOICodeToOI.isEmpty()){
             return mapOfOICodeToOI;
         }else{
             return new map<string,orderitem__x>();
         }
    }
    /** END -  Pre Page Processing Logic **/


    /** BEGIN -  Post Page Processing Logic **/
        @RemoteAction
    global static Case saveCaseAndCancellationItems(Case caseInstance,OrderAndOrderItemWrap ordAndOIWrap){

        system.debug('***CaseFromPage***'+caseInstance);
        system.debug('***wrapFromPage***'+ordAndOIWrap);
		String cancellationItemCodes = '';
		Boolean caseContainsMTOItems = false; 
		String MTOItemCodes = '';
        ordAndOIWrap = validateSummedUpValueOfOitems(ordAndOIWrap);

        system.debug('***Final ordAndOIWrap for Insertion ***'+ordAndOIWrap);

        Cancellation_Item__c ciInstance;

        list<Cancellation_Item__c> canItemList = new list<Cancellation_Item__c>();
        list<Cancellation_Item__c> canItemList_Insert = new list<Cancellation_Item__c>();

        String ordCode= ordAndOIWrap.ordWrap.order_code;
         
        String ordEli= ordAndOIWrap.ordWrap.order_eligibility;
        
        object O_ReasonString='';
        for(object o_obj:ordAndOIWrap.ordWrap.order_reason){
            O_ReasonString +=o_obj+';';
        }


        for(OrderItemWrap oiWrap:ordAndOIWrap.oiWrapList){
            if(oiWrap.isSelected == TRUE){
            ciInstance = new Cancellation_Item__c();
            if(cancellationItemCodes==''){
            	cancellationItemCodes = oiWrap.order_item_code ;
            }else{
            	 cancellationItemCodes = cancellationItemCodes + ','+oiWrap.order_item_code ;
            }          
            ciInstance.O_Reason__c = (String)O_ReasonString;            
            ciInstance.O_Code__c = ordCode;
            ciInstance.Order__c = ordCode;
            ciInstance.O_Eligibility__c = ordEli;
            ciInstance.O_Reason__c = (String)O_ReasonString;
            ciInstance.OI_Code__c = oiWrap.order_item_code;
            ciInstance.Order_Item__c = oiWrap.order_item_external_id;
            ciInstance.OI_Eligibility__c = oiWrap.order_item_eligibility;
            ciInstance.OI_Existing_Case_Number__c = oiWrap.order_item_existingCaseNumber;
            ciInstance.OI_Existing_Case_SF_ID__c = oiWrap.order_item_existingCaseSFID;
            ciInstance.OI_IsSelected__c = oiWrap.isSelected;
            ciInstance.OI_Name__c = oiWrap.order_item_name;
            ciInstance.OI_Parent_SKU__c = oiWrap.order_item_parent_sku;           
            //For loop for this one
            string OI_ReasonString='';
            for(Object oi_obj:oiWrap.order_item_reason){
                OI_ReasonString+= oi_obj+';';
                String reason = (String)oi_obj ; 
	            if(reason.contains('MTO')){
	            	caseContainsMTOItems = true ; 
	            	if(MTOItemCodes==''){
	            		MTOItemCodes = oiWrap.order_item_code ;
	            	}else{
	            		MTOItemCodes = MTOItemCodes +' ; '+oiWrap.order_item_code ;
	            	}
	            }
            }
            ciInstance.OI_Reason__c = (String)OI_ReasonString;
            ciInstance.OI_Revised_Eligibility__c = oiWrap.order_item_revised_eligibility;
            ciInstance.OI_Total_Price__c = oiWrap.total_price;
            ciInstance.OI_Revised_Reason__c = oiWrap.order_item_revised_reason;
            ciInstance.Approval_Status__c = oiWrap.ApprovalStatus;
            ciInstance.Cancellation_Reason__c = caseInstance.Reason_For_Cancellation__c ;
            canItemList.add(ciInstance);
        }
    }
            try{
               if(!Test.isRunningTest()){
                  caseInstance =  PA_Utility.addOrderDetailsToCase(caseInstance,ordCode);
                  caseInstance.Cancellation_Status__c = 'Created' ;
                  caseInstance.Item_Codes__c = cancellationItemCodes ;
                  caseInstance.All_MTO_Item_codes__c = MTOItemCodes.length()>254 ? MTOItemCodes.subString(0,254) : MTOItemCodes ;
                  // SF-990 Change of MTO Items field to Long text area
                  caseInstance.All_MTO_Order_Item_codes__c = MTOItemCodes ;
                  if(caseContainsMTOItems==true){
                  	caseInstance.MTO_Items__c = true ; 
                  }
               }
                system.debug('---Case after taking Order bath---'+caseInstance);
                Database.UpsertResult sr = Database.upsert(caseInstance);
                String caseID;
            if(sr.isSuccess()){
                caseID = sr.getID(); 
                if(!canItemList.isEmpty()){
                   for(Cancellation_Item__c eachCI : canItemList){
                    eachCI.Case__c = caseID;
                    if(caseInstance.Reason_For_Cancellation__c == 'COD CONFIRMATION FAILED'){
                    	eachCI.Approval_Status__c = 'Auto Approved';	
                    }
                    //eachCI.Approval_Status__c = 'Needs Approval'; // This is added to test the insertion logic
                    canItemList_Insert.add(eachCI);
                } 
                }
                insert canItemList_Insert;
            }
           return [select id,caseNumber from Case where id =: caseID];
        }catch(Exception e){
            system.debug('*** Unable to Insert Case***'+e.getMessage());
        }
        return new Case();
    }
    
    //** Method to Validate Summed Up OI's > 30,000
    public static OrderAndOrderItemWrap validateSummedUpValueOfOitems(OrderAndOrderItemWrap ordAndOIWrap){
        Decimal summedOItemValue;
        summedOItemValue = 0.0;
        for(OrderItemWrap oiWrap:ordAndOIWrap.oiWrapList){
            if(oiWrap.isSelected==true && oiWrap.total_price!=null ){
                system.debug('***oiWrap.total_price***'+oiWrap.total_price);
                summedOItemValue +=oiWrap.total_price;
            }
        }
        if(summedOItemValue>30000){
            for(OrderItemWrap oiWrap:ordAndOIWrap.oiWrapList){
                if(oiWrap.isSelected==true){
                    oiWrap.ApprovalStatus = 'NEEDS APPROVAL';
                    oiWrap.order_item_revised_reason += 'OI Sum Up Value More than 30,000'+'\n';
                    oiWrap.order_item_reason.add('OI Sum Up Value More than 30,000');
                    system.debug('*** Eligibility Modified Post Selection***');
                    oiWrap.order_item_revised_eligibility = 'NEEDS APPROVAL';
                }  
            }
        }
        ordAndOIWrap = validateBundle(ordAndOIWrap);
        return ordAndOIWrap;
    }


    //** Method to Validate the Bundled SKU's
    public static OrderAndOrderItemWrap validateBundle(OrderAndOrderItemWrap ordAndOIWrap){
        system.debug('*** Inside Validate Bundle Method****');
        system.debug('*** ordAndOIWrap****'+ordAndOIWrap);

        map<String,set<String>> skuToOIWrap_Actuals = new map<String,set<String>>();
        map<String,set<String>> skuToOIWrap_Selected = new map<String,set<String>>();
    
        for(OrderItemWrap oiWrap:ordAndOIWrap.oiWrapList){

            system.debug('*** Looks like oiWrap.order_item_parent_sku is not null***'+oiWrap.order_item_parent_sku);

            if(oiWrap.order_item_parent_sku!=null){
                system.debug('*** Inside IF condition of not null ***'+oiWrap.order_item_parent_sku);
                if(!skuToOIWrap_Actuals.containsKey(oiWrap.order_item_parent_sku)){
                    skuToOIWrap_Actuals.put(oiWrap.order_item_parent_sku,new set<String>{oiWrap.order_item_code});
                }else{
                    skuToOIWrap_Actuals.get(oiWrap.order_item_parent_sku).add(oiWrap.order_item_code);
                }
            }
            if(oiWrap.isSelected == true){
              if(oiWrap.order_item_parent_sku!=null){
                if(!skuToOIWrap_Selected.containsKey(oiWrap.order_item_parent_sku)){
                    skuToOIWrap_Selected.put(oiWrap.order_item_parent_sku,new set<String>{oiWrap.order_item_code});
                }else{
                    skuToOIWrap_Selected.get(oiWrap.order_item_parent_sku).add(oiWrap.order_item_code);
                }
            }  
            }
        }

        system.debug('*** skuToOIWrap_Actuals***'+skuToOIWrap_Actuals);
        system.debug('*** skuToOIWrap_Selected***'+skuToOIWrap_Selected);
        
        system.debug('*** Processing Final - ordAndOIWrap.oiWrapList***'+ordAndOIWrap.oiWrapList);

        for(OrderItemWrap oiWrap:ordAndOIWrap.oiWrapList){
            if(oiWrap.order_item_parent_sku!=null && oiWrap.isSelected == true){
            if(skuToOIWrap_Actuals.containsKey(oiWrap.order_item_parent_sku) && skuToOIWrap_Selected.containsKey(oiWrap.order_item_parent_sku)){
                if(skuToOIWrap_Actuals.get(oiWrap.order_item_parent_sku).size()!=skuToOIWrap_Selected.get(oiWrap.order_item_parent_sku).size() && oiWrap.isSelected == true){
                    oiWrap.ApprovalStatus = 'Needs Approval';
                    String tempString=''; 
                    for(String oiCode:skuToOIWrap_Actuals.get(oiWrap.order_item_parent_sku)){
                        if(!skuToOIWrap_Selected.get(oiWrap.order_item_parent_sku).contains(oiCode)){
                            tempString += oiCode+';';
                        }
                    }
                    oiWrap.order_item_revised_reason += 'Bundled SKU Not Present:'+tempString.removeEnd(';')+'\n';
                    oiWrap.order_item_reason.add('Bundled SKU Not Present for:'+tempString);
                    system.debug('*** Eligibility Modified Post Selection***');
                    oiWrap.order_item_revised_eligibility = 'NEEDS APPROVAL';
                }/*else if(skuToOIWrap_Actuals.get(oiWrap.order_item_parent_sku).size()==skuToOIWrap_Selected.get(oiWrap.order_item_parent_sku).size()){
                  oiWrap.ApprovalStatus = 'Auto Approved';
                  oiWrap.order_item_revised_reason +='Eligibility Modified,Since All Bundle Items are Selected'+'\n';
                  oiWrap.order_item_reason.add('Eligibility Modified,Since All Bundle Items are Selected');
                  system.debug('*** Eligibility Modified - Since All Bundle Items are Selected***');
                  oiWrap.order_item_revised_eligibility = 'Auto Approved';
                }*/
            }
        } 
    }
        return ordAndOIWrap;
    }
    /** END -  Post Page Processing Logic **/

    global class OrderWrap{

        public string order_code;
        public string order_eligibility;
        public list<object> order_reason;

        global OrderWrap(String order_code,string order_eligibility,list<object> order_reason){
            this.order_code = order_code;
            this.order_eligibility = order_eligibility;
            this.order_reason = order_reason;
        }
    }

    global class OrderItemWrap{
        public string order_item_code;
        public string order_item_eligibility;
        public list<object> order_item_reason;
        public string order_item_parent_sku;
        
        //** SFDC Revised Status and Revised Reason - If any
        public string order_item_revised_eligibility;
        public string order_item_revised_reason;
        public string order_item_existingCaseNumber;
        public string order_item_existingCaseSFID;

        //** OData Related Data Wrapped Up
        public string order_item_name;
        public Decimal total_price;
        public string order_item_external_id;

        //** Boolean to Select
        public Boolean isSelected;

        //** Approval Related Attributes
        public String ApprovalStatus;

        global OrderItemWrap(string order_item_code,string order_item_eligibility,list<object> order_item_reason,string order_item_parent_sku){
            this.order_item_code = order_item_code;
            this.order_item_eligibility = order_item_eligibility; // Original Eligibility Received in Response.
            this.order_item_reason = order_item_reason;
            this.order_item_parent_sku = order_item_parent_sku;
            this.order_item_revised_eligibility =order_item_eligibility; //Revised Eligibility Post SF Check.
            this.order_item_revised_reason='';
            this.order_item_existingCaseNumber = '';
            this.order_item_existingCaseSFID = '';
            this.order_item_name = '';
            this.total_price=0.0;
            this.isSelected = FALSE;
            this.Approvalstatus = 'Auto Approved';
            this.order_item_external_id = '';
        }
    }

    global class OrderAndOrderItemWrap{
        public OrderWrap ordWrap;
        public list<OrderItemWrap> oiWrapList;

        global OrderAndOrderItemWrap(OrderWrap ordWrap,list<OrderItemWrap> oiWrapList){
            this.ordWrap = ordWrap;
            this.oiWrapList = oiWrapList;
        }
    }
}