global class CaseCustomAction
{
    webservice static String updateCancellationStatus(String caseNumber)
    { 
        System.debug('Received approval request for Case: '+ caseNumber);
       
        list<String> casesToApprove = new list<String>{caseNumber}; // Please add case numbers here.
        
        List<Cancellation_Item__c> canItem = [SELECT ID From Cancellation_Item__c WHERE Case__r.CaseNumber IN:casesToApprove AND Approval_Status__c = 'Needs Approval'];
        
        if(canItem.size()== 0) {
            return 'NO_ITEM_TO_APPROVE';
        }

        System.debug('Processing approval request for '+ canItem.size() + ' pending approval items under case');
        
        Set<id> clID = new Set<ID>();
        Set<id> proin = new Set<ID>();
        
        for (Cancellation_Item__c c :canItem){
            clID.add(c.id);
        }
        
        list<ProcessInstance> listProcess = new List<ProcessInstance>();
        listProcess = [SELECT Id, Status, TargetObjectId FROM ProcessInstance WHERE TargetObjectId IN: clID and Status = 'Pending' ];
        
        system.debug(listProcess.size() + ' - listProcess');
        
        for(ProcessInstance tempins : listProcess){
            proin.add(tempins.id);
        }
        
        list<ProcessInstanceWorkitem> processwork  = new list<ProcessInstanceWorkitem>();
        processwork = [SELECT Id,ProcessInstanceId FROM ProcessInstanceWorkitem where ProcessInstanceId IN: proin ];
        
        system.debug(processwork.size() + ' - process work size');
        
        List<Approval.ProcessWorkitemRequest> listappReq = new List<Approval.ProcessWorkitemRequest>();
        
        for(ProcessInstanceWorkitem tempr : processwork){
            Approval.ProcessWorkitemRequest appReq = new Approval.ProcessWorkitemRequest();
            appReq.setComments('Approve');
            appReq.setAction('Approve');
            appReq.setWorkitemId(tempr.Id);
            listappReq.add(appReq);
        }
        
        list<Approval.ProcessResult> result2 =  Approval.process(listappReq);
        
        system.debug('Approval processing results: ' + result2);
        
        return 'SUCCESS';
    }
}