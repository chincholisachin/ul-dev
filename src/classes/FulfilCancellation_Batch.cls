global class FulfilCancellation_Batch implements Database.Batchable<sObject> {
    global Database.QueryLocator start(Database.BatchableContext BC) {
        
        String query = 'SELECT Name,Id, Json_body__c,Error_Occured__c,Processed__c FROM Fulfil_Cancellation__c where Processed__c= false';
        return Database.getQueryLocator(query);
    }
   
    global void execute(Database.BatchableContext BC, List<sObject> scope) {
            CaseFulfilParser cinstance                                  = new CaseFulfilParser();
            CaseFulfilParser.cls_Data data                              = new CaseFulfilParser.cls_Data();
            List<Case> caseToInsert                                     = new List<Case>();
            List<Cancellation_Item__c> CancellationItemToInsert         = new List<Cancellation_Item__c>();
            List<Cancellation_Item__c> CancellationItemToInsertFinal    = new List<Cancellation_Item__c>();
            List<Contact> ContactList                                   = new List<Contact>();
            List<Cancellation_Item__c> CancellationItemToCheck          = new List<Cancellation_Item__c>();
            Map<String,ID> mapOfOrderIdAndCase                          = new Map<String,ID>();
            Map<String,contact> mapOfCustomerIdAndContactId             = new Map<String,Contact>();
            Set<Integer> setOfCustomerID                                = new Set<Integer>();
            Set<String> setOfOIItems                                    = new Set<String>();
            list<Fulfil_Cancellation__c> updateStaging                  = new list<Fulfil_Cancellation__c>();
            case caseins ;
            Cancellation_Item__c canitemins ;
           
            for(sObject temp : scope){
            Fulfil_Cancellation__c sps =(Fulfil_Cancellation__c)temp;
            try{
                
                system.debug(sps.Json_body__c+'JSON');
                
                cinstance= CaseFulfilParser.parse(String.valueOF(sps.Json_body__c));
                for(CaseFulfilParser.cls_Data tempcaseData : cinstance.Data ){
                    setOfCustomerID.add(Integer.valueOf(tempcaseData.Customer_Id));
                    //for(CaseFulfilParser.cls_Cancellation_items_Data tempcanItem : tempcaseData .Cancellation_items_Data){
                        
                    //}
                }
            }catch(Exception e){
                sps.Processed__c = false;
                sps.Error_Occured__c = e.getMessage();
                update sps;
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
                String[] toAddresses = new String[] {'akshat.apurva@etmarlabs.com'}; 
                mail.setToAddresses(toAddresses);  
                mail.setSubject('Error Occured during Processing' + sps.Name); 
                String str =  System.URL.getSalesforceBaseURL().toExternalForm()+'/'+sps.Id;
                mail.setPlainTextBody('Hi,'+' \r\n'+ 'Error Occured during Processing ' + sps.Name +' \r\n'+ 'Please check the link for more info.'+ ' \r\n' +str );  
                
                
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });  
              
              }
           }
           
            if(setOfCustomerID.size() > 0){
                for(Integer custStr : setOfCustomerID){
                    ContactList = [SELECT id,Customer_ID__c from Contact WHERE Customer_ID__c IN:setOfCustomerID ];
                }
            }
            system.debug(setOfCustomerID+'setOfCustomerID');
            system.debug(ContactList+'ContactList');
            if(ContactList.size() > 0){
                for(Contact tempcon: ContactList){
                    mapOfCustomerIdAndContactId.put(String.valueOF(tempcon.Customer_ID__c),tempcon);
                }
            }
            system.debug(mapOfCustomerIdAndContactId+'mapOfCustomerIdAndContactId');
            for(sObject temp : scope){
                try{
                Fulfil_Cancellation__c sps =(Fulfil_Cancellation__c)temp;
                cinstance= CaseFulfilParser.parse(sps.Json_body__c);
                system.debug(cinstance+'SSSS');
                for(CaseFulfilParser.cls_Data caseData : cinstance.Data ){
                    system.debug(caseData.Origin+'Origin');
                    if(mapOfCustomerIdAndContactId.containsKey(caseData.Customer_Id)){
                        caseins = new case(Refund_Status__c = '' ,Reason = caseData.Reason_For_Cancellation,Subject = 'Cancellation Requested on Delivery',Description = 'Cancellation Requested on Delivery',Cancellation_Status__c = 'Cancelled',Origin= 'Delivery App',Type=caseData.Type,Sub_Category__c=caseData.Sub_Category,Order__c=caseData.Order,O_Code__c = caseData.Order ,O_PaymentMethod__c=caseData.Payment_Method,O_FacilityID__c=caseData.O_FacilityID,ContactId =mapOfCustomerIdAndContactId.get(caseData.Customer_Id).id);
                    }else{
                        caseins = new case(Refund_Status__c = '', Reason = caseData.Reason_For_Cancellation, Subject = 'Cancellation Requested on Delivery',Description = 'Cancellation Requested on Delivery',Cancellation_Status__c = 'Cancelled',Origin= 'Delivery App',Type=caseData.Type,Sub_Category__c=caseData.Sub_Category,Order__c=caseData.Order,O_Code__c = caseData.Order ,O_PaymentMethod__c=caseData.Payment_Method,O_FacilityID__c=caseData.O_FacilityID);
                    }

                    setOfOIItems                                    = new Set<String>();
                    CancellationItemToCheck                         = new List<Cancellation_Item__c>();
                    for(CaseFulfilParser.cls_Cancellation_items_Data canItem : caseData.Cancellation_items_Data){
                        canitemins = new Cancellation_Item__c(Order_Item__c = canItem.OI_ExternalID ,O_Code__c = caseData.Order ,OI_Code__c=canItem.OI_Code, Approval_Status__c='Auto Approved',Order__c=caseData.Order,OI_Name__c=canItem.OI_Name,Cancellation_Reason__c=canItem.OI_Reason,OI_Total_Price__c=Decimal.ValueOF(canItem.OI_Total_Price),delivery_date__c=canItem.OI_Delivery_Date);
                        
                        CancellationItemToCheck.add(canitemins);
                        system.debug(canItem.OI_Code+'OI_Code');
                        setOfOIItems.add(canItem.OI_Code);
                    }
                    list<Cancellation_Item__c> cancellationItemList = new list<Cancellation_Item__c>();
                    case caseInst = new case ();
                    system.debug(caseins+'inmethode');
                    system.debug(setOfOIItems+'setOfOIItems');
                    cancellationItemList = CaseFulfilCancellationDuplicateCheck.CaseDuplicateCheckMathod(caseins,setOfOIItems,CancellationItemToCheck);
                    system.debug(cancellationItemList+'cancellationItemList');
                    List<Case> dc_opsCaseAssignement = new List<Case>();
                    system.debug(cancellationItemList+'cancellationItemList');
                    CancellationItemToInsert.addAll(cancellationItemList);
                    system.debug(CancellationItemToInsert+'CancellationItemToInsert');
                    system.debug(caseData.Parent_Order+'%%%%%%');
                    if(caseData.Parent_Order != null){
                        system.debug('Parent order');
                        caseins.Pickup_Status__c = 'Pickup Pending';
                        dc_opsCaseAssignement.add(caseins);
                        List<Case> dc_opsCaseAssignementReturnFromMethod = new List<Case>();
                        dc_opsCaseAssignementReturnFromMethod = CaseService.changeOwnerDCOpsQueue(dc_opsCaseAssignement);
                        caseToInsert.addAll(dc_opsCaseAssignementReturnFromMethod);
                    }else{
                        system.debug('Original order');
                        caseInst = CaseAssignmentQueue.CaseAssignmentQueueMathod(caseins,setOfOIItems);
                        caseToInsert.add(caseInst);
                    }
                    
                }
                sps.Processed__c = true;
                updateStaging.add(sps);
                }catch(Exception e){
            
                }
            }
            system.debug(caseToInsert+'caseToInsert');
            if(caseToInsert.size() > 0){
                insert caseToInsert;
            }
            for(Case casein : caseToInsert){
                mapOfOrderIdAndCase.put(casein.Order__c,casein.id);
            }
            system.debug(CancellationItemToInsert.size() + 'dddddd');
            if(CancellationItemToInsert.size() > 0){
                for(Cancellation_Item__c tempcanin : CancellationItemToInsert){
                    if(mapOfOrderIdAndCase.containsKey(tempcanin.Order__c)){
                        tempcanin.Case__c = mapOfOrderIdAndCase.get(tempcanin.Order__c);
                    }
                    CancellationItemToInsertFinal.add(tempcanin);  
                }
            }
            system.debug(CancellationItemToInsertFinal+'CancellationItemToInsertFinal');
            if(CancellationItemToInsertFinal.size() > 0){
                insert CancellationItemToInsertFinal;
            }
            if(updateStaging.size() > 0){
                update updateStaging;
            }
        
        
    }   
    

    global void finish(Database.BatchableContext BC) {
            
    }
}