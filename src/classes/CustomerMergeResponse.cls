@isTest
global class CustomerMergeResponse implements HttpCalloutMock {
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        // Optionally, only send a mock response for a specific endpoint
        // and method.
        //System.assertEquals('http://api.salesforce.com/foo/bar', req.getEndpoint());
        //System.assertEquals('GET', req.getMethod());
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"status": "success","data": {"consumer": {"id": 278666, "email": "rachit.sharma774450@test.com","prefix": 0,"first_name": "tes7754","last_name": "test75544","created_at": "2015-12-15T05:19:37.000Z","updated_at": "2015-12-15T05:19:37.000Z","parent_consumer_id": null,"addresses": [{"id":"21","first_name": "1111"}, {"id":"22","first_name":"qqqqq"}],"consumer_emails": [{"id": 14781,"consumer_id": 278666,"email": "rachit.sharma774450@test.com","created_at": "2015-12-15T05:19:37.000Z","updated_at": "2015-12-15T05:19:37.000Z"},{"id": 14780,"consumer_id": 278668,"email": "rachit.sharma77450@test.com","created_at": "2015-12-15T05:18:30.000Z","updated_at": "2015-12-15T05:18:30.000Z"}],"consumer_phones": [{"id": 160049,"consumer_id": 278666,"phone": "9945322447","created_at": "2015-12-15T05:19:37.000Z","updated_at": "2015-12-15T05:19:37.000Z","consumer_raw_phone_id": 84}],"consumer_mappings": [],"searchable": true,"child_id": [278666,278668]}}}');
        res.setStatusCode(201);
        res.setStatus('OK');
        return res;
    }
}