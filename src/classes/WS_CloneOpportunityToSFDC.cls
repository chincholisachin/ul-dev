/**
* Class             : WS_CloneOpportunityToSFDC
* Created by        : ETMarlabs - Jiten
* Version           : 1.0  -- Initail Draft(26-02-2017)	
* Description       : This Web Service class will expose and contain all the logic for clonning an opportunity from external source.
*                   : Inline comments are added to get specific details.
**/

@RestResource(urlMapping='/WS_CloneOpportunityToSFDC/*')
global without sharing class WS_CloneOpportunityToSFDC {
	
    //Constants
    global static final String OPPORTUNITY_NOT_FOUND  = 'Opportunity not found in Salesforce !';
    global static final String INAPPROPRIATE_REQUEST_TO_PROCEED  = 'Inappropriate Request to Proceed !';
    global static final String OPPORTUNITY_CLONING_FAILED = 'Opportunity cloning failed !';
    global static final String OPPORTUNITY_CLONING_SUCCESSFUL = 'Opportunity successfully cloned !';
    global static final String OPPORTUNITY_START_STRING = '006';
    
    @HttpGet
    global static WS_cloneOpportunityResponse cloneOpportunity() {
    
        Opportunity oppObj ;
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        String oppId = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);

        try{
            
            if(oppId == null){
                CalloutService.generateServiceLog('Request - Opportunity ID is blank --> ', null, 'OPPORTUNITY_CLONING', null, null);
                return new WS_cloneOpportunityResponse(404,INAPPROPRIATE_REQUEST_TO_PROCEED,null);
            }
            else if(!oppId.startsWith(OPPORTUNITY_START_STRING)){
                CalloutService.generateServiceLog('Request - INAPPROPRIATE Id --> '+oppId , null, 'OPPORTUNITY_CLONING',null, oppId);
                return new WS_cloneOpportunityResponse(404,INAPPROPRIATE_REQUEST_TO_PROCEED,null);
            }
            else {
                
                oppObj = [Select id,Name from Opportunity where id =:oppId];
                if(oppObj == null){
                    CalloutService.generateServiceLog('OPPORTUNITY_NOT_FOUND --> ' +oppId , null, 'OPPORTUNITY_CLONING',null, oppId);
                    return new WS_cloneOpportunityResponse(404,OPPORTUNITY_NOT_FOUND,null);
                }
                else{
                    
                    sObject originalSObject = (sObject) oppObj;
                    List<sObject> originalSObjects = new List<sObject>{originalSObject};
                    List<sObject> clonedSObjects = Utility.cloneObjects(
                                                          originalSobjects,
                                                          originalSobject.getsObjectType());
                    
                    if(clonedSObjects.size() > 0){                                     
                        Opportunity clonedOpportunity = (Opportunity)clonedSObjects.get(0);
                        insert clonedOpportunity;
                        return new WS_cloneOpportunityResponse(200,OPPORTUNITY_CLONING_SUCCESSFUL,clonedOpportunity.id);
                    }
                    else{
                        CalloutService.generateServiceLog('OPPORTUNITY_CLONING_FAILED--> '+oppId, null, 'OPPORTUNITY_CLONING', null, oppId);
                        return new WS_cloneOpportunityResponse(500,OPPORTUNITY_CLONING_FAILED,null);
                    }
                }
            }
        }
        catch(Exception e){
            CalloutService.generateServiceLog('OPPORTUNITY_CLONING_FAILED --> '+oppId, e, 'OPPORTUNITY_CLONING', null, oppId);
            return new WS_cloneOpportunityResponse(500,OPPORTUNITY_CLONING_FAILED,null); 
        }
    }
    
    /*** This Wrapper class handles the data for response Result ***/ 
       
    global class WS_cloneOpportunityResponse{
        public Integer statusCode;
        public string responseMessage;
        public string clonedOppId ;
        public WS_cloneOpportunityResponse(Integer statusCode, String responseMessage,String clonedOppId){
            this.statusCode = statusCode;
            this.responseMessage = responseMessage;
            this.clonedOppId = clonedOppId;
    	}
    } 
    
}