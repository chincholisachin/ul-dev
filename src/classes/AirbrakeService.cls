public with sharing class AirbrakeService {
    
   
    public static void createExceptionOnAB(Exception e){
        
        UL_EndPoint_URLs__c uEU;
        UL_Wulverine_Credentials__c uWC;
        
        uWC = UL_Wulverine_Credentials__c.getValues('Airbrake');
        uEU = UL_EndPoint_URLs__c.getValues('Airbrake');
        
        String jsonString = JSON.serialize(new AirbrakeModel.AirbrakeRequestWrapper(e));
        
        if(System_Configs__c.getValues('Airbrake').Is_Run__c) {
            system.debug('---Raising Exception with Airbrake---');
            raiseException(uWC.User_Name__c, uWC.Password__c, uEU.End_point_URL__c,'POST',(Integer)uEU.Time_Out__c,jsonString);
        }
    }
    
   @Future(callout=true)
   public static void raiseException(String ProjectID,String ProjectKey,String endPointurl,String reqType,Integer timeOutValue,String jsonString){
        
        Httpresponse response = new  Httpresponse();
        Httprequest request = new HttpRequest();
        
        Http http = new Http();
        request.setMethod(reqType);
        request.setEndpoint(endPointUrl+'/'+ProjectID+'/notices?key='+ProjectKey);
        request.setHeader('Content-Type', 'application/json');
        request.setTimeout(timeOutValue*1000); // timeout in milliseconds
        
        system.debug('---jsonString---'+jsonString);   
        request.setBody(jsonString);
        
        system.debug('*** Request***'+request);
        
        try{
            response = http.send(request);
        }
        catch(Exception ex){
            system.debug('---Exception in Airbrake Callout---'+ex.getMessage());
        }
        system.debug('*** Response***'+response);
   } 
    
}