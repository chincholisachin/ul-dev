@RestResource(urlMapping='/WS_FulfilCancellationDataToSFDC/*')
global class WS_FulfilCancellationDataToSFDC{
    
    /* Creating the Fulfil Cancellation records with the Request Body in staging.*/
    @HttpPost
    global static void WS_PostOrderDataToSFDC() {
        
        RestRequest req = RestContext.request;   
        CaseFulfilParser c = new CaseFulfilParser();
        CaseFulfilParser.cls_Data data = new CaseFulfilParser.cls_Data(); 
        list<Case> caseToInsert = new list<Case>();
        list<Cancellation_Item__c> CancellationItemToInsert = new list<Cancellation_Item__c>();
        list<Cancellation_Item__c> CancellationItemToInsertFinal = new list<Cancellation_Item__c>();
        Map<String,ID> mapOfOrderIdAndCase = new Map<String,ID>();
        case caseins ;
        Cancellation_Item__c canitemins ;
        
        Fulfil_Cancellation__c fulfilCan = new Fulfil_Cancellation__c();
        fulfilCan.Json_body__c = req.requestBody.toString();
        fulfilCan.Processed__c = false;
        insert fulfilCan;
        /*
        c= CaseFulfilParser.parse(req.requestBody.toString());
        system.debug(c+'SSSS');
        for(CaseFulfilParser.cls_Data caseData : c.Data ){
            system.debug(caseData.Origin+'Origin');
            caseins = new case(Origin=caseData.Origin,Type=caseData.Type,Sub_Category__c=caseData.Sub_Category,Order__c=caseData.Order,O_PaymentMethod__c=caseData.Payment_Method,O_FacilityID__c=caseData.O_FacilityID,ContactId ='003p0000006DDax');
            caseToInsert.add(caseins);
            for(CaseFulfilParser.cls_Cancellation_items_Data canItem : caseData.Cancellation_items_Data){
                canitemins = new Cancellation_Item__c(Approval_Status__c='Auto Approved',Order__c=caseData.Order,Order_Item__c=canItem.OI_Code,OI_Name__c=canItem.OI_Name,OI_Reason__c=canItem.OI_Reason,OI_Total_Price__c=Decimal.ValueOF(canItem.OI_Total_Price),delivery_date__c=canItem.OI_Delivery_Date);
                CancellationItemToInsert.add(canitemins);
                system.debug(canItem.OI_Code+'OI_Code');
            }
        }
        system.debug(caseToInsert+'caseToInsert');
        if(caseToInsert.size() > 0){
            insert caseToInsert;
        }
        for(Case casein : caseToInsert){
            mapOfOrderIdAndCase.put(casein.Order__c,casein.id);
        }
        if(CancellationItemToInsert.size() > 0){
            for(Cancellation_Item__c tempcanin : CancellationItemToInsert){
                if(mapOfOrderIdAndCase.containsKey(tempcanin.Order__c)){
                    tempcanin.Case__c = mapOfOrderIdAndCase.get(tempcanin.Order__c);
                }
                CancellationItemToInsertFinal.add(tempcanin);  
            }
        }
        system.debug(CancellationItemToInsertFinal+'CancellationItemToInsertFinal');
        if(CancellationItemToInsertFinal.size() > 0){
            insert CancellationItemToInsertFinal;
        }
        */
     }
}