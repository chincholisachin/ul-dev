@isTest(SeeAllData=false)
private class Trigger_On_Event_Handler_Test {
    


    //Creat a lead
    //Update the Lead - with PVD
    //Get the Event for PVD
    //Update the PVD-Event status to 'Reschedule'
    //Assert the Lead Stage, PVD and Queue
    
    @isTest static void pvdPositive_test() {
        //Insert User
        User Newuser1 = InitializeTestdata.createUser1();
        insert Newuser1;
        
        //Creating a lead
        Lead lead1 = InitializeTestdata.createLead();
        insert lead1;

        //Create the Custom-Setting for LeadEventOwerShip
        Lead_Event_Owner__c leadEventOwerShip = InitializeTestdata.createLeadEventOwerShip(Newuser1.Id,Newuser1.Id);
        Insert leadEventOwerShip;
        
        //Update lead - Stage, Sub stage and Consultant  & All the site Address
        lead1 = InitializeTestdata.updateLeadsStageAndSubStage(lead1, Newuser1.Id);

        update lead1;
        
        //1 PVD Event
        Integer NO_EVENTS = [Select count() from Event where whoId =: lead1.id];
        system.assert((NO_EVENTS==1), 'Total PVD events');
        if(NO_EVENTS == 1){
            
            Event event = [Select Id, Status__c,Reason_for_Reschedule__c from event where WhoId =:lead1.Id];
            event.Status__c = 'Rescheduled';
            event.Reason_for_Visit_Reschedule__c='Customer not reachable';
            update event;            
        }

        //Check for the Lead's lead Sub-Stage, PVD 
        Lead lead2Check = [Select Id,Sub_Stage__c,Proposed_Visit_Date_1__c from Lead where Id =:lead1.id];
        //Sub-Stage
        system.assert((lead2Check.Sub_Stage__c == 'To Reschedule'), 'Sub-Stage has to be reshedule');
        //PVD
        system.assert((lead2Check.Proposed_Visit_Date_1__c == NULL), 'PVD has to be Blank');
        
    }

    @isTest static void eventTATPositive_test() {
        //Insert User
        User Newuser1 = InitializeTestdata.createUser1();
        insert Newuser1;
        
        //Creating a lead
        Lead lead1 = InitializeTestdata.createLead();
        insert lead1;
        
        //Update lead - Stage, Sub stage and Consultant  & All the site Address
        lead1 = InitializeTestdata.updateLeadsStageAndSubStage(lead1, Newuser1.Id);
       
        update lead1;
        
        //1 Update the TAT box
        Integer NO_EVENTS = [Select count() from Event where whoId =: lead1.id];
        system.assert((NO_EVENTS==1), 'Total PVD events');
        if(NO_EVENTS == 1){
            
            Event event = [Select Id,Is_Lead_Tat_Breached__c, Status__c from event where WhoId =:lead1.Id LIMIT 1];
            event.Is_Lead_Tat_Breached__c = TRUE;
            update event;        
        }
        
    }

    
}