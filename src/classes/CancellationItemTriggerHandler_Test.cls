@isTest
private class CancellationItemTriggerHandler_Test
{
    @isTest(seeAllData = true)
    static void testMethod1()
    {
        Case eachCase = new Case(Type = 'Cancellation', Sub_Category__c = 'Product Complaint',O_Code__c ='R321945713',OI_Code__c ='988484-67239');
        insert eachCase;

        Cancellation_Item__c eachCI = new Cancellation_Item__c(Case__c = eachCase.id,O_Code__c ='R321945713',OI_Code__c ='988484-67239',Processing_Status__c='CANCELLED',Approval_Status__c = 'Approved');
        insert eachCI;

        list<Cancellation_Item__c> newList = new list<Cancellation_Item__c>{eachCI};
        list<Cancellation_Item__c> oldList = new list<Cancellation_Item__c>{eachCI};
        map<ID,Cancellation_Item__c> newMap = new map<ID,Cancellation_Item__c>{eachCI.ID => eachCI};
        map<ID,Cancellation_Item__c> oldMap = new map<ID,Cancellation_Item__c>{eachCI.ID => eachCI};

        CancellationItemTriggerHandler c = new CancellationItemTriggerHandler();

        c.afterInsertMethod(newList,oldList,newMap,oldMap);

        c.afterUpdateMethod(newList,oldList,newMap,oldMap);




    }
}