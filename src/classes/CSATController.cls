//Author:   Vickal
//Dated:    March 03, 2016
//Purpose:  To get CSAT score and feedback.

Public class CSATController {
    
   public List<Case>          caseList            {get;set;}
   public List<CSAT_Score__c> csatList            {get;set;}   
   public string              feedback            {get; set;}
   
   private string csatVal {get; set;}
   private string caseid {get; set;}
    private CSAT_Score__c   cScore{get; set;}
        private case cs{get;set;}   
        
   public CSATController () {
       system.debug('***Constructor Called ***');
    }
   
   //*** Page Action Method***//
   public void saveCsatscores() {
   
        system.debug('***Page Action Method Called***');
        try{
        csatVal = ApexPages.CurrentPage().getparameters().get('cval');
        caseid = ApexPages.CurrentPage().getparameters().get('id');
        
        cScore = new CSAT_Score__c();
        cs = new case();
        caseList  = new list<Case>();
        
        system.debug('case id'+caseid);
        system.debug('case Value'+csatVal);
    
            //CSAT_Score__c cScore = new CSAT_Score__c();
            cScore.CSAT__c         = caseid;
            cScore.CSAT_Value__c   = csatVal;
            insert cScore;
            
                caseList = [select id, CSAT_Count__c from case where id=: caseid limit 1];
                
                if(csatVal == 'no'){
                    cs.id       = caseid;
                    cs.Status   = 'Re-Open';
                    cs.OwnerId  = [select queue.Id from queueSobject where queue.Name='CS Leads' limit 1].queue.Id;
                    update cs;
                }
                }catch(Exception e){
                    system.debug('***e.getMessage()***');
                }
    }
    
     //*** submit button methos to submit feedback against CSAT***//
    public PageReference submitFeedback(){
    try{
        string csid = ApexPages.CurrentPage().getparameters().get('id');
        string csatVal = ApexPages.CurrentPage().getparameters().get('cval');
        csatList = new List<CSAT_Score__c>();
        if(csid != null){
            try{
                csatList = [select id, CSAT_Value__c, CSAT__c from CSAT_Score__c where CSAT__c=: csid order by createddate DESC];
                csatList[0].feedback__c = feedback;
                update csatList[0];
            }catch(DMLException de){
            
            
            }
        }
        if(csatVal == 'yes'){
            PageReference mainHome= new PageReference('https://www.urbanladder.com/feedback-thankyou');
            mainHome.setRedirect(true);
                return mainHome;
        }else{
            PageReference mainHome= new PageReference('https://www.urbanladder.com/feedback-nexttime');
             mainHome.setRedirect(true);
                return mainHome;
        }
    }
    catch(Exception e){
         system.debug('***e.getMessage()***');
    }
    return null;
}
}