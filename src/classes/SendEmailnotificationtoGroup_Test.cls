@isTest
private class SendEmailnotificationtoGroup_Test {
    private static testMethod void method1() {
    	
    	//----- Inserting custom setting value for testing 
        System_Configs__c sysCustom = new System_Configs__c();
        sysCustom.Name = 'EmailMessage';
        sysCustom.Is_Run__c = true ;
        insert sysCustom ;
        
        Case cs = new Case();
        cs.Origin = 'Email';
        cs.Subject = 'Email Notification';
        cs.Description = 'To Group';
        cs.Status = 'Open';
        insert cs;
        EmailMessage em = new EmailMessage();
        em.Status = '1';
        em.BccAddress = 'upendar@gmail.com';
        em.CcAddress = 'test@test.com';
        em.TextBody = 'Hi How are you';
        em.FromAddress = 'upendar@yahoo.com';
        em.Incoming = True;
        em.ParentId = cs.Id;
        em.MessageDate = Date.today();
        insert em;
        cs.Email_Notification_to_Group__c = True;
        update cs;
    }
}