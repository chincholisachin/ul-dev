public with sharing class CaseTriggerHelper {
    
    private static CaseTriggerHelper instance;
    // public static Boolean isExecuted = false;
    
    //Singleton Pattern to fetch the instance
    public static CaseTriggerHelper getInstance(){
        if(instance == null){
            instance = new CaseTriggerHelper();     
        }
        return instance;    
    }
    public void beforeInsertCreateContact(list<Case> newCases){
        
        List<String> emailAddresses = new List<String>();
        //First exclude any cases where the contact is set
        for (Case caseObj:newCases) {
            
            if (caseObj.ContactId==null &&
                caseObj.SuppliedEmail!='')
            {
                emailAddresses.add(caseObj.SuppliedEmail);
            }
        }
        
        
        List<Contact> listContacts = new list<Contact>();
        listContacts  =  [Select Id,Email  From Contact Where Email in :emailAddresses];
        
        Set<String> takenEmails = new Set<String>();
        
        for (Contact c:listContacts) {
            takenEmails.add(c.Email);
        }
        
        Map<String,Contact> emailToContactMap = new Map<String,Contact>();
        List<Case> casesToUpdate = new List<Case>();
        
        for (Case caseObj:newCases){
            if (caseObj.ContactId==null && caseObj.SuppliedName!=null && caseObj.SuppliedEmail!=null && caseObj.SuppliedName!='' &&!caseObj.SuppliedName.contains('@') && caseObj.SuppliedEmail!='' &&!takenEmails.contains(caseObj.SuppliedEmail)){
                String[] nameParts = caseObj.SuppliedName.split(' ',2);
                if (nameParts.size() == 2)
                {
                    Contact cont = new Contact(FirstName=nameParts[0],
                                               LastName=nameParts[1],
                                               Email=caseObj.SuppliedEmail
                                               
                                              );
                    emailToContactMap.put(caseObj.SuppliedEmail,cont);
                    casesToUpdate.add(caseObj);
                }
            }           
        }
        
        List<Contact> newContacts = emailToContactMap.values();
        insert newContacts;
        
        system.debug('Upendar 0 '+newContacts );
        
        for(Contact c1 : listContacts) {
            if(c1.Email!=null) {
                
            }
        }
        update newContacts;
        
        
        
        for (Case caseObj:casesToUpdate) {
            Contact newContact = emailToContactMap.get(caseObj.SuppliedEmail);
            
            caseObj.ContactId = newContact.Id;
        }
        
    }
    
   /* public void ownerUpdate(list<Case> caseList) {
        
        map<String,ID> queueMap = new map<String,ID>();
        set<ID> queueIds = new set<ID>();
        set<String> regionSet = new set<String>();
        list<case> caseListWithRegions = new list<case>();
        if(RecursiveCaseTriggerHandler.run) {
            
            for(Case c : caseList) {
                if(c.Region__c !='' && c.Owner_Name__c != null && c.Owner_Name__c.StartsWithIgnoreCase('DC_OPS')) { //!queueIds.contains(c.OwnerId)&&
                    
                    regionSet.add('%'+c.Region__c+'%');
                    caseListWithRegions.add(c);
                                      
                }  
            }
            
            if(regionSet.size() > 0) {
                for(Group q: [select id, developername from Group where Type = 'Queue' and Name Like : regionSet]) {
                    queueMap.put(q.developername.toLowerCase(),q.id); //s1.toLowerCase()
                    
                }
            }
            
            for(Case c : caseListWithRegions) {
                
                if(queueMap.containsKey('DC_OPS_'.toLowerCase()+c.Region__c.toLowerCase())) {
                    
                    c.OwnerID = queueMap.get('DC_OPS_'.toLowerCase()+c.Region__c.toLowerCase());
                }           
            }
            
            
            
            if(caseListWithRegions.size() > 0) {
                try{
                    RecursiveCaseTriggerHandler.run = false;
                 
                }
                catch(Exception e) {
                    
                }
            }
            
        }
    }*/
}