public class CaseFulfilCancellationDuplicateCheck{

    public static list<Cancellation_Item__c> CaseDuplicateCheckMathod(case caseRecord,Set<String> oiItemList,List<Cancellation_Item__c> CancellationItemToCheck){
        list<Cancellation_Item__c> cancellationItemList = new list<Cancellation_Item__c>();
        list<Cancellation_Item__c> cancellationItemListUpdated = new list<Cancellation_Item__c>();
        list<Cancellation_Item__c> canitemListToUpdateApproval = new list<Cancellation_Item__c>();

        try{
            system.debug(caseRecord+'caseRecordDup');
            system.debug(oiItemList+'oiItemList');
            system.debug(CancellationItemToCheck+'CancellationItemToCheck');
        cancellationItemList = [SELECT id,Name,O_Code__c,OI_Code__c,Approval_Status__c,Processing_status__c,Case__r.CaseNumber from Cancellation_Item__c where O_Code__c=:caseRecord.O_Code__c AND OI_Code__c IN:oiItemList AND Case__r.Status != 'Closed' AND Case__r.Type = 'Cancellation'];

        system.debug(cancellationItemList+'cancellationItemList');

        if(cancellationItemList.size() > 0){
        	  for(Cancellation_Item__c temcanItemCheck : CancellationItemToCheck){
            	for(Cancellation_Item__c temcanItem : cancellationItemList){
                    if(temcanItem.O_Code__c == temcanItemCheck.O_Code__c && temcanItem.OI_Code__c == temcanItemCheck.OI_Code__c){
                        system.debug('Entered7655');
                        temcanItemCheck.Approval_Status__c = 'Auto Approved';
                        temcanItemCheck.OI_Reason__c = 'Case Already exist:'+temcanItem.Case__r.CaseNumber;
                        temcanItem.Approval_Status__c = 'Rejected';
                        if(temcanItem.OI_Reason__c == null || temcanItem.OI_Reason__c ==''){
                        	temcanItem.OI_Reason__c='\n Rejected - Fulfill Cancellation Case Present';
                        }else{
                        	temcanItem.OI_Reason__c+='\n Rejected - Fulfill Cancellation Case Present';
                        }
                        canitemListToUpdateApproval.add(temcanItem);
                    }
                }
                 cancellationItemListUpdated.add(temcanItemCheck);
            }     
        }
        system.debug('---canitemListToUpdateApproval---'+canitemListToUpdateApproval);
        system.debug('---cancellationItemListUpdated---'+cancellationItemListUpdated);
        
        if(!canitemListToUpdateApproval.isEmpty()){
            try{
                update canitemListToUpdateApproval;
                }catch(Exception e){
                    system.debug('---Exception---'+e.getMessage());
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
                    String[] toAddresses = new String[] {'akshat.apurva@etmarlabs.com','sachin.chincholi@etmarlabs.com'}; 
                    mail.setToAddresses(toAddresses);  
                    mail.setSubject('Error Occured during Processing:Case Fulfil Duplicate Check'); 
                    //String str =  System.URL.getSalesforceBaseURL().toExternalForm()+'/'+e.getDmlId();
                    mail.setPlainTextBody('Hi,'+' \r\n'+ 'Error Occured during Processing ' +e.getCause()+' \r\n'+ 'Please check the link for more info.'+ ' \r\n' +e.getMessage());

                    Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });  
                }
           processApprovalStatusChange(canitemListToUpdateApproval);
           system.debug('---canitemListToUpdateApproval---'+canitemListToUpdateApproval);
        }
        
        }catch(Exception e){
            system.debug('***Exception while checking the duplicate***'+e.getMessage());
        }
        system.debug(cancellationItemListUpdated+'cancellationItemListUpdated');
        if(cancellationItemListUpdated.size() > 0){
            system.debug('return updated');
            return cancellationItemListUpdated;
        }else{
            system.debug('return Original');
            return CancellationItemToCheck;
        }
       
    }

    public static void processApprovalStatusChange(list<Cancellation_Item__c> canItemList){


        Set<id> clID = new Set<ID>();
        Set<id> proin = new Set<ID>();
        for(Cancellation_Item__c c :canItemList){
            clID.add(c.id);    
        }

        list<ProcessInstance> listProcess = new List<ProcessInstance>();

        listProcess = [SELECT Id, Status,TargetObjectId FROM ProcessInstance WHERE TargetObjectId IN: clID and status='Pending'];
        system.debug(listProcess.size()+'listProcess');

        for(ProcessInstance tempins : listProcess){
            proin.add(tempins.id);
        }

            list<ProcessInstanceWorkitem> processwork  = new list<ProcessInstanceWorkitem>();
            processwork = [SELECT Id,ProcessInstanceId FROM ProcessInstanceWorkitem where ProcessInstanceId IN: proin ];
            system.debug(processwork.size()+'dff');

            List<Approval.ProcessWorkitemRequest> listappReq = new List<Approval.ProcessWorkitemRequest>();

            for(ProcessInstanceWorkitem tempr : processwork){
                Approval.ProcessWorkitemRequest appReq = new Approval.ProcessWorkitemRequest();
                appReq.setComments('Rejected - Fulfill Cancellation Case Present');
                appReq.setAction('Reject');
                appReq.setWorkitemId(tempr.Id);
                listappReq.add(appReq);
            }   
            list<Approval.ProcessResult> result2 =  Approval.process(listappReq);
            system.debug(result2+'result2');
    }
}