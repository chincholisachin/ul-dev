public class OrderReplacementExtension {
    
    public OrderReplacementExtension(ApexPages.StandardController controller) {
        Case caseIns;
        if(!Test.isRunningTest()) {
            controller.addFields(new List<String>{'Success_Message__c', 'Error_Message__c', 'Replacement_Order_Placed_with_UL__c'}); 
            
        } else {
            caseIns = [select Error_Message__c, Success_Message__c, Replacement_Order_Placed_with_UL__c from case where id=: controller.getId()];
        }
        caseIns = (Case)controller.getRecord();
        if(caseIns.Error_Message__c != '' && caseIns.Error_Message__c != null) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, caseIns.Error_Message__c));
        } else if(caseIns.Success_Message__c != '' && caseIns.Success_Message__c != null) {
            if(!caseIns.Replacement_Order_Placed_with_UL__c) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.INFO, caseIns.Success_Message__c));
            } else {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.CONFIRM, caseIns.Success_Message__c));
            }
        }
    }
}