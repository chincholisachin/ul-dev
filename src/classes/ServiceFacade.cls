public class ServiceFacade{
    
    public static void completeMilestone(List<Id> caseIds, String milestoneName, DateTime complDate){
        CaseMilestoneService.completeMilestone(caseIds,milestoneName,complDate);
    }
    
    public static list<Contact> fetchContactsByEmail(list<String> emailAddresses){
        return ContactService.fetchContactsByEmail(emailAddresses);
    }
    
    public static list<Contact> insertnewContactList(list<Contact> newContacts){
        return ContactService.insertnewContactList(newContacts);
    }
    
    public static Entitlement fetchEntitlementByName(String EntitleMentName){
        return EntitlementService.fetchEntitlementByName(EntitleMentName);
    }
    
    public static list<Account> createNewAccounts(list<Account> accountList){
        return AccountService.createNewAccounts(accountList);
    }
    
     public static list<Account> updateAccounts(list<Account> accountList){
        return AccountService.updateAccounts(accountList);
    }
    
    public static list<Contact> fetchContactByPhone(String phoneNumber){
        system.debug('*** Inside Service Facade :fetchContactByPhone ***'+phoneNumber);
        return ContactService.fetchContactByPhone(phoneNumber);
    }
    
    public static Case fetchLastUpdatedOpenCaseByContact(Contact con){
        return CaseService.fetchLastUpdatedOpenCaseByContact(con);
    }
    
    public static Case createNewCaseFromContact(Contact con){
        return CaseService.createNewCaseFromContact(con);
    }
    
    public static Contact createNewContact(String phoneNumber){
        return ContactService.createNewContact(phoneNumber);
    }
    
    public static list<task> updateAllTasks(String ContactID){
        return TaskService.updateAllTasks(ContactID);
    }
    
    public static Task createNewTaskFromCallWrapper(Contact con,Case ca,WS_UL_SendCallLogsToSFDC.DataFromCall callLogWrapper){
        return TaskService.createNewTaskFromCallWrapper(con,ca,callLogWrapper);
    }
    
    public static Case findBestMatchingCase_existingCustomer(String contactID,WS_UL_SendCallLogsToSFDC.DataFromCall callLogWrapper,List<Contact> allCon){
        return CaseService.findBestMatchingCase_existingCustomer(contactID,callLogWrapper,allCon);
    }
    
    //---- Added for Call Logs of Lead and Opportunity---//
    public static list<Lead> fetchLeadsByPhone(String phoneNumber){
        return LeadService.fetchOpenLeadsByPhone(phoneNumber);
    }
    
    public static Task createTasksforLeadCall(Contact con,Lead eachLead,WS_UL_SendCallLogsToSFDC.DataFromCall callLogWrapper){
        return TaskService.createTasksforLeadCall(con,eachLead,callLogWrapper);
    }
    public static Task createTasksforLeadCall_NotAnswered(Contact con,Lead eachLead,WS_UL_SendCallLogsToSFDC.DataFromCall callLogWrapper){
        return TaskService.createTasksforLeadCall_NotAnswered(con,eachLead,callLogWrapper);
    }
    
    
    public static list<Task> getTasksByLeadIDs(set<ID> setOfLeadIDs){
        return TaskService.getTasksByLeadIDs(setOfLeadIDs);
    }
    
    public static list<Task> updateTasksAsComplete(set<ID> setOfTaskIDs){
        return TaskService.updateTasksAsComplete(setOfTaskIDs);
    }
    
    //--- Added for the CNR Module ---//
    public static list<case> updateCNRCase(list<case> caseListToUpdate){
    	return CaseService.updateCNRCase(caseListToUpdate);
    }
    
    public static list<task> findCNRTasksOfCase(Contact con, Case eachCase){
     	return TaskService.findCNRTasksOfCase(con,eachCase);	
    }
    
    public static list<task> updateCNRTasksOfCase(ID contactID,Case bestMatchingCase,list<task> openCNRTaskList){
    	return TaskService.updateCNRTasksOfCase(contactID,bestMatchingCase,openCNRTaskList);
    }
    
    public static Task createCNRTasksOfCase(Contact con, Case eachCase, WS_UL_SendCallLogsToSFDC.DataFromCall callLogWrapper){
    	return TaskService.createCNRTasksOfCase(con,eachCase,callLogWrapper);
    }   
	
	 public static Task createNewTaskForContact(Contact con, WS_UL_SendCallLogsToSFDC.DataFromCall callLogWrapper){
    	return TaskService.createNewTaskForContact(con,callLogWrapper);
    }   
}