public  class Constants {
	//*Case Related Constants
	public static final string ORIGIN_EMAIL = 'EMAIL';
	public static final string ORIGIN_CALL = 'Call';
	public static final string TYPE_AFTERSALES = 'After sales';
	public static final string SC_PRODUCTCOMPLAINT='Product Complaint';
	public static final string STATUS_CLOSED = 'Closed';
	public static final string AS_ST_VISITSCHEDULED = 'Visit Scheduled';
	public static final string AS_ST_PRO_REP = 'Product Repaired Delivery Scheduled';
	public static final string CASE_TYPE_BFL = 'BFL';  // SF-932 
	public static final string CASE_BFL_STATUS_DO_RELEASED_AND_CONFIRMATION_PENDING = 'DO released & Confirmation pending';  // SF-932  
	public static final string CASE_BFL_STATUS_ORDER_CONFIRMATION_NOTIFICATION_FAILED = 'Order Confirmation Notification Failed';  // SF-932
	public static final string CASE_BFL_STATUS_ORDER_CONFIRMED_AND_DISBURSEMENT_PENDING = 'Order confirmed & Disbursement pending';  // SF-932  
	public static final string CASE_RECORD_TYPE_CUSTOM_PAGE = Label.CASE_RECORD_TYPE_CUSTOM_PAGE ;  // SF-1113 
    public static final string REASON_FOR_CANCELLATION_LONGER_DELIVERY_TIMELINES = 'LONGER DELIVERY TIMELINES';
	public static final string SUB_CATEGORY_USER_INITIATED_CANCELLATION = 'User Inititated Cancellation';
	//*Milestone Related Constants
	public static final string MS_Email = 'CS: Respond to Incoming Email From Customer';
	public static final string MS_RESP_TAT = 'CS:Response TAT';
	public static final string MS_RESO_TAT = 'CS:Resolution TAT';
	public static final string MS_CALL ='CS : Respond to Call';
	//*
	public static final string ENT_NAME = 'UL : CS Standard SLA/Entitlement';
	
	//* Recursion Handler
	public static Boolean AccountRecHandler =FALSE;
	
	//** BFL Module Related Constants
	public static final string BFL_STATUS_TO_DC = 'Yet to schedule a visit';
	
	/* Replacement Order Stagging constants */
    public static final string EVENT_TYPE_ORDER_UPDATED= 'ORDER_UPDATED';
    public static final string STATUS_PROCESSED_LMS_STATUS_NOT_MATCHING = 'Processed - LMS Status Not Matching';
	
}