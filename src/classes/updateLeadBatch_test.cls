@isTest
private class updateLeadBatch_test {
    private static testMethod void testone() {
	Lead leadInfo = new Lead(LastName = 'Test Lead',Company = 'UrbanLadder');
    insert leadInfo;
    leadInfo.State = 'Closed';
    leadInfo.Sub_Stage__c = 'Duplicate';
    leadInfo.Customer_ID__c+= Date.today();
    update leadInfo;
    System.Test.startTest();
    Database.executeBatch(new updateLeadBatchClass());
    System.Test.stopTest();
     
    }
}