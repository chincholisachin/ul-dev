@isTest
public class Test_CaseTriggertoCreateContact {
    
        public static Contact contactobj;
        public static Case caseobj;
        
   
    
    public static testmethod void testone() {
    
    contactobj = new contact();
    
        Test.startTest();
    
        contactobj.FirstName = 'Urban';
        contactobj.lastName = 'Ladder';
        contactobj.Email = 'testuser@urbanladder.com';
        insert contactobj;
        
        Account acc = new Account();
        acc.Name ='Test';
        insert acc;
        
        Entitlement ent = new Entitlement();
        
        ent.AccountID = acc.Id;
        ent.Name = 'UL : CS Standard SLA/Entitlement';
        insert ent;
        
        caseobj = UL_Utilities.createcase(contactobj.Id);
        caseobj.entitlementID = ent.id;
        insert caseobj;
        
        Test.stopTest();
                
    }
    

}