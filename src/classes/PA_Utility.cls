global class PA_Utility {
    
    public static Case addOrderAndOIDetailsToCase(Case caseToUpdate,String OrderCode,String OrderItemExternalID){
        
        
        
        Order__x orderRecord = new Order__x();
        orderitem__x orderItemRecord = new orderitem__x();
        try{
            orderRecord = [SELECT ExternalId,id__c,Code__c,DC_facility__c,discount_total__c,facility_id__c,Current_facility_Id__c,nps_score__c,payment_method__c,status_text__c,total__c,voucher_code__c,parent_order_id__c,channel_code__c from Order__x where ExternalId=:OrderCode LIMIT 1];
            
            for(orderitem__x ordI : [SELECT ExternalId, code__c,discount_total__c,FC_facility__c,facility_id__c,id__c,item_name__c,lms_status_string__c,order_code__c,total_price__c,lms_status_text__c,sku__c from orderitem__x where Code__c=:OrderItemExternalID AND order_code__c=:OrderCode]) {
                if(ordI.code__c == OrderItemExternalID) {
                    orderItemRecord = ordI;  
                }
            }
        }
        catch(Exception e){
            system.debug('***orderRecord***'+orderRecord);
            system.debug('***orderItemRecord***'+orderItemRecord);
            system.debug('*** inside PA_Utility***'+e.getMessage());
        }
        
        caseToUpdate.Order__c = OrderCode;
        caseToUpdate.Order_Item__c = orderItemRecord.ExternalId;
        //OrderItemExternalID+'-'+OrderCode;
        
        
        //Order Details Update
        caseToUpdate.O_DCFacility__c = orderRecord.DC_facility__c;
        caseToUpdate.O_Discount__c =  orderRecord.discount_total__c;
        caseToUpdate.O_FacilityID__c =  orderRecord.Current_facility_Id__c;
        caseToUpdate.o_npsscore__c =  orderRecord.nps_score__c;
        caseToUpdate.O_PaymentMethod__c = orderRecord.payment_method__c;
        caseToUpdate.O_Status__c = orderRecord.status_text__c;
        caseToUpdate.O_Total__c = orderRecord.total__c;
        caseToUpdate.O_Voucher_Code__c = orderRecord.voucher_code__c;
        caseToUpdate.O_ID__c =  orderRecord.id__c;
        caseToUpdate.O_Code__c =  orderRecord.Code__c;
        caseToUpdate.o_parent_order_id__c = orderRecord.parent_order_id__c;
        caseToUpdate.O_Channel_Name__c = orderRecord.channel_code__c ;
        
        //Order Item Details Update
        caseToUpdate.OI_Code__c = orderItemRecord.code__c;
        caseToUpdate.OI_Discount_Total__c =  orderItemRecord.discount_total__c;
        caseToUpdate.OI_FacilityID__c =  orderItemRecord.facility_id__c;
        caseToUpdate.OI_ID__c =  orderItemRecord.id__c;
        caseToUpdate.OI_ItemName__c = orderItemRecord.item_name__c;
        caseToUpdate.OI_LMS_Status_String__c = orderItemRecord.lms_status_string__c;
        caseToUpdate.OI_TotalPrice__c = orderItemRecord.total_price__c;
        caseToUpdate.OI_Status__c = orderItemRecord.lms_status_text__c;
        caseToUpdate.OI_OrderCode__c = orderItemRecord.order_code__c;
        caseToUpdate.OI_FC_Facility__c = orderItemRecord.FC_facility__c;
        caseToUpdate.OI_SKU__c= orderItemRecord.sku__c;
        
        //Added on 11/02/2016
        try{
        if(orderRecord.Current_facility_Id__c != null){
            String regionName = Regions__c.getValues(string.valueOF(orderRecord.Current_facility_Id__c)).region__c;
            caseToUpdate.DC_Ops_Facility_Region_Name__c = regionName;
        }
        }catch(Exception e){
            system.debug('*** Exception in assigning the Region Name on Publisher Action***'+e.getMessage());
        }
        
        return caseToUpdate;
    }

    //** This method has been added for the Main Cancellation Flow**//
    public static Case addOrderDetailsToCase(Case caseToUpdate,String OrderCode){
        
        system.debug('---'+caseToUpdate+'---'+OrderCode);
         
         Order__x orderRecord = new Order__x();

        try{
            orderRecord = [SELECT ExternalId,id__c,Code__c,DC_facility__c,discount_total__c,channel_code__c,facility_id__c,Current_facility_Id__c,nps_score__c,payment_method__c,status_text__c,total__c,voucher_code__c,parent_order_id__c from Order__x where ExternalId=:OrderCode LIMIT 1];

        if(orderRecord!=null){
            caseToUpdate.Order__c = OrderCode;
            //Order Details Update
            caseToUpdate.O_DCFacility__c = orderRecord.DC_facility__c;
            caseToUpdate.O_Discount__c =  orderRecord.discount_total__c;
            caseToUpdate.O_FacilityID__c =  orderRecord.Current_facility_Id__c;
            caseToUpdate.o_npsscore__c =  orderRecord.nps_score__c;
            caseToUpdate.O_PaymentMethod__c = orderRecord.payment_method__c;
            caseToUpdate.O_Status__c = orderRecord.status_text__c;
            caseToUpdate.O_Total__c = orderRecord.total__c;
            caseToUpdate.O_Voucher_Code__c = orderRecord.voucher_code__c;
            caseToUpdate.O_ID__c =  orderRecord.id__c;
            caseToUpdate.O_Code__c =  orderRecord.Code__c;
            caseToUpdate.o_parent_order_id__c = orderRecord.parent_order_id__c;
            caseToUpdate.O_Channel_Name__c = orderRecord.channel_code__c ;
         }
        }
        catch(Exception e){
            system.debug('***Exception in addOrderDetails Method***'+e.getMessage());
        }

        return caseToUpdate;
    }   
}