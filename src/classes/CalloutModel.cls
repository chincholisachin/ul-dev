public class CalloutModel {
    
    /* Merge Customer Wrapper */
    public class MergeRequestWrapper {
        
        public String id1;
        public String id2;
        public String primary;
        public string user;    
        
        public MergeRequestWrapper(string id1, string id2, string primary, string user) {
            
            this.id1 = id1;
            this.id2 = id2;
            this.primary = id1 == primary ? 'id1' : 'id2';
            this.user = user;
        }
    }
    
    public class MergeResponseWrapper {
        
        public String status;
        public Data data; 
    }
    
    public class Addresses {
        
        public String id;
        public String first_name;
        public String last_name;
        public String line1;
        public String line2;
        public String city;
        public String zipcode;
        public String state;
        public String address_type;
        public String country_code;
        public String phone;
        public String created_at;
        public String updated_at;
    }
    
    public class Consumer_phones {
        
        public String id;
        public String consumer_id;
        public String phone;
        public String created_at;
        public String updated_at;
        public String consumer_raw_phone_id;
        
        /*public Consumer_phones() {

}

public Consumer_phones(string id, string consumer_id, string phone, String created_at, String updated_at, String consumer_raw_phone_id) {

this.id                     = id;
this.consumer_id            = consumer_id;
this.phone                  = phone;
this.created_at             = created_at;
this.updated_at             = updated_at;
this.consumer_raw_phone_id  = consumer_raw_phone_id;
} */
    }
    
    public class Consumer {
        
        public String id;
        public String email;
        public String prefix;
        public String first_name;
        public String last_name;
        public String created_at;
        public String updated_at;
        public String parent_consumer_id;
        public List<Addresses> addresses;
        public List<Consumer_emails> consumer_emails;
        public List<Consumer_phones> consumer_phones;
        public List<Addresses> consumer_mappings;
        public Boolean searchable;
        public List<String> child_id;
    }
    
    public class Consumer_emails {
        public String id;
        public String consumer_id;
        public String email;
        public String created_at;
        public String updated_at;
        
        /*public Consumer_emails() {}

public Consumer_emails() {

}*/
    }
    
    public class Data {
        public Consumer consumer;
        
        /*public Data(Consumer consumer) {

this.consumer = consumer;
}*/
    }
    
    public class params {
        
        public String userName;
        public String password;
        public String endPointUrl;
        public Integer timeOut;
        
        public params(String userName, String password, String endPointUrl, Integer timeOut) {
            
            this.userName    = userName;
            this.password    = password;
            this.endPointUrl = endPointUrl;
            this.timeOut     = timeOut;
        }
    }
    
    public class orderReplacement {
        
        public String order_code;
        public String order_item_code;
        
        public orderReplacement(String order_code, String order_item_code) {
            this.order_code = order_code;
            this.order_item_code = order_item_code;
        }
    }
    
    // This is for the Order Cancellation
    public class orderCancellation {

    public List<String> item_codes;
    public String request_source;
    public String reason;
    public Datetime update_time;
    
        public orderCancellation(List<String> liStr_item_codes, String str_request_source,String str_reason,Datetime dt_update_time) {
            this.item_codes = liStr_item_codes;
            this.request_source = str_request_source;
            this.reason = str_reason;
            this.update_time = dt_update_time;
        }
    }
    
    public class OrderCreated {
        
        public String event_type; 
        public String event_id ; 
        public String version ; 
        public String event_datetime ; 
        public Event_data event_data;   
    }
    
    public class Order_items {
        public Integer id ; 
        public String code ; 
        public Integer order_id ; 
        public String sku ;
        public String total_price ; 
        public String selling_price ; 
        public String discount ;
        public String amount_due ; 
        public String promised_delivery_date ; 
        public String requested_delivery_date ; 
        public Integer status ; 
        public Integer source_facility_id ; 
        public String created_at ;
        public String updated_at ;
        public String delivery_planning_datetime ; 
        public String voucher_code ; 
        public Boolean pre_order ; 
        public string parent_sku ; 
        public String parent_sku_description; 
        public Boolean service_item ; 
        public String ul_special_instructions ; 
        public String lms_status_text ; 
        public String lms_status_string ; 
        public String item_name ; 
    }
    
    public class Event_data {
        public Integer id ; 
        public String code ; 
        public String number_Z ; // in json: number
        public String total ; 
        public String discount_total ; 
        public String placed_at ;
        public String voucher_code ;
        public Integer status ;
        public String consumer_id ; 
        public String created_at ; 
        public String updated_at ;
        public Integer facility_id ;
        public String agent_code ; 
        public String channel_code; 
        public String parent_order_id ; 
        public String nps_score ;
        public List<Order_items> order_items;
    }
    
    // Added this for Service Order Payment
    public class Service_Order_Payment{
    	public string status;
    	public string user_email;
    	public string reason;
    	
    	public Service_Order_Payment(String status,String email,String reason){
    		this.status = status;
    		this.user_email = email;
    		this.reason = reason;	
    	}
    }
    
     //----- Updating order service on all customer request status
    public class CancellationStatus{
    	 public String[] item_codes;
	 	 public String status;	//cancellation_requested	
	 	 public Datetime update_time;
	 	 
	 	 public CancellationStatus(String[] allItemCodes, String caseStatus,DateTime caseupdated){
	 	 	this.item_codes = allItemCodes ;
	 	 	this.status = caseStatus ; 
	 	 	this.update_time = caseupdated;
	 	 } 
    }
}