public class TaskService {
    
    public static Task createNewTaskFromCallWrapper(Contact con,Case ca,WS_UL_SendCallLogsToSFDC.DataFromCall callLogWrapper){
        
        List<Task> taskCheckList = new List<Task>();
        taskCheckList = [SELECT UCID__c from Task where whoID=:con.ID and UCID__c=:callLogWrapper.monitorUCID];
        system.debug('***taskCheckList***'+taskCheckList);
        
        if(taskCheckList.isEmpty()){
            Task taskToInsert = createTaskFromCallLogWrapper(callLogWrapper);
                taskToInsert.whoid = con.id;
                if(ca!=null){
                    taskToInsert.whatid = ca.id;   
                }
                try{
                     insert taskToInsert;
                      if(ca.Origin==null && ca != null){
                        ca.Origin='Call';
                        update ca;
                     }
                     return taskToInsert;
                }catch(DMLException de){
                    system.debug('*** Exception de***'+de.getMessage());
                }
                return taskToInsert;
        }   
        
                return null;
    }
    
    public static list<Task> updateAllTasks(String contactID){
        system.debug('*** inside updateAllTasksMethod***');
        
        list<Task> taskList = new list<Task>();
        list<Task> taskListToUpdate = new list<Task>();
        
        taskList = [SELECT id,status from Task where whoid=:contactID AND (CTI_Call_Log_Status__c='NotAnswered' OR CTI_Call_Log_Status__c='Failed') AND subject!='CNR-Call'];
        
        if(!taskList.isEmpty()){
            for(Task eachTask : taskList){
                eachTask.status='Complete';
                eachTask.CTI_Call_Log_Status__c='Called Back';
                taskListToUpdate.add(eachTask);
            }
        }
        try{
            update taskListToUpdate;
            return taskListToUpdate;
        }catch(Exception e){
            system.debug('***Exception e***'+e.getMessage());
        }
        system.debug('*** Updated Task List***'+taskListToUpdate);
        return taskListToUpdate;
    }
    
    //** Helper Method to Create Task from the call log wrapper **//
    public static Task createTaskFromCallLogWrapper(WS_UL_SendCallLogsToSFDC.DataFromCall callLogWrapper){
                Task taskInstance = new Task();
        
                taskInstance.Subject='Call';
                taskInstance.Priority='Normal';
                //taskInstance.RecordTypeID ='012p000000009i2';
                taskInstance.Status= 'Complete';
               
                taskInstance.CTI_Call_Log_Status__c = callLogWrapper.status;
                if(callLogWrapper.Type=='Manual'){
                    taskInstance.CallType='Outbound';
                }else{
                     taskInstance.CallType=callLogWrapper.Type;
                }
                taskInstance.CallObject = callLogWrapper.monitorUCID;
                taskInstance.CallDisposition = callLogWrapper.Disposition;
               
                taskInstance.Skill__c = callLogWrapper.Skill;
                taskInstance.UCID__c = callLogWrapper.monitorUCID;
                taskInstance.AudioFile__c = callLogWrapper.AudioFile;
                taskInstance.End_Time__c = DateTime.valueOf(callLogWrapper.EndTime);
                taskInstance.Start_Time__c = DateTime.valueOf(callLogWrapper.StartTime);
       
                taskInstance.CallerId__c = callLogWrapper.callerID;
                taskInstance.Location__c = callLogWrapper.location;
                taskInstance.Did__c = callLogWrapper.DID;
                taskInstance.UUI__c = callLogWrapper.UUI;
                taskInstance.Location__c = callLogWrapper.Location;
                taskInstance.PhoneName__c = callLogWrapper.PhoneName;
                //taskInstance.Time_To_Answer__c =  String.valueOf(callLogWrapper.TimeToAnswer);
                //taskInstance.Duration__c =  String.valueOf(callLogWrapper.Duration);
                taskInstance.FallbackRule__c = callLogWrapper.FallBackRule;
                taskInstance.Type__c = callLogWrapper.Type;
                taskInstance.AgentID__c = callLogWrapper.AgentID;
                taskInstance.AgentUniqueID__c = callLogWrapper.AgentUniqueID;
                taskInstance.Disposition__c = callLogWrapper.Disposition;
                taskInstance.HangupBy__c = callLogWrapper.HangupBy;  
                //taskInstance.Status= callLogWrapper.Status;
                taskInstance.TransferredTo__c = callLogWrapper.TransferredTo;
                taskInstance.TransferType__c = callLogWrapper.TransferType;             
                taskInstance.DailStatus__c = callLogWrapper.DialStatus;             
                taskInstance.Apikey__c = callLogWrapper.Apikey;
                taskInstance.Description =  callLogWrapper.Comments;  
                
                return taskInstance;
        
    }
    
    public static Task createTasksforLeadCall(Contact con,Lead eachLead, WS_UL_SendCallLogsToSFDC.DataFromCall callLogWrapper){
        system.debug('---Inside createTasksforLeadCall---');
        system.debug('---Contact---'+con);
        system.debug('---Lead---'+eachLead);
        system.debug('---callLogWrapper---'+callLogWrapper);
        
        List<Task> taskCheckList = new List<Task>();
        if(eachLead!=null){
            system.debug('---eachLead is not null---'+eachLead);
            //taskCheckList = [SELECT UCID__c from Task where whoID=:eachLead.ID and UCID__c=:callLogWrapper.monitorUCID];
            taskCheckList = [SELECT UCID__c from Task where UCID__c=:callLogWrapper.monitorUCID];
        }else{
            system.debug('---eachLead is null---'+eachLead);
            taskCheckList = [SELECT UCID__c from Task where whoID=:con.ID and UCID__c=:callLogWrapper.monitorUCID];
        }
        
        system.debug('***taskCheckList***'+taskCheckList);
        
        if(taskCheckList.isEmpty()){
            Task taskToInsert = createTaskFromCallLogWrapper(callLogWrapper);
               
                if(eachLead!=null && eachLead.isConverted ==false){
                    taskToInsert.whoid = eachLead.id;
                    taskToInsert.Subject='Lead - Call';
                    //taskToInsert.whatid = eachLead.id;   
                }
                else if(eachLead!=null && eachLead.isConverted && eachLead.convertedOpportunityID!=null){
                    taskToInsert.whatid = eachLead.convertedOpportunityID;
                    taskToInsert.whoid = con.id;
                    taskToInsert.Subject='Opportunity - Call';
                }else{
                    taskToInsert.whoid = con.id;
                    taskToInsert.Subject='Sales Ops - Call';
                }
                try{
                     insert taskToInsert;
                     return taskToInsert;
                }catch(DMLException de){
                    system.debug('*** Exception de***'+de.getMessage());
                }
        }
                return new task();
    }
    
    public static Task createTasksforLeadCall_NotAnswered(Contact con,Lead eachLead, WS_UL_SendCallLogsToSFDC.DataFromCall callLogWrapper){
        system.debug('---Inside createTasksforLeadCall_NotAnswered---');
        system.debug('---Contact---'+con);
        system.debug('---Lead---'+eachLead);
        system.debug('---callLogWrapper---'+callLogWrapper);
        List<Task> taskCheckList = new List<Task>();
    
            system.debug('---Each Lead is not null---'+eachLead);
            
            if(eachLead!=null && !eachLead.isConverted){
                system.debug('--Lead--');
                taskCheckList = [SELECT id from Task where ((Subject='Call customer to schedule visit - Lead' and WhoID=:eachLead.ID) and Status='Open')];
            }/*else if(eachLead!=null && eachLead.isConverted){
                system.debug('--Opportunity--');
                taskCheckList = [SELECT id from Task where ((Subject='Call customer to schedule visit - Opportunity' and WhoID=:eachLead.ConvertedOpportunityID) and Status='Open')];
            }else{
                system.debug('--Contact--');
                taskCheckList = [SELECT id from Task where ((Subject='Call customer to schedule visit' and WhoID=:con.ID) and Status='Open')];
            }*/
        system.debug('***taskCheckList***'+taskCheckList);
         if(taskCheckList.isEmpty()){
                Task taskToInsert = createTaskFromCallLogWrapper(callLogWrapper);
                if(eachLead!=null && eachLead.isConverted ==false && (eachLead.Sub_Stage__c =='Attempted to Contact' || eachLead.Sub_Stage__c =='To reshedule' || eachLead.Sub_Stage__c =='Yet to Contact')){
                    system.debug('---1Not Answered---');
                    taskToInsert.whoid = eachLead.id;
                    taskToInsert.OwnerID = eachLead.OwnerID;
                    taskToInsert.Subject='Call customer to schedule visit - Lead';
                    taskToInsert.Status = 'Open';
                    String day = Utility.getDayOfDate(Date.Today()+1);
                    if(day =='Sunday'){
                        taskToInsert.ActivityDate = Date.today()+2;
                    }else{
                        taskToInsert.ActivityDate = Date.today()+1;
                    }
                     
                }/*
                else if(eachLead!=null && eachLead.isConverted && eachLead.convertedOpportunityID!=null){
                    system.debug('---2Not Answered---');
                    taskToInsert.whatid = eachLead.convertedOpportunityID;
                    taskToInsert.whoid = con.id;
                    taskToInsert.OwnerID = eachLead.OwnerID;
                    taskToInsert.Status = 'Open';
                    String day = Utility.getDayOfDate(Date.Today()+1);
                    if(day =='Sunday'){
                        taskToInsert.ActivityDate = Date.today()+1;
                    }else{
                        taskToInsert.ActivityDate = Date.today();
                    }
                    taskToInsert.Subject='Call customer to schedule visit - Opportunity';
                }else{
                    system.debug('---3Not Answered---');
                    taskToInsert.whoid = con.id;
                    taskToInsert.Subject='Call customer to schedule visit';
                    taskToInsert.Status = 'Open';
                    String day = Utility.getDayOfDate(Date.Today());
                    if(day =='Sunday'){
                        taskToInsert.ActivityDate = Date.today()+1;
                    }else{
                        taskToInsert.ActivityDate = Date.today();
                    }
                }*/
                try{
                     insert taskToInsert;
                     return taskToInsert;
                }catch(DMLException de){
                    system.debug('*** Exception de***'+de.getMessage());
                }
            }
            return new task();
         }
         
       public static list<Task> getTasksByLeadIDs(set<ID> setOfLeadIDs){
            try{
                return [SELECT id,Status,Subject from Task where whoID IN:setOfLeadIDs];
            }catch(Exception e){
                system.debug('---Exception getTasksByLeadIDs--'+e.getMessage());
            }
            return new list<Task>();
       }
       
       public static list<Task> updateTasksAsComplete(set<ID> setOfTaskIDs){
        list<Task> taskListToUpdate = new list<Task>();
        try{
         for(Task eachTask : [SELECT id,Status from Task where ID IN:setOfTaskIDs]){
                eachTask.status = 'Complete';
                taskListToUpdate.add(eachTask);
         }
         
         if(!taskListToUpdate.isEmpty()){
            update taskListToUpdate;
            return taskListToUpdate;
         }
        }catch(Exception e){
            system.debug('---Exception updateTasksAsComplete---'+e.getMessage());
        }
         return new list<task>();       
       }
       
       //BEGIN----Added for CNR Module---//
       public static Task createCNRTasksOfCase(Contact con, Case eachCase, WS_UL_SendCallLogsToSFDC.DataFromCall callLogWrapper){
            system.debug('---Inside createCNRTasksOfCase---');
            system.debug('---Contact---'+con);
            system.debug('---eachCase---'+eachCase);
            system.debug('---callLogWrapper---'+callLogWrapper);
            List<Task> taskCheckList = new List<Task>();
            Task_Due_Date_Calculator__c taskCalcInstance;
            taskCheckList = [SELECT UCID__c from Task where UCID__c=:callLogWrapper.monitorUCID and subject= 'CNR-Call'];
            
            if(taskCheckList.isEmpty()){
                 Task taskToInsert = createTaskFromCallLogWrapper(callLogWrapper);
                 taskToInsert.whoid = con.id;
                 taskToInsert.Subject='CNR-Call';
                 taskToInsert.Status= 'Open';
                if(eachCase !=null){
                    taskToInsert.whatid = eachCase.id;
                    System.debug('case ownerId '+eachCase.OwnerId);
                     if(eachCase.OwnerId != null){
                        Schema.DescribeSObjectResult userKey = User.sObjectType.getDescribe();
                        String userKeyPrefix = userKey.getKeyPrefix();       
                        System.debug('User Key prefix '+userKeyPrefix);
                        String caseOwnerId = eachCase.OwnerId ;
                        if(caseOwnerId.startswith(userKeyPrefix)){
                            taskToInsert.OwnerId = eachCase.OwnerId ;
                        }                       
                    }    
                    if(eachCase.type!=null){
                        taskCalcInstance = Task_Due_Date_Calculator__c.getValues(eachCase.Type);
                    }
                    if(taskCalcInstance!=null){
                        Decimal noOfDaysForDueDate = taskCalcInstance.No_of_Days__c;
                        taskToInsert.ActivityDate = Date.today()+Integer.valueOf(noOfDaysForDueDate);  
                    }else{
                         taskToInsert.ActivityDate = Date.today()+1;  
                    }
                }
                insert taskToInsert;
                return taskToInsert;
            }
            return null;
    }
       
        public static list<Task> findCNRTasksOfCase(Contact con, Case eachCase){
        try{
        list<task> openCNRTaskList = [SELECT id,Status from Task where whoid=:con.id and status='Open' and subject='CNR-Call'];
        
        if(!openCNRTaskList.isEmpty()){
            return openCNRTaskList;
        }
        }catch(Exception e){
            //--Call Airbrake here--
            system.debug('---Exception e---'+e.getMessage());
            return null;
        }
        return null;
    }
    
    public static list<Task> updateCNRTasksOfCase(ID contactID,Case bestMatchingCase,list<task> openCNRTaskList){
        list<Task> taskListToUpdate = new list<Task>();
        try{
        for(Task eachTask:openCNRTaskList){
            eachTask.status = 'Complete';
            eachTask.CTI_Call_Log_Status__c='Reached Customer';
            taskListToUpdate.add(eachTask);
        }
        if(!taskListToUpdate.isEmpty()){
            update taskListToUpdate;
        }
        return taskListToUpdate;
        } catch(Exception e){
            //-- Call Airbrake Here--
            system.debug('---Exception in updateCNRTasksOfCase---'+e.getMessage());
            return null;
        }
        return null;
    }
    
    //------ Update CNR task for a contact
    public static list<Task> updateCNRTasksOfContact(list<task> openCNRTaskList){
        list<Task> taskListToUpdate = new list<Task>();
        try{
        for(Task eachTask:openCNRTaskList){
            eachTask.status = 'Complete';
            eachTask.CTI_Call_Log_Status__c='Reached Customer';
            taskListToUpdate.add(eachTask);
        }
        if(!taskListToUpdate.isEmpty()){
            update taskListToUpdate;
        }
        return taskListToUpdate;
        } catch(Exception e){
            //-- Call Airbrake Here--
            system.debug('---Exception in updateCNRTasksOfCase---'+e.getMessage());
            return null;
        }
        return null;
    }
	
	//------ Create Task for a contact when there are no open cases 
    public static Task createNewTaskForContact(Contact con,WS_UL_SendCallLogsToSFDC.DataFromCall callLogWrapper){
        
        List<Task> taskCheckList = new List<Task>();
        taskCheckList = [SELECT UCID__c from Task where whoID=:con.ID and UCID__c=:callLogWrapper.monitorUCID];
        system.debug('***taskCheckList***'+taskCheckList);
        
        if(taskCheckList.isEmpty()){
            Task taskToInsert = createTaskFromCallLogWrapper(callLogWrapper);
                taskToInsert.whoid = con.id;                
                try{
                     insert taskToInsert;                      
                     return taskToInsert;
                }catch(DMLException de){
                    system.debug('*** Exception de***'+de.getMessage());
                }
                return taskToInsert;
        }   
        
                return null;
    }
}