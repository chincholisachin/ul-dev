global class ReplacementOrderSchedulable implements Schedulable {

   global void execute(SchedulableContext sc) {
      ReplacementOrderBatch roBatch = new ReplacementOrderBatch(); 
      database.executebatch(roBatch);
   }
}