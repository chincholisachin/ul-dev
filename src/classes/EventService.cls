public with sharing class EventService {
    
    @future(callout=true)
    public static void processServiceOrderPaymentCallout(set<ID> eventsToProcess,set<ID> leadsToProcess){
        try{
        list<event> eventList = new list<event>();
        set<ID> leadIDs = new set<ID>();
        map<ID,Lead> leadMap = new map<ID,Lead>();
        
        
        for(Lead eachLead:[SELECT id,Service_Order_Number__c from Lead where ID IN :leadsToProcess]){
            leadMap.put(eachLead.ID,eachLead);
        }
        
        CalloutModel.params calloutParams = CalloutService.populateCalloutParams('Service Order Payment', 'Wulverine Server');
        String endPointURL = calloutParams.endPointUrl;
        String userName = calloutParams.userName;
        String password = calloutParams.password;
        Integer timeOut_x = calloutParams.timeOut*1000;
        String jsonString;
        httpresponse response = new httpresponse();
        httprequest request = new httprequest();
        Event eachEventToUpdate;
        
        map<ID,User> userIDtoUserDetailsMap;
        userIDtoUserDetailsMap = UserService.getUserIDToUserDetailsMap();
        
        for(Event eachEvent:[SELECT id,WhoID,Status__c,OwnerID from Event where ID IN:eventsToProcess]){
            if(leadMap.containsKey(eachEvent.whoID) && leadMap.get(eachEvent.whoID).Service_Order_Number__c != ''){
                /*if(leadMap.get(eachEvent.whoID).Service_Order_Number__c == ''){
                    //eachEvent.addError('Something');
                }else{*/
                      endPointURL+='/'+leadMap.get(eachEvent.whoID).Service_Order_Number__c+'/';
                //}
              
                //endPointURL ='http://private-9b2cc-orders26.apiary-mock.com/v1/orders/R912970015/'; // Apiary Testing
            }
            String email='';
            if(userIDtoUserDetailsMap.containsKey(eachEvent.OwnerID)){
                email = userIDtoUserDetailsMap.get(eachEvent.OwnerID).Email;
            }
            if(eachEvent.Status__c == 'Completed'){
                jsonString = JSON.serialize(new CalloutModel.Service_Order_Payment('COMPLETED',email,'OTHERS'));
            }else if(eachEvent.Status__c == 'Completed without payment' ||eachEvent.Status__c =='Cancelled'){
                jsonString = JSON.serialize(new CalloutModel.Service_Order_Payment('CANCEL',email,'OTHERS'));
            }
            system.debug('---endPointUrl---'+endPointUrl);
            response = CalloutService.callOutToExternalSystem(userName, password, endPointUrl, timeOut_x, 'PUT', jsonString);
            System.debug('responseBody: '+response.getBody());
            
            if(response.getStatus() == 'Created' || response.getStatus() == 'OK' || response.getStatus() == 'Success' || response.getStatusCode() == 200) {
                system.debug('*** Placed Service Order Payment 200 OK***');
                eachEventToUpdate = new Event(id=eachEvent.ID,Payment_Service_Status__c ='Processed to UL',Error_Message__c = '', Success_Message__c='Service Order Payment Update has been placed');
            }else{
                eachEventToUpdate = new Event(id=eachEvent.ID,Payment_Service_Status__c ='Processing Failed',Error_Message__c = response.getBody().left(255),Success_Message__c=' ');
                CalloutService.generateServiceLog(jsonString, null, 'Service Order', response, eachEvent.id);
            }
            eventList.add(eachEventToUpdate);
        }
        update eventList;
        }catch(Exception e) {
            system.debug('---Exception in Processing Service Order Callout---'+e.getMessage());
        }
    }
    
    
}