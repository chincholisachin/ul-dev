@isTest
public class OrderAdditionalDetailsControllerTest{

    private static testmethod void testone() {
        
        Regions__c eachReg = new Regions__c(Name='2',Region__c='Bangalore South');
        insert eachReg; 
        Order__x ord = new Order__x(ExternalID='R123456',Code__c = 'O8765433',facility_id__c=2,Current_facility_Id__c = 2);
        Database.insertAsync(ord);
        ApexPages.StandardController controller = new ApexPages.StandardController(ord);
        new OrderAdditionalDetailsController(controller);
    }
    
    private static testmethod void testtwo() {
 
        Order__x ord = new Order__x(ExternalID='R123456',Code__c = 'O8765433',facility_id__c=02,Current_facility_Id__c = 2);
        Database.insertAsync(ord);
        ApexPages.StandardController controller = new ApexPages.StandardController(ord);
        new OrderAdditionalDetailsController(controller);
    }
}