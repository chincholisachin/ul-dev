@isTest
public class EventServiceTest {
    
    private static testMethod void positiveTesting() {
    
        System_Configs__c texec = new System_Configs__c(Name='Service Order Payment', Is_Run__c = true);
        insert texec;
        
        UL_Wulverine_Credentials__c ul = new UL_Wulverine_Credentials__c(User_Name__c = 'salesforce@urbanladder.com', Password__c = 'VXJiYU5fRm9yY0U2NzM=', Name = 'Wulverine Server',Sandbox_Username__c = 'salesforce@urbanladder.com', Sandbox_Password__c	 = 'VXJiYU5fRm9yY0U2NzM=',Wulverine_Sandbox_Host__c='https://esgp3-wulverine.urbanladder.com',Wulverine_Production_Host__c='https://stg-salesforce.urbanladder.com');
        insert ul;
        UL_EndPoint_URLs__c ue = new UL_EndPoint_URLs__c(Name = 'Service Order Payment', End_point_URL__c = '/v1/Orders', Time_Out__c = 5);
        insert ue;
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new ServiceOrderInsertResponse());
        Test.startTest();
        Contact con = new Contact(lastName = 'Test Name', phone = '9848098481');
        insert con;
        
        Lead leadIns = new Lead(firstName='Test Name',lastName='Test Last Name',Phone='9848098481',Lead_Stage__c='Open',Company='ET'); 
        insert leadIns;
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'Consultant' LIMIT 1];
        
        
        User usr = new User(LastName = 'LIVESTONNewTestUser',
                           FirstName='JASONNewTestUser',
                           Alias = 'jliv',
                           Email = 'jason.liveston@asdfNewTestUser.com',
                           Username = 'jason.liveston@asdfNewTestUser.com2',
                           ProfileId = profileId.id,
                           TimeZoneSidKey = 'GMT',
                           LanguageLocaleKey = 'en_US',
                           EmailEncodingKey = 'UTF-8',
                           LocaleSidKey = 'en_US'
                           );
        insert usr;
        
        Event eveIns = new Event(Subject='Test Event',WhoId=leadIns.Id,OwnerID=usr.id,Status__c='Pending',DurationInMinutes=120,StartDateTime=Datetime.Now()+1);
        insert eveIns;
        eveIns.Status__c='Completed';
        update eveIns;
        Test.stopTest();
    } 
    
    
        private static testMethod void positiveTesting_1() {
    
        System_Configs__c texec = new System_Configs__c(Name='Service Order Payment', Is_Run__c = true);
        insert texec;
        
        UL_Wulverine_Credentials__c ul = new UL_Wulverine_Credentials__c(User_Name__c = 'salesforce@urbanladder.com', Password__c = 'VXJiYU5fRm9yY0U2NzM=', Name = 'Wulverine Server',Sandbox_Username__c = 'salesforce@urbanladder.com', Sandbox_Password__c	 = 'VXJiYU5fRm9yY0U2NzM=',Wulverine_Sandbox_Host__c='https://esgp3-wulverine.urbanladder.com',Wulverine_Production_Host__c='https://stg-salesforce.urbanladder.com');
        insert ul;
        UL_EndPoint_URLs__c ue = new UL_EndPoint_URLs__c(Name = 'Service Order Payment', End_point_URL__c = '/v1/Orders', Time_Out__c = 5);
        insert ue;
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new ServiceOrderInsertResponse());
        Test.startTest();
        Contact con = new Contact(lastName = 'Test Name', phone = '9848098481');
        insert con;
        
        Lead leadIns = new Lead(firstName='Test Name',lastName='Test Last Name',Phone='9848098481',Lead_Stage__c='Open',Company='ET'); 
        insert leadIns;
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'Consultant' LIMIT 1];
        
        
        User usr = new User(LastName = 'LIVESTON',
                           FirstName='JASON',
                           Alias = 'jliv',
                           Email = 'jason.liveston@asdf.com',
                           Username = 'jason.liveston@asdf-1.com2',
                           ProfileId = profileId.id,
                           TimeZoneSidKey = 'GMT',
                           LanguageLocaleKey = 'en_US',
                           EmailEncodingKey = 'UTF-8',
                           LocaleSidKey = 'en_US'
                           );
        insert usr;
        
        Event eveIns = new Event(Subject='Test Event',WhoId=leadIns.Id,OwnerID=usr.id,Status__c='Pending',DurationInMinutes=120,StartDateTime=Datetime.Now()+1);
        insert eveIns;
        eveIns.Status__c='Completed without payment';
        update eveIns;
        Test.stopTest();
    } 
    
    private static testMethod void negativeTesting() {
    
        System_Configs__c texec = new System_Configs__c(Name='Service Order Payment', Is_Run__c = true);
        insert texec;
        
        UL_Wulverine_Credentials__c ul = new UL_Wulverine_Credentials__c(User_Name__c = 'salesforce@urbanladder.com', Password__c = 'VXJiYU5fRm9yY0U2NzM=', Name = 'Wulverine Server',Sandbox_Username__c = 'salesforce@urbanladder.com', Sandbox_Password__c	 = 'VXJiYU5fRm9yY0U2NzM=',Wulverine_Sandbox_Host__c='https://esgp3-wulverine.urbanladder.com',Wulverine_Production_Host__c='https://stg-salesforce.urbanladder.com');
        insert ul;
        UL_EndPoint_URLs__c ue = new UL_EndPoint_URLs__c(Name = 'Service Order Payment', End_point_URL__c = '/v1/Orders', Time_Out__c = 5);
        insert ue;
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new ServiceOrderInsertResponse1());
        Test.startTest();
        Contact con = new Contact(lastName = 'Test Name', phone = '9848098481');
        insert con;
        
        Lead leadIns = new Lead(firstName='Test Name',lastName='Test Last Name',Phone='9848098481',Lead_Stage__c='Open',Company='ET'); 
        insert leadIns;
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'Consultant' LIMIT 1];
        
        
        User usr = new User(LastName = 'LIVESTONTestUser',
                           FirstName='JASON_TestUser',
                           Alias = 'jliv',
                           Email = 'jason.liveston@asdfTestUser.com',
                           Username = 'jason.liveston@asdfTestUser.com2',
                           ProfileId = profileId.id,
                           TimeZoneSidKey = 'GMT',
                           LanguageLocaleKey = 'en_US',
                           EmailEncodingKey = 'UTF-8',
                           LocaleSidKey = 'en_US'
                           );
        insert usr;
        
        Event eveIns = new Event(Subject='Test Event',WhoId=leadIns.Id,OwnerID=usr.id,Status__c='Pending',DurationInMinutes=120,StartDateTime=Datetime.Now()+1);
        insert eveIns;
        
        eveIns.Status__c='Completed';
        update eveIns;
        Test.stopTest();
        
    }
    
    private static testMethod void exceptionTesting() {
    
        System_Configs__c texec = new System_Configs__c(Name='Service Order Payment', Is_Run__c = true);
        insert texec;
        
        UL_Wulverine_Credentials__c ul = new UL_Wulverine_Credentials__c(User_Name__c = 'salesforce@urbanladder.com', Password__c = 'VXJiYU5fRm9yY0U2NzM=', Name = 'Wulverine Server',Sandbox_Username__c = 'salesforce@urbanladder.com', Sandbox_Password__c	 = 'VXJiYU5fRm9yY0U2NzM=',Wulverine_Sandbox_Host__c='https://esgp3-wulverine.urbanladder.com',Wulverine_Production_Host__c='https://stg-salesforce.urbanladder.com');
        insert ul;
        UL_EndPoint_URLs__c ue = new UL_EndPoint_URLs__c(Name = 'Service Order Payment', End_point_URL__c = '/v1/Orders', Time_Out__c = 5);
        insert ue;
        // Set mock callout class 
        //Test.setMock(HttpCalloutMock.class, new ServiceOrderInsertResponse1());
        Test.startTest();
        Contact con = new Contact(lastName = 'Test Name', phone = '9848098481');
        insert con;
        
        Lead leadIns = new Lead(firstName='Test Name',lastName='Test Last Name',Phone='9848098481',Lead_Stage__c='Open',Company='ET'); 
        insert leadIns;
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'Consultant' LIMIT 1];
        
        
        User usr = new User(LastName = 'LIVESTON',
                           FirstName='JASON',
                           Alias = 'jliv',
                           Email = 'jason.liveston@asdf.com',
                           Username = 'jason.liveston-3@asdf.com2',
                           ProfileId = profileId.id,
                           TimeZoneSidKey = 'GMT',
                           LanguageLocaleKey = 'en_US',
                           EmailEncodingKey = 'UTF-8',
                           LocaleSidKey = 'en_US'
                           );
        insert usr;
        
        Event eveIns = new Event(Subject='Test Event',WhoId=leadIns.Id,OwnerID=usr.id,Status__c='Pending',DurationInMinutes=120,StartDateTime=Datetime.Now()+1);
        insert eveIns;
        
        eveIns.Status__c='Completed';
        update eveIns;
        Test.stopTest();
        
    }
}