/* Author : Satyanarayana M
Description : Callout to send primary and secondary customer ids to the UL server for Merge operation 
and receives response with primary customer details.

*/
public class CustomerMergeCalloutService {
    
    /* Converting JSON data to wrapper format for Customer Merge */
    /*public static CalloutModel.MergeRequestWrapper parseCustomerMerge(String json) {
return (CalloutModel.MergeRequestWrapper) System.JSON.deserialize(json, CalloutModel.MergeRequestWrapper.class);
}*/
    
    /* Converting data to JSON format for Customer Merge Request.*/
    public static string createCustomerMergeJson(string primaryCustomerId, string secondaryId) {
        CalloutModel.MergeRequestWrapper mrWrap = new CalloutModel.MergeRequestWrapper(primaryCustomerId, secondaryId, primaryCustomerId, userInfo.getUserEmail());
        return JSON.serialize(mrWrap);
    }
    
    /* Converting JSON to wrapper */
    public static CalloutModel.MergeResponseWrapper parseCustomerMergeResponse(String json) {
        return (CalloutModel.MergeResponseWrapper) System.JSON.deserialize(json, CalloutModel.MergeResponseWrapper.class);
    }
    
    /* Callout from SF to the wulverine server(UL) for Merge Customers*/
    public static boolean customerMergeCallout(String recordId, string primaryCustomerId, string secondaryId) {
        
        boolean successfullyMerged = false;
        HttpResponse res = new HttpResponse();
        String customerJson;
        try {
            CalloutModel.params cusParams = CalloutService.populateCalloutParams('Customer_Merge_UL', 'Wulverine Server');
            
            customerJson = createCustomerMergeJson(primaryCustomerId, secondaryId);
            String endPointUrl = cusParams.endPointUrl;
            String userName = cusParams.userName;
            String password = cusParams.Password;
            Integer timeOut_x = cusParams.timeOut * 1000; //timeout in milli seconds.
            res = CalloutService.callOutToExternalSystem(userName, password, endPointUrl, timeOut_x, 'PUT', customerJson);
            system.debug(res.getBody());
            
            if(res.getStatus() == 'OK' || res.getStatus() == 'Success') { //&& res.getStatusCode() == 200
                CalloutModel.MergeResponseWrapper mergeResponse = parseCustomerMergeResponse(res.getBody());
                boolean isSucessResponse = parseResponseWrapperToObject(mergeResponse);
                if(isSucessResponse) { // Merge functionality of child object records to parent Customer.
                    CustomerMergeUtilityClass cusMerge = new CustomerMergeUtilityClass();
                    successfullyMerged = cusMerge.mergeSecondaryAccountWithPrimaryAccount(Integer.valueOf(primaryCustomerId), Integer.valueOf(secondaryId));
                }
            } else {
                system.debug(res);
                successfullyMerged = false;
                CalloutService.generateServiceLog(customerJson, null, 'Customer', res, recordId); 
                ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Error, res.getStatusCode() + ' : ' +res.getStatus()+' : '+res.getBody()));
            }
        } catch(Exception ex) {
            successfullyMerged = false;
            CalloutService.generateServiceLog(customerJson, ex, 'Customer', res, recordId); 
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Error, ex.getMessage()));
        }
        return successfullyMerged;
    } 
    
    /* Parse the received response from wulverine server  and update it to the primary Customer.*/
    public static boolean parseResponseWrapperToObject(CalloutModel.MergeResponseWrapper mergeResponse) {
        
        boolean flag = false;
        try {
            if(mergeResponse.status == 'success') {
                System.debug(mergeResponse);
                System.debug(mergeResponse.Data.consumer);
                CalloutModel.Consumer consumerData = mergeResponse.Data.consumer;
                List<CalloutModel.Addresses> addressList = new List<CalloutModel.Addresses>();
                //= consumerData.addresses;
                if(consumerData.addresses.size() >= 2) {
                    addressList.add(consumerData.addresses.get(0));
                    addressList.add(consumerData.addresses.get(1));
                } else {
                    addressList = consumerData.addresses;
                }
                //addressList = ((addressList.size() > 2) ? addressList[2] : addressList);
                Contact primaryContact = new Contact();
                String lastName = consumerData.last_name != null ? consumerData.last_name : null;
                String firstName = consumerData.first_name != null ? consumerData.first_name : null;
                String salutation = consumerData.prefix != null && consumerData.prefix != '0' ? consumerData.prefix : null;
                String customerId = consumerData.id != null ? consumerData.id : null;
                String email = consumerData.email != null ? consumerData.email : null;
                String billingAddress, shippingAddress, billingCity, shippingCity, billingState, shippingState, billingZipCode, shippingZipCode, primaryEmail, secondaryEmail, primaryPhone, secondaryPhone;
                
                if(addressList.size() > 0) {
                    billingAddress  =  addressList[0].line1 != null ? addressList[0].line1 : null;
                    billingAddress += addressList[0].line2 != null ? addressList[0].line2 : null;
                    billingCity = addressList[0].city != null ? addressList[0].city : null;
                    billingState = addressList[0].state != null ? addressList[0].state : null;
                    billingZipCode = addressList[0].zipcode != null ? addressList[0].zipcode : null;
                    if(addressList.size() == 2) {
                        shippingAddress = addressList[1].line1 != null ? addressList[1].line1 : null;
                        shippingAddress += addressList[1].line2 != null ? addressList[1].line2 : null;
                        shippingCity = addressList[1].city != null ? addressList[1].city : null;
                        shippingState = addressList[1].state != null ? addressList[1].state : null;
                        shippingZipCode = addressList[1].zipcode != null ? addressList[1].zipcode : null;
                    }
                }
                
                /*Parsing Emails in the response. */
                if(consumerData.consumer_emails.size() > 0) {
                    integer count=1;
                    for(calloutModel.consumer_emails ce : consumerData.consumer_emails) {
                        if(count == 1) {
                            primaryEmail = ce.email;
                        } else if(count == 2) {
                            secondaryEmail = ce.email;
                        }
                        count++;
                    }
                }
                
                /*Parsing Mobile numbers in the response. */
                if(consumerData.consumer_phones.size() > 0) {
                    integer count=1;
                    for(calloutModel.consumer_phones cp : consumerData.consumer_phones) {
                        if(count == 1) {
                            primaryPhone = cp.phone;
                        } else if(count == 2) {
                            secondaryPhone = cp.phone;
                        }
                        count++;
                    }
                }
                
                if(lastname != null)  primaryContact.put('lastName',lastName);
                if(firstName != null)  primaryContact.put('firstName', firstName);
                if(salutation != null)  primaryContact.put('Salutation', Salutation);
                if(customerId != null)  primaryContact.put('Customer_ID__c', Decimal.valueOf(customerId));
                if(email != null)  primaryContact.put('Email', email);
                if(billingAddress != null) primaryContact.put('MailingStreet', billingAddress);
                if(shippingAddress != null) primaryContact.put('OtherStreet', shippingAddress);
                if(billingCity != null) primaryContact.put('MailingCity', billingCity);
                if(shippingCity != null) primaryContact.put('OtherCity', shippingCity);
                if(billingState != null) primaryContact.put('MailingState', billingState);
                if(shippingState != null) primaryContact.put('OtherState', shippingState);
                if(billingZipcode != null) primaryContact.put('MailingPostalCode', billingZipcode);
                if(shippingZipcode != null) primaryContact.put('OtherPostalCode', shippingZipcode);
                if(primaryEmail != null) primaryContact.put('Email', primaryEmail);
                if(secondaryEmail != null) primaryContact.put('Secondary_Email__c', secondaryEmail);
                if(primaryPhone != null) primaryContact.put('MobilePhone', primaryPhone);
                if(secondaryPhone != null) primaryContact.put('OtherPhone', secondaryPhone);
                contactRecursion.fireTrigger = false; // To stop trigger from being executed.
                Database.UpsertResult upsertRes = database.upsert(primaryContact, Contact.Fields.Customer_ID__c);
                
                if(upsertRes.isSuccess()) {
                    flag = true;
                } else {
                    flag = false;
                    string errorMsg = '';
                    for(Database.Error err : upsertRes.getErrors()) {
                        errorMsg += err;
                    }
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, errorMsg));
                }
                //mergeSecondaryAccountWithPrimaryAccount
            }
        } catch(Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Line NO: '+e.getLineNumber() +' '+ e.getMessage()));
        }
        return flag;
    }
}