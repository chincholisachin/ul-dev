public without sharing class CaseMilestoneService{

    public static void completeMilestone(List<Id> caseIds, String milestoneName, DateTime complDate) {
      system.debug('*** completeMilestone Method Called ***');
    /* SF-934 --Cuasing ths issue while updating the case records -- when case Milestone is Violated and we try to update completionDate it throws the
    exception
    - Added try - catch to handle exceptional condition 
    */  
    List<CaseMilestone> cmsToUpdate = [select Id, completionDate from CaseMilestone cm where caseId in :caseIds and cm.MilestoneType.Name LIKE: milestoneName+'%' and completionDate = null and IsViolated = false and TargetDate > : System.now() limit 1];
    
    if (cmsToUpdate.isEmpty() == false){
      for (CaseMilestone cm : cmsToUpdate){
        cm.completionDate = complDate;
      }
      try{
        update cmsToUpdate;
        system.debug('*** updated cmsToUpdate ***'+cmsToUpdate);
      }
      catch(Exception ex){
        system.debug('*** Exception while updating case Milestone  Exception Line number -'+ex.getLineNumber()+' Exception -- '+ex);
        CalloutService.generateServiceLog(JSON.serialize('{"message":"Exeception in Case Milestone Update", "error" : "' + ex.getMessage() + '"," cmsToUpdate data" : "'+cmsToUpdate+'"}'), null, 'CASE_MILESTONE_UPDATE', null, caseIds[0]);
      }
      
    } // end if
  }

}