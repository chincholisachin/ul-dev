/* Author : Upendar Shivanadri
   Description : Converting the Lead - Custom */
global class LeadConvertService{
    public LeadConvertService(ApexPages.StandardController controller) {
        
    }
    
    @InvocableMethod
    public static void autoConvertLead(List<Lead> Leads) {
        for (Lead aLead : Leads) {
            LeadConvertService.customConvertLead(String.valueOf(aLead.Id), null);
        }
        LeadRecursion.fireTrigger = true;
    }
    
    webService static string customConvertLead(String LeadId, String ownerId) {
        
        Lead leadobj;
        string oppId = '';
        List<Opportunity> oppList = new List<Opportunity>();
        try{
            leadobj = [select id,Name,Remove_from_Workflow_Queue__c,Field_for_w_f__c,Contact__c,AccountId__c,IsConverted,Customer_ID__c,Products__c,UL_Lead_Date__c,ConvertedOpportunityId from Lead where id=: LeadId];
            
            if(!leadobj.IsConverted ) {
              
                Database.LeadConvert lc = new Database.LeadConvert();
                lc.setLeadId(leadobj.id);
                lc.setAccountId(leadobj.AccountId__c );
                lc.setContactId(leadobj.Contact__c);
                lc.setOpportunityName(leadobj.Name+' converted');
                
                /*
                //If ownerId is not null, then consultant will be the owner of the converted opp.
                if(ownerId != null){
                    system.debug('ownerId  ****'+ownerId);
                     lc.setOwnerId(leadobj.Consultant_Name__c);
                }
                */
                
                lc.setConvertedStatus([select MasterLabel from LeadStatus where isConverted = true].MasterLabel);
                Database.LeadConvertResult lcr = Database.convertLead(lc);
                system.debug('***** LeadConvertedResult'+lcr);
                system.debug('***** Lead Object****'+leadobj );
                Leadobj = [select convertedOpportunityId from Lead where id =: LeadId];
                oppId  = leadobj.ConvertedOpportunityId;
                system.debug('***** Converted Opportunity Id'+oppId  );
                
                if(lcr.isSuccess()) {
                    return oppId  ;
                } else {
                    string errMsg = '';
                    for(Database.Error err : lcr.getErrors()) {
                        errMsg += err.getMessage();
                    }
                    return errMsg;
                }
            } else {
                return 'Lead already Converted';
            }
        }catch(Exception e) {
            system.debug('**** Exception Name '+e.getMessage());
            return e.getMessage();
        }
        return null;
    }
}