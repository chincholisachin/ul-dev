/* ClassName : UserService
   Purpose   : All User Related Query and DML's will be done here.
   Author    : Sachin Chincholi
   Last Modified Reason :  
*/
public with sharing class UserService {
    
    private static map<String,User> userMap = new map<String,User>();
    private static map<ID,User> userIDtoUserDetailsMap = new map<ID,User>();
    
    private static boolean isUserCacheLoaded = false;
    private static boolean isUserIDtoUserDetailsCacheLoaded = false;
    
    //**Method to return userDetailsMap
    public static map<String,User> getUserDetailsMap(){
        if(isUserCacheLoaded){
            return userMap;
        }
        cacheUserInfo();
        system.debug('---userMap---'+userMap);
        return userMap;
    }
    
    
    public static map<ID,User> getUserIDToUserDetailsMap(){
        if(isUserIDtoUserDetailsCacheLoaded){
            return userIDtoUserDetailsMap;
        }
        cacheUserInfo();
        system.debug('---userIDtoUserDetailsMap---'+userIDtoUserDetailsMap);
        return userIDtoUserDetailsMap;
    }
    
    
    //**Method to cache the UserInfo in a transaction
    private static void cacheUserInfo(){
        try{
        for(User eachUser:[SELECT id,userName,Email,ProfileID from User]){
            userMap.put(eachUser.userName,eachUser);
            userIDtoUserDetailsMap.put(eachUser.ID,eachUser);
        }
        isUserCacheLoaded = true;
        isUserIDtoUserDetailsCacheLoaded = true;
        }catch(Exception e){
            system.debug('---Exception e---'+e.getMessage());
            system.debug('---Centralized Exception Handling---');
        }
    }
}