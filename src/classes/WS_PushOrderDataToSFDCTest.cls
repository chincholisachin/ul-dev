@isTest
private class WS_PushOrderDataToSFDCTest {

    static testMethod void WS_PostOrderDataToSFDCTest() {
         //do request
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/WS_PostOrderDataToSFDC/';  //2015-10-05 02:00:34
        req.requestBody = Blob.valueOf('{"event_type":"ORDER_ITEM_UPDATED","event_id":"3F746E937EAB347D38E4386JFO","version":"v1","event_datetime":"2015-10-05T00:00:00.000Z","event_data":{"order_items":[{"id":255825,"code":"1058094-R547774996","order_id":"R547774996-NEW","sku":"ACLTWL61BSY0001","total_price":"5299.0","selling_price":"5299.0","discount":"0.0","amount_due":"0.0","promised_delivery_date":"2015-12-02T00:00:00Z","requested_delivery_date":null,"status":1,"source_facility_id":3,"created_at":"2015-11-17T21:41:48.000Z","updated_at":"2015-11-17T21:41:48.000Z","delivery_planning_datetime":null,"voucher_code":null,"pre_order":false,"parent_sku":null,"parent_sku_description":null,"service_item":false,"ul_special_instructions":null,"lms_status_text":"FULFILLABLE","lms_status_string":"Item has been processed","item_name":"Sheren Extendable Wall Sconce (Colour : brass)"}]}}');
        
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        WS_PushOrderDataToSFDC.WS_PostOrderDataToSFDC();
    }
    
     static testMethod void WS_PostOrderDataToSFDCTest1() {
         //do request
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/WS_PostOrderDataToSFDC/';  //2015-10-05 02:00:34
         req.requestBody = Blob.valueOf('{"event_type":"ORDER_ITEM_UPDATED","event_id":"3F746E937EAB347D38E4386JFJ","version":"v1","event_datetime":"2015-10-06T00:00:00.000Z","event_data":{"order_items":[{"id":2554825,"code":"1058194-R547773996","order_id":"R547744996-NEW","sku":"ACLTWL61BSY0002","total_price":"5299.0","selling_price":"5299.0","discount":"0.0","amount_due":"0.0","promised_delivery_date":"2015-12-02T00:00:00Z","requested_delivery_date":null,"status":1,"source_facility_id":3,"created_at":"2015-11-17T21:41:48.000Z","updated_at":"2015-11-17T21:41:48.000Z","delivery_planning_datetime":null,"voucher_code":null,"pre_order":false,"parent_sku":null,"parent_sku_description":null,"service_item":false,"ul_special_instructions":null,"lms_status_text":"FULFILLABLE","lms_status_string":"Item has been processed","item_name":"Sheren Extendable Wall Sconce (Colour : brass)"},{"id":255845,"code":"1058094-R547784996","order_id":"R547774996-NEW","sku":"ACLTWL61BSY0001","total_price":"5299.0","selling_price":"5299.0","discount":"0.0","amount_due":"0.0","promised_delivery_date":"2015-12-02T00:00:00Z","requested_delivery_date":null,"status":1,"source_facility_id":3,"created_at":"2015-11-17T21:41:48.000Z","updated_at":"2015-11-17T21:41:48.000Z","delivery_planning_datetime":null,"voucher_code":null,"pre_order":false,"parent_sku":null,"parent_sku_description":null,"service_item":false,"ul_special_instructions":null,"lms_status_text":"FULFILLABLE","lms_status_string":"Item has been processed","item_name":"Sheren Extendable Wall Sconce (Colour : brass)"}]}}');
        
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        WS_PushOrderDataToSFDC.WS_PostOrderDataToSFDC();
    }
}