/* Callout to external system(Wulverine) on insert and update of Contact */
public class FetchCustomer{
    
    public FetchCustomer() {}     
    
    public static void sendNewCustomers(){
        
        Customer cust1=new Customer(new List<String>{'atreyeebanerjee@gmail.com'},'Mr','Atreyee','Banerjee',new List<String>{'9910070119'},'','');
        String jsonString=JSON.serialize(cust1);
        //sendCalloutREST(jsonString);    
    }
    
    /* Exceutes the logic for parsing data.*/
    public static void sendNewCustomers(Contact newContact, String action) {
        //Id newCustomerId,
        //Contact newContact=[Select Id,Email,Salutation,FirstName,LastName,Phone,Customer_ID__c from Contact where Id =:newCustomerId limit 1]; 
        Customer cust=new Customer(new List<String>{newContact.Email},newContact.Salutation,newContact.FirstName,newContact.LastName,new List<String>{newContact.Phone},newContact.Id,'salesforce::new_contact');
        updateCustomer updateCust= new updateCustomer(newContact.Salutation,newContact.FirstName,newContact.LastName);
        String jsonString=JSON.serialize(cust);
        String updateJsonString=JSON.serialize(updateCust);
        //sendCalloutREST(newContact.Id,jsonString);
        system.debug(newContact);
        if(action=='Insert' && newContact.Customer_ID__c == null) {
                sendCalloutREST(newContact.Id,jsonString);
        } else if(action=='Update') {   
            system.debug('---Incoming Cust ID---'+newContact.Customer_ID__c);
            String custID;
            if(String.valueOf(newContact.Customer_ID__c).contains('.0')){
                String[] customerIDArr = String.valueOf(newContact.Customer_ID__c).split('\\.');
                custID = customerIDArr[0];
                system.debug('---here---customerIDArr---'+customerIDArr);
                system.debug('---here---custID---'+custID);
            }else{
                custID = String.valueOf(newContact.Customer_ID__c);
            }
            system.debug('---Final Customer ID Sent In Request ---'+custID);
            //updateCalloutREST(newContact.Id,String.valueOf(newContact.Customer_ID__c),updateJsonString);
            updateCalloutREST(newContact.Id,custID,updateJsonString);
        }
    }
    
    /*Description:  Callout to wulverine system on Update of contact. In detail SF is sending contact details to the Wulverine system.
       Request Format : {"prefix":"Mr.","last_name":"testLastName","first_name":"testFirstName"}    
       Response Format : {"status":"Success","error":{"message":"path: 'v1/consumers/296850.0'","errors":[]}}   */
    @future(callout=true) 
    public static void updateCalloutREST(Id recordId,String customerId,String jsonString) {
        system.debug('***jsonString***'+jsonString);
        HttpResponse response;
        try {
            //String endPointURL = 'https://stg-salesforce.urbanladder.com/v1/consumers/'+customerId+'.json';
            CalloutModel.params calloutParams = CalloutService.populateCalloutParams('Customer_Update_UL', 'Wulverine Server');
            String endPointURL = calloutParams.endPointUrl+customerId;
            //'http://private-2853c-consumer3.apiary-mock.com/v1/consumers/'+customerId;
            String userName = calloutParams.userName;
            String password = calloutParams.password;
            Integer timeOut_x = calloutParams.timeOut*1000;
            response = CalloutService.callOutToExternalSystem(userName, password, endPointUrl, timeOut_x, 'PUT', jsonString);
            System.debug('responseBody: '+response.getBody());
            if(response.getStatus() != 'Created' && response.getStatus() != 'OK' && response.getStatus() != 'Success') {
                Contact updateContact=new Contact(Id=recordId, Error_Message__c = response.getBody(), Success_Message__c = '');
                contactRecursion.fireTrigger = false;
                update updateContact;
                CalloutService.generateServiceLog(jsonString, null, 'Customer', response, recordId); 
            } else {
                Contact updateContact=new Contact(Id=recordId, Error_Message__c = '', Success_Message__c = response.getBody());
                contactRecursion.fireTrigger = false;
                update updateContact;
            }
            
        } catch(Exception ex){
            System.debug('Error::'+ex.getMessage());
            Contact updateContact=new Contact(Id=recordId, Error_Message__c = ex.getMessage(), Success_Message__c = '');
            contactRecursion.fireTrigger = false;
            update updateContact;
            CalloutService.generateServiceLog(jsonString, ex, 'Customer', response, recordId);
            //throw new customException(ex.getLineNumber()+' '+ex.getMessage());
        }
    }
    
    /* Description: Callout to wulverine system on Insert of new contact. In detail SF is sending contact details to the Wulverine system, in response wulverine system is sending Customer Id to the SF . 
       Request Format : {"prefix":"Mr.","phones":["0987654321","1234567890"],"last_name":"testLastname","first_name":"testFirstName","emails":["9840802148@urbanladder.com"]}   
       Response Format : {"status":"success","data":{"consumer":{"id":386635,"email":"9840802148@urbanladder.com","prefix":"Mr.","first_name":"testFirstName","last_name":"testLastname","created_at":"2016-01-25T07:02:29.341Z","updated_at":"2016-01-25T07:02:29.341Z","parent_consumer_id":null}}}*/
    @future(callout=true) 
    public static void sendCalloutREST(Id recordId, String jsonString) {
        HttpResponse response;
        try {
            //String endPointURL = 'https://stg-salesforce.urbanladder.com/v1/consumers.json';
            CalloutModel.params calloutParams = CalloutService.populateCalloutParams('Customer_Insert_UL', 'Wulverine Server');
            String endPointURL = calloutParams.endPointUrl;
            String userName = calloutParams.userName;
            String password = calloutParams.password;
            Integer timeOut_x = calloutParams.timeOut*1000;
            
            response = CalloutService.callOutToExternalSystem(userName, password, endPointUrl, timeOut_x, 'POST', jsonString);
            System.debug('responseBody: '+response.getBody());
            if(response.getStatus() == 'Created' || response.getStatus() == 'OK' || response.getStatus() == 'Success') {
                System.debug('responseBody: '+response.getBody());
                System.debug('---responseBody---: '+response.getBody());
                //CustomerResponse rawResponse;
                Map<String, Object> rawBodyMap = new Map<String,Object>();
                Map<String, Object> postRawbodyMap = new Map<String,Object>();
                Map<String, Object> bodyMap = new Map<String,Object>();
                Decimal consumerID;
                try{
                //rawResponse=(CustomerResponse) JSON.deserialize(response.getBody(),CustomerResponse.class);
                //system.debug('---rawResponse---'+rawResponse);
                rawBodyMap = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
                postRawbodyMap = (Map<String, Object>)rawBodyMap.get('data');
                bodyMap = (Map<String, Object>)postRawbodyMap.get('consumer');
                consumerID = Decimal.valueOf((Integer)bodyMap.get('id'));
                
                system.debug('---rawBodyMap---'+rawBodyMap);
                system.debug('---postRawbodyMap---'+postRawbodyMap);
                system.debug('---bodyMap---'+bodyMap);
                system.debug('---consumerID---'+consumerID);
                
                }catch(Exception e){
                    system.debug('---Exception Caought Here---'+e.getMessage());
                }
               	system.debug('---consumerID---'+consumerID);
                //Map<String,String> cunsumerRecord=rawResponse.data.get('consumer');
                 //system.debug('---cunsumerRecord---'+cunsumerRecord);
                //Decimal customerID = Decimal.valueOf(consumerID);
                Contact updateContact=new Contact(Id=recordId, Customer_ID__c =consumerID);
                updateContact.Success_Message__c =  response.getBody();
                contactRecursion.fireTrigger = false;
                update updateContact;
            } else {
                Contact updateContact=new Contact(Id=recordId, Error_Message__c = response.getBody(), Success_Message__c = '');
                contactRecursion.fireTrigger = false;
                update updateContact;
                CalloutService.generateServiceLog(jsonString, null, 'Customer', response, recordId); 
            }
        } catch(Exception ex) {
            System.debug('Error::'+ex.getMessage());
            Contact updateContact=new Contact(Id=recordId, Error_Message__c = ex.getMessage(), Success_Message__c = '');
            contactRecursion.fireTrigger = false;
            update updateContact;
            CalloutService.generateServiceLog(jsonString, ex, 'Customer', response, recordId);
            //throw new customException(ex.getLineNumber()+' '+ex.getMessage());
        }
    }
    
    /* Wrapper format to send the new contact details (on Insert Operation) */
    public class Customer {
        public List<String> emails { get; set; }
        public String prefix { get; set; }
        public String first_name { get; set; }
        public String last_name { get; set; }
        //public List<String> phone_numbers { get; set; }
        public List<String> phones { get; set; }

        //** Added as per Phase 2 Changes
        public string user_id {get;set;}
        public string user_source{get;set;}

        public Customer(List<String> emails,String prefix,String first_name,String last_name,List<String> phone_numbers,String user_id, String user_source) {
            this.emails = emails;
            this.prefix= prefix;
            this.first_name = first_name;
            this.last_name = last_name;
            //this.phone_numbers = phone_numbers;
           this.phones = phone_numbers;
           this.user_id = user_id;
           this.user_source = user_source;
        }
    }
    
    /* Wrapper response format from the Wulverine server. */
    public class CustomerResponse {
        public String status {get; set;}
        //public Map<String,Map<String,String>> data {get; set;}
        public Map<String,Object> data {get; set;}
        
        public CustomerResponse(String emails,Map<String,Map<String,String>> data) {
            this.status = status;
            this.data= data;   
        }
    }
    
    /* Wrapper format to send the updated contact details (on Update Operation) */
    public class updateCustomer {
        public String prefix { get; set; }
        public String first_name { get; set; }
        public String last_name { get; set; }
        
        public updateCustomer(String prefix,String first_name,String last_name) {
            this.prefix= prefix;
            this.first_name = first_name;
            this.last_name = last_name;  
        }
    }
}