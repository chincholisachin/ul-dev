global class OrderPAPageController {
    
    public  Case currentCase {get;set;}
    public  Contact currentCaseContact{get;set;}
    global List<Order__x> ordersRelatedToContact {get;set;}
    global static List<orderitem__x> ordersItemsRelatedToOrder {get;set;}
    
    global OrderPAPageController(ApexPages.StandardController controller) {
        currentCase = new Case();
        currentCaseContact = new Contact();
        ordersRelatedToContact = new List<Order__x>();
        ordersItemsRelatedToOrder = new List<orderitem__x>();
        try{
            currentCase = [SELECT id,Status,ContactID,Order__c, Order_Item__c, Type, Sub_Category__c, Priority, subject, Description, origin,Type_of_Refund__c,Reason_for_cancellation__c from Case where ID=:controller.getID()];
            system.debug('*** currentCase***'+currentCase);
            
            if(currentCase.ContactID !=null){
                currentCaseContact = [SELECT id,Customer_ID__c,firstName,LastName from Contact where ID=:currentCase.ContactID];
            }
            
            
            if(currentCaseContact!=null){
                ordersRelatedToContact = [SELECT ExternalId,Code__c from Order__x where consumer_id__c=:currentCaseContact.Customer_ID__c];
            }
            
        }
        catch(Exception e){
            system.debug('***currentCase***'+currentCase);
            system.debug('***currentCaseContact***'+currentCaseContact);
            system.debug('***ordersRelatedToContact***'+ordersRelatedToContact);
        }
    }
    
    
    //*** Method used to fetch existing attachments on the Quote ***//
    @RemoteAction
    global static List<orderitem__x> getOrderItemsFromOrder(String caseID,String OrderCode){
        
        ordersItemsRelatedToOrder = new List<orderitem__x>();
        ordersItemsRelatedToOrder =  [SELECT ExternalID,Code__c, Item_Name__c, lms_status_text__c FROM orderitem__x WHERE order_code__c=: OrderCode];
        if(ordersItemsRelatedToOrder!=null){
            return ordersItemsRelatedToOrder;
        }
        return null;
    }
    
    @RemoteAction
    global static void updateCaseDetails(Case caseIns,String OrderCode,String OrderItemExternalID){
        
        Case caseToUpdate = new Case();
        caseToUpdate = caseIns;
        caseToUpdate =  PA_Utility.addOrderAndOIDetailsToCase(caseToUpdate,OrderCode,OrderItemExternalID);
        system.debug('---Updated Case from Case Publisher Action---'+caseToUpdate);
        update caseToUpdate;
        
    }
}