@IsTest
public class Utility_TestClass{

public static list<Contact> createContacts(){

    list<Contact> listOfContacts = new list<Contact>();

    for(integer i=0;i<10;i++){
    
        Contact con = new Contact();
        con.firstName = 'Test First Name'+i;
        con.lastName = 'Test Last Name'+i;
        con.phone = '9703994950';
    
        listOfContacts.add(con);    
    }
    return listOfContacts;
}

public static list<Case> createCases(){
 list<Case> listOfCases = new list<Case>();

    for(integer i=0;i<10;i++){
    
        Case eachCase = new Case();
        eachCase.Origin = 'Email';
        eachCase.Status = 'Open';
        
        listOfCases.add(eachCase);   
    }
    return listOfCases;
}

public static WS_UL_SendCallLogsToSFDC.DataFromCall createCallLogWrapper(){

WS_UL_SendCallLogsToSFDC.DataFromCall callLog = new WS_UL_SendCallLogsToSFDC.DataFromCall();

        callLog.monitorUCID = '3651446794333676';
        callLog.UUI =     'ABCD70';   
        callLog.Did = '918067417870';
        callLog.Location = '';
        callLog.CallerID = '9703994950';
        callLog.PhoneName='';
        
        callLog.Skill='None';
        callLog.StartTime = '2015-11-06 12:48:53';
        callLog.EndTime = '2015-11-06 12:49:24';
        callLog.TimeToAnswer = '';
        callLog.Duration = '00:00:00';
        callLog.FallBackRule = '';
        callLog.DialedNumber ='';
        callLog.Type ='';
        callLog.AgentID='';
        callLog.AgentUniqueID='';
        callLog.Disposition='';
        callLog.HangupBy='';
        
        callLog.Status='';
        callLog.AudioFile='';
        callLog.TransferType='';
        callLog.TransferredTo='';
        callLog.DialStatus='';
        callLog.Apikey='';






return callLog;
}
    
    public static CSAT_Score__c createCSAT(Id cid){
		CSAT_Score__c cScr	= new CSAT_Score__c();
        cScr.CSAT__c	= cid;
        cScr.CSAT_Value__c	= 'yes';
        return cScr;
	}


}