/*##############################################################################
########################## TestClass LeadConvertService #########################

Developed By : Baba S
Created Date : 20 July 16
Company : ET Marlabs


Last MOdified Date : 20 July 16

##################################################################################
################################################################################*/
@isTest(SeeAllData=false)
private class RestLeadConvertService_Test {

	static Lead newLead;

	 //Creating a lead
	 private static void init() {
	 	
	 	Account acc = new Account(Name = 'Test');
        insert acc;
        Contact con = new Contact(LastName = 'Test LastName',AccountId=acc.Id, Email = 'test1@urbanLadder.com',Customer_ID__c = 1209100);
        insert con;


    	System_Configs__c config = new System_Configs__c(Name='Lead', Is_Run__c = true);
        insert config;

	 	//New Lead
	 	newLead = InitializeTestdata.createLead();
	 	newLead.Email = 'test1@urbanLadder.com';
	 }

	
	 /*
		- Lead not found - with the passed customer-ID
	 */
       @isTest private static  void leadNotFoundTest() {
      	
      	//Pass random  -customerId

   	 	Test.startTest();
		    //do request
	        RestRequest req = new RestRequest(); 
	        RestResponse res = new RestResponse();

	        req.requestURI = '/services/apexrest/LeadConvert/13xxx50';  
	       
	        req.httpMethod = 'GET';
	        RestContext.request = req;
	        RestContext.response = res;

	        RestLeadConvertService.validateAndConvertLead();

        Test.stopTest();	

       		String resBody = JSON.serialize(res.responseBody);

       		Blob b = res.responseBody;
       		String resMessage = b.toString();
			
			System.debug('resMessage :'  +resMessage);
       		//Check for bad req status ->500
    		System.assertEquals(500, res.statusCode);

      }

	/*
		- Lead consultant is blank 
	*/
	 @isTest private static  void leadConsultantCheck() {
      	
    	//Create a lead - with no consultant
    	init();
    	//Customer_ID__c
    
		insert newLead;

		//Get the customer Id
		newLead = [Select Id,Customer_ID_2__c from lead where id =: newLead.Id];
		System.debug('Customer Id ='+newLead.Customer_ID_2__c);    	

		//pass the customer-id
   	 	Test.startTest();
		    
		    RestRequest req = new RestRequest(); 
	        RestResponse res = new RestResponse();

	        //System.debug('customerId #### ='+customerId);

	        req.requestURI = '/services/apexrest/LeadConvert/'+newLead.Customer_ID_2__c;  
	       
	        req.httpMethod = 'GET';
	        RestContext.request = req;
	        RestContext.response = res;

	        RestLeadConvertService.validateAndConvertLead();

        Test.stopTest();	
			
       		Blob b = res.responseBody;
       		String resMessage = b.toString();
       		System.debug('resMessage :'  +resMessage);
       		System.assertEquals(RestLeadConvertService.LEAD_CONSULTANT_NOT_FOUND, resMessage);
       		//Check for bad req status ->500
    		System.assertEquals(400, res.statusCode);

      }	

	/*
		- Lead has pending Activitis
	*/	 
	@isTest private static  void leadPendingEventCheck() {
      	
    	//Create a lead - with no consultant
    	init();
    	//Customer_ID__c

    	//Insert User
        User Newuser1 = InitializeTestdata.createUser1();
        insert Newuser1;
    
		insert newLead;

		//Get the customer Id
		newLead = [Select Id,Customer_ID_2__c from lead where id =: newLead.Id];
		System.debug('Customer Id ='+newLead.Customer_ID_2__c);    	

		 //Update lead - Stage, Sub stage and Consultant  & All the site Address
        newLead = InitializeTestdata.updateLeadsStageAndSubStage(newLead, Newuser1.Id);
        update newLead;
		//pass the customer-id

		//Check for the penind Event
        Integer NO_EVENTS = [Select count() from Event where whoId =: newLead.id  and status__c = 'Pending'];
        system.assert((NO_EVENTS==1), 'Total PVD events');

   	 	Test.startTest();
		    
		    RestRequest req = new RestRequest(); 
	        RestResponse res = new RestResponse();

	        //System.debug('customerId #### ='+customerId);
	        req.requestURI = '/services/apexrest/LeadConvert/'+newLead.Customer_ID_2__c;  
	       
	        req.httpMethod = 'GET';
	        RestContext.request = req;
	        RestContext.response = res;

	        RestLeadConvertService.validateAndConvertLead();

        Test.stopTest();	
			
       		Blob b = res.responseBody;
       		String resMessage = b.toString();
       		System.debug('resMessage :'  +resMessage);
       		System.assertEquals(RestLeadConvertService.LEAD_PENDING_ACTIVITIES, resMessage);
       		//Check for bad req status ->500
    		System.assertEquals(400, res.statusCode);

      }	

	
	/*
		- Lead successfully converted 
	*/	

}