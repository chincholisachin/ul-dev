@RestResource(urlMapping='/WS_PostOrderDataToSFDC/*')
global class WS_PushOrderDataToSFDC{
    
    /* Creating the replacement order stagging records with the Request Body.*/
    @HttpPost
    global static void WS_PostOrderDataToSFDC() {
        
        RestRequest req = RestContext.request;   
        Replacement_Order_Stagging__c orderStagging = new Replacement_Order_Stagging__c();
        orderStagging.Request_Body__c = req.requestBody.toString();
        
        //Added on 12/02/2016 to add filters for the Staging Records Created
        try{
        Map<String, Object> bodyMap;
        bodyMap = (Map<String, Object>) JSON.deserializeUntyped(req.requestBody.toString());
         if(bodyMap != null) {
         	//Top Level Variables - Headers
         	orderStagging.Event_Type__c = (String)bodyMap.get('event_type');
         	orderStagging.Event_ID__c =   String.valueOf(bodyMap.get('event_id'));
         	system.debug('---Event Datetime from JSON---'+bodyMap.get('event_datetime'));
         	
         	//-- Datetime Helper Code--BEGIN
         	string dttm = (String)bodyMap.get('event_datetime');
			string dttm2 = dttm.replace('T',' ');
			Integer indx = dttm2.indexOf('.');
			string newstr = dttm2.substring(0,indx);
			system.debug('===========Time=========  ' +newstr );
			datetime objdt  = datetime.valueof(newstr );
			system.debug('===========Final Time=========  ' +objdt  );
			
		 	//-- Datetime Helper Code--END
		 	
         	//orderStagging.Event_Datetime__c = Datetime.Parse((String)(bodyMap.get('event_datetime')));
         	orderStagging.Event_Datetime__c = objdt;
         	
         	//Top Level Variables - Event Data
         	 Map<string, Object> eventData = (Map<String, Object>) bodyMap.get('event_data');
         	 
         	 orderStagging.Parent_Order_Code__c = (String) eventData.get('parent_order_code'); // Order Id
             orderStagging.Replaced_Order_Code__c = (String) eventData.get('code'); // Replacement Order Id
         	
         	//Inner Event Data Variables
         	List<object> orderItems =  (List<object>) eventData.get('order_items');
            Map<String, object> orderItemsMap;
              if(orderItems.size() == 1) {
                 orderItemsMap = (Map<String, object>)orderItems[0];
                 orderStagging.Order_Item_Code__c = (string)orderItemsMap.get('code');  // Order Item Id
             	 orderStagging.LMS_Status_Text__c = (string)orderItemsMap.get('lms_status_text'); // LMS Status Text
		 	 	}
		 	 else if(orderItems.size()>1){
		 	 	//** Adding Code for Multiple Order Items here**
	             String OIToStatus = '';
	             orderStagging.Has_Multiple_OI_s__c = TRUE;
	             for(integer i=0;i<orderItems.size();i++){
			        orderItemsMap = (Map<String, object>)orderItems[i];
			        OIToStatus = OIToStatus+(string)orderItemsMap.get('code')+'=>'+(string)orderItemsMap.get('lms_status_text')+'\n';
			     }
			     orderStagging.OI_to_Status__c  = OIToStatus;
		 	 } 
         }
        }
        catch(Exception e){
        	system.debug('--- Exception in Parsing the Events---'+e.getMessage());
        }
         system.debug('---orderStagging---'+orderStagging);
        Database.SaveResult sr = Database.insert(orderStagging); 
        string errorMsg = '';
        
        RestContext.response.addHeader('Content-Type', 'application/json');
        if(!sr.isSuccess()) {
            for(Database.Error err : sr.geterrors()) {
                errorMsg += err.getMessage();
            }
            RestContext.response.responseBody = Blob.valueOf('{ "Status Code" : "422", "Message" :errorMsg}');
        } else {
            RestContext.response.responseBody = Blob.valueOf('{ "Status Code" : "200", "Message" : "Sucessfully Inserted"}');  
        }
    }
}