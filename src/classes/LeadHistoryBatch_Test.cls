/*
Author      : Baba
Description : Postive Test class for LeadHistoryBatch

*/
@isTest
private class LeadHistoryBatch_Test {
	private static testMethod void testone() {
         //Insert User
        User Newuser1 = InitializeTestdata.createUser1();
        insert Newuser1;
        
        Lead eachLead = new Lead();
        eachLead.FirstName = 'Urban';
        eachLead.LastName  =  'Ladder';
        eachLead.Lead_stage__c = 'Open';
        eachLead.Sub_Stage__c = 'Yet to Contact';
        eachLead.Status = 'New';
        eachLead.Requirement__c = 'Furniture';
        eachLead.Company = 'UrbanLadder';
        insert eachLead;
        eachLead.Lead_stage__c = 'Qualified';
		eachLead.Sub_Stage__c = 'yet to Schedule';
        eachLead.Consultant_Name__c = Newuser1.id;
        eachLead.Next_Action_Date__c = Date.today();
        update eachLead;
        System.Test.startTest();
        Database.executeBatch(new LeadHistoryBatch());
        System.Test.stopTest();
    }
}