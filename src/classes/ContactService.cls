public class ContactService {
    
    /* Fetch Contacts based on EmailIds provided*/
    public static list<Contact> fetchContactsByEmail(list<String> emailAddresses){
        try{
            return [Select Id,Email  From Contact Where Email IN :emailAddresses];
        }catch(QueryException qe){
            system.debug('***Exception***'+qe.getMessage());
        }   
        return null;
    }
    
    public static list<Contact> insertnewContactList(list<Contact> newContacts){
        try{
            insert newContacts;
            return newContacts;
        }
        catch(DMLException de){
            system.debug('***Exception***'+de.getMessage());
            CalloutService.generateServiceLog(JSON.serialize('{"message":"Exeception in contact creation method - ContactService.insertnewContactList", "error" : "' + de.getMessage() +' Line number '+ de.getLineNumber()+'","newContacts :"'+newContacts+'"}'), null, 'LEAD_CONTACT_CREATION', null, null);
        }
        return null;
    }
    
    public static void createNewAccountMethod(list<Contact> contactList) {
    
    	system.debug('*** Creating a new Account***'+contactList);
    		if(Constants.AccountRecHandler == FALSE){
    			 Constants.AccountRecHandler = TRUE;
        map<ID,Account> conIDToAccountMap = new map<ID,Account>();
        List<Contact> cList = new List<Contact>();
        
        for(Contact con : contactList){
        	Account acc;
            if(con.lastName!=null && con.firstName!=null) {
            	acc = new Account(Name=con.firstName+' '+con.lastName);
            }
            else if(con.lastName!=null){
            		acc = new Account(Name=con.lastName);
            	}
                conIDToAccountMap.put(con.ID,acc);
            }
            
        if(!conIDToAccountMap.isEmpty()){
            ServiceFacade.createNewAccounts(conIDToAccountMap.values());    
        }
        
        for (Id cId : conIDToAccountMap.keySet()) {
            Contact con = new Contact();
            con.Id = cId;
            con.AccountId = conIDToAccountMap.get(cId).Id;
            cList.add(con);
        }
        system.Debug('***Contact List***'+cList);
        try{
            update cList;
        }catch(Exception e){
            system.debug('*** Exception de***'+e.getMessage());
        }
      
    		}
    }
    
    public void updateMethod(list<Contact> contactList) {
        
        map<ID,Account> conIDToAccountMap = new map<ID,Account>();
        List<Contact> cList = new List<Contact>();
        
    }
    public static list<Contact> fetchContactByPhone(String phoneNumber){
        system.debug('*** Contact Service - inside fetchContactByPhone***');
        list<Contact> conList = new list<Contact>();
        String phoneNum = String.valueOf(phoneNumber).trim();
        try{
            system.debug('***try - phoneNumber***'+phoneNum);
            conList =  [SELECT id,Name from Contact where Phone=:phoneNum OR MobilePhone=:phoneNum];
            system.debug('***conList***'+conList.size());
            return conList;
        }
        catch(Exception e){
            system.debug('*** Exception e***'+e.getMessage());
        }
        system.debug('***conList***'+conList.size());
        return null;
    }
    
    public static Contact createNewContact(String phoneNumber){
    	system.debug('***Inside Create New Contact Method***');
        Contact con = new Contact(LastName='Missed Call Contact',Phone=phoneNumber,Created_From_Missed_Call__c=True);
        try{
            insert con;
            return con;
        }catch(DMLException de){
            system.debug('*** Exception de***'+de.getMessage());
        }
        system.debug('*** New Contact Created***'+con);
        return null;
    }
    
    public static string getcustomerIdForContact(Id contactId) {
        
        return (string.valueOf([select Customer_ID__c from Contact where id =: contactId].Customer_ID__c));
    }
}