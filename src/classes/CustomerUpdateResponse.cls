@isTest
global class CustomerUpdateResponse implements HttpCalloutMock {
    
    global HTTPResponse respond(HTTPRequest req) {
        // Optionally, only send a mock response for a specific endpoint
        // and method.
        //System.assertEquals('http://api.salesforce.com/foo/bar', req.getEndpoint());
        //System.assertEquals('GET', req.getMethod());
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"status": "success","data": {"consumer": {"id": 111,"email": "guptanvi@gmail.com","prefix": 1,"first_name": "testingdfgfd","last_name": "putmethohjdfkhsdfjjsdfhd","created_at": "2013-02-27T13:00:31.000Z","updated_at": "2015-12-15T06:38:21.163Z","parent_consumer_id": null}}}');
        res.setStatusCode(200);
        return res;
    }
}