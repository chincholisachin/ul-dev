/* Author : Upendar Shivanadri*/
@isTest
private class LeadTrigger_Test {
    
    /* if Lead don't have Existing Contact Email address; Create Conatct and populate the contact lookup in lead */
    Private static testMethod void init() {
        Account acc = new Account(Name = 'Test');
        insert acc;
        Contact con = new Contact(LastName = 'Test LastName',accountId = acc.Id);
        insert con;
        System_Configs__c config = new System_Configs__c(Name='Lead', Is_Run__c = true);
        insert config;
        Lead lead = new Lead(LastName = 'Lastname', FirstName = 'Firstname', Company='Urban Ladder', Email = 'test1@urbanLadder.com',Customer_Id__c='167834');
        insert lead;
        Lead queryLead = [select Contact__c from Lead where id = : lead.Id];
        Contact queryContact = [select id from Contact where email =: lead.Email limit 1];
        system.assertEquals(queryLead.Contact__c, queryContact.Id);
    }   
    
    /* if Lead Email has Existing Contact Email address; populate the contact lookup in lead with the existing contact */
    Private static testMethod void init2() {
        Account acc = new Account(Name = 'Test');
        insert acc;
        Contact con = new Contact(LastName = 'Test LastName', Email = 'test1@urbanLadder.com');
        insert con;
        System_Configs__c config = new System_Configs__c(Name='Lead', Is_Run__c = true);
        insert config;
        Lead lead = new Lead(LastName = 'Lastname', FirstName = 'Firstname', Company='Urban Ladder', Email = 'test1@urbanLadder.com');
        insert lead;
        Lead queryLead = [select Contact__c from Lead where id = : lead.Id];
        //Contact queryContact = [select id from Contact where email =: lead.Email limit 1];
        system.assertEquals(queryLead.Contact__c, con.Id);
    }
   
     Private static testMethod void init3() {
        Account acc = new Account(Name = 'Test');
        insert acc;
        Contact con = new Contact(LastName = 'Test LastName', Email = 'test1@urbanLadder.com');
        insert con;
        System_Configs__c config = new System_Configs__c(Name='Lead', Is_Run__c = true);
        insert config;
        Lead lead = new Lead(LastName = 'Lastname', FirstName = 'Firstname', Products__c = 'Full Furniture',Company='Urban Ladder', Email = 'test1@urbanLadder.com');
        insert lead;
        Lead queryLead = [select Contact__c,Products__c from Lead where id = : lead.Id];
        //Contact queryContact = [select id from Contact where email =: lead.Email limit 1];
        system.assertEquals(queryLead.Contact__c, con.Id);
        lead.Products__c = 'Half Furniture';
        lead.Consultant_Name__c = UserInfo.getUserId() ;
        update lead;
         queryLead = [select Contact__c,Products__c from Lead where id = : lead.Id];
        //system.assertEquals('Full Furniture;Half Furniture',queryLead.Products__c);
    }
    
     Private static testMethod void init4() {
        Account acc = new Account(Name = 'Test');
        insert acc;
        Contact con = new Contact(LastName = 'Test LastName', Email = 'test1@urbanLadder.com');
        insert con;
        System_Configs__c config = new System_Configs__c(Name='Lead', Is_Run__c = true);
        insert config;
        Lead lead = new Lead(LastName = 'Lastname', FirstName = 'Firstname', Products__c = 'Full Furniture',Company='Urban Ladder', Email = 'test1@urbanLadder.com',First_Visit_Date__c=Date.today(),Consultant_Name__c = UserInfo.getUserId());
        insert lead;
        Task tsk = new Task();
        tsk.ActivityDate = lead.First_Visit_Date__c.Date();
        tsk.Subject = 'First Visit Date';
        tsk.OwnerId = lead.Consultant_Name__c;
        tsk.WhoId = lead.Id;
        tsk.Status = 'Pending';
        tsk.Priority = 'normal';
        insert tsk;
    }
    Private static testMethod void init5() {
        Account acc = new Account(Name = 'Test');
        insert acc;
        Contact con = new Contact(LastName = 'Test LastName', Email = 'test1@urbanLadder.com');
        insert con;
        System_Configs__c config = new System_Configs__c(Name='Lead', Is_Run__c = true);
        insert config;
        Lead lead = new Lead(LastName = 'Lastname', FirstName = 'Firstname', Products__c = 'Full Furniture',Company='Urban Ladder', Email = 'test1@urbanLadder.com',Next_Action_Date__c=Date.today(),Consultant_Name__c = UserInfo.getUserId());
        insert lead;
        Task tsk = new Task();
        tsk.ActivityDate = lead.Next_Action_Date__c.Date();
        tsk.Subject = 'Next Action Date';
        tsk.OwnerId = lead.Consultant_Name__c;
        tsk.WhoId = lead.Id;
        tsk.Status = 'Pending';
        tsk.Priority = 'normal';
        insert tsk;
    }
    
    // Test Method for creating multiple opportunities.
    private static testMethod void init6() {
        Account acc = new Account(Name = 'Test');
        insert acc;
        Contact con = new Contact(LastName = 'Test LastName', Email = 'test1@urbanLadder.com', accountId = acc.Id);
        insert con;
        System_Configs__c config = new System_Configs__c(Name='Lead', Is_Run__c = true);
        insert config;
        Opp_Validations__c ov = new Opp_Validations__c(Is_True__c=true);
        insert ov;
        Lead lead = new Lead(contact__c=con.id, LastName = 'Lastname', FirstName = 'Firstname', Products__c = 'Full Furniture',Company='Urban Ladder', Email = 'test1@urbanLadder.com',Next_Action_Date__c=Date.today(),Consultant_Name__c = UserInfo.getUserId());
        insert lead;
        
        lead.Lead_stage__c = 'Qualified';
        lead.Sub_Stage__c = 'Visit scheduled';
        lead.Site_Street__c='Test Street';
        lead.Site_Area__c = 'Test Area';
        lead.Site_City__c = 'Bangalore';
        lead.Site_State__c = 'Test State';
        lead.Site_Zip_Code__c = 123123;
        lead.Place_of_Visit__c = 'Site of the customer';
        lead.Service_Order_Number__c ='R123';
        lead.Proposed_Visit_Date_1__c = Date.today();
        update lead;

        Try{
            LeadConvertService.customConvertLead(lead.Id,null);
            Lead leadinfo = [select convertedopportunityId, convertedcontactid, convertedaccountid from lead where id =: lead.Id];
            Account accInfo = [select id from Account];
            Contact conInfo = [select id,AccountId from contact where AccountId = :accInfo.Id];
            Opportunity oppInfo = [select id,AccountId from Opportunity where AccountId = :accInfo.Id];
            system.assertEquals(oppInfo.Id,leadinfo.convertedopportunityId);
            system.assertEquals(conInfo.Id, leadinfo.ConvertedContactId);
        }catch(exception ex){
            system.debug('exception '+ex);
            }
        
    }
   
}