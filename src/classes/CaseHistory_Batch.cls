/* Author       : Upendar Shivanadri
Description : To update the CS Agent's assignment date from case History */

public  class CaseHistory_Batch implements Database.Batchable<sObject> {
    
    public  Database.QueryLocator start(Database.BatchableContext BC) {
        
        set<String> fieldApiSet = new set<String>{'Owner','Email_Responded__c'}; 
            try{    
                return Database.getQueryLocator([select id ,Email_Responded__c,CS_Agent_Assigned_Time__c,First_Response_User__c,(select CreatedById,CreatedDate,Field,NewValue,OldValue FROM Histories where Field IN:fieldApiSet ORDER BY CreatedDate DESC) from Case where CS_Agent_Assigned_Time__c = null AND Email_Responded__c = true  ]);
            }
        catch(Exception e) {
            system.debug('*** Exceptiion in Case History Batch Start***'+e.getMessage());
        }
        return null;
    }
    
    public void execute(Database.BatchableContext BC,List<Case> scope) {
        
        Map<Id, User> userMap  = new Map<Id, User>([select id, Name from User where isActive=:true]);
        try{
            if(!scope.isEmpty()){
            for (Case c : Scope) {
                boolean emailResponded = false;
                string userName = '';
                if(c.Histories!=null){
                for(CaseHistory ch : c.Histories) {
                    if(ch.Field == 'Email_Responded__C' && ch.NewValue == true) { 
                        emailResponded = true;
                        userName = userMap.containsKey(ch.CreatedById) ? userMap.get(ch.CreatedById).Name : '';
                        c.First_Response_User__c = userName;
                        c.Agent_Picks_up_Time__c = ch.createdDate;
                        
                    }
                    
                    if(ch.Field == 'Owner'  && ch.NewValue != userName /*userMap.containsKey(ch.CreatedByID)ch.NewValue == userName*/ ) { //userMap.containsKey(ch.CreatedByID)
                        c.CS_Agent_Assigned_Time__c = ch.CreatedDate;
                        break;
                    }   
                }
                }
                
            }
            database.update(scope, false);
            }  
        }
        
        catch(Exception e) {
            system.debug('*** Exceptiion in Case History Batch***'+e.getMessage());
        }
        
    }
    public void finish(Database.BatchableContext BC) {
        
    }
}