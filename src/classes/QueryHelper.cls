public with sharing class QueryHelper {
	
	private static boolean queueMapCached = false;
	public static map<String,ID> queueMap = new map<String,ID>();

	public static map<String,ID> getQueueMap(){
		if(queueMapCached){
			return queueMap;
		}else{
				for(QueueSobject qsIns:[Select QueueId,Queue.Name from QueueSobject where SobjectType = 'Case']){
					queueMap.put(qsIns.Queue.Name,qsIns.QueueID);
					queueMapCached = true;
				}
				return queueMap;
			}
			return new map<String,ID>();
		}
	}