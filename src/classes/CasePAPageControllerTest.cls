@isTest(seeAllData = false)
public with sharing class CasePAPageControllerTest {
    
    private static testMethod void method1(){
    Order__x ord = new Order__x(ExternalID='R123456',Code__c = 'O8765433',facility_id__c=02);
    Database.insertAsync(ord);
    ApexPages.StandardController controller = new ApexPages.StandardController(ord);
    CasePAPageController casePA = new CasePAPageController(controller);
    Case caseInstance = new Case(Origin='Email',Status='Open');
    insert caseInstance;
    
    CasePAPageController.createCaseFromInstance(caseInstance,'R1234567','99880');
    
    CasePAPageController.getFilteredOrderItems('After sales1','R1234567');
    }
    
    private static testMethod void method2(){
       
    Order__x ord = new Order__x(ExternalID='R1234567',Code__c ='R123456',facility_id__c=02);
    Database.insertAsync(ord);
        
    orderitem__x ordItem = new orderitem__x(ExternalID ='R123456-291371303',order_code__c ='R1234567',lms_status_text__c='DELIVERED');
    Database.insertAsync(ordItem);
        
    ApexPages.StandardController controller = new ApexPages.StandardController(ord);
    
        
        
    CasePAPageController casePA = new CasePAPageController(controller);
        
     Contact con = new Contact(lastName = 'Test',Customer_Id__c = 12345,Phone='9898098980');
     insert con;
        
    Case caseInstance = new Case(Origin='Email',Status='Open',Type ='After sales',Order__c ='R1234567',Order_Item__c = '291371303',contact=con);
    insert caseInstance;
    casePA.newCase= caseInstance;
    casePA.currentOrder= new Order__x(ExternalID='R1234567',Code__c ='R123456',consumer_id__c =12345,Id = ord.id);
    casePA.currentOrderContact = con;
    casePA.currentOrder=ord;
    CasePAPageController.createCaseFromInstance(caseInstance,'R1234567','291371303');
    
    CasePAPageController.getFilteredOrderItems('After sales','R1234567');
    PA_Utility.addOrderDetailsToCase(caseInstance,'R1234567');
    }
    
    private static testMethod void method3(){
       
    Order__x ord = new Order__x(ExternalID='R1234567',Code__c ='R1234567',DC_Facility__c = '45',facility_id__c=02);
    Database.insertAsync(ord);
        
    orderitem__x ordItem = new orderitem__x(ExternalID ='R1234567-291371303',order_code__c ='R1234567',lms_status_text__c='DELIVERED');
    Database.insertAsync(ordItem);
        
    Contact con = new Contact(lastName = 'Test',Customer_Id__c = 12345,Phone='9898098980');
    insert con;
        
    Case caseInstance = new Case(Origin='Email',Status='Open',Type ='After sales',Order__c ='R1234567',Order_Item__c = '291371303',Contact = con);
    upsert caseInstance;
    test.startTest();
        PA_Utility.addOrderDetailsToCase(caseInstance,'R1234567');
    test.stopTest();
        
    }
}