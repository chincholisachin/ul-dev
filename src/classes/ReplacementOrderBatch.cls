global class ReplacementOrderBatch implements Database.Batchable<sObject> {
    
    /* Querying all the non processed replacement order stagging records.*/
    global Database.QueryLocator start(Database.BatchableContext BC) {
        //string statusToProcess = 'Processed - Success';
        string statusToProcess = 'Not Processed';
        //list<string> tempID = new list<String>{'a05p000000160wV'};

        //String query = 'Select Request_Body__c, Status__c from Replacement_Order_Stagging__c where Status__c  !=: statusToProcess ORDER BY Name';
        //String query = 'Select Request_Body__c, Status__c from Replacement_Order_Stagging__c where Status__c  =: statusToProcess ORDER BY Name';
        //String query = 'Select Request_Body__c, Status__c from Replacement_Order_Stagging__c where ID  IN: tempID ORDER BY Name';
		String eventType = Constants.EVENT_TYPE_ORDER_UPDATED ;
        String lmsStatusText = Constants.STATUS_PROCESSED_LMS_STATUS_NOT_MATCHING ;
        String query = 'Select Request_Body__c, Status__c from Replacement_Order_Stagging__c where Status__c  =: statusToProcess and Status__c  !=: lmsStatusText and Event_Type__c !=: eventType ORDER BY Name';        
        return Database.getQueryLocator(query);
    }
    
    /* Executes all the scope records. */
    global void execute(Database.BatchableContext BC, List<Replacement_Order_Stagging__c> scope) {
        system.debug('***Scope***'+scope);
        Map<string, orderDetails> orderDetailsCreatedList = new Map<String, orderDetails>();
       
        
        List<String> OrderId_Created = new List<String>();
        List<String> OrderItemId_Created = new List<String>();
       
        
        Map<Id, Replacement_Order_Stagging__c> staggingOrderCreatedMap = new Map<Id, Replacement_Order_Stagging__c>();
        Map<Id, Replacement_Order_Stagging__c> staggingOrderUpdatedMap = new Map<Id, Replacement_Order_Stagging__c>();
        Map<Id, Replacement_Order_Stagging__c> staggingListToUpdate = new Map<Id, Replacement_Order_Stagging__c>(); 

        //** Added on 29th
        Map<String, orderWrapper> orderToOrderItemsUpdatedMap = new map<String, orderWrapper>();

        String successMessage;
        String errorMessage;

        /* Processing the Batch records.*/
        for(Replacement_Order_Stagging__c eachStagingRecord : scope) {
            successMessage = '';
            errorMessage = '';
            //system.debug('*** each Replacement Staging Record***'+s);
            //system.debug('*** s.Request_Body__c***'+s.Request_Body__c);
            Map<String, Object> bodyMap;
            
            if(eachStagingRecord.Request_Body__c != null) {
                try {
                    bodyMap = (Map<String, Object>) JSON.deserializeUntyped(eachStagingRecord.Request_Body__c);
                    
                    if(bodyMap != null) {
                        system.debug('*** bodyMap***'+bodyMap);
                         Map<string, Object> eventData = (Map<String, Object>) bodyMap.get('event_data');
                         
                        if((String) bodyMap.get('event_type') == 'ORDER_CREATED') { // Processing the order created information.
                            
                            string orderId = (String) eventData.get('parent_order_code'); // Order Id
                            string replacementOrderId = (String) eventData.get('code'); // Replacement Order Id
                            List<object> orderItems =  (List<object>) eventData.get('order_items');
                            Map<String, object> orderItemsMap;
                            string orderItemId = '';

                            if(orderItems.size() > 0) {
                                orderItemsMap = (Map<String, object>)orderItems[0];
                                orderItemId = (string)orderItemsMap.get('code');  // Order Item Id
                            }

                           
                            if(orderId != null && orderId != '') OrderId_Created.add(orderId);
                            system.debug('TS*** OrderId_Created***'+OrderId_Created);

                            if(orderItemId != null && orderItemId != '') OrderItemId_Created.add(orderItemId);
                               system.debug('TS*** OrderItemId_Created***'+OrderItemId_Created);

                            if(orderId != null && orderId != '' && orderItemId != null && orderItemId != '') {
                                orderDetailsCreatedList.put(orderId, new orderDetails(orderId, orderItemId, eachStagingRecord.Id, replacementOrderId, null, null));
                            }
                            system.debug('TS***orderDetailsCreatedList***'+orderDetailsCreatedList);
                            
                        } else if((String) bodyMap.get('event_type') == 'ORDER_ITEM_UPDATED') { // processing the order item updated information.
                            
                            String orderId, orderItemId, lms_status_text,delivery_date;
                            Integer parent_order_id =  (Integer) eventData.get('parent_order_id');
                            List<object> orderItems =  (List<object>) eventData.get('order_items'); 

                            orderId = (String) eventData.get('code');// Order Id

                            Map<String,object> orderItemsMap = new Map<String,object>();
                            
                            list<OrderItemWrapper> orderItemWrapperList = new list<OrderItemWrapper>();
                            
                            for(integer i=0;i<orderItems.size();i++) {
                                orderItemsMap = (Map<String, object>)orderItems[i];
                                //** Each Order Item Attributes off Multiple Order Items in a JSON.
                                delivery_date = (String)orderItemsMap.get('delivery_date');
                                lms_status_text = (string)orderItemsMap.get('lms_status_text');
                                orderItemId = (string)orderItemsMap.get('code');

                                if(lms_status_text == 'DELIVERED' || lms_status_text == 'CANCELLED' || lms_status_text == 'CANCEL_INITIATED'){
                                    OrderItemWrapper oiWrap = new OrderItemWrapper(orderItemId,lms_status_text,delivery_date,eachStagingRecord.Id);
                                    orderItemWrapperList.add(oiWrap);
                                }
                                else{
                                        Replacement_Order_Stagging__c rs = new Replacement_Order_Stagging__c(Id = eachStagingRecord.Id, Status__c = 'Processed - LMS Status Not Matching', Error_Message__c = errorMessage);
                                        staggingListToUpdate.put(rs.Id, rs);    
                                    }     
                                }
                                system.debug('***orderId***'+orderId);
                                system.debug('***orderItemWrapperList***'+orderItemWrapperList);

                                if(orderToOrderItemsUpdatedMap.containsKey(orderId)){
                                   list<OrderItemWrapper> existing_oiList = orderToOrderItemsUpdatedMap.get(orderID).oiList;
                                   existing_oiList.addAll(orderItemWrapperList);
                                   Set<OrderItemWrapper> tempSet = new Set<OrderItemWrapper>(existing_oiList);
                                   list<OrderItemWrapper> revised_oiList = new list<OrderItemWrapper>(tempSet);
                                   orderToOrderItemsUpdatedMap.get(orderID).oiList = revised_oiList;
                                }else{
                                   orderToOrderItemsUpdatedMap.put(orderId,new orderWrapper(orderId,orderItemWrapperList,eachStagingRecord.id,null,parent_order_id));
                                }

                                system.debug('-- This will contain the Order to Order Item in one transaction:200--');
                                system.debug('--orderToOrderItemsUpdatedMap--'+orderToOrderItemsUpdatedMap);
                            }
                        }
                    }
                 catch(Exception e) {
                    Replacement_Order_Stagging__c rs = new Replacement_Order_Stagging__c(Id = eachStagingRecord.Id, Status__c = 'JSON Error', Error_Message__c = e.getLineNumber() +' '+ e.getMessage());
                        staggingListToUpdate.put(rs.Id, rs);
                }
            }
        }
        
        // Procesing for the Created Orders --- START
        if(!OrderId_Created.isEmpty() || !OrderItemId_Created.isEmpty()){
            Set<String> orderIdsForCase = new Set<String>();
            Map<Id, Case> caseMap = new Map<Id, Case>([select OI_Code__c, Old_Replaced_Order_Id__c, Order__c, Order_Item__c, Replaced_Order_Id__c,After_Sales_Status__c, Success_Message__c, Error_Message__c from Case where (Order__c IN: OrderId_Created OR Replaced_Order_Id__c IN: OrderId_Created)AND OI_Code__c IN: OrderItemId_Created AND Replacement_Order_Placed_with_UL__c = true AND Status != 'Closed']);
            
            for(Case caseIns : caseMap.values()) {
                orderDetails oDetails;
                if(orderDetailsCreatedList.containsKey(caseIns.Replaced_Order_Id__c)) {
                    oDetails = orderDetailsCreatedList.get(caseIns.Replaced_Order_Id__c);
                } else if(orderDetailsCreatedList.containsKey(caseIns.Order__c)) {
                    oDetails = orderDetailsCreatedList.get(caseIns.Order__c);
                } 
                
                if((oDetails.orderId  == caseIns.Order__c || oDetails.orderId  == caseIns.Replaced_Order_Id__c) && oDetails.orderItemId == caseIns.OI_Code__c) {
                    
                    caseIns.After_Sales_Status__c ='Replacement Order Placed';
                    caseIns.Success_Message__c = 'Replacement Order Placed Successfully.';
                    caseIns.Error_Message__c = '';
                    caseIns.Old_Replaced_Order_Id__c = caseIns.Replaced_Order_Id__c;
                    caseIns.Replaced_Order_Id__c = oDetails.replacementId;
                    oDetails.caseId = caseIns.Id;
                    orderIdsForCase.add(caseIns.Order__c);
                }                
            }
            integer count =0;
            integer insideIfCount = 0;
            for(String oId : OrderId_Created){
                if(!orderIdsForCase.contains(oId)){
                system.debug('*** This is where it is processing***');
                    count+=1;
                    String stagingId = orderDetailsCreatedList.get(oId).stagingId;
                    
                    if(staggingListToUpdate.containsKey(stagingId)){
                        insideIfCount +=1;
                        errorMessage +='No Cases Found'+'\n';
                        staggingListToUpdate.get(stagingId).Status__c = 'Processed - No Cases Found';
                        staggingListToUpdate.get(stagingId).Error_Message__c = errorMessage;
                    }else if(!staggingListToUpdate.containsKey(stagingId)){
                        insideIfCount +=1;
                        errorMessage +='No Cases Found'+'\n';
                        Replacement_Order_Stagging__c rs = new Replacement_Order_Stagging__c(Id = stagingId, Status__c = 'Processed - No Cases Found', Error_Message__c = errorMessage);
                        staggingListToUpdate.put(rs.Id, rs);
                    }
                }
            }
            system.debug('***count***'+count+'***insideIfCount***'+insideIfCount);
            
            
            //Updating the cases and processing the success and error records. [Creating Orders] 
            
            Database.SaveResult [] srList = database.update(caseMap.values(), false);
            for(Database.SaveResult sr : srList) {
                if(caseMap.containsKey(sr.getId())) {
                    String OrderId = caseMap.get(sr.getId()).Order__c;
                    system.debug('*** Save Result OrderId***'+orderID);
                    orderDetails ordDetails = orderDetailsCreatedList.get(OrderId);
                    if(ordDetails == null ) {
                        ordDetails = orderDetailsCreatedList.get(caseMap.get(sr.getId()).Old_Replaced_Order_Id__c);
                        system.debug(ordDetails +'*****');
                        system.debug(orderDetailsCreatedList+'******');
                    }
                    system.debug('*** Save Result ordDetails***'+ordDetails);
                    Replacement_Order_Stagging__c orderStagIns = new Replacement_Order_Stagging__c();
                    orderStagIns.put('Id', ordDetails.stagingId);
                    if(sr.isSuccess()) orderStagIns.put('Status__c', 'Processed - Success');
                    if(sr.isSuccess())
                    {
                        successMessage+='[ORDER_CREATED] : Successfully Updated with '+ ordDetails.replacementId +' to Case '+sr.getId()+'\n';
                        orderStagIns.put('Error_Message__c',successMessage);
                    }
                    if(!sr.isSuccess()){ 
                        errorMessage ='[ORDER_CREATED] : ERROR '+ sr.getErrors()+'\n';
                        orderStagIns.put('Error_Message__c',errorMessage);
                    }
                    system.debug('*** Save Result updated Replacement Order Staging ID***'+orderStagIns);
                    staggingOrderCreatedMap.put(orderStagIns.Id, orderStagIns);
                }
            }
            
            /* Updating all the Staging records; for Create Order */ 
            if(!staggingOrderCreatedMap.isEmpty()) {
                database.update(staggingOrderCreatedMap.values(), false);
                //staggingListToUpdate.addAll(staggingOrderCreatedList);
            }
        } // Procesing - Create Order --- END
        
        
        // Procesing for the Updated Orders
        if(!orderToOrderItemsUpdatedMap.isEmpty()){

            //Set<String> orderIdsForCase = new Set<String>();
            set<string> orderID_ASReplacement = new set<string>();
            set<string> orderID_ASCancellation = new set<string>();
            set<string> orderID_MajorCancellation = new set<string>();

            set<String> OrderItems_Delivered = new set<String>();
            set<String> OrderItems_Cancelled = new set<String>();
            set<String> OrderItems_Cancel_Initiated = new set<String>();

            Set<String> orderIdsForCase = new Set<String>();

            for(orderWrapper oDetails:orderToOrderItemsUpdatedMap.values()){
                for(OrderItemWrapper oiWrap : oDetails.oiList){
                    if((String)oiWrap.orderItemStatus=='DELIVERED'){
                        orderID_ASReplacement.add(oDetails.orderId);
                        OrderItems_Delivered.add(oiWrap.orderItemID);
                    }
                    if((String)oiWrap.orderItemStatus=='CANCELLED'){
                        orderID_ASReplacement.add(oDetails.orderId);
                        orderID_ASCancellation.add(oDetails.orderId);
                        orderID_MajorCancellation.add(oDetails.orderId);
                        OrderItems_Cancelled.add(oiWrap.orderItemID);
                    }
                    if((String)oiWrap.orderItemStatus=='CANCEL_INITIATED'){
                        orderID_MajorCancellation.add(oDetails.orderId);
                        OrderItems_Cancel_Initiated.add(oiWrap.orderItemID);
                    }
                }
            }

            //-- After Sales Replacement Module--
            Map<Id, Case> afterSalesRepCaseMap = new Map<Id, Case>([select OI_Code__c, Old_Replaced_Order_Id__c, Order__c, Order_Item__c,After_Sales_Status__c,Replaced_Order_Id__c from Case where Replaced_Order_Id__c IN: orderID_ASReplacement AND (OI_Code__c IN: OrderItems_Delivered OR OI_Code__c IN: OrderItems_Cancelled) AND Replacement_Order_Placed_with_UL__c = true AND Status != 'Closed']);


            for(Case caseIns : afterSalesRepCaseMap.values()) {
                if(orderToOrderItemsUpdatedMap.containsKey(caseIns.Replaced_Order_Id__c)) {
                    orderWrapper oDetails = orderToOrderItemsUpdatedMap.get(caseIns.Replaced_Order_Id__c);
                    if(oDetails.orderId  == caseIns.Replaced_Order_Id__c) {
                        for(OrderItemWrapper oiWrap : oDetails.oiList){
                            if(oiWrap.orderItemID == caseIns.OI_Code__c && (String)oiWrap.orderItemStatus=='DELIVERED'){
                                caseIns.After_Sales_Status__c = (String)oiWrap.orderItemStatus;
                                caseIns.Success_Message__c = 'Replacement Order has been Delivered Successfully.';
                                oiWrap.oICaseID = caseIns.ID;
                                orderIdsForCase.add(caseIns.Replaced_Order_Id__c);
                            }else if(oiWrap.orderItemID == caseIns.OI_Code__c && (String)oiWrap.orderItemStatus=='CANCELLED'){
                                caseIns.After_Sales_Status__c = (String)oiWrap.orderItemStatus;
                                caseIns.Success_Message__c = 'Replacement Order has been Cancelled.';
                                oiWrap.oICaseID = caseIns.ID;
                                orderIdsForCase.add(caseIns.Replaced_Order_Id__c);
                            }
                        }
                        caseIns.Error_Message__c = '';
                    }
                }
            }
             

              // -- After Sales Cancellation Module --
            Map<Id, Case> afterSalesCanCaseMap = new Map<Id, Case>([select O_Code__c,OI_Code__c, Old_Replaced_Order_Id__c, Order__c, Order_Item__c,After_Sales_Status__c,Replaced_Order_Id__c from Case where O_Code__c IN: orderID_ASCancellation AND OI_Code__c IN:OrderItems_Cancelled AND Type='After sales' AND After_Sales_Status__c != 'Cancelled' AND Status != 'Closed']);

            for(Case caseIns : afterSalesCanCaseMap.values()){
            if(orderToOrderItemsUpdatedMap.containsKey(caseIns.O_Code__c)){
                orderWrapper oDetails = orderToOrderItemsUpdatedMap.get(caseIns.O_Code__c);
                if(oDetails.orderId  == caseIns.O_Code__c){
                    for(OrderItemWrapper oiWrap : oDetails.oiList){
                        if(oiWrap.orderItemID == caseIns.OI_Code__c && (String)oiWrap.orderItemStatus=='CANCELLED'){
                            if(caseIns.After_Sales_Status__c!='Needs Replacement with different SKU' && caseIns.After_Sales_Status__c !='New Product Delivered'){
                                caseIns.After_Sales_Status__c = (String)oiWrap.orderItemStatus;
                            }
                            caseIns.Success_Message__c = 'Original Order to Order Item Is Cancelled'; 
                            oiWrap.oICaseID = caseIns.ID;
                            orderIdsForCase.add(caseIns.O_Code__c);
                        }
                    }
                    caseIns.Error_Message__c = '';
                    }
                }
            }
            

            // -- Main Cancellation Module--
        Map<Id, Cancellation_Item__c> canItemsMap = new Map<Id, Cancellation_Item__c>([select id,Case__c,O_Code__c,OI_Code__c,Processing_Status__c,Approval_Status__c,delivery_date__c,delivery_flag__c from Cancellation_Item__c where O_Code__c IN: orderID_MajorCancellation AND (OI_Code__c IN:OrderItems_Cancelled OR OI_Code__c IN: OrderItems_Cancel_Initiated) AND (Processing_Status__c='Cancellation Requested' OR (Processing_Status__c = 'CANCEL_INITIATED' AND delivery_flag__c = TRUE))]);


        for(Cancellation_Item__c canIns : canItemsMap.values()) {
            if(orderToOrderItemsUpdatedMap.containsKey(canIns.O_Code__c)){
                orderWrapper oDetails = orderToOrderItemsUpdatedMap.get(canIns.O_Code__c);

                if(oDetails.orderId  == canIns.O_Code__c){
                    for(OrderItemWrapper oiWrap : oDetails.oiList){
                        if(oiWrap.orderItemID == canIns.OI_Code__c && (String)oiWrap.orderItemStatus == 'CANCELLED'){
                            system.debug('---Inside Cancelled---');
                            canIns.Success_Message__c = 'Original Order to Order Item - Cancelled'+oDetails.orderId+'=>'+oiWrap.orderItemID;
                            canIns.Processing_Status__c = (String)oiWrap.orderItemStatus;
                            canIns.delivery_date__c  = (String)oiWrap.delivery_date;
                            if((String)oiWrap.delivery_date!=null){
                                canIns.delivery_flag__c = TRUE;
                            }
                            canIns.Error_Message__c = '';
                            oiWrap.oICaseID = canIns.ID;
                            orderIdsForCase.add(canIns.O_Code__c);
                        }
                        if(oiWrap.orderItemID == canIns.OI_Code__c && (String)oiWrap.orderItemStatus == 'CANCEL_INITIATED'){
                            system.debug('---Inside CANCEL_INITIATED---');
                             canIns.Success_Message__c = 'Original Order to Order Item - CANCEL_INITIATED'+oDetails.orderId+'=>'+oiWrap.orderItemID;
                             canIns.Processing_Status__c = (String)oiWrap.orderItemStatus;
                             canIns.delivery_date__c  = (String)oiWrap.delivery_date;
                             if((String)oiWrap.delivery_date!=null){
                                canIns.delivery_flag__c = TRUE;
                            }
                            canIns.Error_Message__c = '';
                            oiWrap.oICaseID = canIns.ID;
                            orderIdsForCase.add(canIns.O_Code__c);
                        }
                    }

                }
            }
        }

        for(String oId : orderToOrderItemsUpdatedMap.keyset()) {
            if(!orderIdsForCase.contains(oId)) {
                        String stagingId = orderToOrderItemsUpdatedMap.get(oId).stagingId;
                        if(staggingListToUpdate.containsKey(stagingId)){
                            errorMessage ='No Cases Found'+'\n';
                            staggingListToUpdate.get(stagingId).Status__c = 'Processed - No Cases Found';
                            staggingListToUpdate.get(stagingId).Error_Message__c =errorMessage;
                        }else{
                             errorMessage ='No Cases Found'+'\n';
                            Replacement_Order_Stagging__c rs = new Replacement_Order_Stagging__c(Id = stagingId, Status__c = 'Processed - No Cases Found', Error_Message__c = errorMessage);
                            staggingListToUpdate.put(rs.Id, rs);
                        }
            }
        }

            Database.SaveResult [] srList1 = database.update(afterSalesRepCaseMap.values(), false);
            Database.SaveResult [] srList2 = database.update(afterSalesCanCaseMap.values(), false);
            Database.SaveResult [] srList3 = database.update(canItemsMap.values(), false);

            Replacement_Order_Stagging__c orderStagIns;

            for(Database.SaveResult sr : srList1) {
                if(afterSalesRepCaseMap.containsKey(sr.getId()) && !orderToOrderItemsUpdatedMap.isEmpty()) { 
                    String OrderId = afterSalesRepCaseMap.get(sr.getId()).Replaced_Order_Id__c;
                    String OrderItemId = afterSalesRepCaseMap.get(sr.getId()).OI_Code__c;
                    successMessage = '';
                    errorMessage = '';

                    orderWrapper oDetails = orderToOrderItemsUpdatedMap.get(OrderId);
                    for(OrderItemWrapper oiWrap:oDetails.oiList){
                        if(oDetails.orderId == OrderId && oiWrap.orderItemID == OrderItemId){
                            orderStagIns  = new Replacement_Order_Stagging__c();
                            orderStagIns.put('Id', oiWrap.oIstagingId);
                            if(sr.isSuccess()){
                                orderStagIns.put('Status__c', 'Processed - Success');
                                successMessage ='[ORDER_UPDATED] : Successfully Updated status to delivered for '+ oDetails.orderId +' to Case '+sr.getId()+'\n';
                                orderStagIns.put('Error_Message__c',successMessage);
                            }else if(!sr.isSuccess()){
                                 orderStagIns.put('Status__c', 'Processed - Error');
                                 orderStagIns.put('Error_Message__c',sr.getErrors());
                            }
                            if(!staggingOrderUpdatedMap.containsKey(orderStagIns.Id)){
                                staggingOrderUpdatedMap.put(orderStagIns.Id, orderStagIns);
                            }else{
                                staggingOrderUpdatedMap.get(orderStagIns.Id).Error_Message__c = successMessage+'\n'+sr.getErrors();
                            }
                        }
                    }
                }
            }

            for(Database.SaveResult sr : srList2){
                if(afterSalesCanCaseMap.containsKey(sr.getId()) && !orderToOrderItemsUpdatedMap.isEmpty()) { 
                    String OrderId = afterSalesCanCaseMap.get(sr.getId()).O_Code__c;
                    String OrderItemId = afterSalesCanCaseMap.get(sr.getId()).OI_Code__c;
                    successMessage = '';
                    errorMessage = '';

                    orderWrapper oDetails = orderToOrderItemsUpdatedMap.get(OrderId);
                    for(OrderItemWrapper oiWrap:oDetails.oiList){
                        if(oDetails.orderId == OrderId && oiWrap.orderItemID == OrderItemId){
                            orderStagIns  = new Replacement_Order_Stagging__c();
                            orderStagIns.put('Id', oiWrap.oIstagingId);
                            if(sr.isSuccess()){
                                orderStagIns.put('Status__c', 'Processed - Success');
                                successMessage ='[ORIGINAL_ORDER_UPDATED] : Successfully Updated status to Cancelled for '+ oDetails.orderId +' to Case '+sr.getId()+'\n';
                                orderStagIns.put('Error_Message__c',successMessage);
                            }else if(!sr.isSuccess()){
                                 orderStagIns.put('Status__c', 'Processed - Error');
                                 orderStagIns.put('Error_Message__c',sr.getErrors());
                            }
                            if(!staggingOrderUpdatedMap.containsKey(orderStagIns.Id)){
                                staggingOrderUpdatedMap.put(orderStagIns.Id, orderStagIns);
                            }else{
                                staggingOrderUpdatedMap.get(orderStagIns.Id).Error_Message__c = successMessage+'\n'+sr.getErrors();
                            }
                        }
                    }
                }
            }

            for(Database.SaveResult sr : srList3){
                if(canItemsMap.containsKey(sr.getId()) && !orderToOrderItemsUpdatedMap.isEmpty()) { 
                    String OrderId = canItemsMap.get(sr.getId()).O_Code__c;
                    String OrderItemId = canItemsMap.get(sr.getId()).OI_Code__c;
                    successMessage = '';
                    errorMessage = '';
                    orderWrapper oDetails = orderToOrderItemsUpdatedMap.get(OrderId);
                    for(OrderItemWrapper oiWrap:oDetails.oiList){
                        if(oDetails.orderId == OrderId && oiWrap.orderItemID == OrderItemId){
                            orderStagIns  = new Replacement_Order_Stagging__c();
                            orderStagIns.put('Id', oiWrap.oIstagingId);
                            if(sr.isSuccess()){
                                orderStagIns.put('Status__c', 'Processed - Success');
                                successMessage ='Revised Main Canc. Success'+oDetails.orderId+':'+oiWrap.orderItemId+':'+oiWrap.OIcaseID+'\n';
                                orderStagIns.put('Error_Message__c',successMessage);
                            }else if(!sr.isSuccess()){
                                 orderStagIns.put('Status__c', 'Processed - Error');
                                 orderStagIns.put('Error_Message__c',sr.getErrors()); 
                            }
                            if(!staggingOrderUpdatedMap.containsKey(orderStagIns.Id)){
                                staggingOrderUpdatedMap.put(orderStagIns.Id, orderStagIns);
                            }else{
                                staggingOrderUpdatedMap.get(orderStagIns.Id).Error_Message__c = successMessage+'\n'+sr.getErrors();
                            }
                        }
                    }
                }
            }

            if(!staggingListToUpdate.isEmpty()) {
                system.debug('---Staging List to Update---'+staggingListToUpdate);
                database.update(staggingListToUpdate.values(), false);
            }

            if(!staggingOrderUpdatedMap.isEmpty()) {
                system.debug('---Staging List to Update---'+staggingOrderUpdatedMap);
                database.update(staggingOrderUpdatedMap.values(),false);
            }

            }
    }
    global void finish(Database.BatchableContext BC){
        
    }
    
    public class orderDetails {
        
        public String orderId;
        public String orderItemId;
        public String stagingId;
        public String replacementId;
        public String caseId;
        public String lmsStatus;
        
        public orderDetails(String orderId, String orderItemId, String stagingId, String replacementId,  String caseId, String lmsStatus) {
            
            this.orderId = orderId;
            this.orderItemId = orderItemId;
            this.stagingId = stagingId;
            this.replacementId = replacementId;
            this.caseId = caseId;
            this.lmsStatus = lmsStatus;
        }
    }
    
    
    public class OrderItemWrapper{
        
        public String orderItemId;
        public String orderItemStatus;
        public string delivery_date;
        public String oIstagingId;
        public String  oICaseID;
        
        public OrderItemWrapper(String orderItemId,String orderItemStatus,String delivery_date,String oIstagingId){
            this.orderItemID = orderItemId;
            this.orderItemStatus  = orderItemStatus;
            this.delivery_date = delivery_date;
            this.oIstagingId= oIstagingId;
        }
    }
    
    public class orderWrapper {
        
        public String orderId;
        public list<OrderItemWrapper> oiList;
        public String stagingId;
        public String caseId;
        public Integer parent_order_id;
        
        public orderWrapper(String orderId,list<OrderItemWrapper> oiList,String stagingId,String caseId,Integer parent_order_id) {
            this.orderId = orderId;
            this.oiList = oiList;
            this.stagingId = stagingId;
            this.caseId = caseId;
            this.parent_order_id = parent_order_id;
        }
    }
    
}