@isTest
private class FulfilCancellation_Batch_TestClass{
    static testmethod void testMethod1() {
        
        
        //List<Fulfil_Cancellation__c> listOfCase = new List<Fulfil_Cancellation__c>();
        
        Fulfil_Cancellation__c fulfilcan = new Fulfil_Cancellation__c(Processed__c = false , Json_body__c = '{"Data":[{"Origin":"Email","Type":"Cancellation","Sub_Category":"On Delivery","Order":"R843221605","Parent_Order":null,"Payment_Method":"","Status":"","Priority":"","O_FacilityID":2,"Reason_For_Cancellation":"BORER","Type_of_Refund":"No Refund","Customer_Id":"86492","Cancellation_items_Data":[{"OI_Code":"1100","OI_Name":"Calabah Swing Chair (Colour : brown)","OI_Reason":"Item has been cancelled","OI_Total_Price":"7799.0","OI_Delivery_Date":null,"OI_ExternalID":null}]}]}');
        insert fulfilcan;
        
        
        FulfilCancellation_Batch nn = new FulfilCancellation_Batch();
        database.executebatch(nn,1); 
        
        
    }
    static testmethod void testMethod2() {
        
        
        //List<Fulfil_Cancellation__c> listOfCase = new List<Fulfil_Cancellation__c>();
        Contact con = new Contact(LastName ='TestContact', Customer_ID__c =86492);
        insert con;
        Fulfil_Cancellation__c fulfilcan = new Fulfil_Cancellation__c(Processed__c = false , Json_body__c = '{"Data":[{"Origin":"Email","Type":"Cancellation","Sub_Category":"On Delivery","Order":"R843221605","Parent_Order":null,"Payment_Method":"","Status":"","Priority":"","O_FacilityID":2,"Reason_For_Cancellation":"BORER","Type_of_Refund":"No Refund","Customer_Id":"86492","Cancellation_items_Data":[{"OI_Code":"1100","OI_Name":"Calabah Swing Chair (Colour : brown)","OI_Reason":"Item has been cancelled","OI_Total_Price":"7799.0","OI_Delivery_Date":null,"OI_ExternalID":null}]}]}');
        insert fulfilcan;
        
        
        FulfilCancellation_Batch nn = new FulfilCancellation_Batch();
        database.executebatch(nn,1); 
        
        
    }
    
    static testmethod void testMethod3() {
        
        
        //List<Fulfil_Cancellation__c> listOfCase = new List<Fulfil_Cancellation__c>();
        Contact con = new Contact(LastName ='TestContact', Customer_ID__c =86492);
        insert con;
        case caseins = new case(subject ='test',Description = 'Test',Status = 'Open' , type = 'Cancellation' );
        insert caseins;
        Cancellation_Item__c canItem = new Cancellation_Item__c(Approval_Status__c = 'Approved' ,O_Code__c = 'R699501397',OI_Code__c = '1218354', Case__c = caseins.id);
        insert canItem ;
        Fulfil_Cancellation__c fulfilcan = new Fulfil_Cancellation__c(Processed__c = false , Json_body__c = '{"Data":[{"Origin":"Email","Type":"Cancellation","Sub_Category":"On Delivery","Order":"R843221605","Parent_Order":null,"Payment_Method":"","Status":"","Priority":"","O_FacilityID":2,"Reason_For_Cancellation":"BORER","Type_of_Refund":"No Refund","Customer_Id":"86492","Cancellation_items_Data":[{"OI_Code":"1100","OI_Name":"Calabah Swing Chair (Colour : brown)","OI_Reason":"Item has been cancelled","OI_Total_Price":"7799.0","OI_Delivery_Date":null,"OI_ExternalID":null}]}]}');
        insert fulfilcan;
        
        
        FulfilCancellation_Batch nn = new FulfilCancellation_Batch();
        database.executebatch(nn,1); 
        
        
    }
    static testmethod void testMethod4() {
        
        
        //List<Fulfil_Cancellation__c> listOfCase = new List<Fulfil_Cancellation__c>();
        Contact con = new Contact(LastName ='TestContact', Customer_ID__c =86492);
        insert con;
        case caseins = new case(subject ='test',Description = 'Test',Status = 'Open' , type = 'Cancellation' );
        insert caseins;
        Cancellation_Item__c canItem = new Cancellation_Item__c(Approval_Status__c = 'Rejected' ,O_Code__c = 'R699501397',OI_Code__c = '1218354', Case__c = caseins.id);
        insert canItem ;
        Fulfil_Cancellation__c fulfilcan = new Fulfil_Cancellation__c(Processed__c = false , Json_body__c = '{"Data":[{"Origin":"Email","Type":"Cancellation","Sub_Category":"On Delivery","Order":"R843221605","Parent_Order":null,"Payment_Method":"","Status":"","Priority":"","O_FacilityID":2,"Reason_For_Cancellation":"BORER","Type_of_Refund":"No Refund","Customer_Id":"86492","Cancellation_items_Data":[{"OI_Code":"1100","OI_Name":"Calabah Swing Chair (Colour : brown)","OI_Reason":"Item has been cancelled","OI_Total_Price":"7799.0","OI_Delivery_Date":null,"OI_ExternalID":null}]}]}');
        insert fulfilcan;
        
        
        FulfilCancellation_Batch nn = new FulfilCancellation_Batch();
        database.executebatch(nn,1); 
        
        
    }
    static testmethod void testMethod5() {
        
        
        //List<Fulfil_Cancellation__c> listOfCase = new List<Fulfil_Cancellation__c>();
        Contact con = new Contact(LastName ='TestContact', Customer_ID__c =86492);
        insert con;
        case caseins = new case(subject ='test',Description = 'Test',Status = 'Open' , type = 'After sales' ,OI_Code__c = '1218354',O_Code__c = 'R699501397',Order__c ='R699501397',Order_Item__c='1218354');
        insert caseins;
        //Cancellation_Item__c canItem = new Cancellation_Item__c(Approval_Status__c = 'Rejected' ,O_Code__c = 'R699501397',OI_Code__c = '1218354', Case__c = caseins.id);
        //insert canItem ;
        Fulfil_Cancellation__c fulfilcan = new Fulfil_Cancellation__c(Processed__c = false , Json_body__c = '{"Data":[{"Origin":"Email","Type":"Cancellation","Sub_Category":"On Delivery","Order":"R843221605","Parent_Order":null,"Payment_Method":"","Status":"","Priority":"","O_FacilityID":2,"Reason_For_Cancellation":"BORER","Type_of_Refund":"No Refund","Customer_Id":"86492","Cancellation_items_Data":[{"OI_Code":"1100","OI_Name":"Calabah Swing Chair (Colour : brown)","OI_Reason":"Item has been cancelled","OI_Total_Price":"7799.0","OI_Delivery_Date":null,"OI_ExternalID":null}]}]}');
        insert fulfilcan;
        
        
        FulfilCancellation_Batch nn = new FulfilCancellation_Batch();
        database.executebatch(nn,1); 
        
        
    }
    
    static testmethod void testMethod6() {
        
        //List<Fulfil_Cancellation__c> listOfCase = new List<Fulfil_Cancellation__c>();
        Contact con = new Contact(LastName ='TestContact', Customer_ID__c =86492);
        insert con;
        case caseins = new case(subject ='test',Description = 'Test',Status = 'Open' , type = 'Cancellation' ,OI_Code__c = '1100',O_Code__c = 'R843221605',Order__c ='R843221605',Order_Item__c='1100');
        insert caseins;
        Cancellation_Item__c canItem = new Cancellation_Item__c(Approval_Status__c = 'Needs Approval',O_Code__c = 'R843221605',OI_Code__c = '1100', Case__c = caseins.id);
        insert canItem ;
        Fulfil_Cancellation__c fulfilcan = new Fulfil_Cancellation__c(Processed__c = false , Json_body__c = '{"Data":[{"Origin":"Email","Type":"Cancellation","Sub_Category":"On Delivery","Order":"R843221605","Parent_Order":null,"Payment_Method":"","Status":"","Priority":"","O_FacilityID":2,"Reason_For_Cancellation":"BORER","Type_of_Refund":"No Refund","Customer_Id":"86492","Cancellation_items_Data":[{"OI_Code":"1100","OI_Name":"Calabah Swing Chair (Colour : brown)","OI_Reason":"Item has been cancelled","OI_Total_Price":"7799.0","OI_Delivery_Date":null,"OI_ExternalID":null}]}]}');
        insert fulfilcan;
        
        
        FulfilCancellation_Batch nn = new FulfilCancellation_Batch();
        database.executebatch(nn,1); 
        
        
    }
    
    
    /*
    static testMethod void myUnitTest2() {
        Test.startTest();
        scheduledBatch_AutomaticCaseCloser temp = new scheduledBatch_AutomaticCaseCloser();
        String sch = '0 0 23 * * ?'; 
        system.schedule('Test Territory Check', sch, temp); 
        Test.stopTest();
    }*/
}