@isTest
public class UL_Utilities { // User Creation

    public static User createUserdata(String LName ,String FName,String profileName){
        
        Profile p = [SELECT Id, Name FROM Profile WHERE Name ='Chatter Free User']; 
        User userData = new User(Alias = 'standt1', 
                                  Email= FName+LName+'@urbanladder.com', 
                                  EmailEncodingKey='UTF-8', 
                                  FirstName = FName,
                                  LastName = LName, 
                                  LanguageLocaleKey='en_US', 
                                  LocaleSidKey='en_AU', 
                                  ProfileId = p.Id, 
                                  TimeZoneSidKey='Asia/Kolkata', 
                                  UserName= FName+LName+'@urbanladder.com'
                                  );
        return userData; 
    }
    
    public static Contact createContact() {
    
        Contact contactData = new Contact();
        return contactData ;
    }
    
    public static Case createcase(Id ContactID) {
    
    Case caseobj = new Case(SuppliedEmail='testuser@urbanladder.com',SuppliedName='Urban',subject='Test',Priority = 'High');
     return caseobj;
    }
    
    public static Task createtask (Id ContactID) {
    
    Task taskobj = new Task(Location__c = 'Bangalore');
    return taskobj ;
    }

}