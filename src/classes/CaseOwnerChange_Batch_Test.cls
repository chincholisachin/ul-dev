/*Author: Baba S
Dated : 07/07/2016
Purpose: Test class for CaseOwnerChange_Batch.
*/
@isTest(seeAllData =false)
public class CaseOwnerChange_Batch_Test {
    
    private static testMethod void testBatch() {
        QueuesObject q1;
        QueuesObject q2;
        
        System.runAs ( new User(Id = UserInfo.getUserId()) ) {

            Group g1 = new Group(Name='Customer Service - Email(Temp)', type='Queue');
            insert g1;
            q1 = new QueueSObject(QueueID = g1.id, SobjectType = 'Case');
            insert q1;
            
            Group g2 = new Group(Name='Customer Service - Email', type='Queue');
            insert g2;
            q2 = new QueueSObject(QueueID = g2.id, SobjectType = 'Case');
            insert q2;
        }
        string qId   = [select Id, name from Group where Type = 'Queue' AND name = 'Customer Service - Temp' Limit 1].id;
        list<case> csList   =   Utility_TestClass.createCases();
        for(case cs: csList){
            cs.Status   = 'Open';
            cs.Priority = 'Low';
            cs.origin= 'Email';
            cs.Type     = 'Category';
            cs.Subject  = 'Testing';
            cs.Description='Bla';
            cs.OwnerId  = qId;
            cs.Sub_Category__c ='Catalog';            
            
        }
        insert csList;
        
        System.Test.startTest();
        Database.executeBatch(new CaseOwnerChange_Batch());
        System.Test.stopTest();
    }
}