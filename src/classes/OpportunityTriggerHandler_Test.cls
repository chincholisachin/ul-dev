@isTest
private class OpportunityTriggerHandler_Test {

    //Account 
    //Opp
    private static Account account;
    private static Opportunity opportunity;

     private static void init() {
        
        account = InitializeTestdata.createAccount();
        insert account;

        opportunity = InitializeTestdata.createOpportunity(account.Id, 'Kitchens');
        opportunity.Consultant_Name__c   = Userinfo.getUserId() ;
        insert opportunity;

        System_Configs__c sc = new System_Configs__c();
        sc.Name = 'Opportunity';
        sc.Is_Run__c = true;
        insert sc;
     }
    
    @isTest static void opportunity_TAT_test() {
        // Implement test code
        init();

        opportunity.Sub_Stage__c = 'Req Documentation';
        update opportunity;

        //Req_Documentation_TAT_Breached__c 
        opportunity.Sub_Stage__c = 'Design/Quote prep';
        opportunity.Req_Documentation_TAT_Breached__c = True;
        
        //** Opportunity Validations Fix
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
            User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standarduser-2@testorg.com.test');
            
            insert u;
            
        opportunity.First_Visit_Date__c = Date.Today()+2;
        opportunity.First_Requirement_Date__c  = Date.Today()+3;
        opportunity.Consultant_Name__c   = u.id;
        opportunity.Site_Zip_Code__c   = 500008;
        opportunity.Site_Street__c   = 'Test Street';
        opportunity.Site_City__c   = 'Madras';
        opportunity.Site_State__c   = 'TN';
        opportunity.Site_Area__c   = 'Annasalai';
        opportunity.LeadSource   = 'Email';
        opportunity.Requirement__c   = 'Kitchens';
        
        
        update opportunity;
        
        //Design_Quote_Prep_TAT_Breached__c 
        opportunity.Sub_Stage__c = 'Design/Quote prep';
        opportunity.Design_Quote_Prep_TAT_Breached__c = True;
        update opportunity;

        //Re_design_Re_quote_TAT_Breached__c 
        opportunity.Sub_Stage__c = 'Re design/Re quote';
        opportunity.Re_design_Re_quote_TAT_Breached__c = True;
        update opportunity;

        //First_advance_awaited_TAT_Breached__c 
        opportunity.Sub_Stage__c = 'First advance awaited';
        opportunity.First_advance_awaited_TAT_Breached__c = True;
        update opportunity;

        //Site_Check_Diagram_TAT_Breached__c 
        opportunity.Sub_Stage__c = 'Site diagram';
        opportunity.Site_Check_Diagram_TAT_Breached__c = True;
        update opportunity;

        //Site_Check_TAT_Breached__c 
        opportunity.Sub_Stage__c = 'Site check';
        opportunity.Site_Check_TAT_Breached__c = True;
        update opportunity;

        //Post_Order_Redesign_TAT_Breached__c 
        opportunity.Sub_Stage__c = 'Post order Redesign / Requote';
        opportunity.Post_Order_Redesign_TAT_Breached__c = True;
        update opportunity;

        //Post_Order_Reproposal_TAT_Breached__c 
        opportunity.Sub_Stage__c = 'Post Order Reproposal';
        opportunity.Post_Order_Reproposal_TAT_Breached__c = True;
        update opportunity;

        //Post_Order_Redesign_TAT_Breached__c 
        opportunity.Sub_Stage__c = 'Pending internal approval';
        opportunity.Pending_internal_approval_TAT_Breached__c = True;
        update opportunity;

        //Post_Order_Reproposal_TAT_Breached__c 
        opportunity.Sub_Stage__c = 'Second advance awaited';
        opportunity.Second_advance_awaited_TAT_Breached__c = True;
        update opportunity;

        //Post_Order_Reproposal_TAT_Breached__c 
        opportunity.Sub_Stage__c = 'Req Documentation';
        //opportunity.Post_Order_Reproposal_TAT_Breached__c = True;
        update opportunity;

        opportunity.Sub_Stage__c = 'Post Order Reproposal';
        //opportunity.Post_Order_Reproposal_TAT_Breached__c = True;
        update opportunity;

    }
    
}