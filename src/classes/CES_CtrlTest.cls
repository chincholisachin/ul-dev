/*
*Purpose: Test class for Controller of Case CES_Page
*Date : 06/01/2017
*VERSION    AUTHOR                     DETAIL                                 
*1.0        Pavithra Gajendra          INITIAL DEVELOPMENT               
*
*/
@isTest
private class CES_CtrlTest {

    //------- Scenario When CES score & Case Id exists
    static testMethod void myUnitTest() {
    	
       PageReference pageRef = Page.OpportunityCES_Page;
       Test.setCurrentPage(pageRef);
       
       //----- Add parameters to page URL  
    
       ApexPages.currentPage().getParameters().put('caseId', '500p00000030CE5');
       ApexPages.currentPage().getParameters().put('cesVal', '1');
       
       //----- Instantiate a new controller with all parameters in the page 
       CES_Ctrl controller = new CES_Ctrl();
       
       //----- Save the CES record
       controller.saveCES();
       
       //----- Update the CES record
       controller.updateCES();
        
    }
    
    //------- Scenario When CES score is null 
    static testMethod void myUnitTest1() {
       ApexPages.currentPage().getParameters().put('caseId', '500p00000030CE5');
       ApexPages.currentPage().getParameters().put('cesVal', '');
       CES_Ctrl controller = new CES_Ctrl();
       
       //----- Save the CES record
       controller.saveCES();
       
       //----- Update the CES record
       controller.updateCES();
    }
    
    //------- Scenario When Case Id is null 
    static testMethod void myUnitTest2() {
       ApexPages.currentPage().getParameters().put('caseId', null);
       ApexPages.currentPage().getParameters().put('cesVal', '3');
       CES_Ctrl controller = new CES_Ctrl();
       
       //----- Save the CES record
       controller.saveCES();
       
       //----- Update the CES record
       controller.updateCES();
    }
    
    //------- Scenario When CES value is more than 4 
    static testMethod void myUnitTest3() {
       ApexPages.currentPage().getParameters().put('caseId', '500p00000030CE5');
       ApexPages.currentPage().getParameters().put('cesVal', '5');
       CES_Ctrl controller = new CES_Ctrl();
       
       //----- Save the CES record
       controller.saveCES();
       
       //----- Update the CES record
       controller.updateCES();
    }
}