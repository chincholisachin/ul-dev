global class RetriggerServiceLogBatch implements Database.Batchable<sObject>, Database.AllowsCallouts {
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        
        Retrigger_Limit__c rt_Limit = Retrigger_Limit__c.getValues('Retry_Limit');
        Integer retryLimit = (Integer)rt_Limit.Max_Limit__c;
        String query = 'Select Number_Of_Retries_Performed__c, Request_Format__c, Status__c, Record_Id__c, Log_Message__c, Callout_Service_Name__c, Status_Code__c, Sys_Record_Id__c from Service_Logs__c where Status__c = \'Not Processed\' ';
        system.debug(query);
        //'Select Request_Format__c, Status__c, Record_Id__c, Callout_Service_Name__c, Status_Code__c, Sys_Record_Id__c from Service_Logs__c where ((Number_Of_Retries_Performed__c <'+ retryLimit+') OR Number_Of_Retries_Performed__c = null) AND Status__c  != \'Processed - Success\' ';
        return Database.getQueryLocator(query);
    }
    
    /* Retrigger logic for the Service Log */
    global void execute (Database.BatchableContext BC, List<Service_Logs__c> scope) {
        
        set<Id> contactIdSet = new set<Id>();
        set<Id> contact_SuccessIdSet = new set<Id>();
        set<Id> contact_InsertIdSet = new set<Id>();
        set<Id> contact_UpdateIdSet = new set<Id>();
        set<Id> contact_MergeIdSet = new set<Id>();
        list<serviceLogWrapper> serviceLogInsertList = new list<serviceLogWrapper>();
        list<serviceLogWrapper> serviceLogUpdateList = new list<serviceLogWrapper>();
        list<serviceLogWrapper> serviceLogSuccessList = new list<serviceLogWrapper>();
        list<Contact> contactSuccessList = new list<Contact>();
        list<Contact> contactInsertList = new list<Contact>();
        list<Contact> contactUpdateList = new list<Contact>();
        list<Service_Logs__c> success_serviceLogList = new list<Service_Logs__c>();
        
        for(Service_Logs__c sLogs : scope) {
            //Executes when the customer record get failed, due to standard or custom validations.
            if(sLogs.Callout_Service_Name__c == 'Customer' && (sLogs.Status_Code__c == '200' || sLogs.Status_Code__c == '201')) { 
                contact_SuccessIdSet.add(sLogs.Sys_Record_Id__c);
                Contact con = parseData(sLogs.Sys_Record_Id__c, sLogs.Log_Message__c);
                serviceLogSuccessList.add(new serviceLogWrapper(con, sLogs));
                contactSuccessList.add(con);
            } 
            //Executes when the customer record get failed, due to callout .
            else if(sLogs.Callout_Service_Name__c == 'Customer' && sLogs.Request_Format__c != null && sLogs.Record_Id__c != null) {
                Map<String, Object> requestDeserializedInfo = (Map<String, Object>) JSON.deserializeUntyped(sLogs.Request_Format__c);
                
                if(requestDeserializedInfo.containsKey('emails') && requestDeserializedInfo.containsKey('phones')) { // Checking whether the record get failed, while Contact Insert .
                    contact_InsertIdSet.add(sLogs.Sys_Record_Id__c);
                } else if(requestDeserializedInfo.containsKey('primary')) { // Check whether the record get failed, while Customer Merge.
                    contact_MergeIdSet.add(sLogs.Sys_Record_Id__c);
                    
                } else if(requestDeserializedInfo.containsKey('first_name') && requestDeserializedInfo.size() == 3) { // Check whether the record get failed, while Contact Update.
                    contact_UpdateIdSet.add(sLogs.Sys_Record_Id__c);
                }                    
            } 
            //Exceutes when the order record get failed, due to 
            else if(sLogs.Callout_Service_Name__c == 'Order') {
                
            }
        }
        
        
        //if(!contact_SuccessIdSet.isEmpty()) contactIdSet.addAll(contact_SuccessIdSet);
        if(!contact_InsertIdSet.isEmpty()) contactIdSet.addAll(contact_InsertIdSet);
        if(!contact_UpdateIdSet.isEmpty()) contactIdSet.addAll(contact_UpdateIdSet);
        //if(!contact_MergeIdSet.isEmpty()) contactIdSet.addAll(contact_MergeIdSet);
        
        if(!contactIdSet.isEmpty()) {
            Map<Id, Contact> contactMap = new Map<Id, Contact>([select Phone, Salutation, lastName, firstName, Email, MobilePhone, Customer_Id__c from Contact where Id IN: contactIdSet]);
            for(Service_Logs__c sLogs : scope) {
                if(contact_UpdateIdSet.contains(sLogs.Sys_Record_Id__c)) {
                    serviceLogUpdateList.add(new serviceLogWrapper(contactMap.get(sLogs.Sys_Record_Id__c), sLogs));
                } else if(contact_InsertIdSet.contains(sLogs.Sys_Record_Id__c)) {
                    serviceLogInsertList.add(new serviceLogWrapper(contactMap.get(sLogs.Sys_Record_Id__c), sLogs));
                }
            }
            
            
            for(Contact con : contactMap.values()) {
                if(contact_InsertIdSet.contains(con.Id)) {
                    contactInsertList.add(calloutToInsertCustomer(con));
                } else if(contact_UpdateIdSet.contains(con.Id)) {
                    contactUpdateList.add(calloutToUpdateCustomer(con));
                }
            }
        }
        
        // Update the Contact records; which are failed due to system or Custom Validations.
        Database.SaveResult[] res = new List<Database.SaveResult>();
        if(!contactSuccessList.isEmpty()) {
            res =  database.update(contactSuccessList, false);
        }
        
        /* processing the success and error records. */
        for(Database.SaveResult con : res) {
            if(con.isSuccess()) {
                for(serviceLogWrapper sl : serviceLogSuccessList) {
                    if(con.getId() == sl.con.Id) {
                        sl.serviceLog.Number_Of_Retries_Performed__c = sl.serviceLog.Number_Of_Retries_Performed__c != null ? sl.serviceLog.Number_Of_Retries_Performed__c + 1 : 1;
                        sl.serviceLog.Status__c = 'Processed - Success';
                        system.debug(sl.serviceLog.Number_Of_Retries_Performed__c+'ID');
                        success_serviceLogList.add(sl.serviceLog);
                    } 
                }   
            } else {
                string errorMsg = '';
                for(serviceLogWrapper sl : serviceLogSuccessList) {
                    if(con.getId() == sl.con.Id) {
                        for(Database.Error err : con.getErrors()) {
                            errorMsg += err.getMessage();
                        }
                        sl.serviceLog.Error_Log__c  = errorMsg;
                        sl.serviceLog.Status__c = 'Processed - Error';
                        success_serviceLogList.add(sl.serviceLog);
                    }
                }
                
            }
        }
        
        
        // Update the Contact records which Update Failed, coz of callout.
        Database.SaveResult[] res_Update = new List<Database.SaveResult>();
        if(!contactUpdateList.isEmpty()) {
            res_Update =  database.update(contactUpdateList, false);
        }
        
        /* processing the success and error records. */
        for(Database.SaveResult con : res_Update) {
            if(con.isSuccess()) {
                for(serviceLogWrapper sl : serviceLogUpdateList) {
                    if(con.getId() == sl.con.Id) {
                        sl.serviceLog.Number_Of_Retries_Performed__c = sl.serviceLog.Number_Of_Retries_Performed__c != null ? sl.serviceLog.Number_Of_Retries_Performed__c + 1 : 1;
                        sl.serviceLog.Status__c = 'Processed - Success';
                        success_serviceLogList.add(sl.serviceLog);
                    }
                }
            } else {
                string errorMsg = '';
                for(serviceLogWrapper sl : serviceLogUpdateList) {
                    if(con.getId() == sl.con.Id) {
                        for(Database.Error err : con.getErrors()) {
                            errorMsg += err.getMessage();
                        }
                        sl.serviceLog.Error_Log__c  = errorMsg;
                        sl.serviceLog.Status__c = 'Processed - Error';
                        success_serviceLogList.add(sl.serviceLog);
                    }
                }   
            }   
        }
        
        // Update the Contact records which Failed while insert Operation, coz of callout.
        Database.SaveResult[] res_Insert  = new List<Database.SaveResult>();
        if(!contactInsertList.isEmpty()) {
            res_Insert =  database.update(contactInsertList, false);
        }
        
        /* processing the success and error records. */
        for(Database.SaveResult con : res_Insert) {
            if(con.isSuccess()) {
                for(serviceLogWrapper sl : serviceLogInsertList) {
                    if(con.getId() == sl.con.Id) {
                        sl.serviceLog.Number_Of_Retries_Performed__c = sl.serviceLog.Number_Of_Retries_Performed__c != null ? sl.serviceLog.Number_Of_Retries_Performed__c + 1 : 1;
                        sl.serviceLog.Status__c = 'Processed - Success';
                        success_serviceLogList.add(sl.serviceLog);
                    } 
                }
            } else {
                string errorMsg = '';
                for(serviceLogWrapper sl : serviceLogInsertList) {
                    if(con.getId() == sl.con.Id) {
                        for(Database.Error err : con.getErrors()) {
                            errorMsg += err.getMessage();
                        }
                        sl.serviceLog.Error_Log__c  = errorMsg;
                        sl.serviceLog.Status__c = 'Processed - Error';
                        success_serviceLogList.add(sl.serviceLog);
                    }
                }   
            }   
            
        }
        
        
        //updating the status back to Service log.
        if(!success_serviceLogList.isEmpty()) database.update(success_serviceLogList, false);
        
    }
    
    /* Parse Data for updating Contact.*/
    public Contact parseData(Id conId, string jsonData) {
        
        /*FetchCustomer.CustomerResponse rawResponse=(FetchCustomer.CustomerResponse) JSON.deserialize(jsonData,FetchCustomer.CustomerResponse.class);
        Map<String,String> cunsumerRecord = rawResponse.data.get('consumer');
        Contact updateContact = new Contact(Id=conId, Customer_ID__c = Decimal.valueOf(cunsumerRecord.get('id')));*/
        Contact updateContact = new Contact(Id=conId);
        return updateContact;
    }
    
    /* call out for failed contacs, while updating*/
    public contact calloutToUpdateCustomer(Contact con) {
        
        string customerId;
        if(String.valueOf(con.Customer_ID__c).contains('.0')){
            String[] customerIDArr = String.valueOf(con.Customer_ID__c).split('\\.');
            customerId = customerIDArr[0];
        } else {
            customerId =  string.valueOf(con.Customer_ID__c);
        }
        string recordId = con.Id;
        FetchCustomer.updateCustomer updateCust= new FetchCustomer.updateCustomer(con.Salutation,con.FirstName,con.LastName);
        String jsonString=JSON.serialize(updateCust);
        HttpResponse response;
        Contact updateContact;
        try {
            //String endPointURL = 'https://stg-salesforce.urbanladder.com/v1/consumers/'+customerId+'.json';
            CalloutModel.params calloutParams = CalloutService.populateCalloutParams('Customer_Update_UL', 'Wulverine Server');
            String endPointURL = calloutParams.endPointUrl+customerId;
            //'http://private-2853c-consumer3.apiary-mock.com/v1/consumers/'+customerId;
            String userName = calloutParams.userName;
            String password = calloutParams.password;
            Integer timeOut_x = calloutParams.timeOut*1000;
            response = CalloutService.callOutToExternalSystem(userName, password, endPointUrl, timeOut_x, 'PUT', jsonString);
            if(response.getStatus() != 'Created' && response.getStatus() != 'OK' && response.getStatus() != 'Success') {
                updateContact=new Contact(Id=recordId, Error_Message__c = response.getBody(), Success_Message__c = '');
            } else {
                updateContact=new Contact(Id=recordId, Error_Message__c = '', Success_Message__c = response.getBody());
            }
            
        } catch(Exception ex){
            updateContact=new Contact(Id=recordId, Error_Message__c = ex.getLineNumber() + ex.getMessage(), Success_Message__c = '');
        }
        return updateContact;
    }
    
    
    /* call out for failed contacs, while Inserting*/
    public contact calloutToInsertCustomer(Contact con) {
        
        HttpResponse response;
        Contact updateContact;
        try {
            //String endPointURL = 'https://stg-salesforce.urbanladder.com/v1/consumers.json';
            CalloutModel.params calloutParams = CalloutService.populateCalloutParams('Customer_Insert_UL', 'Wulverine Server');
            String endPointURL = calloutParams.endPointUrl;
            String userName = calloutParams.userName;
            String password = calloutParams.password;
            Integer timeOut_x = calloutParams.timeOut*1000;
            FetchCustomer.Customer cust=new FetchCustomer.Customer(new List<String>{con.Email},con.Salutation,con.FirstName,con.LastName,new List<String>{con.Phone},con.id,'salesforce::new_contact');
            String jsonString=JSON.serialize(cust);
            
            response = CalloutService.callOutToExternalSystem(userName, password, endPointUrl, timeOut_x, 'POST', jsonString);
            
            if(response.getStatus() == 'Created' || response.getStatus() == 'OK' || response.getStatus() == 'Success') {
                
                /*FetchCustomer.CustomerResponse rawResponse=(FetchCustomer.CustomerResponse) JSON.deserialize(response.getBody(),FetchCustomer.CustomerResponse.class);
                Map<String,String> cunsumerRecord=rawResponse.data.get('consumer');
                updateContact=new Contact(Id=con.Id, Customer_ID__c = Decimal.valueOf(cunsumerRecord.get('id')));*/
                updateContact=new Contact(Id=con.Id);
                updateContact.Success_Message__c =  response.getBody();
            } else {
                updateContact=new Contact(Id=con.Id, Error_Message__c = response.getBody(), Success_Message__c = '');
                //contactRecursion.fireTrigger = false;
            }
        } catch(Exception ex) {
            System.debug('Error::'+ex.getMessage());
            updateContact=new Contact(Id=con.Id, Error_Message__c = ex.getLineNumber() + ex.getMessage(), Success_Message__c = '');
            //contactRecursion.fireTrigger = false;
        }
        return updateContact;
    }
    
    
    
    
    public class serviceLogWrapper {
        
        public Contact con;
        public Service_Logs__c serviceLog;
        
        public serviceLogWrapper(Contact con, Service_Logs__c serviceLog) {
            this.con = con;
            this.serviceLog = serviceLog;
        }
    }
    
    global void finish(Database.BatchableContext BC) {
        
    }
    
}