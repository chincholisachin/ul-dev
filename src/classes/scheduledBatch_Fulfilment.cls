global class scheduledBatch_Fulfilment implements Schedulable{
   
   global void execute(SchedulableContext sc) {
      
      FulfilCancellation_Batch b = new FulfilCancellation_Batch(); 
      database.executebatch(b,1);
   }
}