/*--++----------------------------------------------------------------------
||        Author      : BABA/Pavithra
||
||        Purpose     : Controller for the CES_Page 
||                  
||        Modified By : 16/02/2016 / Baba
|| 
||        Reason      : - Code beautification/Review and made code to follow the best practices. 
                        - Methods 'Save' and 'Update'.  
						- Changes to new field from Masterdetail to Lookup(04/01/2017)
||
++----------------------------------------------------------------------- */
global class CES_Ctrl {

	//Varibles
	public String        CES_Val  {get; set;}
    public String        CaseId   {get; set;}
    public CES_Score__c  CES_Score{get; set;}
    public Boolean       isError   {get; set;} 
    public String        feedback  {get; set;}
    Schema.SObjectField externalIdCESScore = CES_Score__c.Fields.Unique_Id__c ;

	//Constructor 
	global CES_Ctrl() {

		//Set the isError flag to false	 
	 	system.debug('***Constructor Called ***');
	    isError = false;   
        CES_Score = new CES_Score__c();
	    
	}

	//Init
	global void saveCES(){
		try {
			    CES_Val = ApexPages.CurrentPage().getparameters().get('cesVal');
		        CaseId = ApexPages.CurrentPage().getparameters().get('caseId');
				
		        
				system.debug('***Page Action Method Called***');
				if((CES_Val != null || CES_Val != '') && ((CaseId != null || CaseId != '') && ValidateCaseId(CaseId))){
										
					CES_Score.Score__c = CES_Val;
					//CES_Score.Case__c  = CaseId;
					CES_Score.Case_Number__c  = CaseId;
					CES_Score.Unique_Id__c = CaseId;
					Database.upsert(CES_Score,externalIdCESScore,false) ; 

					
					isError = false;
				}else if (CES_Val == null || CES_Val == '' ) {
					 ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR,'Sorry, Error in Rating'));
					 CalloutService.generateServiceLog(JSON.serialize('{"message":"Error in Creating/Updating CES Record", "Error":"Error in Rating"}'), null, 'CES_CREATE_UPDATE_FAILED', null,CaseId);
				}else if (CaseId == null || CaseId == '' || ValidateCaseId(CaseId)) {
					 ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR,'Sorry, Error in ID'));
					 CalloutService.generateServiceLog(JSON.serialize('{"message":"Error in Creating/Updating CES Record", "Error":"Error in ID"}'), null, 'CES_CREATE_UPDATE_FAILED', null,CaseId);
				}
				
			} catch(Exception e) {
				isError = true;
				System.debug(e.getMessage());
				ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR,'Sorry, Error on Saving CES'));
				CalloutService.generateServiceLog(JSON.serialize('{"message":"Error in Creating/Updating CES Record", "Exception":"'+ e + '"}'), null, 'CES_CREATE_UPDATE_FAILED', null,CaseId);

			}
	}

     //*** submit button methos to submit feedback against CES***//
    global PageReference updateCES(){  

    	PageReference mainHome;
	    try{
	        if(!isError){
	            try{
	            	
	            	CES_Score.Feedback__c = feedback;	            	
	                Database.upsert(CES_Score,externalIdCESScore,false) ; 
	            }catch(DMLException dmle){
	            	isError = true;
	            	System.debug(dmle.getMessage());
	            }
	      
		        if(CES_Val == '1' || CES_Val == '2' || CES_Val == '3'){
		           mainHome= new PageReference('https://www.urbanladder.com/feedback-thankyou');
		        }else if(CES_Val == '4' || CES_Val == '5' || CES_Val == '6' || CES_Val == '7'){
		             mainHome= new PageReference('https://www.urbanladder.com/feedback-nexttime');
	           }
           	   mainHome.setRedirect(true);   
			}
	    }catch(Exception e){
	    	 isError = true;
	         system.debug('***e.getMessage()***'+e);
	         mainHome = null;
	    }
	    return mainHome;
	}

	//Checking for the valid ID + Case Object Id
	static public Boolean validateCaseId(String caseId) {
		Boolean isValid;
		try {
			//Get the Id 
			Id testId = Id.valueOf(caseId);
			//Get the Object
			String testIdObjectName = testId.getSObjectType().getDescribe().getName();
	         //Cehck for case-Obj
	         isValid = (testIdObjectName == 'Case')  ? true : false ;
		} catch(Exception e) {
			isValid = false;
			System.debug(e.getMessage());
		}
    	return isValid ;
	}

}