@isTest
private class CaseCustomAction_Test
{
    @isTest
    static void testNewCancellationCaseApproval()
    {
        Case approvalCase = new Case(Type = 'Cancellation', Sub_Category__c = 'Product Complaint',O_Code__c ='R702608126',OI_Code__c ='597069-44882');
        insert approvalCase;
        
        approvalCase = [SELECT CaseNumber FROM Case WHERE Id = :approvalCase.Id];
        
        Cancellation_Item__c eachCI = new Cancellation_Item__c(Case__c = approvalCase.id, O_Code__c ='R702608126', OI_Code__c ='597069-44882', Processing_Status__c='Not Processed', Approval_Status__c = 'Needs Approval');
        insert eachCI;
        
        String response = CaseCustomAction.updateCancellationStatus(approvalCase.CaseNumber);
                
        System.assertEquals('SUCCESS', response);
    }
    
    @isTest
    static void testApprovedCancellationCaseApproval()
    {
        Case approvalCase = new Case(Type = 'Cancellation', Sub_Category__c = 'Product Complaint',O_Code__c ='R702608126',OI_Code__c ='597069-44882');
        insert approvalCase;
        
        approvalCase = [SELECT CaseNumber FROM Case WHERE Id = :approvalCase.Id];
        
        Cancellation_Item__c eachCI = new Cancellation_Item__c(Case__c = approvalCase.id, O_Code__c ='R702608126', OI_Code__c ='597069-44882', Processing_Status__c='Not Processed', Approval_Status__c = 'Approved');
        insert eachCI;
        
        String response = CaseCustomAction.updateCancellationStatus(approvalCase.CaseNumber);
                
        System.assertEquals('NO_ITEM_TO_APPROVE', response);
    }
}