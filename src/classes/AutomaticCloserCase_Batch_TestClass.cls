@isTest(SeeAllData=true)
private class AutomaticCloserCase_Batch_TestClass{

    static testmethod void testLoadData() {
        
            Contact con = new Contact();
            con.Email = 'testContact@mailForContact.com';
            con.LastName = 'Pavithra';
            insert con ;
            
            
        List<Case> listOfCase = new List<Case>();
        case caseins = new case(Origin= 'Email',Subject = 'Cancellation Requested on Delivery',Description = 'Cancellation Requested on Delivery',After_Sales_Status__c = 'Repaired',status = 'Open');
        caseins.ContactId = con.Id ;
        insert caseins;
        QueueSobject queueOwner= new QueueSobject();
        queueOwner = [Select QueueId from QueueSobject where SobjectType = 'Case' AND Queue.Name = 'Customer Service - AfterSales' Limit 1];
        
        AutomaticCloserCase_Batch nn = new AutomaticCloserCase_Batch();
        database.executebatch(nn,200); 
        
        
    }
    
    static testMethod void myUnitTest2() {
        Test.startTest();
        scheduledBatch_AutomaticCaseCloser temp = new scheduledBatch_AutomaticCaseCloser();
        String sch = '0 0 23 * * ?'; 
        system.schedule('Test Territory Check', sch, temp); 
        Test.stopTest();
    }
}