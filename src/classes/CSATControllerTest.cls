//Author:   Vickal
//Dated:    March 10, 2016
//Purpose: unit testing for CSATController.

@isTest
private class CSATControllerTest {
    
    public static testMethod void pageTestWithYes(){
        list<case> cs = new list<case>();
        cs			  =	Utility_TestClass.createCases();
        cs[0].Priority='low';
        cs[0].Type	  =	'Feedback';
        cs[0].Subject = 'Test';
        cs[0].Description='abcd';
        insert cs[0];        
        PageReference PageRef = Page.CSAT_Chat_Page;
        Test.setCurrentPage(PageRef);
        ApexPages.currentPage().getParameters().put('id', cs[0].Id);
        ApexPages.currentPage().getParameters().put('cval', 'yes');
        CSATController csat = new CSATController();
        csat.saveCsatscores();
    }
    
    public static testMethod void pageTestWithNo(){
        list<case> cs = new list<case>();
        cs			  =	Utility_TestClass.createCases();
        cs[0].Priority='low';
        cs[0].Type	  =	'Feedback';
        cs[0].Subject = 'Test';
        cs[0].Description='abcd';
        insert cs[0];        
        PageReference PageRef = Page.CSAT_Chat_Page;
        Test.setCurrentPage(PageRef);
        ApexPages.currentPage().getParameters().put('id', cs[0].Id);
        ApexPages.currentPage().getParameters().put('cval', 'No');
        CSATController csat = new CSATController();
        csat.saveCsatscores();
    }
    
    public static testMethod void submitFeedbackTest(){
        list<case> cs = new list<case>();
        cs			  =	Utility_TestClass.createCases();
        cs[0].Priority='low';
        cs[0].Type	  =	'Feedback';
        cs[0].Subject = 'Test';
        cs[0].Description='abcd';
        insert cs[0]; 
        
        CSAT_Score__c cScr = new CSAT_Score__c();
        cScr			  =	Utility_TestClass.createCSAT(cs[0].Id);
        insert cScr;        
        PageReference PageRef = Page.CSAT_Chat_Page;
        Test.setCurrentPage(PageRef);
        ApexPages.currentPage().getParameters().put('id', cs[0].Id);
        ApexPages.currentPage().getParameters().put('cval', 'No');
        CSATController csat = new CSATController();
        csat.submitFeedback();
    }
}