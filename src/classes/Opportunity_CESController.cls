/*
*Purpose: Controller for the Opportunity CES_Page
*Date : 22/12/2016
*VERSION    AUTHOR                     DETAIL                                 
*1.0        Pavithra Gajendra          INITIAL DEVELOPMENT               
*
*/
global class Opportunity_CESController {
	
	//-----Varibles
	public String        CES_Val  {get; set;}
    public String        oppId   {get; set;}
    public CES_Score__c  CES_Score{get; set;}
    public Boolean       isError   {get; set;} 
    public String        feedback  {get; set;}
    Schema.SObjectField externalIdCESScore = CES_Score__c.Fields.Unique_Id__c ;
    
    
    //-----Constructor 
	global Opportunity_CESController() {

		//-----Set the isError flag to false	 
	 	system.debug('***Constructor Called ***');
	    isError = false;   
        CES_Score = new CES_Score__c();
	    
	}
	
	//------Init
	global void saveCES(){
		try {
			    CES_Val = ApexPages.CurrentPage().getparameters().get('cesVal');
		        oppId = ApexPages.CurrentPage().getparameters().get('oppId');
				
		        
				system.debug('***Page Action Method Called***');
				if((CES_Val != null || CES_Val != '') && ((oppId != null || oppId != '') && ValidateCaseId(oppId))){
										
					CES_Score.Score__c = CES_Val;
					CES_Score.Opportunity__c  = oppId;
					CES_Score.Unique_Id__c = oppId;
					Database.upsert(CES_Score,externalIdCESScore,false) ; 

					
					isError = false;
				}else if (CES_Val == null || CES_Val == '' ) {
					 ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR,'Sorry, Error in Rating'));
					 CalloutService.generateServiceLog(JSON.serialize('{"message":"Error in Creating/Updating CES Record", "Error":"Error in Rating"}'), null, 'CES_CREATE_UPDATE_FAILED', null,oppId);
				}else if (oppId == null || oppId == '' || ValidateCaseId(oppId)) {
					 ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR,'Sorry, Error in ID'));
					 CalloutService.generateServiceLog(JSON.serialize('{"message":"Error in Creating/Updating CES Record", "Error":"Error in ID"}'), null, 'CES_CREATE_UPDATE_FAILED', null,oppId);
				}
				
			} catch(Exception e) {
				isError = true;
				System.debug(e.getMessage());
				ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR,'Sorry, Error on Saving CES'));
				CalloutService.generateServiceLog(JSON.serialize('{"message":"Error in Creating/Updating CES Record", "Exception":"'+ e + '"}'), null, 'CES_CREATE_UPDATE_FAILED', null,oppId);
				

			}
	}

    //------- When the feedback data is filled In & submitted
    global PageReference updateCES(){

    	PageReference mainHome;
	    try{
	        if(!isError){
	            try{
	            	
	            	CES_Score.Feedback__c = feedback;	            	
	                Database.upsert(CES_Score,externalIdCESScore,false) ; 
	            }catch(DMLException dmle){
	            	isError = true;
	            	System.debug(dmle.getMessage());
	            }
	      
		        if(CES_Val == '1' || CES_Val == '2' || CES_Val == '3'){
		           mainHome= new PageReference('https://www.urbanladder.com/feedback-thankyou');
		        }else if(CES_Val == '4' || CES_Val == '5' || CES_Val == '6' || CES_Val == '7'){
		             mainHome= new PageReference('https://www.urbanladder.com/feedback-nexttime');
	           }
           	   mainHome.setRedirect(true);   
			}
	    }catch(Exception e){
	    	 isError = true;
	         system.debug('***e.getMessage()***'+e);
	         mainHome = null;
	    }
	    return mainHome;
	}

	//-------Checking for the valid ID + Opportunity Object Id
	static public Boolean validateCaseId(String oppId) {
		Boolean isValid;
		try {
			Id testId = Id.valueOf(oppId);
			//------ Get the Object
			String testIdObjectName = testId.getSObjectType().getDescribe().getName();
	         //Check Opp object
	         isValid = (testIdObjectName == 'Opportunity')  ? true : false ;
		} catch(Exception e) {
			isValid = false;
			System.debug(e.getMessage());
		}
    	return isValid ;
	}
    
    
    
    
}