global class LeadHistorySchedulable implements Schedulable{
    
      global void execute(SchedulableContext sc) {
      LeadHistoryBatch lhBatch = new LeadHistoryBatch(); 
      database.executebatch(lhBatch);
   }

}