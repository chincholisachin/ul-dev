/* Author : Upendar Shivanadri
   Description : Creating Multiple Opportunities based on Products and creating Contact roles 
                 -Add Products for the LEAD CONVERTION 
*/
   
public class LeadTriggerHandler {
    
    private static LeadTriggerHandler instance;
    
    //----CONSTANTS
    public static String QUALIFIED_STATUS = 'Qualified' ;
    public static String LEAD_STAGE_CLOSED = 'Closed' ;
    public static String KITCHENS_PRODUCT = 'Kitchens' ;
    public static String WARDROBES_PRODUCT = 'Wardrobes' ;
    public static String INTERIORS_PRODUCT = 'Interiors' ;
    public static String FULL_FURNITURE_PRODUCT = 'Full Furniture' ;
    public static String PARTIAL_FURNITURE_PRODUCT = 'Partial Furniture' ;
    public static String LOOSE_FURNITURE_PRODUCT = 'Loose Furniture' ;
    public static String COMPLETE_SERVICE_TYPE = 'complete service' ;
    public static String EXP_CENTRE_SOURCE = 'Experience centre' ;
    
    //*** Singleton Pattern to obtain instance of the Handler***
    public static LeadTriggerHandler getInstance(){
        if(instance==null){
            instance = new LeadTriggerHandler ();
        }
        return instance;
    }
    
    // Method for creating contact from email , firstname and lastname
    
    public void beforeInsertMethod(list<Lead> newList,map<ID,Lead> newMap){
        system.debug('*** Before Insert Method Called***');
        LeadService.createContactFromEmail(newList);
    }
    
   public void beforeUpdateMethod(list<lead> newList,map<Id,Lead> newmap,list<Lead> oldlist,map<Id,lead> oldmap) {
        User integrationUser = [select id from User where Name = 'Integration User'];
        List<Lead> leadList = new List<Lead>();
        
        for(Lead eachLead : newList) {
        system.debug('*****EachLead****'+eachLead );
            String oldProductValues = oldmap.get(eachLead.id).Products__c;
            List<String> allProducts = oldProductValues.split(';');
            String leadStatus = oldmap.get(eachLead.id).Status ; 
            String serviceType = oldmap.get(eachLead.id).Service_Type__c ;
            Set<String> allProductsSet = new Set<String>();
            for(String pro :allProducts){
                allProductsSet.add(pro);
            }
            String oldServiceOrderNumbers = oldmap.get(eachLead.id).Service_Order_Number__c;
            System.debug('Product Old '+oldProductValues);
            System.debug('Product New '+eachLead.Products__c);
            if(UserInfo.getUserId() == integrationUser.id && eachLead.Products__c != '' && eachLead.Products__c != null && eachLead.IsConverted == false && oldProductValues != eachLead.Products__c) {
                if(!allProductsSet.contains(eachLead.Products__c)){
                    eachLead.Products__c = oldProductValues  + ';' +eachLead.Products__c;   
                }else{
                    eachLead.Products__c = oldProductValues ;
                }                  
            }    
            
            if(leadStatus == QUALIFIED_STATUS){
                eachLead.Status = leadStatus ;
            }
            
            if(serviceType == COMPLETE_SERVICE_TYPE){
                eachLead.Service_Type__c = serviceType ;
            }
                
             if(eachLead.LastModifiedById ==integrationUser.id && oldServiceOrderNumbers!= null) {                
                eachLead.Service_Order_Number__c = oldServiceOrderNumbers;               
                system.debug('---eachLead.Service_Order_Number__c---'+eachLead.Service_Order_Number__c);
            }
            
            if(eachLead.Service_Order_Number__c =='') {
                oldServiceOrderNumbers =  eachLead.Service_Order_Number__c;
            }
            
            
             
        }    
        
        //Check for the Lead email
        Set<Id> closeLeadIds = new Set<Id>(); 
        for(Lead ld :newList){
             if(ld.Lead_stage__c == 'Closed' || ld.Lead_stage__c == 'Visited and lost'){
              //Get the lead Ids
              closeLeadIds.add(ld.Id);
          }
        }
       if(!closeLeadIds.isEmpty()){
           for(Event event: [Select Id,whoId,Status__c from Event where whoId IN :closeLeadIds and Status__c = 'Pending']){
               newmap.get(event.WhoId).addError(Label.Lead_Close_Error_Msg);
           }
       }
       
    }
    
    public void afterInsertMethod(list<Lead> newList){
      
    }
       
    //*** After Update Action***//
    public void afterUpdateMethod(list<lead> newList,list<Lead> oldList,map<ID,Lead> newMap,map<ID,Lead> oldMap){
        //createTasksFromLeadMethod(newList,oldMap);
         createOpportunities(newList,oldMap);
    }
    
    // This method is for creating multiple opportunities based on products.
    public void createOpportunities(list<lead> newList,map<ID,Lead> oldMap) {
        Map<Id,Id> ConvertedLeadWithOppMap = new Map<Id,Id>();
        List<Lead> ConvertedLeads = new List<Lead>();
        for(Lead lead : newList) {
            if(lead.isConverted && oldMap.get(lead.id).isConverted ==false) {
                ConvertedLeads.add(lead);
                ConvertedLeadWithOppMap.put(lead.id, lead.ConvertedOpportunityId);
            }
        }
        
        if(ConvertedLeads.size() > 0) {
            List<Opportunity> opportunityList = new List<Opportunity>();
            opportunityList = updateLeadOpportunities(ConvertedLeads, ConvertedLeadWithOppMap);
            
            // Added for opportunity contact roles
            List<OpportunityContactRole> oppContactRolelist = new List<OpportunityContactRole>();
            Map<id, OpportunityContactRole> opportunityContactRoleMap = new Map<id, OpportunityContactRole>();
            List<opportunity> oppList = [select id,AccountId, (select id, opportunityId, ContactId from OpportunitycontactRoles) from Opportunity where id IN: opportunityList];
            system.debug(oppList);
            for(Opportunity opp: oppList) {
                if(opp.OpportunitycontactRoles.size() > 0) {
                    opportunityContactRoleMap.put(opp.AccountId, opp.OpportunitycontactRoles[0]);
                }
            }
            
            for(Opportunity opp: oppList) {
                if(opp.OpportunitycontactRoles.size() == 0) {
                    if(opportunityContactRoleMap.containsKey(opp.AccountId)) {
                        OpportunityContactRole oppContactRoleDup = opportunityContactRoleMap.get(opp.AccountId);
                        OpportunityContactRole oppContactRole = new OpportunityContactRole();
                        oppContactRole.ContactId = oppContactRoleDup.ContactId;
                        oppContactRole.OpportunityId = opp.Id; 
                    oppContactRolelist.add(oppContactRole);
                    }
                }
            }
            
            if(oppContactRolelist.size() > 0) {
                insert oppContactRolelist;
            }
        }
    }

    public List<Opportunity> updateLeadOpportunities(List<Lead> ConvertedLeads, Map<Id, Id> ConvertedLeadWithOppMap){
        //Added requried fields to the Query - SF-577
        Map<Id, Opportunity> opportunityMap = new Map<Id, Opportunity>([select Id,Lead_Date__c,Pre_Proposal_link__c,Pre_Proposal_Sent_Date__c,LeadSource,Consultant_Name__c,Email__c,Phone__c,Service_Order_Number__c,House_Type__c,First_Visit_Date__c,Site_Street__c,Site_Area__c,Site_City__c,Site_State__c,Site_Zip_Code__c,StageName, CloseDate, AccountId,Sub_Stage__c,Proposal_Link_Interior__c,Proposal_Link_Kitchens__c,Proposal_Link_Wardrobes__c,Place_of_Visit__c,Lead_Call_Count__c,Lead_Id__c
                                                                            from Opportunity where id IN: ConvertedLeadWithOppMap.values()]);
        List<Opportunity> opportunityList = new List<Opportunity>();
        
        for(Lead convertedLead : ConvertedLeads) {
            if(ConvertedLeadWithOppMap.containsKey(convertedLead.Id) && opportunityMap.containsKey(convertedLead.ConvertedOpportunityId)) {  
                opportunity createdOpp;
                Integer parentCount = 0;
                if(convertedLead.products__c != null) {
                    String qualificationProductValue ;
                    System.debug('All multi products '+convertedLead.products__c);                      
                    String[] products = convertedLead.products__c.split(';');
                    System.debug('All products '+products);
                    for(Integer i=0, j=products.size(); i<j ;i++) {
                        if(products[i]==KITCHENS_PRODUCT){
                                qualificationProductValue = convertedLead.Qualification_Status_Kitchens__c ;
                        }else if(products[i]==WARDROBES_PRODUCT){
                                qualificationProductValue = convertedLead.Qualification_Status_Wardrobes__c ;
                        }else if(products[i]==INTERIORS_PRODUCT||products[i]==PARTIAL_FURNITURE_PRODUCT||products[i]==FULL_FURNITURE_PRODUCT){
                                qualificationProductValue = convertedLead.Qualification_Status_Interior__c ;
                        } else if(products[i]==LOOSE_FURNITURE_PRODUCT){
                                qualificationProductValue = convertedLead.Status ;
                        }
                        System.debug('Qualification Status '+products[i]+' '+ qualificationProductValue);
                        if(parentCount==0 && qualificationProductValue !='Unqualified') {
                            createdOpp = opportunityMap.get(ConvertedLeadWithOppMap.get(convertedLead.Id));
                            createdOpp.Name        = convertedLead.FirstName+' '+convertedLead.LastName+' '+products[i];
                            createdOpp.Requirement__c   = products[i];
                            createdOpp.CloseDate   = convertedLead.ConvertedDate+14;
                            createdOpp.Email__c= convertedLead.Email;
                            createdOpp.Phone__c= convertedLead.Phone; 
                            if (convertedLead.LeadSource == EXP_CENTRE_SOURCE && products[i]==LOOSE_FURNITURE_PRODUCT){
                                createdOpp.StageName   = 'New';
                            } else {
                                createdOpp.StageName   = 'Designing';
                                createdOpp.Sub_Stage__c = 'Req Documentation';
                            }
                            opportunityList.add(createdOpp);
                            parentCount++ ;
                        } else if(qualificationProductValue !='Unqualified') {
                            opportunity opp = createdOpp.clone(false,true);
                            opp.Name = convertedLead.FirstName+' '+convertedLead.LastName+' '+products[i];
                            opp.Requirement__c   = products[i];
                            opp.Opportunity__c = createdOpp.Id;
                            opp.Lead_Id__c = createdOpp.Lead_Id__c;
                            opportunityList.add(opp);
                        }
                    }
                }
            }
        }
        // Calling Custom Setting for Opportunity Validatons
            
        Opp_Validations__c ov =  Opp_Validations__c.getOrgDefaults();
        ov.Is_True__c = False;
        update ov;

        system.debug('******Opp Validations****'+ov);
        if(!opportunityList.isEmpty()) {
            system.debug('**** opportunityList **** '+opportunityList);
            upsert opportunityList;
        }

        ov.Is_True__c = True;
        update ov;
        system.debug('******Opp Validations****'+ov);

        return opportunityList;
    }
}