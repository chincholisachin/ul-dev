//Author:   Vickal
//Dated:    March 24, 2016
//Purpose: unit testing for OrderOItemPageController.

@isTest(seeAllData = false)
public class CancellationHelper_Test {

    public static testMethod void orderOITest(){
        /*list<contact> cont = new list<contact>();
        cont              = Utility_TestClass.createContacts();
        cont[0].Customer_ID__c = 23344;
        insert cont[0];
        
        
        
        system.assert(ord != null);
        
        orderitem__x oItem = new orderitem__x(ExternalID='OI123456');
        oItem.order_code__c = ord.Code__c;
        Database.insertAsync(oItem);
        
        
        list<case> csList   =   Utility_TestClass.createCases();
        insert csList;*/
       list<object> oreason =   new list<object>{'Bla_Bla'};
        CancellationHelper.OrderWrap    OWrp =  new CancellationHelper.OrderWrap('R321945713', 'ELIGIBLE',oreason);
        CancellationHelper.OrderItemWrap    OIWrp = new CancellationHelper.OrderItemWrap('R321945713', 'Hold', oreason,'');
        OIWrp.isSelected =TRUE;
        list<CancellationHelper.OrderItemWrap>  listOIWrp   =   new list<CancellationHelper.OrderItemWrap>{OIWrp};
        CancellationHelper.OrderAndOrderItemWrap    OOIWrp =    new CancellationHelper.OrderAndOrderItemWrap(OWrp, listOIWrp);
     
        //PageReference PageRef = Page.OrderOItmPage;
        //Test.setCurrentPage(PageRef);
        Order__x ord = new Order__x(ExternalID='R1234567', Code__c = 'R321945713');
        ord.consumer_id__c = 09090;
        Database.insertAsync(ord);
        ApexPages.StandardController stdController = new ApexPages.StandardController(ord);
        
        test.startTest();
        Test.setMock(HttpCalloutMock.class, new CancellationOIResponse());
        CancellationHelper cHelper = new CancellationHelper(stdController);
        CancellationHelper.sendCancellationCategory('R321945713','Cancellation');
        //CancellationHelper.saveCaseAndCancellationItems(csList[0], OOIWrp);
        CancellationHelper.makeOrderItemsInEligibleForCan(OOIWrp);
        Case eachCase = new Case(Type = 'After sales', Sub_Category__c = 'Product Complaint',Order__c ='R321945713',Order_Item__c = '988484-67239',O_Code__c ='R321945713',OI_Code__c ='988484-67239');
        insert eachCase;

        Cancellation_Item__c eachCI = new Cancellation_Item__c(Case__c = eachCase.id,O_Code__c ='R321945713',OI_Code__c ='988484-67239');
        insert eachCI;
        CancellationHelper.validate('R321945713',new set<string>{'988484-67239','988484-67240-1'},OOIWrp);
        CancellationHelper.saveCaseAndCancellationItems(new Case(Origin='Email',Type='General Enquiry'),OOIWrp);
        CancellationHelper.fetchOrderItemsByOrder('R321945713');

        test.stopTest();
        //OrderOItemPageController.getOrderItems(ord.code__c);
   }

   public static testmethod void testMethod2(){
            CancellationHelper.fetchCasesByOrdandOrdItems('R321945713',new set<string>{'988484-67239','988484-67240-1'});

            list<object> oreason =   new list<object>{'Bla_Bla'};
            CancellationHelper.OrderWrap    OWrp =  new CancellationHelper.OrderWrap('R321945713', 'ELIGIBLE',oreason);
        CancellationHelper.OrderItemWrap    OIWrp = new CancellationHelper.OrderItemWrap('R321945713', 'Hold', oreason,'');
        OIWrp.isSelected =TRUE;
        OIWrp.ApprovalStatus ='Needs Approval';
        OIWrp.total_price = 60000;

        list<CancellationHelper.OrderItemWrap>  listOIWrp   =   new list<CancellationHelper.OrderItemWrap>{OIWrp};
        CancellationHelper.OrderAndOrderItemWrap    OOIWrp =    new CancellationHelper.OrderAndOrderItemWrap(OWrp, listOIWrp);

        CancellationHelper.validate('R321945713',new set<string>{'988484-67239','988484-67240-1'},OOIWrp);

         CancellationHelper.saveCaseAndCancellationItems(new Case(Origin='Email',Type='General Enquiry'),OOIWrp);
   }
    
    public static testmethod void testMethod3(){
            
        CancellationHelper.sendCancellationCategory('R321945713','Cancellation');
            
        System_Configs__c texec = new System_Configs__c(Name='CancellationItem', Is_Run__c = true);
        insert texec;
        UL_EndPoint_URLs__c ue = new UL_EndPoint_URLs__c(Name='Cancellation_Order', End_point_URL__c = '/v1/orders/', Time_Out__c = 5);
        insert ue;
         UL_Wulverine_Credentials__c uc = new UL_Wulverine_Credentials__c(User_Name__c = 'salesforce@urbanladder.com', Password__c = 'VXJiYU5fRm9yY0U2NzM=', Name = 'Wulverine Server',Sandbox_Username__c = 'salesforce@urbanladder.com', Sandbox_Password__c	 = 'VXJiYU5fRm9yY0U2NzM=',Wulverine_Sandbox_Host__c='https://esgp3-wulverine.urbanladder.com',Wulverine_Production_Host__c='https://stg-salesforce.urbanladder.com');
        insert uc;
        /*Case caseInstance = new Case(Order_Item__c = '10395-R2345632',SuppliedEmail = 'test2@urbanLader.com', Status='Open', Origin=Constants.ORIGIN_EMAIL, Email_Responded__c=true);
        insert caseInstance;*/
        Test.setMock(HttpCalloutMock.class, new CancellationOIResponse());
        Test.startTest();
        CaseRecursion.fireTrigger = true;
        CancellationHelper.sendCancellationCategory('R321945713','Cancellation');
        Test.stopTest();  
   }
}