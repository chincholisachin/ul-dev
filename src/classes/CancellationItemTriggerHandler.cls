public class CancellationItemTriggerHandler {

    private static CancellationItemTriggerHandler instance;
    
    map<Id,list<Cancellation_Item__c>> caseCancellationItemsMap = new map<Id,list<Cancellation_Item__c>>();
    map<Id,map<String,list<Cancellation_Item__c>>> caseToCanStatusMap = new map<Id,map<String,list<Cancellation_Item__c>>>();
    map<String,list<Cancellation_Item__c>> canStatusMap;
    list<Case> caseListToUpdate = new list<Case>();
    public static map<String,case> caseMapToUpdate = new map<String,case>();
    list<Cancellation_Item__c> canItemList;
    map<id,set<ID>> caseToValidListMap =  new map<id,set<ID>>();
     Map<ID,case> MapOFCaseIdAndSubCat = new Map<ID,Case>();
    //*** Singleton Pattern to obtain instance of the Handler***
    public static CancellationItemTriggerHandler getInstance(){
        if(instance==null){
            instance = new CancellationItemTriggerHandler();
        }
        return instance;
    }



    public void afterInsertMethod(list<Cancellation_Item__c> newList,list<Cancellation_Item__c> oldList,map<ID,Cancellation_Item__c> newMap,map<ID,Cancellation_Item__c> oldMap){
        system.debug('*** Inside Cancellation Item Trigger Handler After Insert***');
        processCancellationItems(newList);
    }

    public void afterUpdateMethod(list<Cancellation_Item__c> newList,list<Cancellation_Item__c> oldList,map<ID,Cancellation_Item__c> newMap,map<ID,Cancellation_Item__c> oldMap){
        system.debug('*** Inside Cancellation Item Trigger Handler After Update ***');
        processCancellationItems(newList);
    }

    //*** After Update Action***//
    public void processCancellationItems(list<Cancellation_Item__c> newList){

    if(CaseRecursion.fireCancellation){
        system.debug('*** Count of Can item Fire**'); 
        set<ID> curCaseIDs = new set<ID>();
        
        for(Cancellation_Item__c eachCI:newList){
            curCaseIDs .add(eachCI.Case__c);
        }
        
        try{
            for(Case eachCase : [SELECT ID,CaseNumber,Sub_Category__c,Cancellation_Status__c,(Select id,Processing_Status__c,delivery_flag__c,Approval_Status__c,delivery_date__c,Case__r.Status,Case__r.Cancellation_Status__c from Cancellation_Items__r)from Case where Type='Cancellation' and ID IN:curCaseIDs and CreatedDate>2016-04-24T00:00:01Z]){
                caseCancellationItemsMap.put(eachCase.id,eachCase.Cancellation_Items__r);
                MapOFCaseIdAndSubCat.put(eachCase.id,eachCase);
            }

            system.debug('***caseCancellationItemsMap***'+caseCancellationItemsMap);
            system.debug('***Cancellation Trigger.New ***'+Trigger.New.size());
            
    //list<Cancellation_Item__c> validList;
    
    for(Cancellation_Item__c eachCanItem:newList){

        canItemList = new list<Cancellation_Item__c>();

        if(caseCancellationItemsMap.containsKey(eachCanItem.Case__c)){
            canItemList = caseCancellationItemsMap.get(eachCanItem.Case__c);
        }

        system.debug('***canItemList***'+canItemList);
        
        //validList = new list<Cancellation_Item__c>();
    
        canStatusMap = new map<String,list<Cancellation_Item__c>>();

        for(Cancellation_Item__c ci:canItemList){
            if(!canStatusMap.containsKey(ci.Approval_Status__c)){
                canStatusMap.put(ci.Approval_Status__c,new list<Cancellation_Item__c>{ci});
            }else{
                canStatusMap.get(ci.Approval_Status__c).add(ci);
            }
        }

        for(Cancellation_Item__c ci:canItemList){
            if(!canStatusMap.containsKey(ci.Processing_Status__c)){
                canStatusMap.put(ci.Processing_Status__c,new list<Cancellation_Item__c>{ci});
            }else{
                canStatusMap.get(ci.Processing_Status__c).add(ci);
            }
        }
        
        for(Cancellation_Item__c eachCanItemIns : canItemList){
            if((eachCanItemIns.Processing_Status__c == 'CANCEL_INITIATED' && eachCanItemIns.delivery_flag__c == FALSE) ||(eachCanItemIns.Processing_Status__c == 'CANCELLED')){
                system.debug('*** Adding into the valid list***');
                //validList.add(eachCanItemIns);
                if(caseToValidListMap.containsKey(eachCanItemIns.Case__c)){
                    caseToValidListMap.get(eachCanItemIns.Case__c).add(eachCanItemIns.ID);
                }else{
                    caseToValidListMap.put(eachCanItemIns.Case__c,new set<ID>{eachCanItemIns.ID});
                }
            }
        }
        
        system.debug('*** caseToValidListMap***'+caseToValidListMap);
        
        system.debug('***canStatusMap***'+canStatusMap);
        caseToCanStatusMap.put(eachCanItem.Case__c,canStatusMap);
        system.debug('***caseToCanStatusMap***'+caseToCanStatusMap);
        system.debug('***caseToCanStatusMap Size***'+caseToCanStatusMap.values().size());
        system.debug('***caseToValidListMap***'+caseToValidListMap);
        system.debug('***validList.size()***'+caseToValidListMap.size());
    }
     
    for(String caseId: caseToCanStatusMap.keyset()){
        Integer ValidListSize =0;
        map<String,list<Cancellation_Item__c>> canItemStatusMap = caseToCanStatusMap.get(caseId);
        
        if(caseToValidListMap.containsKey(caseID) && !caseToValidListMap.get(caseID).isEmpty()){
            ValidListSize = caseToValidListMap.get(caseId).size();
        }
        
        system.debug('***canItemStatusMap***'+canItemStatusMap);

        Integer ApprovedSize =0;
        Integer AutoApprovedSize=0;
        Integer RejectedSize=0;
        Integer NeedsApprovalSize=0;

        Integer CancelledSize=0;
        Integer CancelInitiatedSize=0;
        Integer CancellationRequestedSize = 0;

        if(canItemStatusMap.containsKey('Approved')){
            ApprovedSize = canItemStatusMap.get('Approved').size();
        }
        if(canItemStatusMap.containsKey('Auto Approved')){
            AutoApprovedSize = canItemStatusMap.get('Auto Approved').size();
        }
        if(canItemStatusMap.containsKey('Rejected')){
            RejectedSize = canItemStatusMap.get('Rejected').size();
        }
        if(canItemStatusMap.containsKey('Needs Approval')){
            NeedsApprovalSize = canItemStatusMap.get('Needs Approval').size();
        }
        if(canItemStatusMap.containsKey('CANCELLED')){
            CancelledSize = canItemStatusMap.get('CANCELLED').size();
        }
        if(canItemStatusMap.containsKey('CANCEL_INITIATED')){
            CancelInitiatedSize = canItemStatusMap.get('CANCEL_INITIATED').size();
        }
        if(canItemStatusMap.containsKey('Cancellation Requested')){
            CancellationRequestedSize = canItemStatusMap.get('Cancellation Requested').size();
        }
        
        Integer allItemsSize = ApprovedSize+AutoApprovedSize+RejectedSize+NeedsApprovalSize;
        
        system.debug('***canItemStatusMap***'+canItemStatusMap.values().size());

        system.debug('***ApprovedSize***'+ApprovedSize);
        system.debug('***AutoApprovedSize***'+AutoApprovedSize);
        system.debug('***RejectedSize***'+RejectedSize);
        system.debug('***canItemStatusMap.size()***'+canItemStatusMap.values().size());

        system.debug('***CancelledSize***'+CancelledSize);
        system.debug('***CancelInitiatedSize***'+CancelInitiatedSize);
        system.debug('***CancellationRequestedSize***'+CancellationRequestedSize);
        //system.debug('***validList.size()***'+validList.size());
        Case tempCase;
        if(!caseMapToUpdate.containsKey(caseId)){
        if(allItemsSize == ApprovedSize+AutoApprovedSize && NeedsApprovalSize==0 && CancelledSize==0 && CancelInitiatedSize==0 && CancellationRequestedSize ==0 && MapOFCaseIdAndSubCat.get(caseId).Cancellation_Status__c != 'Cancellation Requested' && MapOFCaseIdAndSubCat.get(caseId).Sub_Category__c != 'On Delivery'){
            system.debug('*** All are Approved or Auto Approved and Can/Can_Ini is Zero***');
            caseListToUpdate.add(new Case(id=caseId,Cancellation_Status__c='Process Cancellation',Cancellation_Approval_Pending__c = FALSE));
            tempCase = new Case(id=caseId,Cancellation_Status__c='Process Cancellation',Cancellation_Approval_Pending__c = FALSE);
            caseMapToUpdate.put(caseId,tempCase);
        }
        else if(allItemsSize ==  ApprovedSize+AutoApprovedSize+RejectedSize+NeedsApprovalSize && NeedsApprovalSize !=0 && CancelledSize==0 && CancelInitiatedSize==0 && MapOFCaseIdAndSubCat.get(caseId).Sub_Category__c != 'On Delivery'){
            system.debug('*** All are Approved or Auto Approved or Rej or Needs Approval and Can/Can_Ini is Zero***');
            caseListToUpdate.add(new Case(id=caseId,Cancellation_Status__c='Cancellation - On Hold',Cancellation_Approval_Pending__c = TRUE));
            tempCase = new Case(id=caseId,Cancellation_Status__c='Cancellation - On Hold',Cancellation_Approval_Pending__c = TRUE);
            caseMapToUpdate.put(caseId,tempCase);
        }
        else if(allItemsSize ==  ApprovedSize+AutoApprovedSize+RejectedSize && NeedsApprovalSize ==0 && CancelledSize==0 && CancelInitiatedSize==0 && (ApprovedSize!=0 || AutoApprovedSize!=0) && CancellationRequestedSize == 0 && MapOFCaseIdAndSubCat.get(caseId).Sub_Category__c != 'On Delivery'){
            system.debug('*** All are Approved or Auto Approved or Rej or Needs Approval and Can/Can_Ini is Zero***');
            caseListToUpdate.add(new Case(id=caseId,Cancellation_Status__c='Cancellation - On Hold',Cancellation_Approval_Pending__c = FALSE));
            tempCase = new Case(id=caseId,Cancellation_Status__c='Cancellation - On Hold',Cancellation_Approval_Pending__c = FALSE);
            caseMapToUpdate.put(caseId,tempCase);
        }
        else if(allItemsSize == RejectedSize && NeedsApprovalSize ==0 && CancelledSize==0 && CancelInitiatedSize==0){
            system.debug('*** All are Rejected and Can/Can_Ini is Zero***');
            caseListToUpdate.add(new Case(id=caseId,Cancellation_Status__c='Cancellation Rejected',Cancellation_Approval_Pending__c = FALSE));
            tempCase = new Case(id=caseId,Cancellation_Status__c='Cancellation Rejected',Cancellation_Approval_Pending__c = FALSE);
            caseMapToUpdate.put(caseId,tempCase);
            }
        else if(ApprovedSize+AutoApprovedSize== ValidListSize && NeedsApprovalSize == 0 && MapOFCaseIdAndSubCat.get(caseId).Sub_Category__c != 'On Delivery'){
            caseListToUpdate.add(new Case(id=caseId,Cancellation_Status__c='Cancelled'));
            tempCase = new Case(id=caseId,Cancellation_Status__c='Cancelled');
            caseMapToUpdate.put(caseId,tempCase);
        }
        /*else{
            caseListToUpdate.add(new Case(id=caseId,Cancellation_Status__c='Cancellation - On Hold'));
        }*/
        system.debug('***caseListToUpdate***'+caseListToUpdate);
        system.debug('***caseMapToUpdate***'+caseMapToUpdate);
        }
    }
        //CaseRecursion.fireTrigger = true;
        CaseRecursion.isComingFromCancellation = true;

        /*if(!caseListToUpdate.isEmpty()){
            update caseListToUpdate;    
        }*/
        
        if(!caseMapToUpdate.values().isEmpty()){
            update caseMapToUpdate.values();
        }
        }catch(Exception e){
        	CalloutService.generateServiceLog('Exception in Cancellation item Trigger -- newList--> '+JSON.serialize(newList), e,'CANCELLATIONITEM_CASE_CREATE_UPDATE', null, newList[0].id);
            system.debug('*** Exception in Cancellation item Trigger***'+e.getMessage());
        }
        }
    }
}