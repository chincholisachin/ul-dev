/* Author : Upendar Shivanadri
 Description : To track the agent assigned time through OMni Channel */
public class LeadHistoryBatch implements Database.Batchable<sObject>{
    
    public  Database.QueryLocator start(Database.BatchableContext BC) {
        try{    
            return Database.getQueryLocator([select id ,Lead_Assigned_Time__c,(select CreatedById,CreatedDate,Field,NewValue,OldValue FROM Histories ORDER BY CreatedDate DESC) from Lead where Lead_Assigned_Time__c = null]);
        }
        catch(Exception e) {
            system.debug('*** Exception in Lead History Batch Start***'+e.getMessage());
        }
        return null;
    }
    public void execute(Database.BatchableContext BC,List<Lead> scope) {
        
        Map<Id, User> userMap  = new Map<Id, User>([select id, Name from User where isActive=:true]);
        list<lead> leadListToUpdate = new list<lead>();
        try{
            if(!scope.isEmpty()){
                for (Lead eachLead : Scope) {
                    string userName = '';
                    if(eachLead.Histories!=null){
                        for(LeadHistory lh : eachLead.Histories) {
                            userName = userMap.containsKey(lh.CreatedById) ? userMap.get(lh.CreatedById).Name : '';
                            if(lh.Field == 'Owner' && lh.NewValue == userName) { 
                                eachLead.Lead_Assigned_Time__c = lh.createdDate;
                                system.debug('***** Lead Assigned Time ****'+ eachLead.Lead_Assigned_Time__c);
                                leadListToUpdate.add(eachLead);
                                break;
                                
                            }
                        }
                    }
                }
                LeadRecursion.fireTRigger = false;
                //database.update(scope, false);
                if(!leadListToUpdate.isEmpty()){
                	database.update(leadListToUpdate, false);
                }
                 
            }  
        }
        
        catch(Exception e) {
            system.debug('*** Exception in Lead History Batch***'+e.getMessage());
        }
        
    }
    public void finish(Database.BatchableContext BC) {
        
    }
}