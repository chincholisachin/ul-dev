global class CasePAPageController {
    
    public Order__x currentOrder {get;set;}
    public Contact currentOrderContact{get;set;}
    public List<SelectOption> caseStatusList {get; set;}
    public static list<orderitem__x> orderItemsRelatedToOrder_Filtered{get;set;}
    
    public Case newCase{get{return new case(recordTypeId = Constants.CASE_RECORD_TYPE_CUSTOM_PAGE);}set;}
    
    public CasePAPageController(ApexPages.StandardController controller) {
        caseStatusList      = new List<SelectOption>();
        Schema.DescribeFieldResult statusFieldResult    = Case.Status.getDescribe();
        List<Schema.PicklistEntry> statusList             = statusFieldResult.getPicklistValues();
        for(Schema.PicklistEntry tmpStatus : statusList) {
            if(tmpStatus.getValue() != 'Closed' && !(tmpStatus.getValue().contains('Cancellation') )){
            caseStatusList.add(new SelectOption(tmpStatus.getLabel(), tmpStatus.getValue()));
          }
        }
        String curOrdID = controller.getID();
        try{
            currentOrder = [SELECT ExternalId,id__c,consumer_id__c,Code__c,DC_facility__c,discount_total__c,facility_id__c,Current_facility_Id__c,nps_score__c,payment_method__c,status_text__c,total__c,voucher_code__c from Order__x where id=:curOrdID ];
            if(currentOrder!=null){
                currentOrderContact= [SELECT id,Customer_ID__c,firstName,LastName from Contact where Customer_ID__c=:currentOrder.consumer_id__c];
            }
        }catch(Exception e){
            system.debug('---currentOrder---'+currentOrder);
            system.debug('---currentOrderContact---'+currentOrderContact);
            system.debug('---Exception e ---'+e.getMessage());
        }
    }

    global class caseWrapper{
        public Case caseInstance;
        public Boolean isDuplicate;

        public caseWrapper(){
            caseInstance = new case();
            isDuplicate  = false;
        }
    }
    
    @RemoteAction
    global static caseWrapper createCaseFromInstance(Case caseInstance,String OrderCode,String OrderItemExternalID){
        
        caseWrapper returnCase = new caseWrapper();

        caseInstance =  PA_Utility.addOrderAndOIDetailsToCase(caseInstance,OrderCode,OrderItemExternalID);
        system.debug('---Inserted Case from Order Publisher Action---'+caseInstance);
        try{
            insert caseInstance;

        }catch(Exception e ){
            system.debug('---Exception---'+e.getMessage());
            if(e.getMessage().contains('DUPLICATE_VALUE, duplicate value found: AS_O_OLI__c')){
   
                Integer indexOfCaseId = e.getMessage().indexOf('500');
                String caseId = e.getMessage().mid(indexOfCaseId,15);
                system.debug('caseId' + caseId);  
                if(caseId.length() != -1){
                    //Get the Duplicate Case
                    caseInstance = new case(Id = caseId);
                    returnCase.isDuplicate  = true;
                }
             }
        }
         caseInstance = [select caseNumber from Case where id =: caseInstance.Id];
         returnCase.caseInstance = caseInstance;
        return returnCase;
    }
    
    @RemoteAction
    global static list<orderitem__x> getFilteredOrderItems(String category, String OrderId) {
        
        /*if(OrderCode!=null && FilterApplied=='DELIVERED'){
orderItemsRelatedToOrder_Filtered = [SELECT code__c,ExternalId,discount_total__c,FC_facility__c,facility_id__c,id__c,item_name__c,lms_status_string__c,order_code__c,total_price__c,lms_status_text__c from orderitem__x where order_code__c=:OrderCode AND lms_status_text__c=:'DELIVERED'];
}else{
orderItemsRelatedToOrder_Filtered = [SELECT code__c,ExternalId,discount_total__c,FC_facility__c,facility_id__c,id__c,item_name__c,lms_status_string__c,order_code__c,total_price__c,lms_status_text__c from orderitem__x where order_code__c=:OrderCode AND lms_status_text__c!=:'DELIVERED'];
} */
        system.debug(category);
        if(category != 'After sales') {
            orderItemsRelatedToOrder_Filtered = [SELECT code__c,ExternalId,discount_total__c,FC_facility__c,facility_id__c,id__c,item_name__c,lms_status_string__c,order_code__c,total_price__c,lms_status_text__c from orderitem__x where order_code__c =: OrderId];
        } else if(category == 'After sales'){
            orderItemsRelatedToOrder_Filtered = [SELECT channel__c,code__c,ExternalId,discount_total__c,FC_facility__c,facility_id__c,id__c,item_name__c,lms_status_string__c,order_code__c,total_price__c,lms_status_text__c from orderitem__x where order_code__c =: OrderId AND lms_status_text__c = 'DELIVERED'];
        }
        system.debug(orderItemsRelatedToOrder_Filtered);
        if(!orderItemsRelatedToOrder_Filtered.isEmpty()){
            return orderItemsRelatedToOrder_Filtered;
        }
        return null;
    }  
}