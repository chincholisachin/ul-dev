public class WS_AS_RequestFormatClass{
    public cls_order order;
    
    public class cls_order{
        public String code; //R591486531
        public String num;   //R591486531
        public cls_order_item order_item;
    }
    
    public class cls_order_item {
        public String code; //992498
        public String status;   //NEEDS_REPLACEMENT
    }
}