@isTest(seealldata=false)
private class OrderReplacementExtensionTest {
    
    private static testMethod void test1() {
        /*System_Configs__c texec = new System_Configs__c(Name='Case', Is_Run__c = true);
insert texec;
Account acc = new Account(Name= 'Test account'); 
insert acc;
contact con = new Contact(lastName = 'test last name', Email = 'test2@urbanLader.com');
insert con;
Entitlement ent = new Entitlement(Name='CS Standard SLA/Entitlement', Type ='Phone Support', AccountId=acc.Id);
insert ent;*/ //contactId=con.Id,EntitlementId=ent.Id,
        Case caseInstance = new Case(Error_Message__c = 'test', SuppliedEmail = 'test32@urbanLader.com', Status='Open', Origin=Constants.ORIGIN_EMAIL, Email_Responded__c=true);
        insert caseInstance;
        ApexPages.StandardController std = new ApexPages.StandardController(caseInstance);
        OrderReplacementExtension ext = new OrderReplacementExtension(std);
    }
    
    private static testMethod void test2() {
        Case caseInstance = new Case(Success_Message__c = 'test', SuppliedEmail = 'test3e2@urbanLader.com', Status='Open', Origin=Constants.ORIGIN_EMAIL, Email_Responded__c=true);
        insert caseInstance;
        ApexPages.StandardController std = new ApexPages.StandardController(caseInstance);
        OrderReplacementExtension ext = new OrderReplacementExtension(std);
    }
    
    private static testMethod void test3() {
        Case caseInstance = new Case(Replacement_Order_Placed_with_UL__c = true, Success_Message__c = 'test', SuppliedEmail = 'test3f2@urbanLader.com', Status='Open', Origin=Constants.ORIGIN_EMAIL, Email_Responded__c=true);
        insert caseInstance;
        ApexPages.StandardController std = new ApexPages.StandardController(caseInstance);
        OrderReplacementExtension ext = new OrderReplacementExtension(std);
    }
}