global class updateLeadSchedulable implements Schedulable{
    
      global void execute(SchedulableContext sc) {
      updateLeadBatchClass updateLead = new updateLeadBatchClass(); 
      database.executebatch(updateLead);
   }

}