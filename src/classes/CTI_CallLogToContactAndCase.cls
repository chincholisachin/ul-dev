public class CTI_CallLogToContactAndCase{

public static list<Contact> listOfContacts = new List<Contact>();
    
    public static String getContactIDFromPhone(WS_UL_SendCallLogsToSFDC.DataFromCall callLogWrapper){
        system.debug('*** incoming call log wrapper***'+callLogWrapper);
        string mobileNo = callLogWrapper.CallerID;
        
        if(mobileNo.startsWith('0') && mobileNo.length()== 11){
            mobileNo = mobileNo.subString(1,mobileNo.length());
        }else if(mobileNo.startsWith('91')){
            mobileNo = mobileNo.subString(2,mobileNo.length());
        }
        
        listOfContacts = [SELECT id,Name from Contact where Phone=:mobileNo];
         
         if(listOfContacts.size()>1){
            system.debug('*** Multiple Contacts with same phone no. found ***'+listOfContacts);
            return null;    
         }
         else if (!listOfContacts.isEmpty()){
            createTaskWithCallLogDetails(listOfContacts[0].ID,callLogWrapper);
            return listOfContacts[0].ID;
         }
         else if(listOfContacts.isEmpty()){
            //Create new contact logic here
         }
         return null;
    }
    
    public static void createTaskWithCallLogDetails(String contactID,WS_UL_SendCallLogsToSFDC.DataFromCall callLogWrapper){
        
        system.debug('***Contact ID***'+contactID);
        
        Case caseFound = findBestMatchingCase_existingCustomer(contactID,callLogWrapper);
        
        system.debug('***Best Matching Case Found is***'+caseFound);
          try {
                Task t = new Task();
                t.Subject='Call';
                t.Status='Completed';
                t.Priority='Normal';
                
                t.CallType='Inbound';
                t.CallObject = callLogWrapper.monitorUCID;
                t.CallDisposition = callLogWrapper.Disposition;
                
                t.whoid = contactID;
                if(caseFound!=null){
                    t.whatID = caseFound.ID;
                }
                
                
                t.Skill__c = callLogWrapper.Skill;
                t.UCID__c = callLogWrapper.monitorUCID;
                t.AudioFile__c = callLogWrapper.AudioFile;
                t.End_Time__c = DateTime.valueOf(callLogWrapper.EndTime);
                t.Start_Time__c = DateTime.valueOf(callLogWrapper.StartTime);
       
                t.CallerId__c = callLogWrapper.callerID;
                t.Location__c = callLogWrapper.location;
                t.Did__c = callLogWrapper.DID;
                
                insert t;
                system.debug('Upendar 0'+t);
          }
          
          catch(Exception ex) {
            
            system.debug('***Exception***'+ex.getMessage());
          
          }
    }
    
    public static Case findBestMatchingCase_existingCustomer(String contactID,WS_UL_SendCallLogsToSFDC.DataFromCall callLogWrapper){
        Datetime startTime = Datetime.valueOf(callLogWrapper.StartTime);
        Datetime endTime = Datetime.valueOf(callLogWrapper.EndTime);
        
        system.debug('***Contact ID***'+contactID);
        System.Debug('***startTime***'+startTime);
        System.Debug('***endTime***'+endTime);
        
        list<Case> caseList_New = new list<Case>();
        list<Case> caseList_Modified = new list<Case>();
        list<Case> caseList_Viewed = new list<Case>();
        
        caseList_New = [SELECT id,CreatedDate,LastViewedDate,LastModifiedDate from Case where ContactID=:contactID AND CreatedDate>=:startTime AND CreatedDate<=:endTime];
        caseList_Modified = [SELECT id,CreatedDate,LastViewedDate,LastModifiedDate from Case where ContactID=:contactID AND LastModifiedDate>=:startTime AND LastModifiedDate<=:endTime];
        caseList_Viewed = [SELECT id,CreatedDate,LastViewedDate,LastModifiedDate from Case where ContactID=:contactID AND LastViewedDate>=:startTime AND LastViewedDate<=:endTime];
        
        System.Debug('***caseList_New***'+caseList_New.size()+'***'+caseList_New);
        System.Debug('***caseList_New***'+caseList_Modified.size()+'***'+caseList_Modified);
        System.Debug('***caseList_New***'+caseList_Viewed.size()+'***'+caseList_Viewed);
        
        if(!caseList_New.isEmpty()){
            return caseList_New[0];
        }
        else if(!caseList_Modified.isEmpty()){
            return caseList_Modified[0];
        }
        else if(!caseList_Viewed.isEmpty()){
            return caseList_Viewed[0];
        }
        
        return null;
        
    }
    
    
}