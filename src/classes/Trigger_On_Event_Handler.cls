/*##############################################################################
########################## Trigger Handler #############################################

##Developed By : Baba S
##Created Date : 08 June 16
##Last MOdified Date : 08 June 16
##Company : ET Marlabs

##################################################################################
################################################################################*/
public class Trigger_On_Event_Handler 
{
    private static Trigger_On_Event_Handler instance;
    public static Trigger_On_Event_Handler getInstance()
    {
        if (instance == null) instance = new Trigger_On_Event_Handler();
        return instance;
    }
    //-- PUBLIC METHODS
    public void onBeforeInsert(List<Event> newEvents) 
    {
        
    }
    
    public void onBeforeUpdate(List<Event> oldEvents, Map<Id, Event> oldEventsMap,
                               List<Event> newEvents, Map<Id, Event> newEventsMap) 
    {
        //Get the Events  - status equal to 'Changed Visit Date'
        List<Event> changedPVDEventList = new List<Event>();
         Set<ID> eventsToProcess = new Set<ID>();
         Set<ID> leadsToProcess = new Set<ID>();
        
        for(Event event: newEvents) {
            if(event.Status__c == 'Rescheduled' &&
               event.Status__c != oldEventsMap.get(event.id).Status__c &&
               string.valueOf(event.WhoId).startsWith('00Q')){
                 changedPVDEventList.add(event);
               }
               
            if((event.Status__c == 'Completed' && oldEventsMap.get(event.ID).Status__c !='Completed') || (event.Status__c == 'Completed without payment' && oldEventsMap.get(event.ID).Status__c !='Completed without payment')||(event.Status__c == 'Cancelled' && oldEventsMap.get(event.ID).Status__c !='Cancelled')){
                if(string.valueOf(event.WhoId).startsWith('00Q')){
                    leadsToProcess.add(event.WhoID);
                }
                eventsToProcess.add(event.ID);
            }      
        }
        //Not Emty 
        system.debug('##### '+changedPVDEventList);
        if(!changedPVDEventList.isEmpty()) updateLeadPVD2Blank(changedPVDEventList);
        
        if(!leadsToProcess.isEmpty() &&!eventsToProcess.isEmpty() && System_Configs__c.getValues('Service Order Payment').Is_Run__c){
            Eventservice.processServiceOrderPaymentCallout(eventsToProcess,leadsToProcess);
        }
        
        
    }
    

    public void onAfterInsert(List<Event> newEvents, Map<Id, Event> newEventsMap)
    {
        
    }

    public void onAfterUpdate(List<Event> oldEvents, Map<Id, Event> oldEventsMap,
                               List<Event> newEvents,  Map<Id, Event> newEventsMap) 
    {
        //Check for LEAD / EVENT TAT
        LeadEventTAT(newEvents,oldEventsMap);
        
        //Check for the Cancelled / Completed / Re-Scheduled Events -> And Reminder must not be sent
        EventVisitReminderCheck(newEvents,oldEventsMap);
    }
    //--PRIVATE METHODS

      /*  Jira Ticket = SF-441
        Functionality Changed, So Not deleting the event & task but Updating the event 
            When the Status of visit/event has been set to 'Changed Visit Date', the following should be updated: 
            - Event should be reassigned to 'SF-Admin'
            - Lead owner should be reassigned to 'Sales Ops Queue'
            - Lead Stage should be set to 'Qualified'
            - Lead Sub stage should be set to 'To reshedule'
            - Proposed visit date should be set to blank.
    */
    private void updateLeadPVD2Blank(List<Event> eventList){
        
        Lead_Event_Owner__c leadEventOwners = [Select Id,Event_Owner__c,Lead_Owner__c from Lead_Event_Owner__c where name = 'LeadEventOwerShip'];

        System.debug('leadEventOwners *='+leadEventOwners);

        Set<Id> leadIds = new Set<Id>();
        //Get the Lead-Ids
        for(event event: eventList) leadIds.Add(event.WhoId);

        //Update the Event Owner - SF Admin
        List<event> events2Update = new List<event>();
        for(event eve :eventList){
            eve.OwnerId = leadEventOwners.Event_Owner__c;
            //events2Update.add(eve);
        }
        //if(!events2Update.isEmpty()) Update events2Update;


        //Update the Event Owner - SF Admin
        List<lead> leads2Update = new List<lead>();
        for(Lead lead :[Select Id,Lead_stage__c,Sub_Stage__c,Proposed_Visit_Date_1__c,Next_Action_Date__c,OwnerId from Lead 
                        where Id IN:leadIds AND IsConverted = false]){
            system.debug('lead '+lead);
            
            lead.Sub_Stage__c             = 'To Reschedule';
            lead.Proposed_Visit_Date_1__c = null;
            lead.OwnerId                  = leadEventOwners.Lead_Owner__c;

            leads2Update.add(lead);
        }
        system.debug('leads2Update ='+leads2Update);
        if(!leads2Update.isEmpty()) Update leads2Update;

    }

    private void LeadEventTAT(List<Event> newEvents,Map<Id, Event> oldEventsMap){

        //Leads && Status - pending || Completed 
        //Get the Events  - status equal to 'Changed Visit Date'
        List<Event> completeEventsTAT = new List<Event>();
        
        for(Event event: newEvents) {
        
            if(event.Is_Lead_Tat_Breached__c == true && 
               event.Is_Lead_Tat_Breached__c != oldEventsMap.get(event.id).Is_Lead_Tat_Breached__c && 
               String.valueOf(event.WhoId).startsWith('00Q'))
                completeEventsTAT.add(event);
        }

        //Get the Lead Ids
        List<Id> leadIds = new List<Id>();
        if(!completeEventsTAT.isEmpty()){
           for(Event event :completeEventsTAT){
                leadIds.add(event.WhoId);
           }
        }

        //Update the Non-Converted Leads && Events Ids - as the Complete Visit Lead TAT
        List<Lead> leadTATs = new List<Lead>();
        for(Lead ld : [Select Id,Visit_TAT_Breached__c from Lead where Id IN :leadIds AND IsConverted = false AND Lead_stage__c = 'Qualified' AND Sub_Stage__c = 'Visit scheduled']){
          
            ld.Visit_TAT_Breached__c = true;
            leadTATs.add(ld);
        }
        //Update the leads 
        if(!leadTATs.isEmpty()) update leadTATs;
    }
    
    /* 
        Check for the Cancelled / Completed / Re-Scheduled Events -> And Reminder must not be sent
        Now, If the event is marked as 'Cancelled' / 'Completed' / 'Re-Scheduled', 
        then the email reminder for the customer should not be sent.
    */
    private void EventVisitReminderCheck(List<Event> newEvents,Map<Id, Event> oldEventsMap){
         
        //Get the Leads, where the event status not equal to Pending ->'Changed Visit Date'
        Set<Id> leadIds = new Set<Id>();
        for(Event event: newEvents) {
            //Check for the event is changed 
            if(event.WhoId !=null && event.Status__c != null && event.Status__c != ''){
                    if(String.valueOf(event.WhoId).startsWith('00Q') && 
                    event.Status__c != 'Pending' &&
                    event.Status__c != oldEventsMap.get(event.id).Status__c){
                     System.debug(event.Status__c);
                     leadIds.Add(event.WhoId);
                 }  
           }
             
        }
        System.debug('leadIds '+leadIds);
        //Update the Lead's Reminder to customer to 'false'
        List<lead> leads2Update = new List<lead>();
        for(Lead lead :[Select Id,Send_Visit_Reminder__c  from Lead where Id IN:leadIds AND IsConverted = false]){
            
            lead.Send_Visit_Reminder__c               = FALSE;
            leads2Update.add(lead);
        }
        if(!leads2Update.isEmpty()) Update leads2Update;
        
    }
    
}