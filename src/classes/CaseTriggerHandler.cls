public  class CaseTriggerHandler{
    
    private static CaseTriggerHandler instance;
    
    
    //---- Attributes
    List<Case> cancellationCases = new List<Case>();
    Map<String, String> allCancellationCaseStatus = new Map<String,String>();
    
    //---- CONSTANTS
    public static String CANCELLATION_CASE = 'Cancellation' ;
    public static String CANCELLATION_ITEM_APPROVED_STATUS = 'Approved' ;
    public static String CANCELLATION_ITEM_REJECTED_STATUS = 'Rejected' ;
    public static String CANCELLATION_CASE_REJECTED = 'Cancellation Rejected' ;
    public static final String AUTO_APPROVED_STATUS = 'Auto Approved' ;
    public static final String PROCESS_CANCELLATION_STATUS = 'Process Cancellation' ;
    public static String CANCELLATION_CASE_REQUESTED = 'Cancellation Requested' ;
    public static String CANCELLATION_CASE_CREATED = 'Created' ;
    public static String CANCELLED_CASE = 'Cancelled';
    public static String CANCELLED_CASE_NO_REFUND = 'Not Applicable';
    public static String CANCELLED_CASE_ON_NOLD ='Cancellation - On Hold';
    public static boolean IS_TRIGGER_ALREADY_EXECUTED_FOR_BFL_ORDER_CONFIRMATION = false; // To avoid recursion
    
    
    //*** Singleton Pattern to obtain instance of the Handler***
    public static CaseTriggerHandler getInstance(){
        if(instance==null){
            instance = new CaseTriggerHandler();
        }
        return instance;
    }
    
    public void addCancellationStatus(){
        allCancellationCaseStatus.put('Created','cancellation_requested');
        allCancellationCaseStatus.put('Process Cancellation','cancellation_approved');
        allCancellationCaseStatus.put('Cancellation Rejected','cancellation_rejected');
        allCancellationCaseStatus.put('Refund Initiated','cancellation_refund_initiated');
        allCancellationCaseStatus.put('Refund Processed','cancellation_refund_processed');
        allCancellationCaseStatus.put('Refund Rejected','cancellation_refund_rejected');
        allCancellationCaseStatus.put('Not Applicable','cancellation_refund_na');
        
    }
    
    public void beforeInsertMethod(list<Case> newList,map<ID,Case> newMap){
        system.debug('*** Before Insert Method Called***');
        CaseService.createContactFromEmail(newList);
        CaseService.assignEntitlementToCase(Constants.ENT_NAME,newList);
        CaseService.duplicateFinanceCaseVerification(newList);
         if(System_Configs__c.getValues('EmailToCaseLoopBreak').Is_Run__c) {
            emailToCaseLoopBreaker(newList);
         }
    }
    
    public void afterInsertMethod(list<Case> newList,map<ID,Case> newMap){
        for(Case eachCase: newList){
            if(eachCase.Type==CANCELLATION_CASE && eachCase.Cancellation_Status__c==CANCELLATION_CASE_CREATED){
                List<String> allItems = new List<String>();
                if(eachCase.Item_Codes__c.contains(',')){
                     allItems = eachCase.Item_Codes__c.split(',') ;
                }else{
                     allItems.add(eachCase.Item_Codes__c) ;
                }
                System.debug('Cancellation Status-------- '+eachCase.Cancellation_Status__c);
                try{
	                if((eachCase.Status_sent_to_Order_Service__c =='' ||eachCase.Status_sent_to_Order_Service__c ==null) && ! CaseRecursion.isCancellationRequested){
	                	CaseRecursion.isCancellationRequested = true ;
	                	CaseService.sendCancellationCaseStatus(eachCase.Id,allItems,'cancellation_requested',eachCase.LastModifiedDate,eachCase.Order__c );	                	
	                }             
                }catch(Exception e){
                    
                }
            }       
        }
    }
    
    /* Places the replacement order request to UL Server, Replacement order logic will work with the single record and also with the bulk records(if the batch size is less than 50 */
    public void beforeUpdateMethod(list<case> newList, list<case> oldList, map<ID,Case> newMap, map<ID,Case> oldMap){
        
        list<Case> caseList = new list<Case>();
        list<Case> cancellationCaseList = new list<Case>();
        List<Case> DC_Ops_Case = new List<Case>();
        addCancellationStatus();
        //** Added this variable for the BFL Implementation
        
        list<Case> bflCasesToDCQueue = new list<Case>();
        
        for(Case eachCase : newMap.values()) {
            
            //------ For manual check of CNR
             if(oldMap.get(eachCase.Id).CNR__c != eachCase.CNR__c ){ 
                if(eachCase.CNR__c){
                    eachCase.Status ='Waiting' ;
                  }else{
                    eachCase.Status ='Open' ;
                     eachCase.SLA_On_Hold_Until_Datetime__c = null ;
                  }
            }
            
			//---- When auto approved process after cancellation requested . 
            if(eachCase.Type == CANCELLATION_CASE && eachCase.Cancellation_Status__c == PROCESS_CANCELLATION_STATUS && oldMap.get(eachCase.ID).Cancellation_Status__c ==CANCELLATION_CASE_CREATED){
            	eachCase.Cancellation_Case_Autoapproved__c = true ;
            }
            
            //---- To capture when case goes to waiting 
            if(oldMap.get(eachCase.Id).status!='Waiting' &&eachCase.Status=='Waiting'){             
                    eachCase.Wait_Start_Time__c = Datetime.now();                       
                System.debug('Waiting Time Start '+eachCase.Wait_Start_Time__c);
            }
            
            
            //Send Order & OrderItem id; once status is changed to "Needs Replacement".
            if(eachCase.After_Sales_Status__c != oldMap.get(eachCase.ID).After_Sales_Status__c && eachCase.After_Sales_Status__c == 'Needs Replacement') {
                caseList.add(eachCase);
            }
            
            //Populate DC_Ops related queue, once tagged to DC_Ops
            if(eachCase.ownerId != oldMap.get(eachCase.Id).OwnerId && eachCase.Owner_Name__c.StartsWithIgnoreCase('DC_OPS') && eachCase.Order__c != null) {
                system.debug('*** inside change of DC Ops to Queue***');
                DC_Ops_Case.add(eachCase);
            }
            
            if(eachCase.Type=='Cancellation' && eachCase.Cancellation_Status__c==PROCESS_CANCELLATION_STATUS && (eachCase.Cancellation_Status__c != oldMap.get(eachCase.ID).Cancellation_Status__c) && (eachCase.Cancellation_Status__c != 'Cancellation Requested' || oldMap.get(eachCase.ID).Cancellation_Status__c!='Cancellation Requested')){
                cancellationCaseList.add(eachCase);
            }
            
            if((eachCase.SF_ID__c != oldMap.get(eachCase.Id).SF_ID__c || eachCase.BFL_Status__c != oldMap.get(eachCase.Id).BFL_Status__c) && (eachCase.BFL_Status__c==Constants.BFL_STATUS_TO_DC)){
                bflCasesToDCQueue.add(eachCase);
            }
        }
        
        for(Case caseinstance : caseList) {
            if(caseInstance.OI_Code__c != null && caseInstance.OI_Code__c != '') {
                CaseService.placeReplacementOrderMethod(caseinstance.ID, caseinstance.Order__c, caseinstance.OI_Code__c);
            } /*----Commenting to eliminate dependency on order number --
            else {
                string oi_Code = caseinstance.Order_Item__c.split('-R')[0];
                if(oi_Code == null)  oi_Code = caseinstance.Order_Item__c;
                CaseService.placeReplacementOrderMethod(caseinstance.ID, caseinstance.Order__c, oi_Code); //caseinstance.Order_Item__c
            }*/
            caseInstance.Success_Message__c = 'Your Replacement Order request is processing...';
        }
        
        if(DC_Ops_Case.size() > 0) {
            CaseService.changeOwnerDCOpsQueue(DC_Ops_Case);
        }
        
        if(!cancellationCaseList.isEmpty() && cancellationCounter<1){
            system.debug('*** Calling Cancellation Processing ***'+cancellationCaseList);
            cancellationProcessing(newList,oldList,newMap,oldMap);
        }
        
        if(bflCasesToDCQueue.size()>0 && System_Configs__c.getValues('RunBFLCaseAssignment').Is_Run__c){
            assignBFLCasesToDCOps(bflCasesToDCQueue);
        }
        
        CaseAssignmentQueueForFulfilment(newMap,oldMap);
    }
    
    //*** After Update Action***//
    public void afterUpdateMethod(list<case> newList,list<case> oldList,map<ID,Case> newMap,map<ID,Case> oldMap){
        system.debug('*** After Update Method Called***');         
        CaseService.completeCaseMilestone(newMap,oldMap);
        
        List<Case> waitingToPendingCases = new  List<Case>();
        List<Case> casesToUpdate = new  List<Case>();
        cancellationCases = new List<Case>();
         
        /* SF-932 sent to Details order service i. order number ii. SF-id when BFL status changes to 'DO released & Confirmation pending' -- Start */
        
        proceedOrderConfrimation(newList,oldmap);
        
        /* SF-932 sent to Details order service i. order number ii. SF-id when BFL status changes to 'DO released & Confirmation pending' -- Ends */ 
         
        //------ Updated all task when case is closed
        List<Case> closedCases = new  List<Case>();
        for(Case eachCase : newList){
            if(eachCase.IsClosed){
                closedCases.add(eachCase);
            }
            
            //---Get all cases which ever from waiting status
            if(oldMap.get(eachCase.Id).status=='Waiting' && eachCase.Status!='Waiting' && CaseRecursion.fireTrigger){            
                waitingToPendingCases.add(eachCase);
                CaseRecursion.fireTrigger = false;
            } 
            
            //------ When case is rejected 
            if(eachCase.Type==CANCELLATION_CASE && CaseRecursion.isCustomerRequestNotSent && eachCase.Cancellation_Status__c==CANCELLATION_CASE_REJECTED && eachCase.Status_sent_to_Order_Service__c!='cancellation_rejected' && eachCase.Cancellation_Status__c != oldMap.get(eachCase.Id).Cancellation_Status__c){
                System.debug('Cancellation Case Status on rejected'+eachCase.Cancellation_Status__c);
                cancellationCases.add(eachCase);
            }   
            
            //------- When case is closed on cancellation & refund not applicable 
            if(eachCase.IsClosed && eachCase.Type==CANCELLATION_CASE && CaseRecursion.isCustomerRequestNotSent && eachCase.Cancellation_Status__c==CANCELLED_CASE && eachCase.Refund_Status__c==CANCELLED_CASE_NO_REFUND && eachCase.Status_sent_to_Order_Service__c!='cancellation_refund_na'){
                System.debug('Cancellation Case Status on NA'+eachCase.Refund_Status__c);
                cancellationCases.add(eachCase);
            }       
            
            //----- For all cancellation case send the status to order service on refund
           if(eachCase.Type == CANCELLATION_CASE && eachCase.Status_sent_to_Order_Service__c !='' && CaseRecursion.isCustomerRequestNotSent && (eachCase.Refund_Status__c != oldMap.get(eachCase.ID).Refund_Status__c) && allCancellationCaseStatus.containsKey(eachCase.Refund_Status__c) && eachCase.Refund_Status__c !=CANCELLED_CASE_NO_REFUND){
                System.debug('Cancellation Case Triggered '+eachCase);
                cancellationCases.add(eachCase);
			}
			
			//------ When its approved from approval queue 
			if(eachCase.Type == CANCELLATION_CASE && eachCase.Status_sent_to_Order_Service__c =='cancellation_requested' && CaseRecursion.isCustomerRequestNotSent && (eachCase.Cancellation_Status__c == PROCESS_CANCELLATION_STATUS) && eachCase.Status_sent_to_Order_Service__c=='cancellation_requested' && oldMap.get(eachCase.ID).Cancellation_Status__c ==CANCELLED_CASE_ON_NOLD){
					System.debug('Cancellation Case manually approved '+eachCase);
					cancellationCases.add(eachCase);
			} 
        }
        
        //---- Calculate waiting time for all the cases that moved from waiting
        if(!waitingToPendingCases.isEmpty()){
            for(Case newCases : [Select Id,LastModifiedDate,Wait_Start_Time__c,Waiting_Time__c FROM Case Where Id IN:waitingToPendingCases]){
                Long modifiedTime = newCases.LastModifiedDate.getTime();
                Long previousModifiedTime = newCases.Wait_Start_Time__c.getTime();
                Long totalNoOfMin = (modifiedTime - previousModifiedTime)/60000;
                System.debug('totalNoOfMin'+totalNoOfMin);
                if(newCases.Waiting_Time__c==null){
                    newCases.Waiting_Time__c = totalNoOfMin ;
                }else{
                    newCases.Waiting_Time__c = newCases.Waiting_Time__c + totalNoOfMin ;
                }                
                System.debug('Waiting Time '+newCases.Waiting_Time__c);
                casesToUpdate.add(newCases);
            }
            try{
            update casesToUpdate;
            }catch(Exception e){
            system.debug('--Exception---'+e.getMessage());
            //----Add Airbrake Call here
            }
        }
        
        
         //----- Process All cancellation Cases to update service layer
        if(!cancellationCases.isEmpty()){
            System.debug('Process All Cancellation Cases '+cancellationCases);
            processCancellationCase(cancellationCases,oldMap);
        }
    }

    public static Integer cancellationCounter = 0;
    
    //** Cancellation Processing Method **//
    public void cancellationProcessing(list<case> newList, list<case> oldList, map<ID,Case> newMap, map<ID,Case> oldMap){
        if(cancellationCounter<1){
            
            system.debug('***No. of Times Cancellation Called in this Transaction***'+cancellationCounter);
            list<Case> caseCancellationList = new list<Case>();
            String cancellationReasonCode='';
            
        for(Case eachCase:newList){
        //Place Order and Cancellation Items for Cancellation && eachCase.Status != oldMap.get(eachCase.ID).Status
            if(eachCase.Type=='Cancellation' && eachCase.Cancellation_Status__c=='Process Cancellation' && (eachCase.Cancellation_Status__c != oldMap.get(eachCase.ID).Cancellation_Status__c) && (eachCase.Cancellation_Status__c != 'Cancellation Requested' || oldMap.get(eachCase.ID).Cancellation_Status__c!='Cancellation Requested')){
                system.debug('*** Adding to CasecancellationList***');
                caseCancellationList.add(eachCase);
            }
        }
        
        system.debug('***CasecancellationList***'+caseCancellationList);

        //** Main Cancellation Related Code**//
        map<String,list<Cancellation_Item__c>> canItemMap = new map<String,list<Cancellation_Item__c>>();
        for(Cancellation_Item__c ci :[SELECT id,Case__c,Approval_Status__c,OI_Code__c from Cancellation_Item__c where Case__c IN:caseCancellationList]){
            if(!canItemMap.containsKey(ci.Case__c)){
                canItemMap.put(ci.Case__c,new list<Cancellation_Item__c>{ci});
            }else{
                canItemMap.get(ci.Case__c).add(ci);
            }
        }

        if(caseCancellationList.size()>0 && !canItemMap.isEmpty()){
            list<String> oiCodesToCancel;
            system.debug('*** inside caseCancellationList.size()>0***');
            for(Case caseInstance:caseCancellationList){
                 oiCodesToCancel = new list<String>();
                 system.debug('*** inside caseInstance***');
                 system.debug('*** canItemMap***'+canItemMap);
                 if(canItemMap.containsKey(caseInstance.id)){
                    for(Cancellation_Item__c ci : canItemMap.get(caseInstance.id)){
                        if(ci.Approval_Status__c =='Approved'||ci.Approval_Status__c =='Auto Approved'){
                            oiCodesToCancel.add(ci.OI_Code__c);
                        }
                    }
                 }
                 if(caseInstance.Reason_For_Cancellation__c!=null || caseInstance.Reason_For_Cancellation__c!=''){
                    //cancellationReasonCode = caseInstance.Reason_For_Cancellation__c.split(':')[0];
                    cancellationReasonCode = caseInstance.Reason_For_Cancellation__c;
                 }
                 system.debug('***cancellationReasonCode***'+cancellationReasonCode);

                 if(!oiCodesToCancel.isEmpty() && caseInstance.Order__c!=null && cancellationCounter<1){
                    system.debug('*** Calling Future Method ***');
                    cancellationCounter +=1;
                    CaseService.placeCancellationOrderMethod(caseInstance.ID,caseInstance.Order__c,cancellationReasonCode,caseInstance.lastModifiedDate,oiCodesToCancel);
                 }
            }
        }
        }

    }
    public void CaseAssignmentQueueForFulfilment(map<ID,Case> newMap, map<ID,Case> oldMap){
        List<Case> caseList = new List<Case>();
        List<Case> caseListFinal = new List<Case>();
        Set<ID> CaseIDForCancel = new Set<ID>();
        List<Cancellation_Item__c> cancellationItem =  new List<Cancellation_Item__c>();
        Map<Id,Set<String>> mapOfCaseIdAndRelatedOIItem = new Map<Id,Set<String>>();
        for(Case eachCase : newMap.values()) {
            if(eachCase.Pickup_Status__c != oldMap.get(eachCase.ID).Pickup_Status__c && oldMap.get(eachCase.ID).Pickup_Status__c == 'Pickup Pending') {
                system.debug('Yippy');
                System.debug(eachCase.id+'!!!!!!!!!!');
                caseList.add(eachCase);
                CaseIDForCancel.add(eachCase.id);
            }   
        }
        cancellationItem = [SELECT ID,O_Code__c,OI_Code__c,Case__c FROM Cancellation_Item__c WHERE Case__c IN: CaseIDForCancel];
        system.debug(cancellationItem+'WWWWWW');
        If(cancellationItem.size() > 0){
            for(Cancellation_Item__c  tempcan : cancellationItem){
                if(mapOfCaseIdAndRelatedOIItem.containsKey(tempcan.Case__c)){
                    mapOfCaseIdAndRelatedOIItem.get(tempcan.Case__c).add(tempcan.OI_Code__c);
                }else{
                    
                    mapOfCaseIdAndRelatedOIItem.put(tempcan.Case__c,new set<String>{tempcan.OI_Code__c});
                }
            }
        }
        system.debug(mapOfCaseIdAndRelatedOIItem+'mapOfCaseIdAndRelatedOIItem');
        if(caseList.size() > 0){
            system.debug(caseList+'XXXXXXx');
            for(Case tempCase: caseList){
                case caseinstance = new Case();
                if(mapOfCaseIdAndRelatedOIItem.containsKey(tempCase.id)){
                    system.debug('first If');
                    caseinstance = CaseAssignmentQueue.CaseAssignmentQueueMathod(tempCase,mapOfCaseIdAndRelatedOIItem.get(tempCase.id));
                    caseListFinal.add(caseinstance);
                    system.debug(caseinstance+'BBBBBB');
                }else{
                    system.debug('first If else');
                    caseinstance = CaseAssignmentQueue.CaseAssignmentQueueMathod(tempCase,new set<String>{});
                    caseListFinal.add(caseinstance);
                    system.debug(caseinstance+'AAAAAAA');
                }  
            }
            system.debug(caseListFinal+'XXXXXXx');
        }
        
        
    }
    
    public void emailToCaseLoopBreaker(list<Case> newList){
        
        list<String> suppliedEmailList = new list<String>();
        map<string,list<case>> caseSuppliedEmailToCaseMap = new map<string,list<case>>();
        

        for(Case eachCase:newList){
            if(eachCase.suppliedEmail!=null && eachCase.Description!=null && eachCase.origin=='Email'){
                if(!eachCase.Description.contains('[ ref:')){
                    suppliedEmailList.add(eachCase.suppliedEmail);
                }
            }
        }
        system.debug('---suppliedEmailList---'+suppliedEmailList);
        if(!suppliedEmailList.isEmpty()){
            caseSuppliedEmailToCaseMap = CaseService.getCasesBySuppliedEmailList(suppliedEmailList);
        }
        system.debug('---caseSuppliedEmailToCaseMap---'+caseSuppliedEmailToCaseMap);
        for(Case eachCase: newList){
            system.debug('---eachCase---'+eachCase);
            system.debug('---eachCase.Subject---'+eachCase.Subject);
            if(caseSuppliedEmailToCaseMap.containsKey(eachCase.suppliedEmail) && eachCase.origin=='Email'){
                for(Case eachExistingCase:caseSuppliedEmailToCaseMap.get(eachCase.suppliedEmail)){
                    system.debug('---eachExistingCase---'+eachExistingCase);
                    system.debug('---eachExistingCase.Subject---'+eachExistingCase.Subject);
                    if(eachCase.subject!=null && eachExistingCase.subject!=null){
                        if(eachCase.Subject.Contains(eachExistingCase.Subject) || eachExistingCase.Subject.Contains(eachCase.Subject)){
                            system.debug('---Match Found!---');
                            eachCase.Auto_Response_Disabled__c = true;
                            break;
                        }
                    }
                }
            }else if((eachCase.Subject == null || eachCase.Description == null) && eachCase.origin=='Email'){
                eachCase.Auto_Response_Disabled__c = true;
            }
        }
    }
    
    //-- Added this method as part of BFL Module
    public void assignBFLCasesToDCOps(list<Case> bflCasesToDCQueue){
        list<Case> casesToUpdate = new list<Case>();
        try{
            casesToUpdate = CaseService.changeOwnerDCOpsQueue(bflCasesToDCQueue);
            system.debug('---casesToUpdate---'+casesToUpdate);
            //update casesToUpdate;
        }catch(Exception e){
            system.debug('--Exception---'+e.getMessage());
            //----Add Airbrake Call here
        }
    }
    
        //----- Update Service Layer with Cancellation Status
    public void processCancellationCase(List<Case> allCancellationCases,map<ID,Case> oldMap){
        
        System.debug('Processing All cancellation Cases '+allCancellationCases.size());
        
        List<Service_Logs__c> serviceLogsList =  new List<Service_Logs__c>();
        
         //** Main Cancellation Related Code**//
        Map<String,list<String>> canItemMap = new Map<String,list<String>>();
        Map<String,list<String>> canStatusMap = new map<String,list<String>>();
        List<Cancellation_Item__c> cancelItems = new List<Cancellation_Item__c>();
        cancelItems = [SELECT id,Case__c,Approval_Status__c,OI_Code__c from Cancellation_Item__c where Case__c IN:allCancellationCases ORDER BY Case__c,Approval_Status__c] ;
        
        //----- Process all cancellation cases & add itemcodes to it 
        for(Cancellation_Item__c cancelItem : cancelItems){
            if(cancelItem.Approval_Status__c==CANCELLATION_ITEM_APPROVED_STATUS || cancelItem.Approval_Status__c==CANCELLATION_ITEM_REJECTED_STATUS || cancelItem.Approval_Status__c==AUTO_APPROVED_STATUS){
                if(cancelItem.Approval_Status__c==AUTO_APPROVED_STATUS){
                    cancelItem.Approval_Status__c= CANCELLATION_ITEM_APPROVED_STATUS ;
                }
                System.debug('Cancellation Item '+cancelItem);
                //------ Makes list of cancellation Item tagged to case with status 
                String case_status =cancelItem.Case__c+'_'+cancelItem.Approval_Status__c ;
                if(!canStatusMap.containsKey(case_status)){
                    canStatusMap.put(case_status,new list<String>{cancelItem.OI_Code__c});
                }else{
                    canStatusMap.get(case_status).add(cancelItem.OI_Code__c);
                }
            }
            //------- Map of case & its item codes
            if(!canItemMap.containsKey(cancelItem.Case__c)){
                canItemMap.put(cancelItem.Case__c,new list<String>{cancelItem.OI_Code__c});
            }else{
                canItemMap.get(cancelItem.Case__c).add(cancelItem.OI_Code__c);               
            }                      
        }
               
        System.debug('Cancellation Items '+canItemMap);
        
        //------ Send Cancellation Case status 
        if(allCancellationCases.size()>0 && !canItemMap.isEmpty()){         
            for(Case allCases :allCancellationCases){
                String statusValue ; 
                Integer bothChangedCount = 0 ;
                if(allCases.Cancellation_Status__c != oldMap.get(allCases.ID).Cancellation_Status__c && allCases.Cancellation_Status__c==PROCESS_CANCELLATION_STATUS){
                    statusValue = allCases.Cancellation_Status__c ;
                    bothChangedCount++ ;
                }else if(allCases.Refund_Status__c != oldMap.get(allCases.ID).Refund_Status__c && allCases.Cancellation_Status__c=='Cancelled' && allCases.Refund_Status__c!=CANCELLED_CASE_NO_REFUND){
                    statusValue = allCases.Refund_Status__c ;
                    bothChangedCount++ ;
                }else if(allCases.Refund_Status__c==CANCELLED_CASE_NO_REFUND && allCases.IsClosed && allCases.Cancellation_Status__c==CANCELLED_CASE){
                    statusValue = allCases.Refund_Status__c ;
                }else if(allCases.Cancellation_Status__c==CANCELLATION_CASE_REJECTED && allCases.Cancellation_Status__c != oldMap.get(allCases.Id).Cancellation_Status__c){
                    statusValue = allCases.Cancellation_Status__c ;                
                }
                
                System.debug('Request Status '+allCancellationCaseStatus.get(statusValue));
                 System.debug('Cancellation Status '+allCases.Cancellation_Status__c);
                
                List<String> allItemCodes = canItemMap.get(allCases.Id);
                if(allCancellationCaseStatus.get(statusValue)!=allCases.Status_sent_to_Order_Service__c && statusValue !=null){
                    CaseRecursion.isCustomerRequestNotSent = false ; 
                     Case cancellationCase ;
                    if(statusValue=='cancellation_refund_na'){
                        cancellationCase = new Case(Id=allCases.Id,Status_sent_to_Order_Service__c = statusValue);
                          try{
                               // Service log   
                               serviceLogsList.add(CalloutService.generateInformationLog(statusValue+'_Case_Update', JSON.serialize(cancellationCase), statusValue+'_Case_Update', cancellationCase.Id, 'Case', DateTime.now())); 
                               update cancellationCase ;
                          }catch(Exception e){
                               System.debug('Exception: '+ e);
                               // Sevice log
                               serviceLogsList.add(CalloutService.generateExceptionLog(statusValue+'_Case_Update_Failed', JSON.serialize(cancellationCase), cancellationCase.Id, 'Case', e,DateTime.now()));
                          }
                     }                  
                    //------- When Cancellation Case status is process cancellation from cancellation Rejected
                    if(statusValue==PROCESS_CANCELLATION_STATUS && oldMap.get(allCases.ID).Cancellation_Status__c==CANCELLATION_CASE_REJECTED){
                        List<String> approvedCodes = new List<String>();
                        List<String> rejectedCodes = new List<String>();
                        approvedCodes = canStatusMap.get(allCases.Id+'_'+CANCELLATION_ITEM_APPROVED_STATUS);
                        system.debug('---approvedCodes---'+approvedCodes);
                        rejectedCodes = canStatusMap.get(allCases.Id+'_'+CANCELLATION_ITEM_REJECTED_STATUS);
                        system.debug('---rejectedCodes---'+rejectedCodes);
                        CaseService.sendCancellationCaseStatus(allCases.Id,approvedCodes,'cancellation_approved',allCases.LastModifiedDate,allCases.Order__c );
                        CaseService.sendCancellationCaseStatus(allCases.Id,rejectedCodes,'cancellation_rejected',allCases.LastModifiedDate,allCases.Order__c );
                        // Service log  
                        serviceLogsList.add(CalloutService.generateInformationLog('cancellation_approved', JSON.serialize('When Cancellation Case status is process cancellation from cancellation Rejected --- '+allCases), 'cancellation_approved_Process', allCases.Id, 'Case', DateTime.now()));
                        serviceLogsList.add(CalloutService.generateInformationLog('cancellation_rejected', JSON.serialize('When Cancellation Case status is process cancellation from cancellation Rejected --- '+allCases), 'cancellation_rejected_Process', allCases.Id, 'Case', DateTime.now())); 
                               
                    }
                    //------- Need to confirm this scenario - where both cancellation status & refund status is updated
                    else if(bothChangedCount==2){
                        CaseService.sendCancellationCaseStatus(allCases.Id,allItemCodes,allCancellationCaseStatus.get(allCases.Cancellation_Status__c),allCases.LastModifiedDate,allCases.Order__c );
                        CaseService.sendCancellationCaseStatus(allCases.Id,allItemCodes,allCancellationCaseStatus.get(allCases.Refund_Status__c),allCases.LastModifiedDate,allCases.Order__c );
                        // Service log
                        serviceLogsList.add(CalloutService.generateInformationLog(allCancellationCaseStatus.get(allCases.Cancellation_Status__c), JSON.serialize('When both cancellation status & refund status is updated --- '+allCases), allCancellationCaseStatus.get(allCases.Cancellation_Status__c)+'_Process', allCases.Id, 'Case', DateTime.now()));
                        serviceLogsList.add(CalloutService.generateInformationLog(allCancellationCaseStatus.get(allCases.Refund_Status__c), JSON.serialize('When both cancellation status & refund status is updated --- '+allCases), allCancellationCaseStatus.get(allCases.Refund_Status__c)+'_Process', allCases.Id, 'Case', DateTime.now()));
                    }
                    //------- Send all the cancellation case status 
                    else{
                        CaseService.sendCancellationCaseStatus(allCases.Id,allItemCodes,allCancellationCaseStatus.get(statusValue),allCases.LastModifiedDate,allCases.Order__c );
                        // Service log
                        serviceLogsList.add(CalloutService.generateInformationLog(allCancellationCaseStatus.get(statusValue), JSON.serialize('Send all the cancellation case status --- '+allCases), allCancellationCaseStatus.get(statusValue)+'_Process', allCases.Id, 'Case', DateTime.now()));
                    }                
                }
            }
        } 
        // Insert service logs
        if(serviceLogsList.size() > 0)
            CalloutService.insertServiceLogs(serviceLogsList);      
    }
    
    /*** Below method Identify the cases that have status as Order confirmed 
    SF-932 sent to Details order service i. order number ii. SF-id when BFL status changes to 'DO released & Confirmation pending'
    ***/
    
    public void proceedOrderConfrimation(List<Case> caseList,Map<Id,Case> oldmap){
        
        // Check for the Feature Activation and notification recursion
        Feature_Activation_Deactivation_Setting__mdt BFLOrderConfirmationSetting = [select QualifiedApiName, Is_Active__c from Feature_Activation_Deactivation_Setting__mdt Where QualifiedApiName = 'BFL_Order_Confirmation_Callout' LIMIT 1];
        if(BFLOrderConfirmationSetting.Is_Active__c && !IS_TRIGGER_ALREADY_EXECUTED_FOR_BFL_ORDER_CONFIRMATION){
            
            
            Set<Id> BFLCaseIDs =  new Set<Id>();
            
            for(Case caseRecord : caseList){
                // Check for the BFL case and Order Number
                if(caseRecord.Type == Constants.CASE_TYPE_BFL && caseRecord.O_Code__c != null){
                    
                    //Check for BFL status
                    if(caseRecord.BFL_status__c != oldMap.get(caseRecord.Id).BFL_status__c && caseRecord.BFL_status__c == Constants.CASE_BFL_STATUS_DO_RELEASED_AND_CONFIRMATION_PENDING)
                        BFLCaseIDs.add(caseRecord.Id);
                }
            }
            
            // Check if BFLCaseIDs list has values
            if(BFLCaseIDs.size() > 0){
                IS_TRIGGER_ALREADY_EXECUTED_FOR_BFL_ORDER_CONFIRMATION = true;
                CaseService.BFLCaseConfirmationToOrderService(BFLCaseIDs,System.now());
            }
        }
    }
     
}