@isTest
global class CancellationOIResponse_Cancel_422 implements HttpCalloutMock {
    
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        // Optionally, only send a mock response for a specific endpoint
        // and method.
        //System.assertEquals('http://api.salesforce.com/foo/bar', req.getEndpoint());        //System.assertEquals('GET', req.getMethod());
        
        // Create a fake response
        String resBody =	'{"status":"failure","error":{"message":"{\"order_code\"=\"100000563\", \"eligibility\"=\"ELIGIBLE\", \"reason\"=[], \"order_items\"={\"716\"={\"eligibility\"=\"ELIGIBLE\", \"reason\"=[], \"parent_sku\"=nil}}}","errors":[{"message":"{\"order_code\"=\"100000563\", \"eligibility\"=\"ELIGIBLE\", \"reason\"=[], \"order_items\"={\"716\"={\"eligibility\"=\"ELIGIBLE\", \"reason\"=[], \"parent_sku\"=nil}}}","code":null,"data":null}]}}';
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');//'{"status": "success","data": {"consumer": {"code__c": R278679,"eligibility":"ELIGIBLE","email": "rachit.sharma77467450@test.com","prefix": 0,"first_name": "tes7754","last_name": "test75544","parent_consumer_id": null,"ship_address_id": 304016,"created_at": "2015-11-04T11:58:19.078Z","updated_at": "2015-11-04T11:58:19.078Z"}}}'
        res.setBody(resBody);
        res.setStatusCode(422);
        res.setStatus('error');
        return res;
    }
    
    

}