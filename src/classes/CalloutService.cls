public class CalloutService {
    
    /* HTTP Callout to External system */
    public static HTTPResponse callOutToExternalSystem(String userName, String password, String endPointUrl, Integer timeOut_x, String requestType, String jsonString) {
        system.debug('***requestType***'+requestType);
        // Specify the required user name and password to access the endpoint 
        // As well as the header and header information 
        HttpResponse response;
        Blob headerValue = Blob.valueOf(userName + ':' + password);
        String authorizationHeader = 'BASIC ' +
            EncodingUtil.base64Encode(headerValue);
        
        Httprequest request = new HttpRequest();
        Http http = new Http();
        request.setMethod(requestType);
        request.setEndpoint(endPointURL);
        request.setHeader('Content-Type', 'application/json');
        request.setHeader('Authorization', authorizationHeader);   // Header info with remote server user name and password
        request.setTimeout(timeOut_x); // timeout in milliseconds
        if(requestType=='POST'|| requestType=='PUT'){
            request.setBody(jsonString);
        }
        system.debug(jsonString);
        //Making call to external REST API
        system.debug('*** Request***'+request);
        response = http.send(request);
        system.debug('*** Response***'+response);
        /* } Catch(Exception e) {
/throw new customException(e.getLineNumber()+' '+e.getMessage());
} */
        return response;
    }
    
    /* */
    public static CalloutModel.params populateCalloutParams(String endPointUrlParam, String credentialsParam) {
        
        UL_Wulverine_Credentials__c uWC;
        UL_EndPoint_URLs__c uEU;
        
        uWC = UL_Wulverine_Credentials__c.getValues(credentialsParam);
        uEU = UL_EndPoint_URLs__c.getValues(endPointUrlParam);
        String userName ;
        String password ;
        String urlHost ;
        Boolean isSandboxEnvironment = [SELECT Id, IsSandbox FROM Organization LIMIT 1].IsSandbox ;
        if(isSandboxEnvironment){
        	userName = uWC.Sandbox_Username__c ;
        	password = EncodingUtil.base64Decode(uWC.Sandbox_Password__c).toString() ;
        	urlHost = uWC.Wulverine_Sandbox_Host__c ;
        }else{
        	userName = uWC.User_Name__c;
        	password = EncodingUtil.base64Decode(uWC.Password__c).toString();
        	urlHost = uWC.Wulverine_Production_Host__c ;
        }
        if(password.contains('\r\n')){
        	System.debug('Replace of \r\n');
        	password = password.replace('\r\n', '');
        }
        String endPointURL = urlHost + uEU.End_point_URL__c;
        return new CalloutModel.params(userName, password, endPointURL, (Integer)uEU.Time_Out__c);
    }
    
    // Log errors while interacting with the external system.
    public static void generateServiceLog(String jsonString, Exception ex, string serviceName, HttpResponse response, String recordId)  {
        
        Service_Logs__c sLogs = new Service_Logs__c(Callout_Service_Name__c = serviceName, Request_Format__c = jsonString, Log_Datetime__c = DateTime.now());
        if(response != null) {
            sLogs.Log_Message__c = response.getBody();
            sLogs.Status_Code__c = string.valueOf(response.getStatusCode());
            sLogs.Status_Msg__c = response.getStatus();
            sLogs.Record_Id__c = URL.getSalesforceBaseUrl().toExternalForm()+'/'+recordId;
        }
        if(ex != null)sLogs.Error_Log__c = ex.getLineNumber() + ' '+ ex.getCause() + ' ::: '+ ex.getMessage() + ' ::: '+ ex.getTypeName(); 
        insert sLogs;
    }
    
    /*** Capture callout Service log errors + add Exception if and only if its related to callout errors while interacting with the external system 
    Developer Note : 
    Params : calloutServiceName -- This is tag to idetify the Callout service for which callout is being failed (Ensure to Pass appropriate tag) 
             payload -- Request format
             response -- Response received after hitting the external servicerecordID -- object record Id to generate the link of the record
             typeOfObject -- Type of object
             exceptionError -- If there is an excpetion while calling the external system   
    ***/
    
    public static Service_Logs__c generateCalloutServiceLog(String calloutServiceName, String payload, HttpResponse response, String recordId, String typeOfObject, Exception exceptionError , DateTime logTimeStamp)  {
        
        Service_Logs__c sLogs = new Service_Logs__c(Callout_Service_Name__c = calloutServiceName, Request_Format__c = payload, Log_Datetime__c = logTimeStamp);
        sLogs.Service_Log_type__c = 'Callout Log';
        sLogs.Type_Of_Object__c = typeOfObject;
        
        if(response != null) {
            sLogs.Log_Message__c = response.getBody();
            sLogs.Status_Code__c = string.valueOf(response.getStatusCode());
            sLogs.Status_Msg__c = response.getStatus();
        }
        if(recordId != null){
            sLogs.Record_Id__c = URL.getSalesforceBaseUrl().toExternalForm()+'/'+recordId;
        }   
        if(exceptionError != null) sLogs.Error_Log__c = exceptionError.getLineNumber() + ' '+ exceptionError.getCause() + ' ::: '+ exceptionError.getMessage() + ' ::: '+ exceptionError.getTypeName(); 
        return sLogs;
    }
    
    /*** Capture Exception errors 
    Developer Note : 
    Params : serviceName -- This is tag to idetify the service for which system is throwing an exception (Ensure to Pass appropriate tag) 
             accociatedRecords -- record(s) which was/were under transaction while this exception occured. 
             recordID -- object record Id to generate the link of the record for which the exception occured
             typeOfObject -- Type of object
             exceptionError -- Exception instance   
    ***/
    
    public static Service_Logs__c generateExceptionLog(String serviceName, String accociatedRecords, String recordId, String typeOfObject, Exception exceptionError, DateTime logTimeStamp){
        
        Service_Logs__c sLogs = new Service_Logs__c(Callout_Service_Name__c = serviceName, Request_Format__c = accociatedRecords , Log_Datetime__c = logTimeStamp);
        sLogs.Service_Log_type__c = 'Exception Log';
        sLogs.Type_Of_Object__c = typeOfObject;
        
        if(recordId != null){
            sLogs.Record_Id__c = URL.getSalesforceBaseUrl().toExternalForm()+'/'+recordId;
        }   
        if(exceptionError != null) sLogs.Error_Log__c = exceptionError.getLineNumber() + ' '+ exceptionError.getCause() + ' ::: '+ exceptionError.getMessage() + ' ::: '+ exceptionError.getTypeName(); 
        return sLogs;
    }
    
    /*** Capture Information logs  
    Developer Note : 
    Params : functionalityName -- This is tag to idetify the SF functionality for which the Information is added (Ensure to Pass appropriate tag) 
             executionRecords -- record(s) which was/were under process while information is logged
             informationKeyword -- Basically gives the Idea about Information logs -Ex : If log is captured before hitting the web service(i.e. Responce body is null ) then informationKeyword --> ex : Cancellation_request_Initiated   
             recordID -- object record Id to generate the link of the record
             typeOfObject -- Type of object 
    ***/
    
    public static Service_Logs__c generateInformationLog(String functionalityName, String executionRecords, String informationKeyword, String recordId, String typeOfObject, DateTime logTimeStamp)  {
        
        Service_Logs__c sLogs = new Service_Logs__c(Callout_Service_Name__c = functionalityName, Request_Format__c = executionRecords, Log_Datetime__c = logTimeStamp);
        sLogs.Service_Log_type__c = 'Information Log';
        sLogs.Type_Of_Object__c = typeOfObject;
        
        if(informationKeyword  != null)
            sLogs.Status_Msg__c = informationKeyword ;
        if(recordId != null){
            sLogs.Record_Id__c = URL.getSalesforceBaseUrl().toExternalForm()+'/'+recordId;
        }
        return sLogs;
    }
    
    /*** Insert Service logs
    Params : serviceLogsList -- List of service logs 
    ***/
    
    public static void insertServiceLogs(List<Service_Logs__c> serviceLogsList)  {
        
        if(serviceLogsList.size() > 0)
            insert serviceLogsList ;
    }
}