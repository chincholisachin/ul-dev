public class CTIService {
    
    public static void processCallForLeads(WS_UL_SendCallLogsToSFDC.DataFromCall callLogWrapper){
        system.debug('--- Inside CTI Service ---');
        system.debug('--- Inside processCallForLeads ---');
        
        list<Lead> leadList = new list<Lead>();
        list<Contact> listOfContacts = new list<Contact>();
        
        String phoneNum = parsePhoneNumber(callLogWrapper.callerID);
        system.debug('***phoneNum'+phoneNum);
        
        String CustomerStatus = callLogWrapper.CustomerStatus;
        
        system.debug('---CustomerStatus---'+CustomerStatus);
        
        listOfContacts = ServiceFacade.fetchContactByPhone(phoneNum);
        leadList = ServiceFacade.fetchLeadsByPhone(phoneNum);
        Lead leadToProcess ;
        try{ 
	        if(!leadList.isEmpty()){
		        for(Lead eachLead :leadList){
		            if(eachLead.Lead_stage__c!='Closed'){
		                if(!eachLead.isConverted){
		                    leadToProcess = eachLead;
		                    break;
		                }
		                else{
		                    leadToProcess = eachLead;
		                }
		            }
		        }
	        }
	        
	        system.debug('---leadToProcess---'+leadToProcess);
	        system.debug('---listOfContacts---'+listOfContacts);
	        
	        if(!listOfContacts.isEmpty() && CustomerStatus !='Fail'){
	                ServiceFacade.createTasksforLeadCall(listOfContacts[0],leadToProcess,callLogWrapper);
	                
	                if(callLogWrapper.Status=='NotAnswered'){
	                    system.debug('---calling Not Answered Task Service from CTI Service---');
	                    // Call Task Method to Create Activity with In Complete
	                    ServiceFacade.createTasksforLeadCall_NotAnswered(listOfContacts[0],leadToProcess,callLogWrapper);
	                }
	            }
	       }catch(Exception e){
                system.debug('---Lead update failed---'+e.getMessage());
            }
    }
    
    
    
    public static void processCallToCreateTask(WS_UL_SendCallLogsToSFDC.DataFromCall callLogWrapper){
        system.debug('*** Inside CTI Service***');
        
        list<Contact> listOfContacts = new list<Contact>();
        String phoneNum = parsePhoneNumber(callLogWrapper.callerID);
        system.debug('***phoneNum'+phoneNum);
        listOfContacts = ServiceFacade.fetchContactByPhone(phoneNum);
        
        //listOfContacts = [SELECT id,Name,Phone from Contact where Phone=:phoneNum];
        system.debug('*** Contacts found by parsed phone number***'+listOfContacts);
        system.debug('*** Incoming Call Log Status ***'+callLogWrapper.status);
        
        list<case> caseListToUpdate = new list<case>();
        
        String typeOfCallLog ;
        String statusOfCallLog ;
        
        typeOfCallLog = callLogWrapper.Type.toLowerCase();
        String callSource = callLogWrapper.source ;
        statusOfCallLog = callLogWrapper.Status.toLowerCase();
     try{  	        	
        if(statusOfCallLog=='answered' && !listOfContacts.isEmpty()){
            
            system.debug('***Status=>Answered, Existing Contacts Found ***');
            list<task> updatedTasks = ServiceFacade.updateAllTasks(listOfContacts[0].Id);
            Case bestMatchingCase = ServiceFacade.findBestMatchingCase_existingCustomer(listOfContacts[0].Id,callLogWrapper,listOfContacts);
            if(bestMatchingCase==null){
                bestMatchingCase = ServiceFacade.fetchLastUpdatedOpenCaseByContact(listOfContacts[0]);
            }
            Task createdTask = ServiceFacade.createNewTaskFromCallWrapper(listOfContacts[0],bestMatchingCase,callLogWrapper);
            
            //--CNR Related Addition--
            //-- Moved this here from the check of openCNRTasks to make sure, Case is updated.
            //--Also we need to update Case to Pending State from Waiting state and CNR to be updated to false--
                if(bestMatchingCase.CNR__c){
                    Case newCase = new Case(ID=bestMatchingCase.ID,status='Open',CNR__c=false,ownerId=bestMatchingCase.OwnerId,SLA_On_Hold_Until_Datetime__c = null);
                    system.debug('---CNR---newCase'+newCase);
                    caseListToUpdate.add(newCase);
                    //Calling the Case Service
                    System.debug('----caseListToUpdate---'+caseListToUpdate);
                    ServiceFacade.updateCNRCase(caseListToUpdate);
                }
                
            //--Find if there are any open CNR tasks for the case, since the call is answered
           list<task> openCNRTasks;
           //Calling the task service
           openCNRTasks = ServiceFacade.findCNRTasksOfCase(listOfContacts[0],bestMatchingCase);
           system.debug('---CNR---openCNRTasks---'+openCNRTasks);
           if(openCNRTasks!=null){
            //--Update CNR tasks to complete
                ServiceFacade.updateCNRTasksOfCase(listOfContacts[0].ID,bestMatchingCase,openCNRTasks);
            
                
           }
        }
        else if((callSource =='' || callSource ==null || !callSource.contains('Website::')) && typeOfCallLog=='inbound' && !listOfContacts.isEmpty()){
            system.debug('***Type => Inbound, Existing Contacts Found ***');
            Case bestMatchingCase = ServiceFacade.findBestMatchingCase_existingCustomer(listOfContacts[0].Id,callLogWrapper,listOfContacts);
            if(bestMatchingCase==null){
                bestMatchingCase = ServiceFacade.fetchLastUpdatedOpenCaseByContact(listOfContacts[0]);
            }
            if(bestMatchingCase==null){
                bestMatchingCase = ServiceFacade.createNewCaseFromContact(listOfContacts[0]);
            }
            Task createdTask = ServiceFacade.createNewTaskFromCallWrapper(listOfContacts[0],bestMatchingCase,callLogWrapper);
            
            //------ Update all the cases CNR false has customer responded . 
            try{
                 List<Case> allCasesOfCustomer = new List<Case>();
                 List<Case> allCasesToUpdate = new List<Case>();
                 allCasesOfCustomer = [SELECT id,Origin,Status,Type,CNR__c,SLA_On_Hold_Until_Datetime__c,OwnerId from Case where ContactID IN:listOfContacts AND status='Waiting'];
                 for(Case eachCase : allCasesOfCustomer){
                    if(eachCase.CNR__c){
                        Case newCase = new Case(ID=eachCase.ID,status='Open',CNR__c=false,OwnerId=eachCase.OwnerId,SLA_On_Hold_Until_Datetime__c = null);
                        allCasesToUpdate.add(newCase);
                    }
                 }               
                 update allCasesToUpdate ;
                 list<task> openCNRTasks;
                 openCNRTasks = ServiceFacade.findCNRTasksOfCase(listOfContacts[0],bestMatchingCase);
                 system.debug('---CNR---openCNRTasks---'+openCNRTasks);
                    //----Update CNR tasks to complete
                    if(openCNRTasks!=null){                 
                        TaskService.updateCNRTasksOfContact(openCNRTasks);
                   }
            }catch(Exception e){
                system.debug('---Case CNR false update failed---'+e.getMessage());
            }
            
        }
        
        else if(statusOfCallLog!='answered' && !listOfContacts.isEmpty() && typeOfCallLog=='manual'){
            // Existing Contact => Existing Open Cases => Last Updated Case
            system.debug('***Status=> NotAnswered, Existing Contacts Found ***');
            Case bestMatchingCase = ServiceFacade.findBestMatchingCase_existingCustomer(listOfContacts[0].Id,callLogWrapper,listOfContacts);
            if(bestMatchingCase==null){
                bestMatchingCase = ServiceFacade.fetchLastUpdatedOpenCaseByContact(listOfContacts[0]);
            }
            /*
            if(bestMatchingCase==null){
                bestMatchingCase = ServiceFacade.createNewCaseFromContact(listOfContacts[0]);
            }
            */
            if(bestMatchingCase != null){
            	//------ Create New Task 
	            ServiceFacade.createNewTaskFromCallWrapper(listOfContacts[0],bestMatchingCase,callLogWrapper);
	            
	            if(callLogWrapper.CustomerStatus !='Fail' && bestMatchingCase.Status !='Closed'){
	                 //----- Get Next action Date 
	                Task_Due_Date_Calculator__c taskCalcInstance = Task_Due_Date_Calculator__c.getValues(bestMatchingCase.Type);
	                Decimal noOfDaysForDueDate = taskCalcInstance.No_of_Days__c;
	                Datetime slaHoldTime = Datetime.now().addDays(Integer.valueOf(noOfDaysForDueDate));  
	                
	                //--CNR Related Flow--//
	                Case newCase = new Case(ID=bestMatchingCase.ID,status='Waiting',CNR__c=true,SLA_On_Hold_Until_Datetime__c=slaHoldTime,ownerId=bestMatchingCase.OwnerId);
	                caseListToUpdate.add(newCase);
	                //Calling the Case Service
	                ServiceFacade.updateCNRCase(caseListToUpdate);
	            
	                list<task> openCNRTasks; 
	                 openCNRTasks = ServiceFacade.findCNRTasksOfCase(listOfContacts[0],bestMatchingCase);
	                    if(openCNRTasks == null){
	                        ServiceFacade.createCNRTasksOfCase(listOfContacts[0],bestMatchingCase,callLogWrapper);
	                    }
	            }        
            } else{
            	//------ Create New Task for contact 
	            ServiceFacade.createNewTaskForContact(listOfContacts[0],callLogWrapper);
            }             
         
        }
        
        else if(listOfContacts.isEmpty()){        	           	
        	if(callSource !=null && callSource !='' && callSource.contains('Website::')){
        		
        	}else{
        		 system.debug('***Status=> NotAnswered, Existing Contacts Not Found ***');
	            Case lastUpdatedCase;
	            Contact con = ServiceFacade.createNewContact(parsePhoneNumber(callLogWrapper.callerID));
	            if(con!=null){
	                lastUpdatedCase = ServiceFacade.createNewCaseFromContact(con);
	            }
	            
	            // Create New Task - 
	            ServiceFacade.createNewTaskFromCallWrapper(con,lastUpdatedCase,callLogWrapper);
        	}           
        }        
       }catch(Exception e){
       	 System.debug('---Call Log failed--- '+e.getMessage());
       }
    }
    
    public static string parsePhoneNumber(String phoneNumber){
        system.debug('*** Inside parsePhoneNumber Method ***');
        system.debug('*** phoneNumber from Call Log ***'+phoneNumber);
        
        if(phoneNumber!=null && phoneNumber.length()> 10){
            phoneNumber = phoneNumber.subString(phoneNumber.length() - 10,phoneNumber.length());
        }
        system.debug('***Parsed Phone Number***'+phoneNumber);
        return phoneNumber;
    }
}