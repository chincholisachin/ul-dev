@isTest(seealldata=false)
private class WS_UL_SendCallLogsToSFDC_Test {   
	
	static Task_Due_Date_Calculator__c dueDateCal ;
	static System_Configs__c caseTrigger ;
	 
	static void init(){
		dueDateCal = new Task_Due_Date_Calculator__c() ;
		dueDateCal.Name = 'General Enquiry';
		dueDateCal.No_of_days__c = 2;
		insert dueDateCal ;
		
		caseTrigger = new System_Configs__c();
		caseTrigger.Name = 'Case' ;
		caseTrigger.Is_Run__c = true ;
		insert caseTrigger ;
	}
    
    static testMethod void testParse() {
        
        /*String json=        '{"data":{"TransferredTo": "", "TransferType": "No Transfers", "Apikey": "KK8d89146fcb2d98ea13f93492c4cfdfdb", "TimeToAnswer": "", "Disposition": "", "AgentUniqueID": "None", "AgentID": "", "AgentStatus": "NotDialed", "CustomerStatus": "NoAnswer", "AgentPhoneNumber": "", "Status": "NotAnswered", "CallerID": "08050216053", "Duration": "00:00:00", "EndTime": "2015-11-06 12:49:24", "DialedNumber": "", "Comments": "", "monitorUCID": "3651446794333676", "Location": "", "UUI": "ABCD70", "StartTime": "2015-11-06 12:48:53", "Did": "918067417870", "Skill": "None", "FallBackRule": "", "UserName": "urbanladder_test", "PhoneName": "", "AgentName": "None", "Type": "IVR", "DialStatus": "Not Answered", "AudioFile": "", "HangupBy": "UserHangup"}'+
            ''+
            '}';*/
        Contact con = new Contact(LastName = 'test', Phone='9703994950');
        insert con;
        Task tsk = new Task(whoId=con.Id, UCID__c='684448634687733');
        insert tsk;
        init();
       Case eachCase = new Case();
        eachCase.Origin = 'Call';
        eachCase.Status = 'Open';
        eachCase.ContactId = con.id;
        insert eachCase;
        //do request
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/UL_SendCallLogsToSFDC/';  
        req.addParameter('data','{"Location": "", "PhoneName": "Sachindev", "DialStatus": "Not Answered", "Status": "NotAnswered", "HangupBy": "AgentHangup", "TransferType": "No Transfers", "StartTime": "2015-12-02 01:17:00", "Skill": "None", "UUI": "", "Duration": "00:00:00", "Apikey": "KK8d89146fcb2d98ea13f93492c4cfdfdb", "TimeToAnswer": "", "FallBackRule": "AgentDial", "DialedNumber": "7795122320", "monitorUCID": "6844486346877336", "TransferredTo": "", "CallerID": "9703994950", "AgentUniqueID": "None", "AudioFile": "", "Type": "manual", "AgentName": "None", "Disposition": "", "EndTime": "2015-12-02 01:20:00", "UserName": "urbanladder_test", "Comments": "", "Did": "918067417870", "AgentID": "", "AgentPhoneNumber": "7795122320", "CustomerStatus": "", "AgentStatus": "","source":""}');
       // req.requestBody = blob.valueOf('');
        
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        WS_UL_SendCallLogsToSFDC.sendCallLogs();
        
        WS_UL_SendCallLogsToSFDC.DataFromCall wrap = new WS_UL_SendCallLogsToSFDC.DataFromCall();
        //system.assertEquals(con.Id, );
        //wrap.monitorUCID='123';
        //wrap.callerID='9703994950';
        // CTI_CallLogToContactAndCase.getContactIDFromPhone(wrap);
    }
    
    static testMethod void testParse_1() {
        
        Contact con = new Contact(LastName = 'test', Phone='9703994950');
        insert con;
        Task tsk = new Task(whoId=con.Id, UCID__c='684448634687733');
        insert tsk;
        init();
        Case eachCase = new Case();
        eachCase.Origin = 'Call';
        eachCase.Status = 'Open';
        eachCase.ContactId = con.id;
		insert eachCase ;
        //do request
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/UL_SendCallLogsToSFDC/';  
        req.addParameter('data','{"Location": "", "PhoneName": "Sachindev", "DialStatus": "Not Answered", "Status": "NotAnswered", "HangupBy": "AgentHangup", "TransferType": "No Transfers", "StartTime": "2015-12-02 01:17:00", "Skill": "None", "UUI": "", "Duration": "00:00:00", "Apikey": "KK8d89146fcb2d98ea13f93492c4cfdfdb", "TimeToAnswer": "", "FallBackRule": "AgentDial", "DialedNumber": "7795122320", "monitorUCID": "684448634687733", "TransferredTo": "", "CallerID": "9703994950", "AgentUniqueID": "None", "AudioFile": "", "Type": "manual", "AgentName": "None", "Disposition": "", "EndTime": "2015-12-02 01:20:00", "UserName": "urbanladder_test", "Comments": "", "Did": "918067417870", "AgentID": "", "AgentPhoneNumber": "7795122320", "CustomerStatus": "", "AgentStatus": "","source":""}');
        
        
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        WS_UL_SendCallLogsToSFDC.sendCallLogs();
        
        WS_UL_SendCallLogsToSFDC.DataFromCall wrap = new WS_UL_SendCallLogsToSFDC.DataFromCall();
      
    }
    
    static testMethod void testParse_11() {
        
        Contact con = new Contact(LastName = 'test', Phone='9703994950');
        insert con;
        Task tsk = new Task(whoId=con.Id, UCID__c='684448634687733');
        insert tsk;
        init();
        Case eachCase = new Case();
        eachCase.Origin = 'Call';
        eachCase.Status = 'Open';
        eachCase.ContactId = con.id;
		insert eachCase ;
		Profile profileId = [SELECT Id FROM Profile WHERE Name = 'Consultant' LIMIT 1];
        
     	User usr = new User(LastName = 'LIVESTONDummyUser',
                           FirstName='JASON',
                           Alias = 'jliv',
                           Email = 'jason.liveston@DummyUser.com',
                           Username = 'jason.liveston@DummyUser.com1',
                           ProfileId = profileId.id,
                           TimeZoneSidKey = 'GMT',
                           LanguageLocaleKey = 'en_US',
                           EmailEncodingKey = 'UTF-8',
                           LocaleSidKey = 'en_US'
                           );
        insert usr;
        
        Lead_Profile_Names__c lp = new Lead_Profile_Names__c(name='1',Profile_Names__c='Consultant');
        insert lp;                   
		
        //do request
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/UL_SendCallLogsToSFDC/';  
        req.addParameter('data','{"Location": "", "PhoneName": "Sachindev", "DialStatus": "Not Answered", "Status": "NotAnswered", "HangupBy": "AgentHangup", "TransferType": "No Transfers", "StartTime": "2015-12-02 01:17:00", "Skill": "None", "UUI": "", "Duration": "00:00:00", "Apikey": "KK8d89146fcb2d98ea13f93492c4cfdfdb", "TimeToAnswer": "", "FallBackRule": "AgentDial", "DialedNumber": "7795122320", "monitorUCID": "684448634687733", "TransferredTo": "", "CallerID": "9703994950", "AgentUniqueID": "None", "AudioFile": "", "Type": "manual", "AgentName": "None", "Disposition": "", "EndTime": "2015-12-02 01:20:00", "UserName": "urbanladder_test", "Comments": "", "Did": "918067417870", "AgentID": "upendaret@etmarlabs.com", "AgentPhoneNumber": "7795122320", "CustomerStatus": "answered", "AgentStatus": "","source":""}');
        
        
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        WS_UL_SendCallLogsToSFDC.sendCallLogs();
        
        WS_UL_SendCallLogsToSFDC.DataFromCall wrap = new WS_UL_SendCallLogsToSFDC.DataFromCall();
      
    }
    
    
    static testMethod void testParse2() {
             
        Contact con = new Contact(lastName = 'Test Name', phone = '9703994950');
        insert con;
        
        Task tsk = new Task(whoId=con.Id, UCID__c='684448634687733');
        insert tsk;
        init();
        Case cse = new Case(contactId=con.Id);
        insert cse;
        DateTime createdDateTime = [select createdDate from case where id=:cse.Id].createddate;
        system.debug('CreatedDate'+createdDateTime);
        DateTime startdt = createdDateTime.addminutes(-2);
        DateTime enddt = createdDateTime.addminutes(2);
        
        //do request
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        
        req.requestURI = '/services/apexrest/UL_SendCallLogsToSFDC/';  
        req.addParameter('data','{"Location": "", "PhoneName": "Sachindev", "DialStatus": "Not Answered", "Status": "Answered", "HangupBy": "AgentHangup", "TransferType": "No Transfers", "StartTime": "'+startdt+'", "Skill": "None", "UUI": "", "Duration": "00:00:00", "Apikey": "KK8d89146fcb2d98ea13f93492c4cfdfdb", "TimeToAnswer": "", "FallBackRule": "AgentDial", "DialedNumber": "917795122320", "monitorUCID": "6844486346877336", "TransferredTo": "", "CallerID": "9703994950", "AgentUniqueID": "None", "AudioFile": "", "Type": "manual", "AgentName": "None", "Disposition": "", "EndTime":"'+ enddt+'", "UserName": "urbanladder_test", "Comments": "", "Did": "918067417870", "AgentID": "", "AgentPhoneNumber": "7795122320", "CustomerStatus": "", "AgentStatus": "","source":""}');
        
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        WS_UL_SendCallLogsToSFDC.sendCallLogs();
        
        WS_UL_SendCallLogsToSFDC.DataFromCall wrap = new WS_UL_SendCallLogsToSFDC.DataFromCall();
        //system.assertEquals(con.Id, );
    }
    
    static testMethod void testParse3() {
        
        Contact con = new Contact(lastName = 'Test Name', phone = '9703994950');
        insert con;
        init();
         Case eachCase = new Case();
        eachCase.Origin = 'Call';
        eachCase.Status = 'Open';
        eachCase.ContactId = con.id;
        insert eachCase;
        Task tsk = new Task(whoId=con.Id, UCID__c='684448634687733');
        insert tsk;
        
        //do request
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/UL_SendCallLogsToSFDC/';  
        req.addParameter('data','{"Location": "", "PhoneName": "Sachindev", "DialStatus": "Not Answered", "Status": "NotAnswered", "HangupBy": "AgentHangup", "TransferType": "No Transfers", "StartTime": "2015-12-02 01:17:00", "Skill": "None", "UUI": "", "Duration": "00:00:00", "Apikey": "KK8d89146fcb2d98ea13f93492c4cfdfdb", "TimeToAnswer": "", "FallBackRule": "AgentDial", "DialedNumber": "7795122320", "monitorUCID": "6844486346877336", "TransferredTo": "", "CallerID": "9703994950", "AgentUniqueID": "None", "AudioFile": "", "Type": "manual", "AgentName": "None", "Disposition": "", "EndTime": "2015-12-02 01:20:00", "UserName": "urbanladder_test", "Comments": "", "Did": "918067417870", "AgentID": "", "AgentPhoneNumber": "7795122320", "CustomerStatus": "", "AgentStatus": "","source":""}');
        
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
       WS_UL_SendCallLogsToSFDC.sendCallLogs();
        
        WS_UL_SendCallLogsToSFDC.DataFromCall wrap = new WS_UL_SendCallLogsToSFDC.DataFromCall();
        //system.assertEquals(con.Id, );
    }
    
    static testMethod void testParse4() {
        
        Contact con = new Contact(lastName = 'Test Name', phone = '9848098480');
        insert con;
        init();
        Case eachCase = new Case();
        eachCase.Origin = 'Call';
        eachCase.Status = 'Open';
        eachCase.ContactId = con.id;
        insert eachCase;
        
        Task tsk = new Task(whoId=con.Id, UCID__c='684448634687733');
        insert tsk;
     
        WS_UL_SendCallLogsToSFDC.DataFromCall callData_Test = new  WS_UL_SendCallLogsToSFDC.DataFromCall();
       	callData_Test  = (WS_UL_SendCallLogsToSFDC.DataFromCall)System.JSON.deserialize('{"Location": "", "PhoneName": "Sachindev", "DialStatus": "Not Answered", "Status": "NotAnswered", "HangupBy": "AgentHangup", "TransferType": "No Transfers", "StartTime": "2015-12-02 01:17:00", "Skill": "None", "UUI": "", "Duration": "00:00:00", "Apikey": "KK8d89146fcb2d98ea13f93492c4cfdfdb", "TimeToAnswer": "", "FallBackRule": "AgentDial", "DialedNumber": "7795122320", "monitorUCID": "6844486346877336", "TransferredTo": "", "CallerID": "9848098480", "AgentUniqueID": "None", "AudioFile": "", "Type": "manual", "AgentName": "None", "Disposition": "", "EndTime": "2015-12-02 01:20:00", "UserName": "urbanladder_test", "Comments": "", "Did": "918067417870", "AgentID": "", "AgentPhoneNumber": "7795122320", "CustomerStatus": "", "AgentStatus": "","source":""}',WS_UL_SendCallLogsToSFDC.DataFromCall.class);
        CTIService.processCallToCreateTask(callData_Test); 
        
       	callData_Test  = (WS_UL_SendCallLogsToSFDC.DataFromCall)System.JSON.deserialize('{"Location": "", "PhoneName": "Sachindev", "DialStatus": "Not Answered", "Status": "NotAnswered", "HangupBy": "AgentHangup", "TransferType": "No Transfers", "StartTime": "2015-12-02 01:17:00", "Skill": "None", "UUI": "", "Duration": "00:00:00", "Apikey": "KK8d89146fcb2d98ea13f93492c4cfdfdb", "TimeToAnswer": "", "FallBackRule": "AgentDial", "DialedNumber": "7795122320", "monitorUCID": "6844486346877336", "TransferredTo": "", "CallerID": "9848098480", "AgentUniqueID": "None", "AudioFile": "", "Type": "Inbound", "AgentName": "None", "Disposition": "", "EndTime": "2015-12-02 01:20:00", "UserName": "urbanladder_test", "Comments": "", "Did": "918067417870", "AgentID": "", "AgentPhoneNumber": "7795122320", "CustomerStatus": "", "AgentStatus": "","source":""}',WS_UL_SendCallLogsToSFDC.DataFromCall.class);
        CTIService.processCallToCreateTask(callData_Test); 
    }
    
    static testMethod void testParse5() {
        
        Contact con = new Contact(lastName = 'Test Name', phone = '9848098482');
        insert con;
        init();
        WS_UL_SendCallLogsToSFDC.DataFromCall callData_Test = new  WS_UL_SendCallLogsToSFDC.DataFromCall();
       	callData_Test  = (WS_UL_SendCallLogsToSFDC.DataFromCall)System.JSON.deserialize('{"Location": "", "PhoneName": "Sachindev", "DialStatus": "Not Answered", "Status": "NotAnswered", "HangupBy": "AgentHangup", "TransferType": "No Transfers", "StartTime": "2015-12-02 01:17:00", "Skill": "None", "UUI": "", "Duration": "00:00:00", "Apikey": "KK8d89146fcb2d98ea13f93492c4cfdfdb", "TimeToAnswer": "", "FallBackRule": "AgentDial", "DialedNumber": "7795122320", "monitorUCID": "6844486346877336", "TransferredTo": "", "CallerID": "09848098481", "AgentUniqueID": "None", "AudioFile": "", "Type": "Inbound", "AgentName": "None", "Disposition": "", "EndTime": "2015-12-02 01:20:00", "UserName": "urbanladder_test", "Comments": "", "Did": "918067417870", "AgentID": "", "AgentPhoneNumber": "7795122320", "CustomerStatus": "", "AgentStatus": "","source":""}',WS_UL_SendCallLogsToSFDC.DataFromCall.class);
        CTIService.processCallToCreateTask(callData_Test); 
        
       	callData_Test  = (WS_UL_SendCallLogsToSFDC.DataFromCall)System.JSON.deserialize('{"Location": "", "PhoneName": "Sachindev", "DialStatus": "Not Answered", "Status": "NotAnswered", "HangupBy": "AgentHangup", "TransferType": "No Transfers", "StartTime": "2015-12-02 01:17:00", "Skill": "None", "UUI": "", "Duration": "00:00:00", "Apikey": "KK8d89146fcb2d98ea13f93492c4cfdfdb", "TimeToAnswer": "", "FallBackRule": "AgentDial", "DialedNumber": "7795122320", "monitorUCID": "6844486346877336", "TransferredTo": "", "CallerID": "9848098481", "AgentUniqueID": "None", "AudioFile": "", "Type": "Inbound", "AgentName": "None", "Disposition": "", "EndTime": "2015-12-02 01:20:00", "UserName": "urbanladder_test", "Comments": "", "Did": "918067417870", "AgentID": "", "AgentPhoneNumber": "7795122320", "CustomerStatus": "", "AgentStatus": "","source":""}',WS_UL_SendCallLogsToSFDC.DataFromCall.class);
        CTIService.processCallToCreateTask(callData_Test); 
    }
    
    static testMethod void testParse6() {
        
        Contact con = new Contact(lastName = 'Test Name', phone = '9848098481');
        insert con;
        init();  
        Profile p=[SELECT Id From Profile WHERE Name='Standard User'];
        User u2 =new User( Alias = 'CallUs' ,
                            Email ='newCallUser12@testorg.com',
                            LastName = 'Testing',
                            ProfileId=p.Id,EmailEncodingKey='UTF-8', LanguageLocaleKey='en_US', 
      						LocaleSidKey='en_US', TimeZoneSidKey='America/Los_Angeles', UserName='callTag@testorg.com');
         insert u2;               
        System.runAs(u2){
        	WS_UL_SendCallLogsToSFDC.DataFromCall callData_Test = new  WS_UL_SendCallLogsToSFDC.DataFromCall();
       		callData_Test  = (WS_UL_SendCallLogsToSFDC.DataFromCall)System.JSON.deserialize('{"Location": "", "PhoneName": "Sachindev", "DialStatus": "Not Answered", "Status": "NotAnswered", "HangupBy": "AgentHangup", "TransferType": "No Transfers", "StartTime": "2015-12-02 01:17:00", "Skill": "None", "UUI": "", "Duration": "00:00:00", "Apikey": "KK8d89146fcb2d98ea13f93492c4cfdfdb", "TimeToAnswer": "", "FallBackRule": "AgentDial", "DialedNumber": "7795122320", "monitorUCID": "6844486346877336", "TransferredTo": "", "CallerID": "09848098481", "AgentUniqueID": "None", "AudioFile": "", "Type": "Inbound", "AgentName": "None", "Disposition": "", "EndTime": "2015-12-02 01:20:00", "UserName": "urbanladder_test", "Comments": "", "Did": "918067417870", "AgentID": "", "AgentPhoneNumber": "7795122320", "CustomerStatus": "", "AgentStatus": "","source":""}',WS_UL_SendCallLogsToSFDC.DataFromCall.class);        
        	CTIService.processCallToCreateTask(callData_Test);
        	callData_Test  = (WS_UL_SendCallLogsToSFDC.DataFromCall)System.JSON.deserialize('{"Location": "", "PhoneName": "Sachindev", "DialStatus": "Not Answered", "Status": "NotAnswered", "HangupBy": "AgentHangup", "TransferType": "No Transfers", "StartTime": "2015-12-02 01:17:00", "Skill": "None", "UUI": "", "Duration": "00:00:00", "Apikey": "KK8d89146fcb2d98ea13f93492c4cfdfdb", "TimeToAnswer": "", "FallBackRule": "AgentDial", "DialedNumber": "7795122320", "monitorUCID": "6844486346877336", "TransferredTo": "", "CallerID": "9848098481", "AgentUniqueID": "None", "AudioFile": "", "Type": "Inbound", "AgentName": "None", "Disposition": "", "EndTime": "2015-12-02 01:20:00", "UserName": "urbanladder_test", "Comments": "", "Did": "918067417870", "AgentID": "", "AgentPhoneNumber": "7795122320", "CustomerStatus": "", "AgentStatus": "","source":""}',WS_UL_SendCallLogsToSFDC.DataFromCall.class);
        	CTIService.processCallToCreateTask(callData_Test); 
        	callData_Test  = (WS_UL_SendCallLogsToSFDC.DataFromCall)System.JSON.deserialize('{"AgentName": "lalit chandnani", "CampaignName": "Inbound_918030178395", "UUI": "Manual Dial", "DialStatus": "NormalUnspecified", "TransferType": "No Transfers", "AudioFile": "", "PhoneName": "pavithra", "UserName": "urbanldder_test2", "Apikey": "KKe20836cfd34fd4e51e6f2e83ee8d9fab", "CallDuration": "00:00:44", "CustomerStatus": "NormalUnspecified", "Type": "Manual", "DialedNumber": "9591918120", "Skill": "None", "AgentUniqueID": "32076", "Status": "NotAnswered", "EndTime": "2016-11-13 21:11:53", "StartTime": "2016-11-13 21:11:09", "ConfDuration": "00:00:00", "Did": "918030178395", "AgentStatus": "answered", "CallerConfAudioFile": "", "AgentPhoneNumber": "9591918120", "Duration": "00:00:44", "CallerID": "08095954940", "Location": "", "FallBackRule": "AgentDial", "Comments": "", "AgentID": "lalit.chandnani@urbanladder.com", "Disposition": "", "HangupBy": "UserHangup", "monitorUCID": "698997905165941", "TransferredTo": "", "TimeToAnswer": "00:00:00","source":""}',WS_UL_SendCallLogsToSFDC.DataFromCall.class);
        	CTIService.processCallToCreateTask(callData_Test); 
        	callData_Test  = (WS_UL_SendCallLogsToSFDC.DataFromCall)System.JSON.deserialize('{"TimeToAnswer":"","StartTime":"2016-10-12 17:43:50","CampaignName":"Inbound_918030178395","CustomerStatus":"Fail","Did":"918030178395","Comments":"","monitorUCID":"654997627443111","Apikey":"KKe20836cfd34fd4e51e6f2e83ee8d9fab","EndTime":"2016-10-12 17:43:50","DialStatus":"NormalCallClearing","AgentUniqueID":"32076","AgentStatus":"NormalCallClearing","AgentName":"lalit chandnani","Duration":"00:00:00","AudioFile":"","PhoneName":"ul_rachit","CallerConfAudioFile":"","UUI":"Manual Dial","Disposition":"","UserName":"urbanldder_test2","DialedNumber":"9945322447","Location":"","Type":"Manual","CallerID":"09591918120","ConfDuration":"00:00:00","Skill":"None","AgentPhoneNumber":"9945322447","Status":"NotAnswered","TransferType":"No Transfers","FallBackRule":"AgentDial","TransferredTo":"","HangupBy":"AgentHangup","AgentID":"lalit.chandnani@urbanladder.com","CallDuration":"00:00:00","source":""}',WS_UL_SendCallLogsToSFDC.DataFromCall.class);
        	CTIService.processCallToCreateTask(callData_Test); 
        	
        }      	
    }
    
    static testMethod void testParse7() {
        
        Contact con = new Contact(lastName = 'Test Name', phone = '9848098481');
        insert con;
        init();
        Lead leadIns = new Lead(firstName='Test Name',lastName='Test Last Name',Phone='9848098481',Lead_Stage__c='Open',Company='ET'); 
        insert leadIns;
          
        WS_UL_SendCallLogsToSFDC.DataFromCall callData_Test = new  WS_UL_SendCallLogsToSFDC.DataFromCall();
       	callData_Test  = (WS_UL_SendCallLogsToSFDC.DataFromCall)System.JSON.deserialize('{"Location": "", "PhoneName": "Sachindev", "DialStatus": "Not Answered", "Status": "NotAnswered", "HangupBy": "AgentHangup", "TransferType": "No Transfers", "StartTime": "2015-12-02 01:17:00", "Skill": "None", "UUI": "", "Duration": "00:00:00", "Apikey": "KK8d89146fcb2d98ea13f93492c4cfdfdb", "TimeToAnswer": "", "FallBackRule": "AgentDial", "DialedNumber": "7795122320", "monitorUCID": "6844486346877336", "TransferredTo": "", "CallerID": "09848098481", "AgentUniqueID": "None", "AudioFile": "", "Type": "Inbound", "AgentName": "None", "Disposition": "", "EndTime": "2015-12-02 01:20:00", "UserName": "urbanladder_test", "Comments": "", "Did": "918067417870", "AgentID": "", "AgentPhoneNumber": "7795122320", "CustomerStatus": "answered", "AgentStatus": ""}',WS_UL_SendCallLogsToSFDC.DataFromCall.class);
        CTIService.processCallForLeads(callData_Test);
        
        System_Configs__c sysConfig = new System_Configs__c(Name='Lead', Is_Run__c = true);
        System_Configs__c sysConfig2 = new System_Configs__c(Name='Opportunity', Is_Run__c = true);
        insert sysConfig;
        insert sysConfig2;
        Lead leadInfo = new Lead(Products__c = 'Full Furniture;Kitchens', LastName = 'Test LastName', Company = 'Test Company', Consultant_Name__c=userInfo.getUserId());
        insert leadInfo;
        LeadConvertService.customConvertLead(leadInfo.Id,userInfo.getUserId());
        
        WS_UL_SendCallLogsToSFDC.DataFromCall callData_Test1 = new  WS_UL_SendCallLogsToSFDC.DataFromCall();
       	callData_Test1  = (WS_UL_SendCallLogsToSFDC.DataFromCall)System.JSON.deserialize('{"Location": "", "PhoneName": "Sachindev", "DialStatus": "Not Answered", "Status": "NotAnswered", "HangupBy": "AgentHangup", "TransferType": "No Transfers", "StartTime": "2015-12-02 01:17:00", "Skill": "None", "UUI": "", "Duration": "00:00:00", "Apikey": "KK8d89146fcb2d98ea13f93492c4cfdfdb", "TimeToAnswer": "", "FallBackRule": "AgentDial", "DialedNumber": "7795122320", "monitorUCID": "6844486346877336", "TransferredTo": "", "CallerID": "09848098481", "AgentUniqueID": "None", "AudioFile": "", "Type": "Inbound", "AgentName": "None", "Disposition": "", "EndTime": "2015-12-02 01:20:00", "UserName": "urbanladder_test", "Comments": "", "Did": "918067417870", "AgentID": "", "AgentPhoneNumber": "7795122320", "CustomerStatus": "answered", "AgentStatus": ""}',WS_UL_SendCallLogsToSFDC.DataFromCall.class);
        CTIService.processCallForLeads(callData_Test1);
         
    }
    
    static testMethod void testParse8() {
        
        Contact con = new Contact(lastName = 'Test Name', phone = '9848098481');
        insert con;
        init();
        WS_UL_SendCallLogsToSFDC.DataFromCall callData_Test = new  WS_UL_SendCallLogsToSFDC.DataFromCall();
       	callData_Test  = (WS_UL_SendCallLogsToSFDC.DataFromCall)System.JSON.deserialize('{"Location": "", "PhoneName": "Sachindev", "DialStatus": "Not Answered", "Status": "NotAnswered", "HangupBy": "AgentHangup", "TransferType": "No Transfers", "StartTime": "2015-12-02 01:17:00", "Skill": "None", "UUI": "", "Duration": "00:00:00", "Apikey": "KK8d89146fcb2d98ea13f93492c4cfdfdb", "TimeToAnswer": "", "FallBackRule": "AgentDial", "DialedNumber": "7795122320", "monitorUCID": "6844486346877336", "TransferredTo": "", "CallerID": "09848098481", "AgentUniqueID": "None", "AudioFile": "", "Type": "Inbound", "AgentName": "None", "Disposition": "", "EndTime": "2015-12-02 01:20:00", "UserName": "urbanladder_test", "Comments": "", "Did": "918067417870", "AgentID": "", "AgentPhoneNumber": "7795122320", "CustomerStatus": "answered", "AgentStatus": ""}',WS_UL_SendCallLogsToSFDC.DataFromCall.class);
        CTIService.processCallForLeads(callData_Test);
        
    }
    
    //----- To test CNR 
    static testMethod void testSendCNRCall(){
    	
    }
    
    
    
    
}