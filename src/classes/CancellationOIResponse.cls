@isTest
global class CancellationOIResponse implements HttpCalloutMock {
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        // Optionally, only send a mock response for a specific endpoint
        // and method.
        //System.assertEquals('http://api.salesforce.com/foo/bar', req.getEndpoint());        //System.assertEquals('GET', req.getMethod());
        
        // Create a fake response
        String resBody =	'{"status":"success","data":{"cancel_eligibility":{"order_code":"R321945713","eligibility":"ELIGIBLE","reason":[],"order_items":{"988484-67239":{"eligibility":"REQUIRE_APPROVAL","parent_sku":"FNDNMHWBC010002","reason":["BUNDLE"]},"988484-67240-1":{"eligibility":"REQUIRE_APPROVAL","parent_sku":"FNDNMHWBC010002","reason":["BUNDLE"]},"988484-67240-2":{"eligibility":"ELIGIBLE","parent_sku":null,"reason":[]},"988484-67240-3":{"eligibility":"ELIGIBLE","parent_sku":null,"reason":[]},"988484-67241-1":{"eligibility":"ELIGIBLE","parent_sku":null,"reason":[]},"988484-67241-2":{"eligibility":"ELIGIBLE","parent_sku":null,"reason":[]},"988484-67241-3":{"eligibility":"ELIGIBLE","parent_sku":null,"reason":[]},"988497-1":{"eligibility":"ELIGIBLE","parent_sku":null,"reason":[]},"988497-2":{"eligibility":"ELIGIBLE","parent_sku":null,"reason":[]},"988513":{"eligibility":"ELIGIBLE","parent_sku":null,"reason":[]},"988527-67242":{"eligibility":"NOT_ELIGIBLE","parent_sku":null,"reason":["LMS_STATUS_REPLACEMENT_INITIATED"]},"988527-672431":{"eligibility":"ELIGIBLE","parent_sku":null,"reason":[]}}}}}';
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');//'{"status": "success","data": {"consumer": {"code__c": R278679,"eligibility":"ELIGIBLE","email": "rachit.sharma77467450@test.com","prefix": 0,"first_name": "tes7754","last_name": "test75544","parent_consumer_id": null,"ship_address_id": 304016,"created_at": "2015-11-04T11:58:19.078Z","updated_at": "2015-11-04T11:58:19.078Z"}}}'
        res.setBody(resBody);
        res.setStatusCode(201);
        res.setStatus('OK');
        return res;
    }
}