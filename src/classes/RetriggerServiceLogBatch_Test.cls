@isTest
private class RetriggerServiceLogBatch_Test {
    
    @testSetup static void testData() {
    Retrigger_Limit__c rt = new Retrigger_Limit__c(Max_Limit__c = 3, Name='Retry_Limit');
        insert rt;
        
    }
    
    private static testMethod void customerCalloutSuccess() {
        Contact con = new Contact(lastName = '.', firstname = 'Captain Purna', phone='9845013286');
        insert con;
     Service_Logs__c sl = new Service_Logs__c();
        sl.Request_Format__c = '{"prefix":"Mr","phones":["9845013286"],"last_name":".","first_name":"Captain Purna","emails":[null]}';
        sl.Status_Code__c = '200';
        sl.Status_Msg__c = 'OK';
        sl.Record_Id__c = '/'+con.Id;
        sl.Callout_Service_Name__c = 'Customer';
        sl.Log_Message__c = '{"status":"success","data":{"consumer":{"id":592951,"email":"9845044486@urbanladder.com","prefix":0,"first_name":"Captain Purna","last_name":".","created_at":"2016-02-15T05:16:59.000Z","updated_at":"2016-02-15T05:21:01.746Z","parent_consumer_id":null}}}';
        insert sl;
        database.executeBatch(new RetriggerServiceLogBatch());
        Service_Logs__c slQuery = [select Number_Of_Retries_Performed__c, Status__c from Service_Logs__c where id =: sl.Id limit 1];
        Contact conQuery = [select Customer_Id__c from Contact where id=:con.Id limit 1];
        //system.assertEquals(slQuery.Number_Of_Retries_Performed__c, 1);
        //system.assertEquals(slQuery.Status__c, 'Processed - Success');
        //system.assertEquals(conQuery.Customer_Id__c, 592951);
    }
    
    
    private static testMethod void customerCalloutInsertError_Error() {
        Contact con = new Contact(lastName = '.', firstname = 'Captain Purna', phone='9845013286');
        insert con;
     Service_Logs__c sl = new Service_Logs__c();
        sl.Request_Format__c = '{"prefix":"Mr","phones":["9845013286"],"last_name":".","first_name":"Captain Purna","emails":[null]}';
        sl.Status_Code__c = '400';
        sl.Record_Id__c = '/'+con.Id;
        sl.Callout_Service_Name__c = 'Customer';
        //sl.Log_Message__c = '{"status":"success","data":{"consumer":{"id":592951,"email":"9845044486@urbanladder.com","prefix":0,"first_name":"Captain Purna","last_name":".","created_at":"2016-02-15T05:16:59.000Z","updated_at":"2016-02-15T05:21:01.746Z","parent_consumer_id":null}}}';
        insert sl;
        database.executeBatch(new RetriggerServiceLogBatch());
    } 
    
     private static testMethod void customerCalloutInsertError_Success() {
         
        UL_Wulverine_Credentials__c ul = new UL_Wulverine_Credentials__c(User_Name__c = 'salesforce@urbanladder.com', Password__c = 'VXJiYU5fRm9yY0U2NzM=', Name = 'Wulverine Server',Sandbox_Username__c = 'salesforce@urbanladder.com', Sandbox_Password__c	 = 'VXJiYU5fRm9yY0U2NzM=',Wulverine_Sandbox_Host__c='https://esgp3-wulverine.urbanladder.com',Wulverine_Production_Host__c='https://stg-salesforce.urbanladder.com');
        insert ul;
        UL_EndPoint_URLs__c ue = new UL_EndPoint_URLs__c(Name = 'Customer_Insert_UL', End_point_URL__c = '/v1/consumers', Time_Out__c = 5);
        insert ue;
  
        
        Contact con = new Contact(lastName = '.', firstname = 'Captain Purna', phone='9845013286');
        insert con;
         Test.setMock(HttpCalloutMock.class, new CustomerInsertResponse());
     Service_Logs__c sl = new Service_Logs__c();
        sl.Request_Format__c = '{"prefix":"Mr","phones":["9845013286"],"last_name":".","first_name":"Captain Purna","emails":[null]}';
        sl.Status_Code__c = '400';
        sl.Record_Id__c = '/'+con.Id;
        sl.Callout_Service_Name__c = 'Customer';
        //sl.Log_Message__c = '{"status":"success","data":{"consumer":{"id":592951,"email":"9845044486@urbanladder.com","prefix":0,"first_name":"Captain Purna","last_name":".","created_at":"2016-02-15T05:16:59.000Z","updated_at":"2016-02-15T05:21:01.746Z","parent_consumer_id":null}}}';
        insert sl;
         
        database.executeBatch(new RetriggerServiceLogBatch());
    }
    
    private static testMethod void customerCalloutUpdateError_Error() {
        Contact con = new Contact(lastName = '.', firstname = 'Captain Purna', phone='9845013286', Customer_ID__c = 123432.0);
        insert con;
     Service_Logs__c sl = new Service_Logs__c();
        sl.Request_Format__c = '{"prefix":"Mr.","last_name":"testLastName","first_name":"testFirstName"}';
        sl.Status_Code__c = '400';
        sl.Record_Id__c = '/'+con.Id;
        sl.Callout_Service_Name__c = 'Customer';
        //sl.Log_Message__c = '{"status":"success","data":{"consumer":{"id":592951,"email":"9845044486@urbanladder.com","prefix":0,"first_name":"Captain Purna","last_name":".","created_at":"2016-02-15T05:16:59.000Z","updated_at":"2016-02-15T05:21:01.746Z","parent_consumer_id":null}}}';
        insert sl;
        database.executeBatch(new RetriggerServiceLogBatch());
    } 
    
    private static testMethod void customerCalloutUpdateError_Success() {
        
        UL_Wulverine_Credentials__c ul = new UL_Wulverine_Credentials__c(User_Name__c = 'salesforce@urbanladder.com', Password__c = 'VXJiYU5fRm9yY0U2NzM=', Name = 'Wulverine Server',Sandbox_Username__c = 'salesforce@urbanladder.com', Sandbox_Password__c	 = 'VXJiYU5fRm9yY0U2NzM=',Wulverine_Sandbox_Host__c='https://esgp3-wulverine.urbanladder.com',Wulverine_Production_Host__c='https://stg-salesforce.urbanladder.com');
        insert ul;
        UL_EndPoint_URLs__c ue = new UL_EndPoint_URLs__c(Name = 'Customer_Update_UL', End_point_URL__c = '/v1/consumers', Time_Out__c = 5);
        insert ue;
        
        Contact con = new Contact(lastName = '.', firstname = 'Captain Purna', phone='9845013286', Customer_ID__c = 123432.0);
        insert con;
        Test.setMock(HttpCalloutMock.class, new CustomerUpdateResponse());
     Service_Logs__c sl = new Service_Logs__c();
        sl.Request_Format__c = '{"prefix":"Mr.","last_name":"testLastName","first_name":"testFirstName"}';
        sl.Status_Code__c = '400';
        sl.Record_Id__c = '/'+con.Id;
        sl.Callout_Service_Name__c = 'Customer';
        //sl.Log_Message__c = '{"status":"success","data":{"consumer":{"id":592951,"email":"9845044486@urbanladder.com","prefix":0,"first_name":"Captain Purna","last_name":".","created_at":"2016-02-15T05:16:59.000Z","updated_at":"2016-02-15T05:21:01.746Z","parent_consumer_id":null}}}';
        insert sl;
        database.executeBatch(new RetriggerServiceLogBatch());
    } 
}