/**
* Class             : WS_CloneOpportunityToSFDC_Test
* Created by        : ETMarlabs - Jiten
* Version           : 1.0  -- Initail Draft(27-02-2017) 
* Description       : This is test class for WS_CloneOpportunityToSFDC.
*                   : Inline comments are added to get specific details.
**/

@isTest(SeeAllData=false)
public with sharing class WS_CloneOpportunityToSFDC_Test {
    
    /*** below method will set the required data needed to test the application ***/
    @testSetup
    private static void setupDataMethod() {
        Account acc = new Account(Name = 'Test');
        insert acc;
        Contact con = new Contact(LastName = 'Test LastName',AccountId=acc.Id, Email = 'test1@urbanLadder.com',Customer_ID__c = 1209100);
        insert con;
        Opportunity  opp = new Opportunity(Name = 'Name',StageName = 'Designing',AccountId = acc.Id ,CloseDate = System.today(),
                                            Requirement__c = 'Full Furniture',Site_City__c = 'Bangalore');
        insert opp;
    }
    
    //---- Scenario 1  : Get the clonned Opportunity Id
    public static testMethod void getClonedOpportunityId(){
    
    //----- Get the required Opportunity Id
    
    Opportunity oppObj = [Select Id,Name from Opportunity Limit 1]; 
    //-----Do request
      Test.startTest();
      RestRequest req = new RestRequest(); 
      RestResponse res = new RestResponse();

      req.requestURI = '/services/apexrest/WS_CloneOpportunityToSFDC/'+oppObj.Id;  
      req.httpMethod = 'GET';
      RestContext.request = req;
      RestContext.response = res;
      
      WS_CloneOpportunityToSFDC.WS_cloneOpportunityResponse result = WS_CloneOpportunityToSFDC.cloneOpportunity();
      Test.stopTest();  
      String resBody = JSON.serialize(res.responseBody);
      //Check for Succesful req status ->200
      System.assertEquals(200,result.statusCode);
      System.assertEquals(1,[SELECT id from Opportunity where id =: result.clonedOppId].size());  
   }
   
   //---- Scenario 2  : Pass Dummy ID
    public static testMethod void passDummyIdToRESTService(){
    
    //-----Do request
      Test.startTest();
      RestRequest req = new RestRequest(); 
      RestResponse res = new RestResponse();

      req.requestURI = '/services/apexrest/WS_CloneOpportunityToSFDC/0067';  
      req.httpMethod = 'GET';
      RestContext.request = req;
      RestContext.response = res;
      
      WS_CloneOpportunityToSFDC.WS_cloneOpportunityResponse result = WS_CloneOpportunityToSFDC.cloneOpportunity();
      Test.stopTest();  
      String resBody = JSON.serialize(res.responseBody);
      //Check for Bad req status ->500
      System.assertEquals(500,result.statusCode);  
   }
   
   //---- Scenario 3  : Pass null ID
    public static testMethod void passNullIdToRESTService(){
    
    //-----Do request
      Test.startTest();
      RestRequest req = new RestRequest(); 
      RestResponse res = new RestResponse();

      req.requestURI = '/services/apexrest/WS_CloneOpportunityToSFDC/';  
      req.httpMethod = 'GET';
      RestContext.request = req;
      RestContext.response = res;
      
      WS_CloneOpportunityToSFDC.WS_cloneOpportunityResponse result = WS_CloneOpportunityToSFDC.cloneOpportunity();
      Test.stopTest();  
      String resBody = JSON.serialize(res.responseBody);
      //Check for Bad req status ->404
      System.assertEquals(404,result.statusCode);  
   }
   
   //---- Scenario 4  : Inappropriate Data
    public static testMethod void passInappropriateIdToRESTService(){
    
    //-----Do request
      Test.startTest();
      RestRequest req = new RestRequest(); 
      RestResponse res = new RestResponse();

      req.requestURI = '/services/apexrest/WS_CloneOpportunityToSFDC/1234';  
      req.httpMethod = 'GET';
      RestContext.request = req;
      RestContext.response = res;
      
      WS_CloneOpportunityToSFDC.WS_cloneOpportunityResponse result = WS_CloneOpportunityToSFDC.cloneOpportunity();
      Test.stopTest();  
      String resBody = JSON.serialize(res.responseBody);
      //Check for Bad req status ->404
      System.assertEquals(404,result.statusCode);  
   }
}