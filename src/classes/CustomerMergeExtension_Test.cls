@isTest(seeAllData = false)
private class CustomerMergeExtension_Test {
    
    private static testMethod void test1() {
        Contact con = new Contact(lastName = 'testing',Phone ='9876543456');
        insert con;
        Contact con1 = new Contact(lastName = 'testing2', Phone ='9873543456', Customer_ID__c = 111333);
        insert con1;
        ApexPages.StandardController stdController = new ApexPages.StandardController(con);
        CustomerMergeExtension conMerge = new CustomerMergeExtension(stdController);
        conMerge.mergeSecondaryCustomerToPrimaryCustomer();
        con.Customer_ID__c = 111222333;
        con.Secondary_Contact__c = con1.Id;
        update con;
        ApexPages.StandardController stdController1 = new ApexPages.StandardController(con);
        CustomerMergeExtension conMerge1 = new CustomerMergeExtension(stdController);
        conMerge1.selectedPrimaryCustomer = con.Id;
        conMerge1.selectedCustomer1 = con.Id;
        conMerge1.selectedCustomer2 = con1.Id;
        Test.setMock(HttpCalloutMock.class, new CustomerInsertResponse());
        conMerge1.mergeSecondaryCustomerToPrimaryCustomer();
    }
    
    private static testMethod void test2() {
        Test.setMock(HttpCalloutMock.class, new CustomerMergeResponse());
        UL_EndPoint_URLs__c uE = new UL_EndPoint_URLs__c(Name='Customer_Merge_UL', Time_Out__c = 10, End_point_URL__c = '/v1/consumers/merge');
        insert uE;
        UL_Wulverine_Credentials__c uc = new UL_Wulverine_Credentials__c(User_Name__c = 'salesforce@urbanladder.com', Password__c = 'VXJiYU5fRm9yY0U2NzM=', Name = 'Wulverine Server',Sandbox_Username__c = 'salesforce@urbanladder.com', Sandbox_Password__c	 = 'VXJiYU5fRm9yY0U2NzM=',Wulverine_Sandbox_Host__c='https://esgp3-wulverine.urbanladder.com',Wulverine_Production_Host__c='https://stg-salesforce.urbanladder.com');
        insert uc;
        
        Contact con = new Contact(lastName = 'testing',Phone ='9874543456');
        insert con;
        Contact con1 = new Contact(lastName = 'testing2',Phone ='9456543456', Customer_ID__c = 111333);
        insert con1;
        Case caseIns = new Case(ContactId =con1.Id);
        insert caseIns;
        Task taskIns = new Task(whoId = con1.Id);
        insert taskIns;
        Event eventIns = new Event(whoId = con1.Id, DurationInMinutes=123, ActivityDateTime=DateTime.now());
        insert eventIns;
        Attachment attIns = new Attachment(Name='TestAtt', parentId = con1.Id, body=Blob.valueOf('111qqqqq'));
        insert attIns;
        ApexPages.StandardController stdController = new ApexPages.StandardController(con);
        CustomerMergeExtension conMerge = new CustomerMergeExtension(stdController);
        conMerge.mergeSecondaryCustomerToPrimaryCustomer();
        con.Customer_ID__c = 111222333;
        con.Secondary_Contact__c = con1.Id;
        update con;
        Test.startTest();
        ApexPages.StandardController stdController1 = new ApexPages.StandardController(con);
        CustomerMergeExtension conMerge1 = new CustomerMergeExtension(stdController);
        conMerge1.selectedPrimaryCustomer = con.Id;
        conMerge1.selectedCustomer1 = con.Id;
        conMerge1.selectedCustomer2 = con1.Id;
        
        conMerge1.mergeSecondaryCustomerToPrimaryCustomer();
        Test.stopTest();
    }
    
    private static testMethod void test3() {
        
        UL_EndPoint_URLs__c uE = new UL_EndPoint_URLs__c(Name='Customer_Merge_UL', Time_Out__c = 10, End_point_URL__c = '/v1/consumers/merge');
        insert uE;
        UL_Wulverine_Credentials__c uc = new UL_Wulverine_Credentials__c(User_Name__c = 'salesforce@urbanladder.com', Password__c = 'VXJiYU5fRm9yY0U2NzM=', Name = 'Wulverine Server',Sandbox_Username__c = 'salesforce@urbanladder.com', Sandbox_Password__c	 = 'VXJiYU5fRm9yY0U2NzM=',Wulverine_Sandbox_Host__c='https://esgp3-wulverine.urbanladder.com',Wulverine_Production_Host__c='https://stg-salesforce.urbanladder.com');
        insert uc;
        
        Contact con = new Contact(lastName = 'testing', Phone ='9876543456');
        insert con;
        Contact con1 = new Contact(lastName = 'testing2',Phone ='9876543455', Customer_ID__c = 111333);
        insert con1;
        // Set mock callout class 
        
        ApexPages.StandardController stdController = new ApexPages.StandardController(con);
        CustomerMergeExtension conMerge1 = new CustomerMergeExtension(stdController);
        conMerge1.selectedPrimaryCustomer = con.Id;
        conMerge1.selectedCustomer1 = con.Id;
        conMerge1.selectedCustomer2 = con1.Id;
        con.Secondary_Contact__c = con1.Id;
        //update con;
        Test.setMock(HttpCalloutMock.class, new CustomerMergeResponse());
        conMerge1.mergeSecondaryCustomerToPrimaryCustomer();   
    }
}