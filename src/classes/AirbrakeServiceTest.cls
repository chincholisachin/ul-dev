@isTest
private class AirbrakeServiceTest {
    
    private static testMethod void raiseExceptionTestPostive() {
        
        System_Configs__c texec = new System_Configs__c(Name='Airbrake', Is_Run__c = true);
        insert texec;
        UL_EndPoint_URLs__c ue = new UL_EndPoint_URLs__c(Name='Airbrake', End_point_URL__c = 'https://airbrake.io/api/v3/projects', Time_Out__c = 5);
        insert ue;
        UL_Wulverine_Credentials__c uc = new UL_Wulverine_Credentials__c(User_Name__c = '126792', Password__c = '09c5ff063b21ecb6d8d54c65f07812b3', Name = 'Airbrake');
        insert uc;
        try{
        String s;
        s.capitalize();
        }
        catch(Exception e){
            Test.setMock(HttpCalloutMock.class, new AirbrakeServiceResponse());
            Test.startTest();
            AirbrakeService.createExceptionOnAB(e);
            system.assert(true,true);
            Test.stopTest();
        }
    }
    
    private static testMethod void raiseExceptionTestNegative() {
        
        System_Configs__c texec = new System_Configs__c(Name='Airbrake', Is_Run__c = true);
        insert texec;
        UL_EndPoint_URLs__c ue = new UL_EndPoint_URLs__c(Name='Airbrake', End_point_URL__c = 'https://airbrake.io/api/v3/projects', Time_Out__c = 5);
        insert ue;
        UL_Wulverine_Credentials__c uc = new UL_Wulverine_Credentials__c(User_Name__c = '126792', Password__c = '09c5ff063b21ecb6d8d54c65f07812b3', Name = 'Airbrake');
        insert uc;
        try{
        String s;
        s.capitalize();
        }
        catch(Exception e){
            //Test.setMock(HttpCalloutMock.class, new AirbrakeServiceResponse());
            Test.startTest();
                AirbrakeService.createExceptionOnAB(e);
            Test.stopTest();
        }
    }
    
    private static testMethod void raiseExceptionTestUserBased() {
        
        System_Configs__c texec = new System_Configs__c(Name='Airbrake', Is_Run__c = true);
        insert texec;
        UL_EndPoint_URLs__c ue = new UL_EndPoint_URLs__c(Name='Airbrake', End_point_URL__c = 'https://airbrake.io/api/v3/projects', Time_Out__c = 5);
        insert ue;
        UL_Wulverine_Credentials__c uc = new UL_Wulverine_Credentials__c(User_Name__c = '126792', Password__c = '09c5ff063b21ecb6d8d54c65f07812b3', Name = 'Airbrake');
        insert uc;
        
       Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standarduser-1@testorg.com.test');

        System.runAs(u) {
        try{
        String s;
        s.capitalize();
        }
        catch(Exception e){
            //Test.setMock(HttpCalloutMock.class, new AirbrakeServiceResponse());
            Test.startTest();
                AirbrakeService.createExceptionOnAB(e);
            Test.stopTest();
        }
        }
    }
    
    
}