@isTest 
public class Test_SLA_CalculateMilestoneTime {
    static testMethod void testMilestoneTimeCalculator() {        
        
        System.runAs ( new User(Id = UserInfo.getUserId()) ) {
            
            //----- Inserting custom setting value for testing 
        	InsertSystemConfig('Contact',true);
        	InsertSystemConfig('Case',true);
        	InsertSystemConfig('EmailToCaseLoopBreak',false);
        	InsertSystemConfig('RunBFLCaseAssignment',false);
        	//InsertSystemConfig('');
        	
             Account acc = new Account();
            acc.Name ='Test';
            insert acc;
            
            Entitlement ent = new Entitlement();           
            ent.AccountID = acc.Id;
            ent.Name = 'UL : CS Standard SLA/Entitlement';
            insert ent;
            
            Contact con = new Contact();
            con.Email = 'test@mail.com';
            con.LastName = 'Pavithra';
            insert con ;
            
            MilestoneType mt =new MilestoneType();
            mt.Name = 'Response';
            //mt.StartDate = Date.Today();
            insert mt;
            
            Case c = new Case(priority = 'High');
            c.Product_Repaired_Delivery_Scheduled_Date__c = Date.Today();
            c.Visit_Scheduled_Date__c = Date.Today();
            c.Product_Picked_for_Repair_Date__c = Date.today();
            c.After_Sales_Status__c = 'Visit Scheduled'; 
            c.ContactId = con.Id ;          
            insert c;
            
            //System.debug('Contact Email '+c.Email);
            
            SLA_CalculateMilestoneTime calculator1 = new SLA_CalculateMilestoneTime ();
            Integer actualTriggerTime1 = calculator1.calculateMilestoneTriggerTime(c.Id, mt.Id);
            c.After_Sales_Defects__c = 'Packaging issue';
            c.After_Sales_Status__c ='Product Picked For Repair';
            update c;
           
            SLA_CalculateMilestoneTime calculator = new SLA_CalculateMilestoneTime ();
            Integer actualTriggerTime = calculator.calculateMilestoneTriggerTime(c.Id, mt.Id);
            c.Delivered_Date__c = System.now();
            c.After_Sales_Status__c =  'Product Repaired Delivery Scheduled';
            update c;
            
            SLA_CalculateMilestoneTime calculator2 = new SLA_CalculateMilestoneTime ();
            Integer actualTriggerTime2 = calculator2.calculateMilestoneTriggerTime(c.Id, mt.Id);
            
            mt.Name = 'Resolution';
            //mt.StartDate = Date.Today();
            update mt;
            c.Inward_Date__c = Date.today();
            c.Type='Delivery delay';
            c.Sub_Category__c='OOS';
            update c;
            SLA_CalculateMilestoneTime calculator3 = new SLA_CalculateMilestoneTime ();
            Integer actualTriggerTime3 = calculator3.calculateMilestoneTriggerTime(c.Id, mt.Id);
            c.QC_Fail_Date__c = Date.today();
            c.Sub_Category__c='QC fail';
            update c;
            SLA_CalculateMilestoneTime calculator4 = new SLA_CalculateMilestoneTime ();
            Integer actualTriggerTime4 = calculator4.calculateMilestoneTriggerTime(c.Id, mt.Id);
            
        }
        
    }
    
    public static void InsertSystemConfig(String configName, Boolean isRun){
    	//----- Inserting custom setting value for testing 
	        System_Configs__c sysCustom = new System_Configs__c();
	        sysCustom.Name = configName;
	        sysCustom.Is_Run__c = isRun ;
	        insert sysCustom ;
    }
}