public with sharing class AirbrakeModel {
    
 public class AirbrakeRequestWrapper{
    
    public cls_errors[] errors;
    public cls_context context;
    public cls_environment environment;
    public cls_session session;
    public cls_params params;
    
        public AirbrakeRequestWrapper(Exception e){
        system.debug('---Sampling Current Exception---'+e.getMessage());
        
        list<cls_errors> errorList = new list<cls_errors>();
        cls_errors errorInstance = new cls_errors();
        errorInstance.Type = e.getTypeName();
        errorInstance.Message = e.getMessage();
        
        list<cls_backtrace> backtraceList = new list<cls_backtrace>();
        
        cls_backtrace backTraceInstance = new cls_backtrace();
        backTraceInstance.file = e.getStackTraceString();
        backTraceInstance.line = e.getLineNumber();
        backTraceInstance.function ='';
        backtraceList.add(backTraceInstance);
        
        errorInstance.backtrace = backtraceList;
        
        errorList.add(errorInstance);
        
        this.errors = errorList; // 1st Param
        
        cls_context classContextInstance = new cls_context();
        
        cls_notifier notifierInstance = new cls_notifier();
        
        notifierInstance.name = '';
        notifierInstance.version = '';
        notifierInstance.url = '';
        
        classContextInstance.notifier = notifierInstance;
        classContextInstance.os = '';
        
        String instanceName = UserInfo.getUserName().substringAfterLast('.');
        if(instanceName!=null ||instanceName!=''){
            classContextInstance.hostname = instanceName ;
        }else{
            classContextInstance.hostname = 'Production';
        }
        
        classContextInstance.language= 'Apex';
        
        if(instanceName!=null ||instanceName!=''){
            classContextInstance.environment = instanceName;
        }else{
            classContextInstance.environment = 'Production';
        }
        classContextInstance.version ='';
        classContextInstance.url ='';
        classContextInstance.rootDirectory ='';
        
        cls_user classUserInstance = new cls_user();
        
        classUserInstance.id = UserInfo.getUserId();
        classUserInstance.name = UserInfo.getName();
        classUserInstance.email = UserInfo.getUserEmail();
        
        classContextInstance.user = classUserInstance;
        
        this.context = classContextInstance; // 2nd Param
        
        
        
    }
    
    
    
    
    }
    public class cls_errors {
        public String type; //error1 *
        public String message;  //message1
        public cls_backtrace[] backtrace;
    }
    class cls_backtrace {
        public String file; //backtrace file*
        public Integer line;    //10
        public String function; //backtrace function
    }
    class cls_context {
        public cls_notifier notifier;
        public String os;   //Linux 3.5.0-21-generic #32-Ubuntu SMP Tue Dec 11 18:51:59 UTC 2012 x86_64
        public String hostname; //production-rails-server-1
        public String language; //Ruby 2.1.1
        public String environment;  //production
        public String version;  //1.1.1
        public String url;//http://some-site.com/example
        public String rootDirectory;    ///home/app-root-directory
        public cls_user user;
    }
    class cls_notifier {
        public String name; //notifier name *
        public String version;  //notifier version *
        public String url;  //notifier url*
    }
    class cls_user {
        public String id;   //12345
        public String name; //root
        public String email;    //root@root.com
    }
    class cls_environment {
        public String PORT; //443
        public String CODE_NAME;    //gorilla
    }
    class cls_session {
        public String basketId; //123
        public String userId;   //456
    }
    class cls_params {
        public String page; //3
        //public String sort; //name
        public String direction;    //asc
    }
}