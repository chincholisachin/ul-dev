public with sharing class SLA_milestoneUtils {
	
	public static void completeMilestone(List<Id> caseIds, String milestoneName, DateTime complDate) {
      system.debug('*** completeMilestone Method Called ***');
      List<CaseMilestone> cmsToUpdate = new List<CaseMilestone>();
      try{
      	cmsToUpdate = [select Id, completionDate from CaseMilestone cm where caseId in :caseIds and cm.MilestoneType.Name LIKE: milestoneName+'%' and completionDate = null limit 1];
      }
      catch(QueryException qe){
      	system.debug('*** No Relevant Case Milestones Found'+qe.getMessage());
      }
    
    if (cmsToUpdate.isEmpty() == false){
      for (CaseMilestone cm : cmsToUpdate){
        cm.completionDate = complDate;
      }
      try{
      update cmsToUpdate;
      }
      catch(DMLException de){
      	system.debug('***Case Milestone Update Failed***'+de.getMessage());
      }
      system.debug('*** updated cmsToUpdate ***'+cmsToUpdate);
    } // end if
  }
}