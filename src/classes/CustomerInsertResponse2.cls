@isTest
global class CustomerInsertResponse2 implements HttpCalloutMock {
    
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        // Optionally, only send a mock response for a specific endpoint
        // and method.
        //System.assertEquals('http://api.salesforce.com/foo/bar', req.getEndpoint());
        //System.assertEquals('GET', req.getMethod());
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"error":{"message":"{\"status\":\"failure\",\"error\":\"Order Item = 1073954-72328 cannot be replaced since it is not yet dispatched yet\"}"}}');
        res.setStatusCode(422);
        res.setStatus('status code 422');
        return res;
    }
}