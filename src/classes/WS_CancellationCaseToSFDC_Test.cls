/*
*
*Purpose: Test class of Cancellation Case Processing
*Author : Pavithra Gajendra
*Date : 26/09/2016  
*                         
*/
@isTest
public class WS_CancellationCaseToSFDC_Test {

	//----CONSTANTS
	private static final String ORG_URL = URL.getSalesforceBaseUrl().toExternalForm() ;
	
	//----ATTRIBUTES
	static System_Configs__c texec ;
	static UL_EndPoint_URLs__c ue ;
	static UL_Wulverine_Credentials__c uc ;
	static UL_EndPoint_URLs__c urlCRS ;
	static Regions__c facility4 ;
	 
	static void init(){
		texec = new System_Configs__c(Name='Case', Is_Run__c = true);
        insert texec;
        ue = new UL_EndPoint_URLs__c(Name='Cancellation_Order', End_point_URL__c = 'https://stg-salesforce.urbanladder.com/v1/orders', Time_Out__c = 5);
        insert ue;
        uc = new UL_Wulverine_Credentials__c(User_Name__c = 'salesforce@urbanladder.com', Password__c = 'UrbaN_ForcE673', Name = 'Wulverine Server');
        insert uc;
        urlCRS = new UL_EndPoint_URLs__c(Name='Customer Request Status', End_point_URL__c = 'https://stg-salesforce.urbanladder.com/v1/orders', Time_Out__c = 5);
        insert urlCRS;
        facility4 = new Regions__c(Name='4',Region__c='Delhi Okhla');
        insert facility4 ;
	}
	
	//---- Scenario 1  : Create case as from Order Service with eligible item
	public static testMethod void createCaseFromOrderService(){
		
		//-----Do request
	    RestRequest req = new RestRequest(); 
	    RestResponse res = new RestResponse();
	    Test.startTest();
	    init();
	    String requestBody = '{"OrderCancelData":[ {  "Origin":"Spree","Type":"Cancellation","Sub_Category":"To Be Cancelled","Order":"R697852663","Parent_Order":"","Payment_Method":"Card On Delivery","O_FacilityID":4,"Customer_Id":"003N000000lOZBy", "Type_of_Refund":"","Reason_For_Cancellation":"Wrong Product Delivered","Channel_Code":"spree","Cancellation_items_Data":[     {   "OI_Code":"1807880-507634","OI_Name":"Arabia Coffee Table (Teak Finish)","OI_Reason":"Size Does Not Fit","OI_Total_Price":"5999.0","OI_ExternalID":"2018762-1-R772992426","OI_Eligibility":"ELIGIBLE","OI_Parent_SKU":"FNBDST11BE15021"}  ]}]}';
		    //-----REST API service URI 
		    req.requestURI = ORG_URL+'/services/apexrest/CancellationCaseToSFDC'; 		
		    req.httpMethod = 'POST';
		    req.requestBody = Blob.valueof(requestBody);
		    RestContext.request = req;
		    RestContext.response = res;
		    System.assertEquals(req.httpMethod, 'POST');
		    System.debug('-------------------request----------------'+req);
		    WS_CancellationCaseToSFDC.WS_APIOutput result = WS_CancellationCaseToSFDC.WS_PostOrderDataToSFDC();
		    System.debug('-------------------result----------------'+result); 
		    Test.stopTest() ;		
	}
	
	//---- Scenario 2  : Create case as from Order Service which requires approval
	public static testMethod void createCaseFromOrderServiceNE(){
		
		//-----Do request
	    RestRequest req = new RestRequest(); 
	    RestResponse res = new RestResponse();
	    Test.startTest();
	    init();
	    String requestBody = '{"OrderCancelData":[ {  "Origin":"Spree","Type":"Cancellation","Sub_Category":"To Be Cancelled","Order":"R697852663","Parent_Order":"","Payment_Method":"Card On Delivery","O_FacilityID":4,"Customer_Id":"003N000000lOZBy", "Type_of_Refund":"","Reason_For_Cancellation":"Wrong Product Delivered","Channel_Code":"spree","Cancellation_items_Data":[     {   "OI_Code":"1807880-507634","OI_Name":"Arabia Coffee Table (Teak Finish)","OI_Reason":"Size Does Not Fit","OI_Total_Price":"5999.0","OI_ExternalID":"2018762-1-R772992426","OI_Eligibility":"Needs Approval","OI_Parent_SKU":"FNBDST11BE15021"}  ]}]}';
		    //-----REST API service URI 
		    req.requestURI = ORG_URL+'/services/apexrest/CancellationCaseToSFDC'; 		
		    req.httpMethod = 'POST';
		    req.requestBody = Blob.valueof(requestBody);
		    RestContext.request = req;
		    RestContext.response = res;
		    System.assertEquals(req.httpMethod, 'POST');
		    System.debug('-------------------request----------------'+req);
		    WS_CancellationCaseToSFDC.WS_APIOutput result = WS_CancellationCaseToSFDC.WS_PostOrderDataToSFDC(); 
		    Test.stopTest() ;		
	}
	
	//---- Scenario 3  : Create case as from Order Service which requires approval as it exceeds 50000
	public static testMethod void createCaseFromOrderServiceAmountLimit(){
		
		//-----Do request
	    RestRequest req = new RestRequest(); 
	    RestResponse res = new RestResponse();
	    Test.startTest();
	    init();
	    String requestBody = '{"OrderCancelData":[ {  "Origin":"Spree","Type":"Cancellation","Sub_Category":"To Be Cancelled","Order":"R697852663","Parent_Order":"","Payment_Method":"Card On Delivery","O_FacilityID":4,"Customer_Id":"003N000000lOZBy", "Type_of_Refund":"","Reason_For_Cancellation":"Wrong Product Delivered","Channel_Code":"spree","Cancellation_items_Data":[     {   "OI_Code":"1807880-507634","OI_Name":"Arabia Coffee Table (Teak Finish)","OI_Reason":"Size Does Not Fit","OI_Total_Price":"45999.0","OI_ExternalID":"2018762-1-R772992426","OI_Eligibility":"Needs Approval","OI_Parent_SKU":"FNBDST11BE15021"} ,{   "OI_Code":"1807880-507635","OI_Name":"Arabia Coffee Table (Teak Finish)","OI_Reason":"Size Does Not Fit","OI_Total_Price":"5999.0","OI_ExternalID":"2018762-1-R772992427","OI_Eligibility":"ELIGIBLE","OI_Parent_SKU":"FNBDST11BE15021"}  ]}]}';
		    //-----REST API service URI 
		    req.requestURI = ORG_URL+'/services/apexrest/CancellationCaseToSFDC'; 		
		    req.httpMethod = 'POST';
		    req.requestBody = Blob.valueof(requestBody);
		    RestContext.request = req;
		    RestContext.response = res;
		    System.assertEquals(req.httpMethod, 'POST');
		    System.debug('-------------------request----------------'+req);
		    WS_CancellationCaseToSFDC.WS_APIOutput result = WS_CancellationCaseToSFDC.WS_PostOrderDataToSFDC(); 
		    Test.stopTest() ;		
	}
}