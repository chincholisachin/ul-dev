/* Author : Upendar Shivanadri*/
@isTest
public class LeadConvertServiceTest {
    
    private static testMethod void init() {
        
        System_Configs__c sysConfig = new System_Configs__c(Name='Lead', Is_Run__c = true);
        System_Configs__c sysConfig2 = new System_Configs__c(Name='Opportunity', Is_Run__c = true);
        insert sysConfig;
        insert sysConfig2;
        Lead leadInfo = new Lead(Products__c = 'Full Furniture;Kitchens', LastName = 'Test LastName', Company = 'Test Company', Consultant_Name__c=userInfo.getUserId());
        insert leadInfo;
        LeadConvertService.customConvertLead(leadInfo.Id,null);
    }
    
}