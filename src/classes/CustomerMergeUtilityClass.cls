/* Author : Satyanarayana M
Description : Merge Secondary Account(Contact) details with Primary Account(Contact).

*/
public class CustomerMergeUtilityClass {
    
    /* Change the parent Id of related customer records(Case, Task, Event, LiveChatTransript & Attachments) to Primary Customer */
    public boolean mergeSecondaryAccountWithPrimaryAccount(Integer primaryCustomerId, Integer secondaryCustomerId) {
        
        List<SObject> sObjectList = new List<SObject>();
        Boolean successFlag = false;
        Contact primaryContact = [select Id from Contact where Customer_ID__c =: primaryCustomerId limit 1]; // Query for Primary customer

        Contact con = [select Primary_Contact__c, (select ContactId from Cases), 
                       (select WhoId from Tasks), (select WhoId from Events where GroupEventType = '0' OR GroupEventType = null) 
                       from Contact where Customer_ID__c =: secondaryCustomerId limit 1];

                               try {
            con.Primary_Contact__c =   primaryContact.Id;
            for(Case caseInstance : con.Cases) {
                caseInstance.ContactId = primaryContact.Id;
                sObjectList.add(caseInstance);
            }
            
            for(Task taskInstance : con.Tasks) {
                taskInstance.WhoId = primaryContact.Id;
                sObjectList.add(taskInstance);
            }
            
            for(Event eventInstance : con.Events) {
                eventInstance.WhoId = primaryContact.Id;
                sObjectList.add(eventInstance);
            }
            
            
            List<Attachment> attachmentList = new List<Attachment>();
            List<Attachment> existingAttachmentList = new List<Attachment>([select ParentId, Body, ContentType, IsPrivate, Name, OwnerId from Attachment where ParentId =: con.Id]);
            
            for(Attachment att : existingAttachmentList) { 
                Attachment atts = new Attachment();
                atts = att.clone(false,false,false,false);
                atts.ParentId = primaryContact.Id;
                attachmentList.add(atts);
            }

            sObjectList.add(con);
            contactRecursion.fireTrigger = false; // To stop trigger from being executed.
            Database.SaveResult[] saveResult = Database.update(sObjectList, false); // updating parent id for all child records.
            insert attachmentList; // Insert cloned Triggers.
            delete existingAttachmentList; // Delete existing attachments under the secondary Customer.
            successFlag = true;

        } catch(Exception e) {

            successFlag = false;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
        }
        return successFlag;
    } 
}