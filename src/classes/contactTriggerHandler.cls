/*  */
public with sharing class contactTriggerHandler{
    
    private static contactTriggerHandler instance;
    
    public static contactTriggerHandler getInstance(){
        if(instance==null){
            instance = new contactTriggerHandler();
        }
        return instance;
    }
    /* Handling duplicate contacts based on EmailId and tagging it as secondary Customer under the Matched record */
    public void beforeInsertMethod(list<Contact> newList,map<ID,Contact> newMap) {
        list<String> emailIds = new list<string>();
        
        for(Contact newContact : newList) {
            if(newContact.Email != null) {
                emailIds.add(newContact.Email);
            }			
			if(newContact.Phone.length()>10){
            	newContact.Phone = newContact.Phone.right(10);
            }
            if(newContact.MobilePhone.length()>10){
            	newContact.MobilePhone = newContact.MobilePhone.right(10);
            }
        }
        
        if(emailIds.size() > 0) {
            Map<String, Id> emailWithContactMap = new Map<String, Id>();
            for(Contact con : [select Email from Contact where Email IN: emailIds]) {
                emailWithContactMap.put(con.Email, con.Id);
            }
            for(Contact newContact : newList) {
                if(emailWithContactMap.containsKey(newContact.Email)) {
                    newContact.Duplicate_Contact__c = true;
                    newContact.Primary_Contact__c = emailWithContactMap.get(newContact.Email);
                }
            }
        }
    }
    
    public void afterInsertMethod(list<Contact> newList,map<ID,Contact> newMap) {
        List<Contact> originalContactsList = new List<Contact>();
        List<Service_Logs__c> serviceLogsList =  new List<Service_Logs__c>();
        
        for(Contact newContact : newList) {
            if(!newContact.Duplicate_Contact__c) {
                originalContactsList.add(newContact);
            }
            
            serviceLogsList.add(CalloutService.generateInformationLog('Contact_Creation', 'Time Stamp :: '+String.valueOf(DateTime.now())+':'+DateTime.now().millisecond()+' Contact Data :: '+JSON.serialize(newContact) ,'Contact_Creation_Process', newContact.Id, 'Contact', DateTime.now()));
        }
        
        if(!originalContactsList.isEmpty()) {
            ContactService.createNewAccountMethod(originalContactsList);
            for(Contact newContact : originalContactsList){
                if(!newContact.Data_Loading__c) //&& newContact.Email != null
                    FetchCustomer.sendNewCustomers(newContact,'Insert');      
            }
        }
        
        if(serviceLogsList.size() > 0)
             CalloutService.insertServiceLogs(serviceLogsList);  
    }
    
    public void afterUpdateMethod(list<Contact> newList,map<ID,Contact> newMap,list<Contact> oldList,map<ID,Contact> oldMap){
        //ContactService.createNewAccountMethod(newList);
        List<Service_Logs__c> serviceLogsList =  new List<Service_Logs__c>();
        for(Contact newContact : newList){
            Contact oldContact=oldMap.get(newContact.id);
            //For Missed call Contacts.
            /*if(oldContact.Email == null && newContact.Customer_ID__c == null && newContact.Email != null) {
                FetchCustomer.sendNewCustomers(newContact,'Insert'); 
            } */
            
            serviceLogsList.add(CalloutService.generateInformationLog('Contact_Update', 'Time Stamp :: '+String.valueOf(DateTime.now())+':'+DateTime.now().millisecond()+' Contact Data :: '+JSON.serialize(newContact) ,'Contact_Update_Process', newContact.Id, 'Contact', DateTime.now()));
            
            if((newContact.FirstName != oldContact.FirstName ||newContact.LastName != oldContact.LastName || newContact.Salutation != oldContact.Salutation) && newContact.Customer_ID__c != null)
                FetchCustomer.sendNewCustomers(newContact,'Update');      
        }
        
        if(serviceLogsList.size() > 0)
             CalloutService.insertServiceLogs(serviceLogsList); 
    }
}