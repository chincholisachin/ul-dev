@isTest(seeAllData=false)
private class ReplacementOrderBatch_Test {
    
    /* Order Created; Updates the replacement OrderId*/
    private static testMethod void testCreated() {
    	
        Contact con1 = new Contact(LastName = 'TestName', phone='9854325679', Email ='test@urbanLadder4.co.in');
        insert con1;
        Case caseInstance = new case(Order__c = '12344', OI_Code__c = '943089', Replacement_Order_Placed_with_UL__c = true);
        caseInstance.contactId = con1.Id ;
        insert caseInstance;
        Replacement_Order_Stagging__c rs = new Replacement_Order_Stagging__c(Request_Body__c = '{"event_type": "ORDER_CREATED","event_id": "3F746E937EAB347D38E4386JFO","version": "v1","event_datetime": "2015-10-05 02:00:34","event_data": {"id": 141990,"code": "R590724503","number": "R590724503","total": "5299.0","discount_total": "0.0","placed_at": "2015-11-17T21:13:22Z","voucher_code": null,"status": 0,"consumer_id": "247193","created_at": "2015-11-17T21:41:48.000Z","updated_at": "2015-11-17T21:41:48.000Z","facility_id": 1,"agent_code": null, "channel_code": "mobile-web","parent_order_code": "12344","nps_score": null,"order_items": [{"id": 255825,"code": "943089","order_id": 141990,"sku": "ACLTWL61BSY0001","total_price": "5299.0","selling_price": "5299.0","discount": "0.0","amount_due": "0.0","promised_delivery_date": "2015-12-02T00:00:00Z","requested_delivery_date": null,"status": 1,"source_facility_id": 3,"created_at": "2015-11-17T21:41:48.000Z","updated_at": "2015-11-17T21:41:48.000Z","delivery_planning_datetime": null,"voucher_code": null,"pre_order": false,"parent_sku": null,"parent_sku_description": null,"service_item": false,"ul_special_instructions": null,"lms_status_text": "FULFILLABLE","lms_status_string": "Item has been processed","item_name": "Sheren Extendable Wall Sconce (Colour : brass)"}]}}');
        insert rs;
        System.Test.startTest();
        Database.executeBatch(new ReplacementOrderBatch());
        System.Test.stopTest();
        Case caseQuery = [Select Id, Order__c, Order_Item__c, Replacement_Order_Placed_with_UL__c, Replaced_Order_Id__c from Case where id=: caseInstance.Id];
        system.assertEquals(caseInstance.Order__c, caseQuery.Order__c);
        system.assertEquals(caseInstance.Order_Item__c, caseQuery.Order_Item__c);
        system.assertEquals(caseInstance.Replacement_Order_Placed_with_UL__c, caseQuery.Replacement_Order_Placed_with_UL__c);
        //system.assertEquals(caseInstance.Replaced_Order_Id__c, 'R590724503'); //R590724503
        //system.debug('12345'+[select id, Status__c, Error_Message__c from Replacement_Order_Stagging__c where id=: rs.Id]);
    }
    
    /*private static testMethod void testCreated_Change_parent() {
        
        Case caseInstance = new case(Old_Replaced_Order_Id__c = 'R590724503-2',Order__c = 'R590724503-2', OI_Code__c = '943089', Replaced_Order_Id__c = 'R590724503', Replacement_Order_Placed_with_UL__c = true);
        insert caseInstance;
        Replacement_Order_Stagging__c rs = new Replacement_Order_Stagging__c(Request_Body__c = '{"event_type": "ORDER_CREATED","event_id": "3F746E937EAB347D38E4386JFO","version": "v1","event_datetime": "2015-10-05 02:00:34","event_data1": {"id": 141990,"code": "R590724503-2","number": "R590724503","total": "5299.0","discount_total": "0.0","placed_at": "2015-11-17T21:13:22Z","voucher_code": null,"status": 0,"consumer_id": "247193","created_at": "2015-11-17T21:41:48.000Z","updated_at": "2015-11-17T21:41:48.000Z","facility_id": 1,"agent_code": null, "channel_code": "mobile-web","parent_order_code": "R590724503","nps_score": null,"order_items": [{"id": 255825,"code": "943089","order_id": 141990,"sku": "ACLTWL61BSY0001","total_price": "5299.0","selling_price": "5299.0","discount": "0.0","amount_due": "0.0","promised_delivery_date": "2015-12-02T00:00:00Z","requested_delivery_date": null,"status": 1,"source_facility_id": 3,"created_at": "2015-11-17T21:41:48.000Z","updated_at": "2015-11-17T21:41:48.000Z","delivery_planning_datetime": null,"voucher_code": null,"pre_order": false,"parent_sku": null,"parent_sku_description": null,"service_item": false,"ul_special_instructions": null,"lms_status_text": "FULFILLABLE","lms_status_string": "Item has been processed","item_name": "Sheren Extendable Wall Sconce (Colour : brass)"}]}}');
        insert rs;
        Database.executeBatch(new ReplacementOrderBatch());
        Case caseQuery = [Select Id, Order__c, Order_Item__c, Replacement_Order_Placed_with_UL__c, Replaced_Order_Id__c from Case where id=: caseInstance.Id];
        system.assertEquals(caseInstance.Order__c, caseQuery.Order__c);
        system.assertEquals(caseInstance.Order_Item__c, caseQuery.Order_Item__c);
        system.assertEquals(caseInstance.Replacement_Order_Placed_with_UL__c, caseQuery.Replacement_Order_Placed_with_UL__c);
        system.assertEquals(caseInstance.Replaced_Order_Id__c, 'R590724503');
    } */
    
    private static testMethod void testUpdated() {
        
        Contact con2 = new Contact(LastName = 'TestName', phone='9854325679', Email ='test@urbanLadder4.co.in');
        insert con2;
        Case caseInstance = new case(Replaced_Order_Id__c = '141990', Order__c = 'R590724503', OI_Code__c = '943089', Replacement_Order_Placed_with_UL__c = true);
        caseInstance.contactId = con2.Id ;
        insert caseInstance;
        Replacement_Order_Stagging__c rs = new Replacement_Order_Stagging__c(Request_Body__c = '{"event_type": "ORDER_ITEM_UPDATED","event_id": "3F746E937EAB347D38E4386JFO","version": "v1","event_datetime": "2015-10-05 02:00:34","event_data": {"id": 141990,"code": "R590724503","number": "R590724503", "total": "5299.0","discount_total": "0.0","placed_at": "2015-11-17T21:13:22Z","voucher_code": null,"status": 0,"consumer_id": "247193","created_at": "2015-11-17T21:41:48.000Z", "updated_at": "2015-11-17T21:41:48.000Z","facility_id": 1, "agent_code": null,"channel_code": "mobile-web", "parent_order_id": null,"nps_score": null,"order_items": [{"id": 255825,"code": "943089","order_id": 141990,"sku": "ACLTWL61BSY0001","total_price": "5299.0", "selling_price": "5299.0","discount": "0.0","amount_due": "0.0","promised_delivery_date": "2015-12-02T00:00:00Z","requested_delivery_date": null,"status": 1,"source_facility_id": 3,"created_at": "2015-11-17T21:41:48.000Z","updated_at": "2015-11-17T21:41:48.000Z","delivery_planning_datetime": null,"voucher_code": null,"pre_order": false,"parent_sku": null,"parent_sku_description": null,"service_item": false,"ul_special_instructions": null,"lms_status_text": "FULFILLABLE","lms_status_string": "Item has been processed","item_name": "Sheren Extendable Wall Sconce (Colour : brass)"}]}}');
         insert rs;
        Database.executeBatch(new ReplacementOrderBatch());
    }
    
    /* Delivered Order Test method */
    private static testMethod void testUpdated2() {
        Contact con3 = new Contact(LastName = 'TestName', phone='9854325679', Email ='test@urbanLadder4.co.in');
        insert con3;
        Case caseInstance = new case(Replaced_Order_Id__c = '141990', Order__c = 'R590724503', OI_Code__c = '943089', Replacement_Order_Placed_with_UL__c = true);
        caseInstance.contactId = con3.Id ;
        insert caseInstance;
        Replacement_Order_Stagging__c rs = new Replacement_Order_Stagging__c(Request_Body__c = '{"event_type": "ORDER_ITEM_UPDATED","event_id": "3F746E937EAB347D38E4386JFO","version": "v1","event_datetime": "2015-10-05 02:00:34","event_data": {"id": 141990,"code": "141990","number": "R590724503", "total": "5299.0","discount_total": "0.0","placed_at": "2015-11-17T21:13:22Z","voucher_code": null,"status": 0,"consumer_id": "247193","created_at": "2015-11-17T21:41:48.000Z", "updated_at": "2015-11-17T21:41:48.000Z","facility_id": 1, "agent_code": null,"channel_code": "mobile-web", "parent_order_id": null,"nps_score": null,"order_items": [{"id": 255825,"code": "943089","order_id": 141990,"sku": "ACLTWL61BSY0001","total_price": "5299.0", "selling_price": "5299.0","discount": "0.0","amount_due": "0.0","promised_delivery_date": "2015-12-02T00:00:00Z","requested_delivery_date": null,"status": 1,"source_facility_id": 3,"created_at": "2015-11-17T21:41:48.000Z","updated_at": "2015-11-17T21:41:48.000Z","delivery_planning_datetime": null,"voucher_code": null,"pre_order": false,"parent_sku": null,"parent_sku_description": null,"service_item": false,"ul_special_instructions": null,"lms_status_text": "DELIVERED","lms_status_string": "Item has been processed","item_name": "Sheren Extendable Wall Sconce (Colour : brass)"}]}}');
         insert rs;
        Database.executeBatch(new ReplacementOrderBatch());
    }
    
    /* Cancelled Order Test method */
    private static testMethod void testUpdated3() {
        Contact con4 = new Contact(LastName = 'TestName', phone='9854325679', Email ='test@urbanLadder4.co.in');
        insert con4;
        Case caseInstance = new case(Replaced_Order_Id__c = '141990', Order__c = 'R590724503', OI_Code__c = '943089', Replacement_Order_Placed_with_UL__c = true);
        caseInstance.contactId = con4.Id ;
        insert caseInstance;
        Replacement_Order_Stagging__c rs = new Replacement_Order_Stagging__c(Request_Body__c = '{"event_type": "ORDER_ITEM_UPDATED","event_id": "3F746E937EAB347D38E4386JFO","version": "v1","event_datetime": "2015-10-05 02:00:34","event_data": {"id": 141990,"code": "141990","number": "R590724503", "total": "5299.0","discount_total": "0.0","placed_at": "2015-11-17T21:13:22Z","voucher_code": null,"status": 0,"consumer_id": "247193","created_at": "2015-11-17T21:41:48.000Z", "updated_at": "2015-11-17T21:41:48.000Z","facility_id": 1, "agent_code": null,"channel_code": "mobile-web", "parent_order_id": null,"nps_score": null,"order_items": [{"id": 255825,"code": "943089","order_id": 141990,"sku": "ACLTWL61BSY0001","total_price": "5299.0", "selling_price": "5299.0","discount": "0.0","amount_due": "0.0","promised_delivery_date": "2015-12-02T00:00:00Z","requested_delivery_date": null,"status": 1,"source_facility_id": 3,"created_at": "2015-11-17T21:41:48.000Z","updated_at": "2015-11-17T21:41:48.000Z","delivery_planning_datetime": null,"voucher_code": null,"pre_order": false,"parent_sku": null,"parent_sku_description": null,"service_item": false,"ul_special_instructions": null,"lms_status_text": "Cancelled","lms_status_string": "Item has been processed","item_name": "Sheren Extendable Wall Sconce (Colour : brass)"}]}}');
         insert rs;
        Database.executeBatch(new ReplacementOrderBatch());
    }
    
        /* Cancelled Order Test method */
    private static testMethod void testUpdated31() {
        
        Contact con5 = new Contact(LastName = 'TestName', phone='9854325679', Email ='test@urbanLadder4.co.in');
        insert con5;
        Case caseInstance = new case(Replaced_Order_Id__c = '141990', Order__c = 'R590724503', OI_Code__c = '943089', Replacement_Order_Placed_with_UL__c = true);
        caseInstance.contactId = con5.Id ;
        insert caseInstance;
        Replacement_Order_Stagging__c rs = new Replacement_Order_Stagging__c(Request_Body__c = '{"event_type": "ORDER_ITEM_UPDATED","event_id": "3F746E937EAB347D38E4386JFO","version": "v1","event_datetime": "2015-10-05 02:00:34","event_data": {"id": 141990,"code": "141990","number": "R590724503", "total": "5299.0","discount_total": "0.0","placed_at": "2015-11-17T21:13:22Z","voucher_code": null,"status": 0,"consumer_id": "247193","created_at": "2015-11-17T21:41:48.000Z", "updated_at": "2015-11-17T21:41:48.000Z","facility_id": 1, "agent_code": null,"channel_code": "mobile-web", "parent_order_id": null,"nps_score": null,"order_items": [{"id": 255825,"code": "943089","order_id": 141990,"sku": "ACLTWL61BSY0001","total_price": "5299.0", "selling_price": "5299.0","discount": "0.0","amount_due": "0.0","promised_delivery_date": "2015-12-02T00:00:00Z","requested_delivery_date": null,"status": 1,"source_facility_id": 3,"created_at": "2015-11-17T21:41:48.000Z","updated_at": "2015-11-17T21:41:48.000Z","delivery_planning_datetime": null,"voucher_code": null,"pre_order": false,"parent_sku": null,"parent_sku_description": null,"service_item": false,"ul_special_instructions": null,"lms_status_text": "CANCEL_INITIATED","lms_status_string": "Item has been processed","item_name": "Sheren Extendable Wall Sconce (Colour : brass)"}]}}');
         insert rs;
        Database.executeBatch(new ReplacementOrderBatch());
    }
    
    
    /*No Case Found in ORDER_CREATED*/
    private static testMethod void testCreated2() {
        
        Replacement_Order_Stagging__c rs = new Replacement_Order_Stagging__c(Request_Body__c = '{"event_type": "ORDER_CREATED","event_id": "3F746E937EAB347D38E4386JFO","version": "v1","event_datetime": "2015-10-05 02:00:34","event_data": {"id": 141990,"code": "R590724503","number": "R590724503","total": "5299.0","discount_total": "0.0","placed_at": "2015-11-17T21:13:22Z","voucher_code": null,"status": 0,"consumer_id": "247193","created_at": "2015-11-17T21:41:48.000Z","updated_at": "2015-11-17T21:41:48.000Z","facility_id": 1,"agent_code": null, "channel_code": "mobile-web","parent_order_code": "12344","nps_score": null,"order_items": [{"id": 255825,"code": "943089","order_id": 141990,"sku": "ACLTWL61BSY0001","total_price": "5299.0","selling_price": "5299.0","discount": "0.0","amount_due": "0.0","promised_delivery_date": "2015-12-02T00:00:00Z","requested_delivery_date": null,"status": 1,"source_facility_id": 3,"created_at": "2015-11-17T21:41:48.000Z","updated_at": "2015-11-17T21:41:48.000Z","delivery_planning_datetime": null,"voucher_code": null,"pre_order": false,"parent_sku": null,"parent_sku_description": null,"service_item": false,"ul_special_instructions": null,"lms_status_text": "FULFILLABLE","lms_status_string": "Item has been processed","item_name": "Sheren Extendable Wall Sconce (Colour : brass)"}]}}');
        insert rs;
        Database.executeBatch(new ReplacementOrderBatch());
    }
    
    /*No Case Found in ORDER_ITEM_UPDATED*/
    private static testMethod void testUpdated4() {
        
        Replacement_Order_Stagging__c rs = new Replacement_Order_Stagging__c(Request_Body__c = '{"event_type": "ORDER_ITEM_UPDATED","event_id": "3F746E937EAB347D38E4386JFO","version": "v1","event_datetime": "2015-10-05 02:00:34","event_data": {"id": 141990,"code": "R590724503","number": "R590724503","total": "5299.0","discount_total": "0.0","placed_at": "2015-11-17T21:13:22Z","voucher_code": null,"status": 0,"consumer_id": "247193","created_at": "2015-11-17T21:41:48.000Z","updated_at": "2015-11-17T21:41:48.000Z","facility_id": 1,"agent_code": null, "channel_code": "mobile-web","parent_order_code": "12344","nps_score": null,"order_items": [{"id": 255825,"code": "943089","order_id": 141990,"sku": "ACLTWL61BSY0001","total_price": "5299.0","selling_price": "5299.0","discount": "0.0","amount_due": "0.0","promised_delivery_date": "2015-12-02T00:00:00Z","requested_delivery_date": null,"status": 1,"source_facility_id": 3,"created_at": "2015-11-17T21:41:48.000Z","updated_at": "2015-11-17T21:41:48.000Z","delivery_planning_datetime": null,"voucher_code": null,"pre_order": false,"parent_sku": null,"parent_sku_description": null,"service_item": false,"ul_special_instructions": null,"lms_status_text": "DELIVERED","lms_status_string": "Item has been processed","item_name": "Sheren Extendable Wall Sconce (Colour : brass)"}]}}');
        insert rs;
        Database.executeBatch(new ReplacementOrderBatch());
    }
    
    /*JSON ERROR */
    private static testMethod void testCreated3() {
        
        Replacement_Order_Stagging__c rs = new Replacement_Order_Stagging__c(Request_Body__c = '{"event_ype": ORDER_CREATED","event_id": "3F746E937EAB347D38E4386JFO","version": "v1","event_datetime": "2015-10-05 02:00:34","event_data": {"id": 141990,"qqqq": "R590724503","number": "R590724503","total": "5299.0","discount_total": "0.0","placed_at": "2015-11-17T21:13:22Z","voucher_code": null,"status": 0,"consumer_id": "247193","created_at": "2015-11-17T21:41:48.000Z","updated_at": "2015-11-17T21:41:48.000Z","facility_id": 1,"agent_code": null, "channel_code": "mobile-web","parent_order_code": "12344","nps_score": null,"order_items": [{"id": 255825,"code": "943089","order_id": 141990,"sku": "ACLTWL61BSY0001","total_price": "5299.0","selling_price": "5299.0","discount": "0.0","amount_due": "0.0","promised_delivery_date": "2015-12-02T00:00:00Z","requested_delivery_date": null,"status": 1,"source_facility_id": 3,"created_at": "2015-11-17T21:41:48.000Z","updated_at": "2015-11-17T21:41:48.000Z","delivery_planning_datetime": null,"voucher_code": null,"pre_order": false,"parent_sku": null,"parent_sku_description": null,"service_item": false,"ul_special_instructions": null,"lms_status_text": "FULFILLABLE","lms_status_string": "Item has been processed","item_name": "Sheren Extendable Wall Sconce (Colour : brass)"}]}}');
        insert rs;
        Database.executeBatch(new ReplacementOrderBatch());
    }
    
    private static testMethod void testCreated4() {
        
        Contact con6 = new Contact(LastName = 'TestName', phone='9854325679', Email ='test@urbanLadder4.co.in');
        insert con6;
        Case caseInstance = new case(Replaced_Order_Id__c = '12344', Order__c = 'R590724503', OI_Code__c = '943089', Replacement_Order_Placed_with_UL__c = true);
        caseInstance.contactId = con6.Id ;
        insert caseInstance;
        Replacement_Order_Stagging__c rs = new Replacement_Order_Stagging__c(Request_Body__c = '{"event_type": "ORDER_CREATED","event_id": "3F746E937EAB347D38E4386JFO","version": "v1","event_datetime": "2015-10-05 02:00:34","event_data": {"id": 141990,"code": "R590724503","number": "R590724503","total": "5299.0","discount_total": "0.0","placed_at": "2015-11-17T21:13:22Z","voucher_code": null,"status": 0,"consumer_id": "247193","created_at": "2015-11-17T21:41:48.000Z","updated_at": "2015-11-17T21:41:48.000Z","facility_id": 1,"agent_code": null, "channel_code": "mobile-web","parent_order_code": "12344","nps_score": null,"order_items": [{"id": 255825,"code": "943089","order_id": 141990,"sku": "ACLTWL61BSY0001","total_price": "5299.0","selling_price": "5299.0","discount": "0.0","amount_due": "0.0","promised_delivery_date": "2015-12-02T00:00:00Z","requested_delivery_date": null,"status": 1,"source_facility_id": 3,"created_at": "2015-11-17T21:41:48.000Z","updated_at": "2015-11-17T21:41:48.000Z","delivery_planning_datetime": null,"voucher_code": null,"pre_order": false,"parent_sku": null,"parent_sku_description": null,"service_item": false,"ul_special_instructions": null,"lms_status_text": "FULFILLABLE","lms_status_string": "Item has been processed","item_name": "Sheren Extendable Wall Sconce (Colour : brass)"}]}}');
        insert rs;
        Database.executeBatch(new ReplacementOrderBatch());
    }
    //** Aftersales Cancellation
    private static testMethod void testCreated5(){
    	Contact con7 = new Contact(LastName = 'TestName', phone='9854325679', Email ='test@urbanLadder4.co.in');
        insert con7;
    	Case caseInstance = new case(Order__c = 'R096039972',O_Code__c = 'R096039972',OI_Code__c = '1274617-88730-10',Type = 'After sales');
    	//caseInstance.contactId = con7.Id ;
    	insert caseInstance;
    	Replacement_Order_Stagging__c rs = new Replacement_Order_Stagging__c(Request_Body__c = '{"event_type":"ORDER_ITEM_UPDATED","event_id":3486161352,"version":"v1","event_datetime":"2016-02-28T05:38:53.000Z","event_data":{"id":198427,"code":"R096039972","number":"R096039972","total":"2388.0","discount_total":"141.0","placed_at":"2016-02-28T05:03:42Z","voucher_code":null,"status":0,"consumer_id":"1075727","created_at":"2016-02-28T05:22:43.000Z","updated_at":"2016-02-28T05:38:53.000Z","facility_id":4,"agent_code":null,"channel_code":"app","parent_order_id":null,"partial_delivery":false,"nps_score":null,"order_items":[{"id":363062,"code":"1274617-88730-10","order_id":198427,"sku":"HDCCTCCTMAPS050","total_price":"187.0","selling_price":"187.0","discount":"12.0","amount_due":"0.0","promised_delivery_date":"2016-03-14T00:00:00Z","requested_delivery_date":null,"status":4,"source_facility_id":8,"created_at":"2016-02-28T05:22:43.000Z","updated_at":"2016-02-28T05:38:51.000Z","delivery_planning_datetime":null,"voucher_code":null,"pre_order":false,"parent_sku":"HDCCTCCTMMT0001","parent_sku_description":"Tito Berry blast Cushion Covers -Set of Four (16\" X 16\" Cushion Size)","service_item":false,"ul_special_instructions":null,"replacement_available":true,"channel":"standard","lms_status_text":"CANCELLED","lms_status_string":"Item has been cancelled","item_name":"Tito Cushion Cover(Berry Conserve)"},{"id":363063,"code":"1274617-88730-2","order_id":198427,"sku":"HDCCTCCTMAPS050","total_price":"187.0","selling_price":"187.0","discount":"12.0","amount_due":"0.0","promised_delivery_date":"2016-03-14T00:00:00Z","requested_delivery_date":null,"status":4,"source_facility_id":8,"created_at":"2016-02-28T05:22:43.000Z","updated_at":"2016-02-28T05:38:51.000Z","delivery_planning_datetime":null,"voucher_code":null,"pre_order":false,"parent_sku":"HDCCTCCTMMT0001","parent_sku_description":"Tito Berry blast Cushion Covers -Set of Four (16\" X 16\" Cushion Size)","service_item":false,"ul_special_instructions":null,"replacement_available":true,"channel":"standard","lms_status_text":"CANCELLED","lms_status_string":"Item has been cancelled","item_name":"Tito Cushion Cover(Berry Conserve)"}],"payment_method":"Cash On Delivery","status_text":"CREATED"}}');
        insert rs;
        Database.executeBatch(new ReplacementOrderBatch());
    }
    
    private static testMethod void testCancellation() {
        Contact con8 = new Contact(LastName = 'TestName', phone='9854325679', Email ='test@urbanLadder4.co.in');
        insert con8;
        Case caseInstance = new case(Order__c = '12344', OI_Code__c = '943089', Type='Cancellation',status='Process Cancellation');
        caseInstance.contactId = con8.Id ;
        insert caseInstance;
        Cancellation_Item__c	ci = new Cancellation_Item__c();
        ci.Case__c	=	caseInstance.Id;
        ci.Approval_Status__c	= 'Approved';
        ci.OI_Code__c	=	'R1223-54322';
        insert ci;
        Replacement_Order_Stagging__c rs = new Replacement_Order_Stagging__c(Request_Body__c = '{"event_type":"ORDER_ITEM_UPDATED","event_id":3486161352,"version":"v1","event_datetime":"2016-03-28T05:38:53.000Z","event_data":{"id":198427,"code":"R096039972","number":"R096039972","total":"2388.0","discount_total":"141.0","placed_at":"2016-02-28T05:03:42Z","voucher_code":null,"status":0,"consumer_id":"1075727","created_at":"2016-02-28T05:22:43.000Z","updated_at":"2016-02-28T05:38:53.000Z","facility_id":4,"agent_code":null,"channel_code":"app","parent_order_id":"12344","partial_delivery":false,"nps_score":null,"order_items":[{"id":363062,"code":"1274617-88730-10","order_id":198427,"sku":"HDCCTCCTMAPS050","total_price":"187.0","selling_price":"187.0","discount":"12.0","amount_due":"0.0","promised_delivery_date":"2016-03-14T00:00:00Z","requested_delivery_date":null,"status":4,"source_facility_id":8,"created_at":"2016-02-28T05:22:43.000Z","updated_at":"2016-02-28T05:38:51.000Z","delivery_planning_datetime":null,"voucher_code":null,"pre_order":false,"parent_sku":"HDCCTCCTMMT0001","parent_sku_description":"Tito Berry blast Cushion Covers -Set of Four (16\" X 16\" Cushion Size)","service_item":false,"ul_special_instructions":null,"replacement_available":true,"channel":"standard","lms_status_text":"","lms_status_string":"Item has been cancelled","item_name":"Tito Cushion Cover(Berry Conserve)"},{"id":363063,"code":"1274617-88730-2","order_id":198427,"sku":"HDCCTCCTMAPS050","total_price":"187.0","selling_price":"187.0","discount":"12.0","amount_due":"0.0","promised_delivery_date":"2016-03-14T00:00:00Z","requested_delivery_date":null,"status":4,"source_facility_id":8,"created_at":"2016-02-28T05:22:43.000Z","updated_at":"2016-02-28T05:38:51.000Z","delivery_planning_datetime":null,"voucher_code":null,"pre_order":false,"parent_sku":"HDCCTCCTMMT0001","parent_sku_description":"Tito Berry blast Cushion Covers -Set of Four (16\" X 16\" Cushion Size)","service_item":false,"ul_special_instructions":null,"replacement_available":true,"channel":"standard","lms_status_text":"CANCELLED","lms_status_string":"Item has been cancelled","item_name":"Tito Cushion Cover(Berry Conserve)"}],"payment_method":"Cash On Delivery","status_text":"CREATED"}}');
        insert rs;
        System.Test.startTest();
        Database.executeBatch(new ReplacementOrderBatch());
        System.Test.stopTest();
        //system.assertEquals(caseInstance.Replaced_Order_Id__c, 'R590724503'); //R590724503
        //system.debug('12345'+[select id, Status__c, Error_Message__c from Replacement_Order_Stagging__c where id=: rs.Id]);
    }
    
       
}