/*
*Purpose: For Creating Cancellation Case records & Cancellation Items
*Date : 26/09/2016
*VERSION    AUTHOR                     DETAIL                                 
*1.0        Pavithra Gajendra          INITIAL DEVELOPMENT               
*
*/
@RestResource(urlMapping='/CancellationCaseToSFDC/*')
global class WS_CancellationCaseToSFDC {
    
    //--Attributes
    public static map<String,ID> mapOfOrderIdAndCase                          = new Map<String,ID>();
    public static case caseins;
    public static Set<String> setOfOIItems = new Set<String>();
    public static list<Cancellation_Item__c> CancellationItemToCheck          = new List<Cancellation_Item__c>();
    public static list<Case> caseToInsert                                     = new List<Case>();
    public static list<Cancellation_Item__c> CancellationItemToInsert         = new List<Cancellation_Item__c>();
    public static list<Cancellation_Item__c> CancellationItemToInsertFinal    = new List<Cancellation_Item__c>();
    public static Cancellation_Item__c canitemins ;
    
    //----CONSTANTS
    public static final String OI_ELIGIBILITY_ELIGIBLE = 'ELIGIBLE' ;
    public static final String OI_ELIGIBILITY_NEED_APPROVAL = 'Needs Approval' ;
    public static final String NEED_APPROVAL_STATUS = 'NEEDS APPROVAL' ;
    public static final String AUTO_APPROVED_STATUS = 'Auto Approved' ;
    public static final String REJECTED_APPROVED_STATUS = 'Rejected' ;
    public static final String PICKUP_PENDING = 'Pickup Pending' ;
    
    /* Creating the Cancellation Case records with the Request Body.*/
    @HttpPost
    global static WS_APIOutput WS_PostOrderDataToSFDC(){
        
        RestRequest request         = RestContext.request;
        RestResponse response       = RestContext.response;   
        WS_APIOutput output         = new WS_APIOutput();
        WS_APIInput input       = new WS_APIInput();
        String requestJSON = request.requestBody.toString() ;
        string errorMsg = '';       
        RestContext.response.addHeader('Content-Type', 'application/json');
        RestResponse res = RestContext.response;
         try{           
            System.debug('-------------Input Request---------------'+requestJSON);
            input = (WS_APIInput)JSON.deserialize(requestJSON,WS_APIInput.class);
            String cancellationItemCodes = '';
            String MTOitems = '';
            //-- Further processing of the serialized input will be done here
            //-- This would include creating the Case and Cancellation Items together along with Approval and Cancellation Status.            
               for(WS_APIInput.CaseDetails caseData : input.OrderCancelData){
                    system.debug(caseData.Origin+'Origin');
                     caseins = new case(Refund_Status__c = '' ,Reason_For_Cancellation__c = caseData.Reason_For_Cancellation,Subject = caseData.subject ,Description = caseData.description ,Origin=caseData.Origin,Type=caseData.Type,Sub_Category__c=caseData.Sub_Category,Order__c=caseData.Order,O_Code__c = caseData.Order ,O_PaymentMethod__c=caseData.Payment_Method,O_FacilityID__c=caseData.O_FacilityID,ContactId =caseData.Customer_Id);
                     caseins.Type_of_Refund__c = caseData.Type_of_Refund ;
                     setOfOIItems                                    = new Set<String>();
                     CancellationItemToCheck                         = new List<Cancellation_Item__c>();                   
                     Decimal summedOItemValue;
                     summedOItemValue = 0.0;
                    
                    for(WS_APIInput.Cancellation_items_Data canItem : caseData.Cancellation_items_Data){
                        if(cancellationItemCodes==''){
                            cancellationItemCodes = canItem.OI_Code ;
                        }else{
                             cancellationItemCodes = cancellationItemCodes + ','+canItem.OI_Code ;
                        }  
                        //---- Sum up the total line items
                        summedOItemValue +=Decimal.valueOf(canItem.OI_Total_Price);
                        canitemins = new Cancellation_Item__c(Order_Item__c = canItem.OI_ExternalID ,O_Code__c = caseData.Order ,OI_Code__c=canItem.OI_Code,Order__c=caseData.Order,OI_Name__c=canItem.OI_Name,Cancellation_Reason__c =canItem.OI_Reason,OI_Total_Price__c=Decimal.ValueOF(canItem.OI_Total_Price),delivery_date__c=canItem.OI_Delivery_Date,OI_Eligibility__c=canItem.OI_Eligibility,OI_Parent_SKU__c=canItem.OI_Parent_SKU);
                         //SF-1134 - Cancellation approval logic change
                          if(caseData.Sub_Category != null && caseData.Reason_For_Cancellation != null && caseData.Sub_Category.toLowerCase() == Constants.SUB_CATEGORY_USER_INITIATED_CANCELLATION.toLowerCase() && caseData.Reason_For_Cancellation.toLowerCase() == Constants.REASON_FOR_CANCELLATION_LONGER_DELIVERY_TIMELINES.toLowerCase()){
                             canitemins.Approval_Status__c = NEED_APPROVAL_STATUS;
                             canitemins.OI_Reason__c = Constants.REASON_FOR_CANCELLATION_LONGER_DELIVERY_TIMELINES;     
                         }
                         else if(summedOItemValue > Decimal.valueOf(System.Label.Order_Item_Amount_Limit) || canItem.OI_Eligibility==OI_ELIGIBILITY_NEED_APPROVAL){
                             canitemins.Approval_Status__c = NEED_APPROVAL_STATUS;
                         }else if(canItem.OI_Eligibility==OI_ELIGIBILITY_ELIGIBLE){
                            canitemins.Approval_Status__c = AUTO_APPROVED_STATUS;
                         }
                         
                         if(canItem.OI_Reason.contains('MTO')){
                            caseins.MTO_Items__c = true ;
                            if(MTOitems==''){
                                MTOitems = canItem.OI_Code ;
                            }else{
                                MTOitems = MTOitems +' ; ' +canItem.OI_Code ;
                            }
                         }
                         if(canItem.OI_Reason== null || canItem.OI_Reason==''){
                             canitemins.Cancellation_Reason__c = caseData.Reason_For_Cancellation ;
                         }
                         
                        CancellationItemToCheck.add(canitemins);
                        system.debug(canItem.OI_Code+'OI_Code');
                        setOfOIItems.add(canItem.OI_Code);
                    }
                    caseins.Item_Codes__c = cancellationItemCodes ;
                    caseins.All_MTO_Item_codes__c = MTOitems.length() > 254 ? MTOitems.subString(0,254) : MTOitems ;
                    // SF-990 Change of MTO Items field to Long text area
                    caseins.All_MTO_Order_Item_codes__c = MTOitems ;
                    caseins.Cancellation_Status__c = 'Created';                    
                    list<Cancellation_Item__c> cancellationItemList = new list<Cancellation_Item__c>();
                    case caseInst = new case ();
                    system.debug(caseins+'--Case for Creation ');
                    system.debug(setOfOIItems+'--SetOfOIItems');
                    cancellationItemList.addAll(CancellationItemToCheck);
                    system.debug(cancellationItemList+'--CancellationItemList');
                    List<Case> dc_opsCaseAssignement = new List<Case>();       
                    CancellationItemToInsert.addAll(cancellationItemList);
                    system.debug(CancellationItemToInsert+'--CancellationItemToInsert');
                    if(caseData.Parent_Order != null){
                        system.debug('Parent order');
                        caseins.Pickup_Status__c = PICKUP_PENDING;
                        dc_opsCaseAssignement.add(caseins);
                        List<Case> dc_opsCaseAssignementReturnFromMethod = new List<Case>();
                        dc_opsCaseAssignementReturnFromMethod = CaseService.changeOwnerDCOpsQueue(dc_opsCaseAssignement);
                        caseToInsert.addAll(dc_opsCaseAssignementReturnFromMethod);
                    }else{
                        system.debug('Original order');                       
                        caseToInsert.add(caseins);
                    }                  
               }
             
            system.debug(caseToInsert+'caseToInsert');
            if(caseToInsert.size() > 0){
                insert caseToInsert;
            }
            for(Case casein : caseToInsert){
                mapOfOrderIdAndCase.put(casein.Order__c,casein.id);
            }
            system.debug('Count of cancellation Items '+CancellationItemToInsert.size());
            if(CancellationItemToInsert.size() > 0){
                for(Cancellation_Item__c tempcanin : CancellationItemToInsert){
                    if(mapOfOrderIdAndCase.containsKey(tempcanin.Order__c)){
                        tempcanin.Case__c = mapOfOrderIdAndCase.get(tempcanin.Order__c);
                        output.sfid =  mapOfOrderIdAndCase.get(tempcanin.Order__c);
                    }
                    CancellationItemToInsertFinal.add(tempcanin);  
                }
            }
            system.debug('CancellationItemToInsertFinal '+CancellationItemToInsertFinal );
            
            if(CancellationItemToInsertFinal.size() > 0){
                insert CancellationItemToInsertFinal;
            }          
            // -- Return the Response Code and Message-- // 
            output.statusCode = '200';
            output.responseMessage = 'Successfully Processed'; 
            RestContext.response.responseBody = Blob.valueOf('{ "Status Code" : "200", "Message" : "Sucessfully Inserted"}');                     
         }catch(Exception error){
            System.debug('Exception '+error.getMessage());
            CustomException.addExceptionWithInput(error,'Input Processing','WS_CancellationCaseToSFDC','Parsing Input For Cancellation Case',requestJSON);
            output.statusCode = '422';
            errorMsg = error.getMessage();
            output.responseMessage = String.valueOf(errorMsg);
            RestContext.response.responseBody = Blob.valueOf('{ "Status Code" : "422", "Message" :errorMsg}');
        }  
        return output ;
    }
    
    global class WS_APIOutput{
        global String statusCode ;
        global String responseMessage ; 
        global String sfID ;       
    }
}