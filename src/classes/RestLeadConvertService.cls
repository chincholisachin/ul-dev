/*##############################################################################
########################## Exposing LeadConvertService #########################

Developed By : Baba S
Created Date : 20 July 16
Company : ET Marlabs


Description :Expose Lead to Opportunity conversion logic as SF API endpoint

Last MOdified Date : 20 July 16

##################################################################################
################################################################################*/
@RestResource(urlMapping='/LeadConvert/*')
global without sharing class RestLeadConvertService  {

    //Constants
    global static String LEAD_NOT_FOUND              = 'Lead not found in Salesforce !';
    global static String LEAD_ALREADY_CONVERTED      = 'Lead is already converted ! ';
    global static String LEAD_EMAIL_NOT_FOUND        = 'Lead’s email-id can not be blank !';
    global static String LEAD_CONSULTANT_NOT_FOUND   = 'Lead’s Consultant can not be blank !';
    global static String LEAD_PENDING_ACTIVITIES     = 'Lead is having are pending activities!';
    global static String LEAD_SUCCESSFUL_CONVERSION  = 'Lead is converted successfully! Parent Opportunity id is : ';
    global static String LEAD_CONVERSION_FAILED      = 'Lead is convertion failed !';


    @HttpGet
    global static void validateAndConvertLead() {

        List<String> returnResult = new List<String>();

        Lead lead;
        Boolean isError = false;
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        String customerId = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);

        try{
            //Get the Lead- with its events and tasks
              lead = [SELECT Id,Name,IsConverted,Field_for_w_f__c,Send_Visit_Reminder__c,OwnerId,Email,Customer_ID__c,Customer_ID_2__c,Consultant_Name__c,
                (Select Id,Subject,status__c from events),
                (Select Id,Subject,status from tasks) 
                FROM Lead WHERE Customer_ID_2__c = :customerId limit 1];

                            //Check for email
                if(lead.Email == null || lead.Email == '') {
                    isError = true;
                    responseBadRequest(RestLeadConvertService.LEAD_EMAIL_NOT_FOUND, 400,res); 
                }

                //Check lead is already converted
                if(lead.IsConverted) {
                    isError = true;
                    responseBadRequest(RestLeadConvertService.LEAD_ALREADY_CONVERTED, 400,res); 
                }

                //Check for consultant    
                if(lead.Consultant_Name__c == null) {
                    isError = true;
                    responseBadRequest(RestLeadConvertService.LEAD_CONSULTANT_NOT_FOUND, 400,res); 
                }

                
                //Check for the pending - Events
                for(Event event : lead.events){
                    if(event.status__c == 'Pending'){
                        isError = true;
                        responseBadRequest(RestLeadConvertService.LEAD_PENDING_ACTIVITIES, 400,res);
                        break;
                    }
                }

                //If no error then call the Convert-functionality
                if(!isError){    
                    /*
                        Opportunity owner will the consultant, in the API convertion
                    */
                    String convertResult = LeadConvertService.customConvertLead(lead.Id,String.valueOf(lead.Consultant_Name__c));

                    //Check for the Converted Opp Id
                    if(convertResult.startsWith('006')){
                        //Send the Opp-id 
                        responseBadRequest(RestLeadConvertService.LEAD_SUCCESSFUL_CONVERSION +' '+convertResult, 200, res);
                    }else{
                        responseBadRequest(RestLeadConvertService.LEAD_CONVERSION_FAILED +' '+convertResult, 500, res);
                    }
                }
        }catch(Exception e){
            
            responseBadRequest(LEAD_NOT_FOUND +' - '+e.getMessage(), 500, res);
    }
    }
    
    
    private static void responseBadRequest(String message, Integer status, RestResponse res) {
        res.responseBody = Blob.valueOf(message);
        res.statusCode = status;
        return;
    }

}