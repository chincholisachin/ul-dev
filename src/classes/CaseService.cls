public  class CaseService {
    
    //*** Class Level Variables
    private static Datetime completionDate = Datetime.Now();
    
    //*** Filter the Cases subject to exit the milestone criteria***//
    public static void completeCaseMilestone(map<ID,Case> newMap,map<ID,Case> oldMap){
        system.debug('*** completeCaseMilestone Method Called***');
        try{
            List<Id> updateCases_EmailFCR = new List<Id>();
            List<Id> updateCases_CallFCR = new List<Id>();
            List<Id> updateCases_AS = new List<Id>();
            List<Id> updateCases_Re = new List<Id>();
            
            
            system.debug('*** newMap ***'+newMap);
            
            for(Case eachCase:newMap.values()){
                
                system.debug('*** eachCase Values ***'+eachCase);
                
                //*** Completion of FCR***//
                if(eachCase.Origin == Constants.ORIGIN_EMAIL && eachCase.Email_Responded__c==TRUE){
                    if(eachCase.Type != NULL && eachCase.Sub_Category__c !=NULL){
                        updateCases_EmailFCR.add(eachCase.ID);
                    }
                }
                
                //** Completion of Call **//
                if(eachCase.Origin == Constants.ORIGIN_CALL && eachCase.Type != NULL && eachCase.Sub_Category__c !=NULL){
                    updateCases_CallFCR.add(eachCase.ID);
                }
                //*** Should come from Constants Class***
                if(eachCase.Type == Constants.TYPE_AFTERSALES && eachCase.Sub_Category__c == Constants.SC_PRODUCTCOMPLAINT){
                    if(eachCase.After_Sales_Status__c != NULL &&((eachCase.After_Sales_Status__c != oldMap.get(eachCase.id).After_Sales_Status__c) ||
                                                                 (eachCase.After_Sales_Status__c ==Constants.AS_ST_VISITSCHEDULED && eachCase.Visit_Scheduled_Date__c != oldMap.get(eachCase.id).Visit_Scheduled_Date__c)||
                                                                 (eachCase.After_Sales_Status__c ==Constants.AS_ST_PRO_REP && eachCase.Product_Repaired_Delivery_Scheduled_Date__c != oldMap.get(eachCase.id).Product_Repaired_Delivery_Scheduled_Date__c)
                                                                )){
                                                                    updateCases_AS.add(eachCase.ID);
                                                                }
                }
                
                if(eachCase.Status != NULL && eachCase.Status ==Constants.STATUS_CLOSED && oldMap.get(eachCase.Id).Status != Constants.STATUS_CLOSED){
                    updateCases_Re.add(eachCase.ID);
                }
                
            }
            
            system.debug('*** Cases to update - FCR ***'+updateCases_EmailFCR);
            system.debug('*** Cases to update - FCR ***'+updateCases_CallFCR);
            system.debug('*** Cases to update  - Response TAT ***'+updateCases_AS);
            system.debug('*** Cases to update  - Resolution TAT ***'+updateCases_Re);
            
            if(!updateCases_EmailFCR.isEmpty()){
                milestone_RespondToEmailCompletion(updateCases_EmailFCR);
            }
            if(!updateCases_CallFCR.isEmpty()){
                milestone_RespondToCallCompletion(updateCases_CallFCR);
            }
            if(!updateCases_AS.isEmpty()){
                milestone_CSResponseTAT(updateCases_AS);
            }
            if(!updateCases_Re.isEmpty()){
                milestone_CSResolutionTAT(updateCases_Re);
            }
        }catch(Exception e){
            system.debug('***Exception e***'+e.getMessage());
        }
    }
    
    // Populate Owner for DC-Ops queue.
    public static List<Case> changeOwnerDCOpsQueue(List<Case> caseList) {
        Group dcOpsGroup = [SELECT id from Group where Type = 'Queue' and Name='DC Ops' LIMIT 1];
        system.debug('*** inside Case Service - changeOwnerDCOpsQueue()***');
        Map<string, string> regionMap = new Map<String, String>();
        String regionName;
        for(Case caseIns : caseList) {
            if(caseIns.O_FacilityID__c != null){
                system.debug('*** Test Value from Custom Settings CS ***'+Regions__c.getValues('2').region__c);
                if(Regions__c.getValues(string.valueOF(caseIns.O_FacilityID__c))!=null){
                    regionName = Regions__c.getValues(string.valueOF(caseIns.O_FacilityID__c)).region__c;
                }
                caseIns.DC_Ops_Facility_Region_Name__c = regionName;
                regionMap.put(string.valueOf(caseIns.O_FacilityID__c),regionName);
            }
        }
        
        set<String> queueNameSet = new set<string>();
        Map<String, Group> queueNameMap = new Map<string, Group>();
        
        for(Case caseIns : caseList) {
            string queueName = 'DC Ops - '+regionMap.get(String.valueOf(caseIns.O_FacilityID__c));
            //DC Ops - Bangalore East AfterSales
            if(caseIns.Type == 'After sales') {
                queueName  += ' AfterSales';
            }
            //queuename += queuename.toLowerCase();
            queueNameSet.add(queueName);
        } 
        system.debug('---queueNameSet---'+queueNameSet);
        
        if(queueNameSet.size() > 0) {
            for(Group q : [select id, Name from Group where name IN: queueNameSet AND type= 'Queue']) {
                queueNameMap.put(q.Name.toLowerCase(), q);
            }
        }
        system.debug('---regionMap---'+regionMap);
        system.debug('---queueNameMap---'+queueNameMap);
        for(Case caseIns : caseList) {
            
            string queueName = 'DC Ops - '+regionMap.get(String.valueOf(caseIns.O_FacilityID__c));
            //DC Ops - Bangalore East AfterSales
            if(caseIns.Type == 'After sales') {
                queueName  += ' AfterSales';
            }
            queuename = queuename.toLowerCase();
            system.debug('---Constructed queuename----'+queuename);
            if(queueNameMap.containsKey(queueName)) {
                system.debug('---Final Step to Update ContainsKey queuename---');
                caseIns.OwnerId = queueNameMap.get(queueName).Id;
            }else if(dcOpsGroup!=null){
                caseIns.OwnerId = dcOpsGroup.Id;
            }
        } 
        return caseList;
    } 
    
    public static void assignEntitlementToCase(String EntitleMentName,list<Case> newCases){
        try{
            Entitlement ent = ServiceFacade.fetchEntitlementByName(EntitleMentName);
           
            for(Case eachCase : newCases){
                eachCase.EntitlementID = ent.ID;    
            }
        }
        catch(Exception e){
            system.debug('***Exception e***'+e.getMessage());
        }   
    }
    
    public static void createContactFromEmail(list<Case> newCases){
        try{
            //*** Local Variables
            list<String> emailAddresses = new List<String>();
            list<Contact> listContacts = new list<Contact>();
            Set<String> takenEmails = new Set<String>();
            map<String,Contact> emailToContactMap = new map<String,Contact>();
            list<Case> casesToUpdate = new list<Case>();
            list<Contact> newContacts = new list<Contact>();
            
            //***Added to override standard functionality where multiple Emails are found***
            map<String,Contact> emailToConMap_New = new map<String,Contact>();
            
            //First exclude any cases where the contact is set
            for (Case caseObj:newCases) {
                
                if (caseObj.ContactId==null &&
                    caseObj.SuppliedEmail!='')
                {
                    emailAddresses.add(caseObj.SuppliedEmail);
                }
            }
            
            listContacts = ServiceFacade.fetchContactsByEmail(emailAddresses);
            
            for (Contact c:listContacts) {
                takenEmails.add(c.Email);
                emailToConMap_New.put(c.Email,c);
            }
            
            
            
            for (Case caseObj:newCases){
                if (caseObj.ContactId==null && caseObj.SuppliedName!=null && caseObj.SuppliedEmail!=null && caseObj.SuppliedName!='' &&!caseObj.SuppliedName.contains('@') && caseObj.SuppliedEmail!='' &&!takenEmails.contains(caseObj.SuppliedEmail)){
                    String[] nameParts = caseObj.SuppliedName.split(' ',2);
                    if (nameParts.size() == 2)
                    {
                        Contact cont = new Contact(FirstName=nameParts[0],
                                                   LastName=nameParts[1],
                                                   Email=caseObj.SuppliedEmail
                                                   
                                                  );
                        emailToContactMap.put(caseObj.SuppliedEmail,cont);
                        casesToUpdate.add(caseObj);
                    }else{
                        Contact cont = new Contact(LastName=nameParts[0],Email=caseObj.SuppliedEmail);
                        emailToContactMap.put(caseObj.SuppliedEmail,cont);
                        casesToUpdate.add(caseObj);
                        
                    }
                }else if(caseObj.ContactId==null && caseObj.SuppliedEmail!=null && caseObj.SuppliedEmail!='' && caseObj.SuppliedName==null && !takenEmails.contains(caseObj.SuppliedEmail)){
                     Contact cont = new Contact(LastName='No Name Found',Email=caseObj.SuppliedEmail);
                        emailToContactMap.put(caseObj.SuppliedEmail,cont);
                        casesToUpdate.add(caseObj);
                }else if(caseObj.ContactId==null && caseObj.SuppliedName!=null && caseObj.SuppliedEmail!=null && caseObj.SuppliedName!='' && takenEmails.contains(caseObj.SuppliedEmail)){
                       if(emailToConMap_New.containsKey(caseObj.SuppliedEmail)){
                           caseObj.ContactId = emailToConMap_New.get(caseObj.SuppliedEmail).id; 
                       }
                }else if(caseObj.ContactId==null && caseObj.SuppliedName!=null && caseObj.SuppliedEmail!=null && caseObj.SuppliedName!='' && !takenEmails.contains(caseObj.SuppliedEmail)){
                    Contact cont = new Contact(LastName=caseObj.SuppliedName,Email=caseObj.SuppliedEmail);
                        emailToContactMap.put(caseObj.SuppliedEmail,cont);
                        casesToUpdate.add(caseObj);
                }            
            }
   
            newContacts = ServiceFacade.insertnewContactList(emailToContactMap.values());
            
            for (Case caseObj:casesToUpdate){
                Contact newContact = emailToContactMap.get(caseObj.SuppliedEmail); 
                caseObj.ContactId = newContact.Id;
            }
        }catch(Exception e){
            system.debug('***Exception e***'+e.getMessage());
        }
    } 
        
    public static list<Case> fetchCasesByContact(Contact con){
        try{
            return [SELECT id,Origin,Type from Case where ContactID=:con.ID];
        }catch(QueryException qe){
            system.debug('***Exception qe***'+qe.getMessage());
        }
        return null;
    }
    
    public static Case createNewCaseFromContact(Contact con){
        
        Case caseInstance = new Case();
        caseInstance.ContactID = con.ID;
        caseInstance.status = 'Open';
        caseInstance.origin = 'Miss Call';
        try{
            insert caseInstance;
            return caseInstance;
        }catch(DMLException de){
            system.debug('*** Exception de***'+de.getMessage());
        }
        return null;
    }
    
    public static Case fetchLastUpdatedOpenCaseByContact(Contact con){
        //Added to fetch non-open case Status!='Open' - Added Closed.
        try{
            return [SELECT id,Origin,Status,Type,CNR__c from Case where Status!='Closed' AND ContactID=:con.ID ORDER BY LastModifiedDate DESC LIMIT 1];
        }catch(QueryException qe){
            system.debug('*** Exception qe***'+qe.getMessage());
        }
        return null;
    }
    
    public static Case findBestMatchingCase_existingCustomer(String contactID,WS_UL_SendCallLogsToSFDC.DataFromCall callLogWrapper,List<Contact> allCon){
        Datetime startTime = Datetime.valueOf(callLogWrapper.StartTime);
        Datetime endTime = Datetime.valueOf(callLogWrapper.EndTime);
        
        system.debug('***Contact ID***'+contactID);
        System.Debug('***startTime***'+startTime);
        System.Debug('***endTime***'+endTime);
        
        list<Case> caseList_New = new list<Case>();
        list<Case> caseList_Modified = new list<Case>();
        list<Case> caseList_Viewed = new list<Case>();
         
        caseList_New = [SELECT id,Origin,CreatedDate,LastViewedDate,LastModifiedDate,Type,CNR__c,SLA_On_Hold_Until_Datetime__c,OwnerId from Case where ContactID=:contactID AND CreatedDate>=:startTime AND CreatedDate<=:endTime];
        caseList_Modified = [SELECT id,Origin,CreatedDate,LastViewedDate,LastModifiedDate,Type,CNR__c,SLA_On_Hold_Until_Datetime__c,OwnerId from Case where ContactID=:contactID AND LastModifiedDate>=:startTime AND LastModifiedDate<=:endTime];
        caseList_Viewed = [SELECT id,Origin,CreatedDate,LastViewedDate,LastModifiedDate,Type,CNR__c,SLA_On_Hold_Until_Datetime__c,OwnerId from Case where ContactID=:contactID AND LastViewedDate>=:startTime AND LastViewedDate<=:endTime];
        
        System.Debug('***caseList_New***'+caseList_New.size()+'***'+caseList_New);
        System.Debug('***caseList_New***'+caseList_Modified.size()+'***'+caseList_Modified);
        System.Debug('***caseList_New***'+caseList_Viewed.size()+'***'+caseList_Viewed);
        
        if(!caseList_New.isEmpty()){
            return caseList_New[0];
        }
        else if(!caseList_Modified.isEmpty()){
            return caseList_Modified[0];
        }
        else if(!caseList_Viewed.isEmpty()){
            return caseList_Viewed[0];
        }else{
            try{
            Case worstCaseScenario = new Case();
            worstCaseScenario = [SELECT id,Origin,Status,Type,CNR__c,SLA_On_Hold_Until_Datetime__c,OwnerId from Case where ContactID IN:allCon AND Status !='Closed' ORDER BY LastModifiedDate DESC LIMIT 1];
            return worstCaseScenario;
            }catch(Exception e){
                system.debug('---Worst Case Scenario failed---'+e.getMessage());
            }
        }
        return null;
        
    }
    //*** Helper Methods for Case Service Class
    private static void milestone_RespondToEmailCompletion(List<ID> updateCases){   
        ServiceFacade.completeMilestone(updateCases,Constants.MS_Email,completionDate);
    }
    
    private static void milestone_RespondToCallCompletion(List<ID> updateCases){   
        ServiceFacade.completeMilestone(updateCases,Constants.MS_CALL,completionDate);
    }
    
    private static void milestone_CSResponseTAT(List<ID> updateCases){
        ServiceFacade.completeMilestone(updateCases,Constants.MS_RESP_TAT,completionDate);
    }
    
    private static void milestone_CSResolutionTAT(List<ID> updateCases){
        ServiceFacade.completeMilestone(updateCases,Constants.MS_RESO_TAT,completionDate);
    }
    
    /* Executes if Case status is moved to "Needs Replacement" */
    @future(callout=true) 
    public static void placeReplacementOrderMethod(String caseID, String oCode, String oiCode) {
        system.debug('*** Inside Place Replacement Order Method ***');
        HttpResponse response;
        String jsonString;
        //String orderItemCode  = (oiCode != null && oiCode != '') ? oiCode.split('-R')[0] : null;
        try {
            CaseRecursion.fireTrigger = false;
            CalloutModel.params calloutParams = CalloutService.populateCalloutParams('Order_Retrigger_UL', 'Wulverine Server');
            String endPointURL = calloutParams.endPointUrl;
            String userName = calloutParams.userName;
            String password = calloutParams.password;
            Integer timeOut_x = calloutParams.timeOut*1000;
            jsonString = JSON.serialize(new CalloutModel.orderReplacement(oCode, oiCode));
            response = CalloutService.callOutToExternalSystem(userName, password, endPointUrl, timeOut_x, 'POST', jsonString);
            System.debug('responseBody: '+response.getBody());
            
            if(response.getStatus() == 'Created' || response.getStatus() == 'OK' || response.getStatus() == 'Success' || response.getStatusCode() == 200) {
               Case caseIns = new Case(Id=caseId, Replacement_Order_Placed_with_UL__c=true, Error_Message__c = '', Success_Message__c='Order has been placed successfully to UL.');
                update caseIns;
            }  else if(response.getStatusCode() == 422){
                CalloutService.generateServiceLog(jsonString, null, 'Order', response, caseID);
                string jsonStr = response.getBody();
                  jsonStr = jsonStr.replaceALL('\\"','"');
                string[] jsonStrSplit = jsonStr.split(',"error":"');
                string errorMsg = jsonStrSplit[1].replaceAll('"}"}}', '');
                Case caseIns = new Case(Id=caseId, Error_Message__c= errorMsg, Success_Message__c = '', After_Sales_Status__c= 'Replacement Order Failed');
                update caseIns;    
            }         
        } catch(Exception ex) {
            System.debug('Error::'+ex.getMessage());
            CalloutService.generateServiceLog(jsonString, ex, 'Order', response, caseID);
            //SF-984 -- Added null check condition
            Case caseIns;  
            if(response != null)
            	caseIns = new Case(Id=caseId, Error_Message__c=response.getBody(), Success_Message__c = '');
            else
            	caseIns = new Case(Id=caseId, Error_Message__c='Response string is Empty', Success_Message__c = '');
            update caseIns;
            
            //throw new customException(ex.getLineNumber()+' '+ex.getMessage());
        }
    }
    
    //** This Method will be used for the Finance and Refunds Duplicate
    public static void duplicateFinanceCaseVerification(list<Case> newCasesList){
        try{

        List<Case> existingFinanceCaseList = new list<Case>();

        existingFinanceCaseList = new List<case>([select id,caseNumber,Type,Sub_Category__c,Order__c,Order_Item__c,Amount_of_Refund__c ,OI_Code__c,O_Code__c from Case where Type = 'Finance' AND Sub_Category__c = 'Refunds']);
        
          for(Case newCases : newCasesList) {
          
              if(!existingFinanceCaseList.isEmpty()) {
              
                  for(Case oldCases: existingFinanceCaseList) {
                  
                      if(oldCases.Type != null && oldCases.Sub_category__c!=null && oldCases.O_Code__c!=null && oldCases.OI_Code__c !=null && oldCases.Amount_of_Refund__c !=null && oldCases.ID!=newCases.ID  && newCases.Type == oldCases.Type && newCases.Sub_Category__c== oldCases.Sub_Category__c && newCases.O_Code__c== oldCases.O_Code__c&&
                      newCases.OI_Code__c== oldCases.OI_Code__c && newCases.Amount_of_Refund__c == oldCases.Amount_of_Refund__c ) {
                          
                            newCases.Duplicate_Case__c= oldCases.ID;
                      }
                  }
              }
          
          } 
            }
          
         catch(Exception e) {
            system.debug('*** Exception Name' + e.getmessage() +'***');
         } 
    }
	
    /* Executes if Case status is moved to "Process Cancellation" */
    @future(callout=true) 
    public static void placeCancellationOrderMethod(String caseID, String oCode, String reasonForCan,Datetime update_time,list<String> oiCodesToCancel) {
       
        system.debug('*** Inside Place Cancellation Order Method ***');
        system.debug('*** oiCodesToCancel ***'+oiCodesToCancel);
        HttpResponse response;
        String jsonString;
        list<String> approvalStatusList = new list<String>{'Approved','Auto Approved'};
        String serviceName = 'Process_Cancellation';
        List<Service_Logs__c> serviceLogsList =  new List<Service_Logs__c>();
        
        try {
            Case caseInstance = [SELECT Id,O_FacilityID__c from Case where ID=:caseID];
            CaseRecursion.fireTrigger = false;
            CaseRecursion.fireCancellation = false;
            CaseRecursion.isComingFromCancellation = false;
            
            CalloutModel.params calloutParams = CalloutService.populateCalloutParams('Cancellation_Order', 'Wulverine Server');
            String endPointURL = calloutParams.endPointUrl+'/'+oCode+'/cancel';
            String userName = calloutParams.userName;
            String password = calloutParams.password;
            Integer timeOut_x = calloutParams.timeOut*1000;
            jsonString = JSON.serialize(new CalloutModel.orderCancellation(oiCodesToCancel,'SF',reasonForCan,update_time));
            
            // Service log
            serviceLogsList.add(CalloutService.generateInformationLog(serviceName, jsonString, serviceName+'_Initiated', caseID, 'Case', DateTime.now()));
            
            response = CalloutService.callOutToExternalSystem(userName, password, endPointUrl, timeOut_x, 'POST', jsonString);
            System.debug('responseBody: '+response.getBody());
            
            if(response.getStatus() == 'Created' || response.getStatus() == 'OK' || response.getStatus() == 'Success' || response.getStatusCode() == 200) {
                system.debug('*** Place Cancellation 200 OK***');
                Case caseIns = new Case(Id=caseId,O_FacilityID__c=caseInstance.O_FacilityID__c, Cancellation_Status__c='Cancellation Requested', Error_Message__c = '', Success_Message__c='Cancellation has been placed successfully to UL.');
                system.debug('*** About to update on 200***'+caseIns);
                caseIns = changeOwnerDCOpsQueue(new list<Case>{caseIns})[0];
                update caseIns;
                system.debug('*** update on 200***'+caseIns);
                list<Cancellation_Item__c> canItemListToUpdate = new list<Cancellation_Item__c>();
                
                for(Cancellation_Item__c ci:[SELECT id,Response_From_UL__c,Processing_Status__c from Cancellation_Item__c where Case__c=:caseId AND Approval_Status__c IN:approvalStatusList]){
                    if(ci!=null){
                    ci.Response_From_UL__c ='Cancellation has been placed successfully to UL';
                    ci.Processing_Status__c ='Cancellation Requested';
                    canItemListToUpdate.add(ci);
                    }
                }

                if(!canItemListToUpdate.isEmpty()){
                    update canItemListToUpdate;
                }

            }  else if(response.getStatusCode() == 422){
                
                // Service log
                serviceLogsList.add(CalloutService.generateCalloutServiceLog(serviceName, jsonString, response, caseID, 'Case', null, DateTime.now()));
                
                string jsonStr = response.getBody();
                Case caseIns = new Case(Id=caseId, Error_Message__c= 'Cancellation Request Failed : '+response.getStatus(), Success_Message__c = '',Cancellation_Status__c='Cancellation Request Failed');
                system.debug('*** About to update on 422***'+caseIns);
                update caseIns;
            }else if(response.getStatusCode() == 500){
                
                // Service log
                serviceLogsList.add(CalloutService.generateCalloutServiceLog(serviceName, jsonString, response, caseID, 'Case', null, DateTime.now()));

                string jsonStr = response.getBody();
                
                Case caseIns = new Case(Id=caseId,Cancellation_Status__c='Cancellation Request Failed',Error_Message__c= 'Cancellation Request Failed : '+response.getStatus(), Success_Message__c = '');
                system.debug('*** About to update on 500***'+caseIns);
                
                update caseIns;
                
            }else
                throw new customException();
            
                     
        } catch(Exception ex) {
            
            // Service log
            serviceLogsList.add(CalloutService.generateCalloutServiceLog(serviceName, jsonString, response, caseID, 'Case', ex, DateTime.now()));
            
            system.debug('*** Place Cancellation Exception***');
            System.debug('Error::'+ex.getMessage());
            Case caseIns;
            if(response != null)
                caseIns = new Case(Id=caseId, Error_Message__c='Cancellation Request Failed : Exception, please contact your Salesforce System Admin :: '+response.getStatus(), Success_Message__c = '',Cancellation_Status__c='Cancellation Request Failed');
            else
                caseIns = new Case(Id=caseId, Error_Message__c='Cancellation Request Failed : Exception, please contact your Salesforce System Admin ', Success_Message__c = '',Cancellation_Status__c='Cancellation Request Failed');
            update caseIns;
        }
        finally{
               
               // Insert service logs
               if(serviceLogsList.size() > 0)
                    CalloutService.insertServiceLogs(serviceLogsList);  
        }  
    }
    
    //-- This method would be used for Email to Case Loop Breaker --//
    public static map<string,list<case>> getCasesBySuppliedEmailList(list<String> suppliedEmailList){
    	map<string,list<case>> caseSuppliedEmailToCaseMap = new map<string,list<case>>();
    	if(!suppliedEmailList.isEmpty()){
    		for(Case eachCase:[SELECT id,CaseNumber,Subject,Description,SuppliedEmail from Case where SuppliedEmail IN :suppliedEmailList and isClosed = false ORDER BY CreatedDate DESC]){
    			if(caseSuppliedEmailToCaseMap.containsKey(eachCase.suppliedEmail)){
    				caseSuppliedEmailToCaseMap.get(eachCase.suppliedEmail).add(eachCase);
    			}else{
    				caseSuppliedEmailToCaseMap.put(eachCase.SuppliedEmail,new list<case>{eachCase});
    			}
    		}
    	}
    	if(!caseSuppliedEmailToCaseMap.isEmpty()){
    		return caseSuppliedEmailToCaseMap;
    	}
    	return null;
    }
    
    //-- CNR Case --
    public static list<Case> updateCNRCase(list<Case> caseListToUpdate){
    	try{
    		if(!caseListToUpdate.isEmpty()){
    			update caseListToUpdate;
    		}
    		return caseListToUpdate;
    	}
    	catch(Exception e){
    		//--Call Airbrake Here--
    		system.debug('---Exception ---'+e.getMessage());
    		
    	}
    	return null;
    }

     //------- Send Case status to order service layer : callout to update case cancellation item status to order service
    @future(callout=true) 
    public static void sendCancellationCaseStatus(String caseId,List<String> itemCodes,String status,DateTime updatedTime,String orderCode){
    	//------ Attributes
        String URL_HTTP ;
        Service_Logs__c servicelog;
        Http http = new Http();
        HttpRequest request = new HttpRequest();      
        HTTPResponse response = new HTTPResponse();
        
        //----- Get all details to make callout
        CalloutModel.params calloutParams = CalloutService.populateCalloutParams('Customer Request Status', 'Wulverine Server');
        String endPointURL = calloutParams.endPointUrl;
        endPointURL = endPointURL+'/'+orderCode +'/customer_request_status';
        String userName = calloutParams.userName;
        String password = calloutParams.password;
        Integer timeOut_x = calloutParams.timeOut*1000;
        String jsonString;
        
        List<Service_Logs__c> serviceLogsList =  new List<Service_Logs__c>();
        system.debug('---endPointUrl---'+endPointUrl);       
        //----- Create a request body for Service callout   
        if(!Test.isRunningTest()) {
             try {
                jsonString = JSON.serialize(new CalloutModel.CancellationStatus(itemCodes,status,updatedTime));
                system.debug('---jsonString---'+jsonString);
                Case cancellationCase ;
                /***
                SF-1046 -- Implementing logging mechanism for Salesforce.
                below method call will create service log before external service callout.
                Status Message will be marked as "Status+_Initiated" 
                ***/
                serviceLogsList.add(CalloutService.generateInformationLog(status, jsonString,status+'_Initiated', caseId, 'Case', DateTime.now()));
                response = CalloutService.callOutToExternalSystem(userName, password, endPointUrl, timeOut_x, 'PUT', jsonString);
                System.debug('responseBody: '+response.getBody());
                if(response.getStatus() == 'Created' || response.getStatus() == 'OK' || response.getStatus() == 'Success' || response.getStatusCode() == 200) {                 
                    cancellationCase = new Case(Id=caseId,Status_sent_to_Order_Service__c = status);
                    Boolean autoapproved = false ;
                    CaseRecursion.isCustomerRequestNotSent = true ;
                    if(status=='cancellation_requested'){
                        Case updatedCase = [Select Id,Cancellation_Status__c,Cancellation_Case_Autoapproved__c From Case Where Id=:caseId];
                        autoapproved = updatedCase.Cancellation_Case_Autoapproved__c ;      
                    }                                                       
                    //-------- 2nd callout if autoapproved
                    if(autoapproved && response.getStatusCode() == 200){
                        jsonString = JSON.serialize(new CalloutModel.CancellationStatus(itemCodes,'cancellation_approved',updatedTime));
                        system.debug('---jsonString---'+jsonString);
                        String newStatus = 'cancellation_approved' ; 
                        /***
                        SF-1046 -- Implementing logging mechanism for Salesforce.
                        below method call will create service log before external service callout for auto approved cancellation case. 
                        Status Message will be marked as "newStatus+_Initiated" 
                        ***/
                        serviceLogsList.add(CalloutService.generateInformationLog(newStatus, jsonString, newStatus+'_Initiated', caseId, 'Case',DateTime.now()));
                        response = CalloutService.callOutToExternalSystem(userName, password, endPointUrl, timeOut_x, 'PUT', jsonString);
                        System.debug('responseBody: '+response.getBody());
                        if(response.getStatus() == 'Created' || response.getStatus() == 'OK' || response.getStatus() == 'Success' || response.getStatusCode() == 200) {
                            cancellationCase = new Case(Id=caseId,Status_sent_to_Order_Service__c = newStatus);
                            try{
                             update cancellationCase ;
                            }catch(Exception e){
                                System.debug('Exception: '+ e);
                                /***
                                SF-1046 -- Implementing logging mechanism for Salesforce
                                Capture update related excpetion and case related callout information
                                ***/
                                serviceLogsList.add(CalloutService.generateCalloutServiceLog(newStatus, jsonString, response, caseId, 'Case', e, DateTime.now()));
                            }
                        }else{
                            /***
                            SF-1046 -- Implementing logging mechanism for Salesforce.
                            below method call will create service log if there is unexpected response After external service callout
                            ***/
                            serviceLogsList.add(CalloutService.generateCalloutServiceLog(newStatus, jsonString, response, caseId, 'Case', null, DateTime.now()));
                            newStatus = newStatus+'_Failed' ;
                            cancellationCase = new Case(Id=caseId,Status_sent_to_Order_Service__c = newStatus);
                             update cancellationCase ;
                        }
                    }else{
                            try{
                              system.debug('---cancellationCase---'+cancellationCase);
                              update cancellationCase ;
                            }catch(Exception e){
                                System.debug('Exception: '+ e);
                                /***
                                SF-1046 -- Implementing logging mechanism for Salesforce
                                Capture update related excpetion and case related callout information
                                ***/
                                serviceLogsList.add(CalloutService.generateCalloutServiceLog(status, jsonString, response, caseId, 'Case', e ,DateTime.now()));
                            }
                   }
                
                }else{          
                    // CalloutService.generateServiceLog(jsonString, null, 'Customer Request Status', response, caseId); // Old logging
                    /***
                    SF-1046 -- Implementing logging mechanism for Salesforce
                    below method call will create service log if there is unexpected response After external service callout
                    ***/
                    serviceLogsList.add(CalloutService.generateCalloutServiceLog(status, jsonString, response, caseId, 'Case', null,DateTime.now()));
                    status = status+'_Failed' ;
                    cancellationCase = new Case(Id=caseId,Status_sent_to_Order_Service__c = status);
                    update cancellationCase ;
                }
             }   
             catch(Exception e) {
                    System.debug('Callout error: '+ e);
                    /***
                    SF-1046 -- Implementing logging mechanism for Salesforce
                    Capture callout related exception
                    ***/
                    serviceLogsList.add(CalloutService.generateCalloutServiceLog(status, jsonString, response, caseId, 'Case', e,DateTime.now()));
             }
             finally{
                
                /***
                    SF-1046 -- Implementing logging mechanism for Salesforce
                    Insert all logs
                ***/
                if(serviceLogsList.size() > 0)
                    CalloutService.insertServiceLogs(serviceLogsList);  
             }      
        }   
    }
    
    /*** This method sends notification to order service about BFL order confirmation 
         Feature is Implemented for Story #SF-932 
    ***/
    
    @future(callout=true) 
    public static void BFLCaseConfirmationToOrderService(Set<ID> BFLCaseIds,DateTime systemTimeStampForBFLStatusChange){
        
        List<Case> CaseList = new List<Case>();
        
        //Fetch the case records  
        CaseList = [SELECT Id, O_Code__c,SF_Id__c, BFL_status__c from Case where Id IN :BFLCaseIds];
        String jsonString;
        HttpResponse response;
        for(Case caseInstance : CaseList){
            CalloutModel.params calloutParams = CalloutService.populateCalloutParams('BFL Order Confirmation', 'Wulverine Server');
            String endPointURL = calloutParams.endPointUrl+'/'+caseInstance.O_Code__c+'/confirm';
            String userName = calloutParams.userName;
            String password = calloutParams.password;
            Integer timeOut_x = calloutParams.timeOut*1000;
            jsonString = CaseService.createJsonString(caseInstance,systemTimeStampForBFLStatusChange); 
            try {
                
                response = CalloutService.callOutToExternalSystem(userName, password, endPointUrl, timeOut_x, 'PUT', jsonString);
                
                if(response.getStatusCode() == 200 || response.getStatusCode() == 409) {
                    caseInstance.BFL_status__c = Constants.CASE_BFL_STATUS_ORDER_CONFIRMED_AND_DISBURSEMENT_PENDING;    
                }
                else{
                    caseInstance.BFL_status__c = Constants.CASE_BFL_STATUS_ORDER_CONFIRMATION_NOTIFICATION_FAILED;
                    CalloutService.generateServiceLog(jsonString, null ,'BFL_ORDER_CONFIRMATION', response, caseInstance.Id); 
                }
            }   
            catch(Exception e) {
                caseInstance.BFL_status__c = Constants.CASE_BFL_STATUS_ORDER_CONFIRMATION_NOTIFICATION_FAILED;
                CalloutService.generateServiceLog(jsonString, e,'BFL_ORDER_CONFIRMATION', response, caseInstance.Id);
            }      
            
        }
        try{
            if(CaseList.size()>0 )
                update CaseList;
        } 
        catch(Exception e) {
            CalloutService.generateServiceLog(JSON.serialize(CaseList), e,'BFL_ORDER_CONFIRMATION_CASE_UPDATE', null, null);
        }
    }
    
    /*** This method Creates a Json String for case object ***/
    
    public static String createJsonString(Case caseRecord,DateTime systemTimeStamp){
        return '{"order_code": "'+caseRecord.O_Code__c+'", "confirmation_time": "'+systemTimeStamp+'", "confirmation_attributes":{"sfId": "'+caseRecord.SF_Id__c+'", "channel": "SalesForce"}}';
    }
}