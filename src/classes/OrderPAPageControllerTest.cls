@isTest(seeAllData = false)
public with sharing class OrderPAPageControllerTest {
    private static testMethod void test1() {
        Contact conInstance = new Contact(firstName='Test',lastName='Name', phone = '9885532345');
        insert conInstance;
        
        Case eachCase = new Case(Status = 'Open',Origin='Email',ContactID=conInstance.ID);
        insert eachCase;
        
        ApexPages.StandardController stdController = new ApexPages.StandardController(eachCase);
        OrderPAPageController ordPACont = new OrderPAPageController(stdController);
        
        ordPACont.currentCase = eachCase;
        ordPACont.currentCaseContact= conInstance;
        
        OrderPAPageController.getOrderItemsFromOrder(eachCase.ID,'R1234567');
        OrderPAPageController.updateCaseDetails(eachCase,'R1234567','99880');
        
    }
    
    /*private static testMethod void test2() {
        Contact conInstance = new Contact(firstName='Test',lastName='Name',phone = '9885532345');
        insert conInstance;
        
        Case eachCase = new Case(Status = 'Open',Origin='Email',ContactID=conInstance.ID);
        insert eachCase;
        
        ApexPages.StandardController stdController = new ApexPages.StandardController(conInstance);
        OrderPAPageController ordPACont = new OrderPAPageController(stdController);
        
        ordPACont.currentCase = eachCase;
        ordPACont.currentCaseContact= conInstance;
        
        OrderPAPageController.getOrderItemsFromOrder('eachCase','');
        OrderPAPageController.updateCaseDetails('eachCase','R1234567','99880');
        
    }*/
}