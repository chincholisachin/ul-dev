global class CaseHistorySchedulable implements Schedulable{
    
      global void execute(SchedulableContext sc) {
      CaseHistory_Batch chBatch = new CaseHistory_Batch(); 
      database.executebatch(chBatch);
   }

}